/* 

SxS : A C++ implementation of the scale climbing algorithm for multiscale image segmentation.
� 2004 Laurent Guigues (laurent.guigues@ign.fr)

This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.

You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
/*! \file 
\brief Code of the methods of the class ImageScaleSets.
*/

#include "sxsImageScaleSets.h"
#include "lglImageIO.h"
#include "lglImageManip.h"
#include <set>

namespace sxs 
{
  //======================================================
  /// Loads the ImageScaleSets from an ".iss" file 
  //****************************************************************************************************
  // MODIFIE PAR Thomas Prelot : les fichiers relatifs � une m�me classification doivent se trouver dans 
  //	le m�me r�pertoire. Les r�f�rences ne sont ainsi plus �crites en dur dans le fichier .iss et 
  //	on peut donc changer de place cet ensemble de fichier sans avoir � r��crire le .iss � la main.
  //****************************************************************************************************
  lgl::BOOL ImageScaleSets::load( const lgl::Filename& filename )
  {
    //  lglLOG("load "<<filename.GetFullPath().c_str()<<ENDL);
    FLUSH;

    lgl::Filename s(filename);
    lgl::setExtension(s,"iss");
    // Reads the .iss
    std::ifstream f((const char*)s.c_str(),PARAM_IFSTREAM_TEXT);
    if (!f.good()) return false;
    /*char line[256];
    f.getline(line,256,'\n'); 
	
    f.getline(line,256,':'); 
    f.getline(line,256,'\n'); 
    lgl::Filename imagename(line);
    //    imagename.MakeAbsolute(filename.GetPath());
	
    f.getline(line,256,':'); 
    f.getline(line,256,'\n'); 
    lgl::Filename basename(line);
    //    basename.MakeAbsolute(filename.GetPath());
	
    f.getline(line,256,':'); 
    f.getline(line,256,'\n'); 
    lgl::Filename sxsname(line);*/
    //    sxsname.MakeAbsolute(filename.GetPath());

    //	lglLOG("image : "<<imagename.GetFullPath()<<ENDL);	
    //	lglLOG("base  : "<<basename.GetFullPath()<<ENDL);	
    //	lglLOG("sxs   : "<<sxsname.GetFullPath()<<ENDL);	

    //double min,max;
    // TO CHANGE TO HANDLE ANY IMAGE TYPE ! 	
    
	lgl::Filename body(filename);
	lgl::Filename imagename(body);
	body = lgl::removeExtension(body);
	lgl::String name = body + lgl::Filename("_image");
	imagename = name;
	lgl::setExtension(imagename,"hdr");
	lgl::Filename basename(filename);
	name = body + lgl::Filename("_base");
	basename = name;
	lgl::setExtension(basename,"hdr");
	lgl::Filename sxsname(filename);
	lgl::setExtension(sxsname,"sxs");

	if (!lgl::readImageAnalyze(imagename,m_input)) return false;
	//if (!lgl::readImageAnalyze(filename,m_input)) return false;
    

	//	lgl::computeMinMax(*m_input, min, max);
    //	lglLOG("image : "<<m_input->size()<<" min/max = "<<min<<"/"<<max<<ENDL);

    // TO CHANGE ! 
    
	if (!lgl::readImageAnalyze(basename,m_base)) return false;
	//if (!lgl::readImageAnalyze(filename,m_base)) return false;
    
	//	lgl::computeMinMax(*m_base, min, max);
    //	lglLOG("base  : "<<m_base->size()<<" min/max = "<<min<<"/"<<max<<ENDL);

    
	if (!ScaleSets::load(sxsname)) return false;
	//if (!ScaleSets::load(filename)) return false;
    
	//	lglLOG("sxs   : "<<this->size()<<" sets, root soa = "<<this->getRoot()->scaleOfAppearance()<<ENDL);


	if (!testBaseImageCoherence()) {
		//lglWARNING("ImageScaleSets::load "<<filename.GetFullPath().c_str()<<" data corrupted (incoherence between base image and sets)"<<ENDL);
      return false;
    }
	


    return true;
  }
  //======================================================

  //======================================================
  /// Save the ImageScaleSets in an ".iss" file 
 
  lgl::BOOL ImageScaleSets::save( const lgl::Filename& filename, const lgl::Filename& ori )
  {

    lgl::Filename s(filename);
    lgl::Filename body(filename);
    body = lgl::removeExtension(body);
    //=========================================================
    // Creates the .iss
    lgl::setExtension(s,"iss");
    std::ofstream f((char*)s.c_str(),PARAM_OFSTREAM_TEXT);
    if (!f.good()) return false;
	
//    f << "sxs::ImageScaleSets - Version "<<__SXS_VERSION__<<" - (c) Laurent Guigues 2004"<<std::endl;

//    f << "input_image       :"; 
    lgl::Filename imagename(body);
    lgl::BOOL writeimage = false;
    /*
    if (ori.IsOk() && ori.FileExists(ori.GetFullPath())) {
      lgl::Filename o(ori);
      o.MakeRelativeTo(s.GetFullPath());
      f << o.c_str() << std::endl;
    }
    else {
    */

    lgl::String name = body + lgl::Filename("_image");
    imagename = name;
    lgl::setExtension(imagename,"hdr");
    // imagename.MakeRelativeTo(filename.GetFullPath());
//    f << imagename << std::endl;
    writeimage = true;
      //}

//    f << "base_segmentation :";
    lgl::Filename basename(filename);
    name = body + lgl::Filename("_base");
    basename = name;
    lgl::setExtension(basename,"hdr");
    //    basename.SetExt((wxChar*)"hdr");
    // basename.MakeRelativeTo(filename.GetFullPath());
//    f << basename << std::endl;

//    f << "scale_sets        :";
    lgl::Filename sxsname(filename);
    lgl::setExtension(sxsname,"sxs");
    //    sxsname.SetExt((wxChar*)"sxs");
    //    sxsname.MakeRelativeTo(filename.GetFullPath());
//    f << sxsname << std::endl;
    //=========================================================
    // TO DO : save parameters, meta data, and so on ...
    //=========================================================		
    f.close();
    // EO .iss creation
    //=========================================================


    //=========================================================
    // If writeimage==true saves the input image
    if (writeimage) {
      //     imagename.MakeAbsolute(filename.GetPath());
      if (!lgl::writeImageAnalyze(imagename,*m_input)) return false;
    }
    //=========================================================
    // Saves the base segmentation 
    //    basename.MakeAbsolute(filename.GetPath());
    if (!lgl::writeImageAnalyze(basename,*m_base)) return false;
    //=========================================================
    // Saves the ScaleSets : calls the ScaleSets::save method
    //   sxsname.MakeAbsolute(filename.GetPath());
    if (!ScaleSets::save(sxsname)) return false;
    //=========================================================

    return true;
  }
  //======================================================
  //****************************************************************************************************
  // MODIFIE PAR Thomas Prelot [FIN DES MODIFICATIONS]
  //****************************************************************************************************

			

  //================================================================
 
  void ImageScaleSets::getSection(lgl::F32 scale, lgl::pImageRam& out)
  {

#ifdef __SXS_SXS_TEST__
    if (!verifySlots()) lglERROR("Slots NOK !");
    if (!testBaseImageCoherence()) { lglERROR("Base Image to Base Sets mapping NOK !"); }
#endif

    // maps labels (indices) to the section sets indices
    std::map<int,int> lmap;
    // marks indices already seen
    Slot seen;
    if (!allocateSlot ( seen )) {
      lglERROR("ImageScaleSets::getSection(scale,image_out) : unable to allocate a 1 bit Slot !");
      return;
    }
    // 
    for (m_base->start();!m_base->stop();++(*m_base)) {
      int l = (int)(*(*m_base));
      //     std::cout << "lab = "<<l<<std::endl;
      Set* b = operator[](l);
      if (!b) {
	lglERROR("ImageScaleSets::getSection(scale,image_out) : incoherence between label image and ScaleSets indices");
      }
      // already seen
      if (b->getMark(seen)==1) {
	// verify that is mapped 
	if ((*(lmap.find(l))).first!=l) { lglWARNING("Label "<<l<<" seen but not mapped !!"<<std::endl); }
	continue;
      }
      b->setMark(seen,1);
      // index of its maximal ancestor of lower scale than scale
      //    std::cout << "getmax"<<std::endl;
      int lmax = b->getMaximalAncestor(scale)->index();
      // std::cout << "ok"<<std::endl;
      // map insertion
      lmap.insert(std::pair<int,int>(l,lmax)); 
    }
    // unmarks the mapped elements 
    std::map<int,int>::const_iterator mit;
    std::set<int> sec;
    for (mit=lmap.begin();mit!=lmap.end();++mit) {
      operator[]((*mit).first)->setMark(seen,0);
      sec.insert((*mit).second);
    }
    // frees the Slot seen
    freeSlot ( seen );
    // image out construction
    //    std::cout << "lgl::map"<<std::endl;
    lgl::map ( *m_base, lmap, out ); 
    //    std::cout << "ok"<<std::endl;
#ifdef __SXS_SXS_TEST__
    if (!verifySlots()) { lglERROR("Slots NOK !"); }
    if (!testBaseImageCoherence()) { lglERROR("Base Image to Base Sets mapping NOK !"); }
#endif

  }
  //================================================================



  //================================================================
 
  void ImageScaleSets::getSection(lgl::ImageRam& in, lgl::F32 scale, lgl::pImageRam& out)
  {
    //	lglLOG("ImageScaleSets::getSection(in,"<<scale<<")"<<ENDL);

	  wxStopWatch sw;


#ifdef __SXS_SXS_TEST__
    if (!verifySlots()) { lglERROR("Slots NOK !"); }
    if (!testBaseImageCoherence()) { lglERROR("Base Image to Base Sets mapping NOK !"); }
#endif

    wxLogMessage(wxT("getSection1 : %ldms "),sw.Time());
	sw.Start();

    // maps labels (indices) to the section sets indices
    std::map<int,int> lmap;
    // marks indices already seen
    Slot seen;
    if (!allocateSlot ( seen )) {
      lglERROR("ImageScaleSets::getSection(image_in,scale,image_out) : unable to allocate a 1 bit Slot !");
      return;
    }
    wxLogMessage(wxT("getSection2 : %ldms "),sw.Time());
	sw.Start();
    //
	//-----------------------------------------------------------------------------------------
	//std::map<int,int>::const_iterator mit2 = lmap.begin();
	//std::pair<int,int> *test;
	//std::set<int> sec;
	Set* b;
	for (in.start();!in.stop();++in) {
		int l = (int)(*in);
		b = operator[](l);
		if (!b) {
			lglERROR("ImageScaleSets::getSection(scale,image_out) : incoherence between label image and ScaleSets indices");
		}
		// already seen
		//if (b->getMark(seen)==1) {
			// verify that is mapped 
			//if ((*(lmap.find(l))).first!=l) { lglWARNING("Label "<<l<<" seen but not mapped !!"<<std::endl); }
			//continue;
		//}
		b->setMark(seen,1);
		// index of its maximal ancestor of lower scale than scale
		int lmax = b->getMaximalAncestor(scale)->index();
		//int lmax=2;
		// map insertion
		//*test = (l,lmax);
		//*mit2 = test;
		//mit2++;
		//lmap.insert(std::pair<int,int>(l,lmax));
		lmap[l] = lmax;
		//lmap.push_back(std::pair<int,int>(l,lmax));
	}
	//-----------------------------------------------------------------------------------------
	/*
    for (in.start();!in.stop();++in) {
      int l = (int)(*in);
      Set* b = operator[](l);
      if (!b) {
	lglERROR("ImageScaleSets::getSection(scale,image_out) : incoherence between label image and ScaleSets indices");
      }
      // already seen
      if (b->getMark(seen)==1) {
	// verify that is mapped 
	if ((*(lmap.find(l))).first!=l) { lglWARNING("Label "<<l<<" seen but not mapped !!"<<std::endl); }
	continue;
      }
      b->setMark(seen,1);
      // index of its maximal ancestor of lower scale than scale
      int lmax = b->getMaximalAncestor(scale)->index();
      // map insertion
      lmap.insert(std::pair<int,int>(l,lmax)); 
    }*/
	wxLogMessage(wxT("getSection3 : %ldms "),sw.Time());
	sw.Start();
    // unmarks the mapped elements 
    std::map<int,int>::const_iterator mit;
    std::set<int> sec;
    for (mit=lmap.begin();mit!=lmap.end();++mit) {
      operator[]((*mit).first)->setMark(seen,0);
      sec.insert((*mit).second);
    }
    wxLogMessage(wxT("getSection4 : %ldms "),sw.Time());
	sw.Start();
    // frees the Slot seen
    freeSlot ( seen );
    // image out construction
    lgl::map ( in, lmap, out ); 
		
    wxLogMessage(wxT("getSection5 : %ldms "),sw.Time());
	sw.Start();
#ifdef __SXS_SXS_TEST__
    if (!verifySlots()) { lglERROR("Slots NOK !"); }
    if (!testBaseImageCoherence()) { lglERROR("Base Image to Base Sets mapping NOK !"); }
#endif
    wxLogMessage(wxT("getSection6 : %ldms "),sw.Time());
  }
  //================================================================


  //================================================================
  /// Computes the label image corresponding to the cut provided
  void  ImageScaleSets::getLabelImageOfCut( lgl::ImageRam &in, 
					    const std::vector<Set*>& cut, 
					    lgl::pImageRam &out) {
    // Creates the map : base index -> index of the set of the cut
    std::map<int,int> lmap;
    getMapOfCut(cut,lmap);
    //   out = in.procreate();
    // image out construction
    lgl::map ( in, lmap, out ); 
  }
  //================================================================
  

  


  //================================================================
  /// Deletes the sets whose persistence is lower than a given threshold. 
  /// Performs a top-down traversal to compute the scale of disappearance of the sets 
  /// and simultaneously removes the sets of too low persistence.
 
  void ImageScaleSets::removeNonPersistentSets( lgl::F32 minimum_persistence )
  {
    if (!verifySlots()) { lglERROR("Slots NOK !"); }
    if (!testBaseImageCoherence()) { lglERROR("BaseSegmentation to Base Sets mapping NOK !"); }

    // slot allocation
    lgl::BitSlot m;
    if (!allocateSlot(m,1)) return;
    // recursive marking and non persistent base mapping
    std::map<int,int> npbase;
    m_rec_mark_non_persistent_and_map_npbase ( *m_list.rbegin(), m, 
					       (std::numeric_limits<lgl::F32>::max)(), minimum_persistence,
					       (*m_list.rbegin())->index(),
					       npbase);

    // maps the old base to the new one
    lgl::map ( *m_base, npbase, m_base );
    // prunes the marked sets
    removeMarkedSets(m);
    // slot unalloc
    freeSlot(m);
		
    if (!verifySlots()) { lglERROR("Slots NOK !"); }
    if (!testBaseImageCoherence()) { lglERROR("Base Image to Base Sets mapping NOK !"); }
  }
  //================================================================

  //================================================================
  /// Recursive marking function used in removeNonPersistentSets.
  /// Also collects non persistent base sets and maps their index to their minimal persistent ancestor (npmap)
 
  void ImageScaleSets::m_rec_mark_non_persistent_and_map_npbase ( Set* s, 
								  const lgl::BitSlot& m, 
								  lgl::F32 scale_of_disappearance, 
								  lgl::F32 minimum_persistence, 
								  int pa,
								  std::map<int,int>& npbase)
  {
    // non persistent
    if ( scale_of_disappearance <= minimum_persistence * s->scaleOfAppearance()) {
      s->setMark (m,1);
      // Base set ?
      if (s->isBase()) {
	// mapping
	npbase.insert(std::pair<int,int>(s->index(),pa)); 
      }
    }
    // persistent : becomes the current minimal persistent node of its branch
    else {
      pa = s->index();
    }
    // sod
    if (s->scaleOfAppearance()<scale_of_disappearance) {
      scale_of_disappearance = s->scaleOfAppearance();	
    }
    // recursive call on childrens
    std::vector<Set*>::const_iterator i;
    for (i=s->sons().begin(); i!=s->sons().end(); ++i) {
      m_rec_mark_non_persistent_and_map_npbase ( *i, m, scale_of_disappearance, minimum_persistence, pa, npbase);
    }
  }
  //================================================================
	


  //================================================================
  /// Tests that the base image labels are in a 1 to 1 mapping with the base sets indices 
 
  lgl::BOOL ImageScaleSets::testBaseImageCoherence()
  {
    lgl::BOOL ok = true;
    std::set<int> labels;
    int n=0;
    for (m_base->start();!m_base->stop();++(*m_base)) {
      int l = (int)(*(*m_base));
      //		std::cout << n << ":" << l << " ";
      n++;
      Set* s = operator[] ( l );
      if (!s) {
	lglWARNING("ImageScaleSets::testBaseImageCoherence() : label "<<l<<" does not corresponds to a set !");
	ok = false;
      }
      else if (!s->isBase()) {
	lglWARNING("ImageScaleSets::testBaseImageCoherence() : the set corresponding to label "<<l<<" is not a base set !");
	ok = false;
      }
      labels.insert(l);
    }
    // converse test
    std::list<Set*>::iterator i;
    for (i=m_list.begin();i!=m_list.end();++i) {
      if ((*i)->isBase()) {
	if ( labels.find((*i)->index()) == labels.end() ) {
	  lglWARNING("ImageScaleSets::testBaseImageCoherence() : the base set of label "<<(*i)->index()<<" is not in the base segmentation !");
	  ok = false;
	}
      }
    }
    return ok;
  }
  //================================================================
		
}
//=================================================================================
// EO namespace sxs
//=================================================================================



//=================================================================================
// EOF
//=================================================================================
