/* 

SxS : A C++ implementation of the scale climbing algorithm for multiscale image segmentation.
� 2004 Laurent Guigues (laurent.guigues@ign.fr)
  
This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.
	
As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.
	  
You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
		
*/
/*! \file 
\brief Code for the class wxScaleSetsBrowser
*/
#ifdef __SXSWX__
#include "sxswxScaleSetsBrowser.h"
#include "sxsImageScaleClimbing.h"
#include "sxsE_MumfordShah.h"
using namespace lgl;
namespace sxs {
  
  
  
  //=======================================================================================
  /// EVENT_TABLE 
  BEGIN_EVENT_TABLE( wxScaleSetsBrowser, wxFrame ) 
    EVT_PAINT( wxScaleSetsBrowser::OnPaint ) 
    EVT_MENU ( idMenuSave, wxScaleSetsBrowser::OnMenuSave ) 
    EVT_MENU ( idMenuSaveLabels, wxScaleSetsBrowser::OnMenuSaveLabels ) 
    EVT_MENU ( idMenuQuit, wxScaleSetsBrowser::OnMenuQuit ) 
    EVT_MENU ( idMenuAbout, wxScaleSetsBrowser::OnMenuAbout ) 
    EVT_SPINCTRL ( idXSliceCtrl, wxScaleSetsBrowser::OnXSliceCtrl ) 
    EVT_SPINCTRL ( idYSliceCtrl, wxScaleSetsBrowser::OnYSliceCtrl ) 
    EVT_SPINCTRL ( idZSliceCtrl, wxScaleSetsBrowser::OnZSliceCtrl ) 
    EVT_COMBOBOX ( idZoomCombo, wxScaleSetsBrowser::OnZoomChange ) 
    EVT_COMBOBOX ( idColormapCombo, wxScaleSetsBrowser::OnColormapChange) 
    EVT_CHECKBOX ( idColormapInverseCheck, wxScaleSetsBrowser::OnColormapInverseCheck ) 
    EVT_CHECKBOX ( idTicksCheck, wxScaleSetsBrowser::OnTicksCheck ) 
    EVT_COMMAND_SCROLL ( idVminSlider, wxScaleSetsBrowser::OnContrastMinChange) 
    EVT_COMMAND_SCROLL ( idVmaxSlider, wxScaleSetsBrowser::OnContrastMaxChange) 
    EVT_COMMAND_SCROLL ( idScaleSlider, wxScaleSetsBrowser::OnScaleSlider ) 
    EVT_TEXT_ENTER ( idScaleCtrl, wxScaleSetsBrowser::OnScaleCtrl ) 
    EVT_RADIOBOX ( idImageStyleRadio, wxScaleSetsBrowser::OnImageStyleChange ) 
    EVT_RADIOBOX ( idSectionStyleRadio, wxScaleSetsBrowser::OnSectionStyleChange ) 
    EVT_COMBOBOX ( idSectionColorCombo, wxScaleSetsBrowser::OnSectionColorChange ) 
    EVT_CHECKBOX ( idSetDrawCheck, wxScaleSetsBrowser::OnSetDrawCheck ) 
    EVT_COMBOBOX ( idSetColorCombo, wxScaleSetsBrowser::OnSetColorChange ) 
    EVT_TEXT_ENTER ( idSetPersistenceCtrl, wxScaleSetsBrowser::OnSetPersistenceCtrl )
    EVT_MOUSE_EVENTS( wxScaleSetsBrowser::OnMouse ) 
    EVT_KEY_DOWN ( wxScaleSetsBrowser::OnKeyPress )
  END_EVENT_TABLE() 
  //=======================================================================================
    
    
    
		
    
  //=======================================================================================
  wxScaleSetsBrowser::wxScaleSetsBrowser(wxFrame *parent, ImageScaleSets& scale_sets, lgl::BOOL delete_scale_sets, const wxString& title )
    : wxFrame(parent,-1, title, wxDefaultPosition, wxDefaultSize, wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxSYSTEM_MENU | wxCAPTION | wxRESIZE_BORDER )
  {
    //   wxLogMessage(_T("wxScaleSetsBrowser::wxScaleSetsBrowser(...)"));
    
    m_color_map = 0;
    m_xslice_img = 0;
    m_yslice_img = 0;
    m_zslice_img = 0;
    m_xslice_base = 0;
    m_yslice_base = 0;
    m_zslice_base = 0;
    m_xslice_lab = 0;
    m_yslice_lab = 0;
    m_zslice_lab = 0;
    m_xslice_set = 0;
    m_yslice_set = 0;
    m_zslice_set = 0;
    
    // scale-sets 
    m_sxs = &scale_sets;
    m_delete_sxs = delete_scale_sets;
    
    if (!InitBrowser()) {
      Close();
    }
    if (!InitInterface()) {
      Close();
    }
		
    OnScaleChange();
  }
  //=======================================================================================
	
	
	
  //=======================================================================================
  wxScaleSetsBrowser::~wxScaleSetsBrowser()
  {
    if (m_delete_sxs) delete m_sxs;
    if (m_color_map) delete m_color_map;
    if (m_is3D) {
      if (m_xslice_img) delete m_xslice_img;
      if (m_yslice_img) delete m_yslice_img;
      if (m_zslice_img) delete m_zslice_img;
      if (m_xslice_base) delete m_xslice_base;
      if (m_yslice_base) delete m_yslice_base;
      if (m_zslice_base) delete m_zslice_base;
    }
    if (m_xslice_lab) delete m_xslice_lab;
    if (m_yslice_lab) delete m_yslice_lab;
    if (m_zslice_lab) delete m_zslice_lab;
    if (m_xslice_set) delete m_xslice_set;
    if (m_yslice_set) delete m_yslice_set;
    if (m_zslice_set) delete m_zslice_set;
  }
  //=======================================================================================
	
	
	
  //======================================================================================= 	
  lgl::BOOL wxScaleSetsBrowser::InitBrowser() 
  {
    wxLogMessage(_T("InitBrowser"));
		
    // maximum scale
    m_scale_max = m_sxs->getRoot()->scaleOfAppearance()*1.00001;
    // minimum scale (for log scale) 
    m_scale_min = m_scale_max / pow(10,6);
    /// The maximum value of logscale
    m_logscale_max = 1;
    m_logscale_max = logscale(m_scale_max);
    // initial scale 
    m_scale = m_scale_max / 4 ;
		
    wxLogMessage(_T("scale min     = %f"),m_scale_min); 
    wxLogMessage(_T("scale max     = %f"),m_scale_max); 
    wxLogMessage(_T("log scale max = %f"),m_logscale_max); 
    wxLogMessage(_T("scale         = %f"),m_scale); 
		
    // palette for regions drawing with random colors
    m_palette_regions.setRandomPalette(0,m_sxs->getRoot()->index());
		
		
    // image
    m_image = &(m_sxs->getInputImage());
    // 3D ?
    m_is3D = (m_image->dimension()==3);


    // allocation of label images by procreation
    m_xslice_lab = m_sxs->getBaseImage().procreate();
    //m_xslice_base = m_sxs->getBaseImage().procreate();
    m_xslice_set = m_sxs->getBaseImage().procreate();
    // allocation of slices by procreation of m_image
    m_xslice_img = m_image->procreate();
		
    if (m_is3D) {
      m_yslice_lab = m_sxs->getBaseImage().procreate();
      m_zslice_lab = m_sxs->getBaseImage().procreate();
      m_yslice_base = m_sxs->getBaseImage().procreate();
      m_zslice_base = m_sxs->getBaseImage().procreate();
      m_yslice_set = m_sxs->getBaseImage().procreate();
      m_zslice_set = m_sxs->getBaseImage().procreate();
      m_yslice_img = m_image->procreate();
      m_zslice_img = m_image->procreate();
    }
		
		
    //
    m_isize = m_image->size();
    m_zoom = 2;
		
    m_slice_position = lgl::ImageSite(0, m_isize(1)/2, m_isize(2)/2, m_isize(3)/2);
		
    // Min/max value of the image
    lgl::computeMinMax(*m_image, m_value_min, m_value_max);
    /*
      m_value_min = (std::numeric_limits<double>::max)();
      m_value_max = (std::numeric_limits<double>::min)();
		
      for (m_image->start(); !m_image->stop(); ++(*m_image)) {
      if (*(*m_image)<m_value_min) m_value_min = *(*m_image);
      if (*(*m_image)>m_value_max) m_value_max = *(*m_image);
      }
    */
    wxLogMessage(_T("Image min/max = %g,%g"),m_value_min,m_value_max);
		
    m_mouse_in_image = false;
		
    // initial set = Root
    //	m_cur_sets.insert(m_sxs->getRoot());
    m_cur_sets.insert(m_sxs->Sets().front());
		
		
    wxLogMessage(_T("Browser init ok"));

    return true;
  }
  //=======================================================================================
	
	
  //=======================================================================================
  lgl::BOOL wxScaleSetsBrowser::InitInterface()
  {
    //=========================================
    // Menu
#if wxUSE_MENUS
    // File menu 
    menuFile = new wxMenu;
    menuFile->Append(idMenuSave, _T("&Save scale-sets\tAlt-S"), _T("Saves the current scale-sets"));
    menuFile->Append(idMenuSaveLabels, _T("&Save label image\tAlt-L"), _T("Saves the current image of labels"));
    menuFile->Append(idMenuQuit, _T("E&xit\tAlt-X"), _T("Quit this program"));
    
    // Help menu
    menuHelp = new wxMenu;
    menuHelp->Append(idMenuAbout, _T("&About...\tF1"), _T("Show about dialog"));
    
    // Menu bar
    menuBar = new wxMenuBar();
    menuBar->Append(menuFile, _T("&File"));
    menuBar->Append(menuHelp, _T("&Help"));
    SetMenuBar(menuBar);
#endif // wxUSE_MENUS
    
    
#if wxUSE_STATUSBAR
    CreateStatusBar(7);
    int* stw = new int[7];
    stw[0] = 100;
    stw[1] = 60;
    stw[2] = 80;
    stw[3] = 80;
    stw[4] = 120;
    stw[5] = 120;
    stw[6] = -1;
    SetStatusWidths(7,stw);
    delete[] stw;
#endif // wxUSE_STATUSBAR
		

    //=========================================
    // Control panel
    wxPanel* ctrlpanel = new wxPanel(this);
    // its topsizer
    wxStaticBoxSizer* ctrlsizer = new wxStaticBoxSizer( new wxStaticBox(ctrlpanel,-1,_T("")), wxVERTICAL );
		
    //=====================================================================================================
    // Image box 
							wxStaticBoxSizer* viewsizer = new wxStaticBoxSizer( new wxStaticBox(ctrlpanel,-1,_T("Image")), wxVERTICAL );
    // Display style
    wxString istyle[3];
    istyle[0] = _T("invisible");
    istyle[1] = _T("image");
    istyle[2] = _T("average");
		
    m_image_style_radio = new wxRadioBox ( ctrlpanel, idImageStyleRadio, _T("Display style"),wxDefaultPosition,wxDefaultSize,
					   3,istyle,1,wxRA_SPECIFY_ROWS);
    m_image_style_radio->SetSelection(1);
    viewsizer->Add(m_image_style_radio,0,wxALIGN_CENTRE|wxBOTTOM|wxGROW,10) ;
		
    // Zoom
    wxBoxSizer* zoomsizer = new wxBoxSizer( wxHORIZONTAL ); 
    wxString zchoice[8];
    zchoice[0] = _T("x1");
    zchoice[1] = _T("x2");
    zchoice[2] = _T("x3");
    zchoice[3] = _T("x4");
    zchoice[4] = _T("x5");
    zchoice[5] = _T("x6");
    zchoice[6] = _T("x7");
    zchoice[7] = _T("x8");
    m_zoom_combo = new wxComboBox( ctrlpanel, idZoomCombo, _T(""), wxDefaultPosition, 
				   wxDefaultSize, 8, zchoice );
    m_zoom_combo->SetSelection(1);
    zoomsizer->Add( new wxStaticText(ctrlpanel,-1,_T("Zoom")),0,wxALIGN_CENTER|wxLEFT,10);
    zoomsizer->Add( m_zoom_combo,0,wxALIGN_CENTRE|wxLEFT,5);
    viewsizer->Add( zoomsizer,0,wxALIGN_CENTRE|wxBOTTOM,10) ;

    // If is a monochannel image
    if (true/*m_image->size(0)==1*/) {
      // Colormap
      wxBoxSizer* cmapsizer = new wxBoxSizer( wxHORIZONTAL ); 
      m_color_maps[0] = _T("gray");
      m_color_maps[1] = _T("red-blue");
      m_color_maps[2] = _T("black-red-white");
      m_color_maps[3] = _T("black-blue-white");
      m_color_maps[4] = _T("black-yellow-white");
      m_color_maps[5] = _T("blue-white");
      m_color_maps[6] = _T("black body");
      m_color_maps[7] = _T("spectrum");
		
      m_color_maps_id[0] = PaletteMultilinear::gray;
      m_color_maps_id[1] = PaletteMultilinear::red_blue;
      m_color_maps_id[2] = PaletteMultilinear::black_red_white;
      m_color_maps_id[3] = PaletteMultilinear::black_blue_white;
      m_color_maps_id[4] = PaletteMultilinear::black_yellow_white;
      m_color_maps_id[5] = PaletteMultilinear::blue_white;
      m_color_maps_id[6] = PaletteMultilinear::black_body;
      m_color_maps_id[7] = PaletteMultilinear::spectrum;
		
      m_colormap_combo = new wxComboBox( ctrlpanel, idColormapCombo, _T(""), wxDefaultPosition, 
					 wxDefaultSize, 8, m_color_maps );
      m_colormap_combo->SetSelection(0);
      cmapsizer->Add( new wxStaticText(ctrlpanel,-1,_T("Palette")),0,wxALIGN_CENTER|wxLEFT,0);
      cmapsizer->Add( m_colormap_combo,0,wxALIGN_CENTRE|wxLEFT,10);
      m_colormap_inverse_check = new wxCheckBox( ctrlpanel, idColormapInverseCheck, _T("Inverse"));	
      m_colormap_inverse_check->SetValue(false);
      cmapsizer->Add( m_colormap_inverse_check,0,wxALIGN_CENTER|wxLEFT,10) ;
      viewsizer->Add( cmapsizer,0,wxALIGN_CENTRE|wxBOTTOM,5) ;
		
      // Contrast box 
      wxStaticBoxSizer* contrastsizer = new wxStaticBoxSizer( new wxStaticBox(ctrlpanel,-1,_T("Value range")), wxVERTICAL );
      m_vmin_slider = new wxSlider( ctrlpanel, idVminSlider,0,0,10000, wxDefaultPosition, wxSize(300,10), wxSL_LABELS );
      m_vmax_slider = new wxSlider( ctrlpanel, idVmaxSlider,10000,0,10000, wxDefaultPosition, wxSize(300,10), wxSL_LABELS  );
      contrastsizer->Add( m_vmin_slider,0,wxALIGN_CENTRE|wxGROW) ;
      contrastsizer->Add( m_vmax_slider,0,wxALIGN_CENTRE|wxGROW) ;
      viewsizer->Add( contrastsizer,0,wxGROW); 
    }
		
    ctrlsizer->Add( viewsizer,0,wxGROW); 
    //=====================================================================================================
		
		
    if (m_is3D) {
      //=====================================================================================================
      // 3D images : slice controls
      //=====================================================================================================
      // Slice box 
      wxStaticBoxSizer* slicesizer = new wxStaticBoxSizer( new wxStaticBox(ctrlpanel,-1,_T("Slices")), wxVERTICAL );
      
      wxBoxSizer* slicectrl = new wxBoxSizer(wxHORIZONTAL);
      m_xslice_ctrl = new wxSpinCtrl ( ctrlpanel, idXSliceCtrl, wxEmptyString, wxDefaultPosition, wxSize(50,20), wxSP_ARROW_KEYS, 0, 128, 64 );
      m_yslice_ctrl = new wxSpinCtrl ( ctrlpanel, idYSliceCtrl, wxEmptyString, wxDefaultPosition, wxSize(50,20), wxSP_ARROW_KEYS, 0, 128, 64 );
      m_zslice_ctrl = new wxSpinCtrl ( ctrlpanel, idZSliceCtrl, wxEmptyString, wxDefaultPosition, wxSize(50,20), wxSP_ARROW_KEYS, 0, 128, 64 );
		  
      slicectrl->Add( new wxStaticText(ctrlpanel,-1,_T("x")),0,wxALIGN_CENTER|wxLEFT,10);
      slicectrl->Add( m_xslice_ctrl,0,wxALIGN_CENTRE|wxLEFT,5);
      slicectrl->Add( new wxStaticText(ctrlpanel,-1,_T("y")),0,wxALIGN_CENTER|wxLEFT,10);
      slicectrl->Add( m_yslice_ctrl,0,wxALIGN_CENTRE|wxLEFT,5);
      slicectrl->Add( new wxStaticText(ctrlpanel,-1,_T("z")),0,wxALIGN_CENTER|wxLEFT,10);
      slicectrl->Add( m_zslice_ctrl,0,wxALIGN_CENTRE|wxLEFT,5);
      slicesizer->Add( slicectrl, 0,wxCENTER|wxBOTTOM,5) ;
		  
      // Ticks
      m_ticks_check = new wxCheckBox( ctrlpanel, idTicksCheck, _T("Draw slicing lines"));	
      m_ticks_check->SetValue(true);
      slicesizer->Add( m_ticks_check,0,wxALIGN_CENTER) ;
		  
      ctrlsizer->Add( slicesizer, 0,wxEXPAND) ;
      //=====================================================================================================	
    }


    //=====================================================================================================
    // Section box
    wxStaticBoxSizer* sectionsizer = new wxStaticBoxSizer( new wxStaticBox(ctrlpanel,-1,_T("Scale-Sets section")), wxVERTICAL );
    // Display style
    wxString sstyle[3];
    sstyle[0] = _T("invisible");
    sstyle[1] = _T("contours");
    sstyle[2] = _T("regions");
		
    m_section_style_radio = new wxRadioBox ( ctrlpanel, idSectionStyleRadio, _T("Display style"),wxDefaultPosition,wxDefaultSize,
					     3,sstyle,1,wxRA_SPECIFY_ROWS);
    m_section_style_radio->SetSelection(1);
		
    sectionsizer->Add(m_section_style_radio,0,wxALIGN_CENTRE|wxBOTTOM|wxGROW,10) ;
    /// Section color 
    wxString scol[6];
    scol[0] = _T("white");
    scol[1] = _T("black");
    scol[2] = _T("red");
    scol[3] = _T("green");
    scol[4] = _T("blue");
    scol[5] = _T("yellow");

    m_set_color[0] = RGBpoint::WHITE();
    m_set_color[1] = RGBpoint::BLACK();
    m_set_color[2] = RGBpoint::RED();
    m_set_color[3] = RGBpoint::GREEN();
    m_set_color[4] = RGBpoint::BLUE();
    m_set_color[5] = RGBpoint::YELLOW();
		
    m_section_color_combo = new wxComboBox( ctrlpanel, idSectionColorCombo, _T(""), wxDefaultPosition, 
					    wxDefaultSize, 6, scol );
    m_section_color_combo->SetSelection(2);

	//-----------------------------------------------------------------------------------------
	//bool wxComboBox::MSWOnDraw(WXDRAWITEMSTRUCT *WXUNUSED(item))
	/*int i = 0;
	while (i < m_section_color_combo->GetItemCount())//->Items->Count)
	{
		if (m_section_color_combo->IsSelected(i))
		{
			//e->Graphics->FillRectangle (new wxBrush(System::Drawing::SystemColors::Highlight),0,comboBox1->ItemHeight*i,comboBox1->Width,comboBox1->ItemHeight);
			//e->Graphics->FillRectangle (new wxBrush(System::Drawing::Color::Black),3,comboBox1->ItemHeight*i+2,23,10);
			//e->Graphics->FillRectangle (new wxBrush(System::Drawing::Color::FromArgb(100+i*50,100+i*25,20)),4,comboBox1->ItemHeight*i+3,21,8);
			//e->Graphics->DrawString (comboBox1->Items->Item[i]->ToString(),this->Font, new SolidBrush(System::Drawing::Color::White),28,comboBox1->ItemHeight*i);
			//ActiveColor = System::Drawing::Color::FromArgb(100+i*50,100+i*25,20);
		}
		else
		{
			//e->Graphics->FillRectangle (new wxBrush(comboBox1->BackColor),0,comboBox1->ItemHeight*i,comboBox1->Width,comboBox1->ItemHeight);
			//e->Graphics->FillRectangle (new wxBrush(System::Drawing::Color::Black),3,comboBox1->ItemHeight*i+2,23,10);
			//e->Graphics->FillRectangle (new wxBrush(System::Drawing::Color::FromArgb(100+i*50,100+i*25,20)),4,comboBox1->ItemHeight*i+3,21,8);
			//e->Graphics->DrawString (comboBox1->Items->Item[i]->ToString(),this->Font, new SolidBrush(System::Drawing::Color::Black),28,comboBox1->ItemHeight*i);
		}



		i++;
		comboBox1->SelectionLength = 0;
		comboBox1->SelectedValue = false;
	}*/
	//-----------------------------------------------------------------------------------------



	m_contours_color = m_set_color[2];
    sectionsizer->Add(m_section_color_combo,0,wxALIGN_CENTRE|wxBOTTOM,10) ;
    ctrlsizer->Add( sectionsizer,0,wxGROW); 
    //=====================================================================================================
		
		
		
		
    //=====================================================================================================
    // Scale box
    wxStaticBoxSizer* scalesizer = new wxStaticBoxSizer( new wxStaticBox(ctrlpanel,-1,_T("Scale")), wxVERTICAL );
    m_scale_slider = new wxSlider( ctrlpanel, idScaleSlider,5000,0,10000, wxDefaultPosition, wxSize(300,30)/*, wxSL_LABELS  */);
    scalesizer->Add( m_scale_slider,0,wxALIGN_CENTRE|wxGROW) ;
    // 
    m_scale_ctrl = new wxTextCtrl(ctrlpanel,idScaleCtrl,_T(""),wxDefaultPosition,wxSize(100,20),wxTE_PROCESS_ENTER|wxTE_CENTRE);
    scalesizer->Add( m_scale_ctrl,0,wxALIGN_CENTRE) ;		
    ctrlsizer->Add( scalesizer,0,wxGROW); 
    //=====================================================================================================
		
		
    //=====================================================================================================
    // Set box
    wxStaticBoxSizer* setsizer = new wxStaticBoxSizer( new wxStaticBox(ctrlpanel,-1,_T("Set")), wxVERTICAL );
    wxBoxSizer* setshowsizer = new wxBoxSizer(wxHORIZONTAL);
    // Draw check
    m_set_draw_check = new wxCheckBox( ctrlpanel, idSetDrawCheck, _T("Draw set"));	
    m_set_draw_check->SetValue(true);
    setshowsizer->Add( m_set_draw_check,0,wxALIGN_CENTER|wxGROW) ;
    // Color
    m_set_color_combo = new wxComboBox( ctrlpanel, idSetColorCombo, _T(""), wxDefaultPosition, wxDefaultSize, 6, scol );
    m_set_color_combo->SetSelection(3);
    m_cur_set_color = m_set_color[3];
    setshowsizer->Add(m_set_color_combo,0,wxALIGN_CENTRE|wxGROW); //|wxBOTTOM,10) ;
    // 
    setsizer->Add(setshowsizer,0,wxGROW);
    // 
    m_set_persistence_ctrl = new wxTextCtrl(ctrlpanel,idSetPersistenceCtrl,_T(""),wxDefaultPosition,wxSize(50,20),wxTE_PROCESS_ENTER|wxTE_CENTRE);
    setsizer->Add(m_set_persistence_ctrl,0,wxCENTRE);
	m_set_persistence = (lgl::F32)1.2;
    wxString persini;
    persini.Printf(_T("%f"),m_set_persistence);
    m_set_persistence_ctrl->SetValue(persini);
    //
    ctrlsizer->Add( setsizer,0,wxGROW); 
    //=====================================================================================================
		
    //=====================================================================================================
    // sets the ctrlsizer to the ctrlpanel
    ctrlpanel->SetSizer( ctrlsizer );  
    ctrlsizer->SetSizeHints( ctrlpanel ); 
    //=====================================================================================================
    // EO control panel creation 
    //=====================================================================================================
		
    //=====================================================================================================
    // image panel
    wxPanel* imagepanel = new wxPanel(this,-1);
    //=====================================================================================================
		
    //=====================================================================================================
    // top level sizer
    topsizer = new wxBoxSizer( wxHORIZONTAL ); 
    // Adds the ctrlpanel to the topsizer 
    topsizer->Add( ctrlpanel,0,wxGROW);		
    // Adds the ctrlpanel to the topsizer 
    topsizer->Add( imagepanel,0,wxGROW);		
    //=====================================================================================================
		
    //=====================================================================================================
    SetSizer( topsizer );
    // sets minimum frame size
    topsizer->SetSizeHints( this );
    SetAutoLayout( true );
    topsizer->Fit( this );
    //=====================================================================================================
		
		
    //=====================================================================================================
    // Works on windows but not on linux (size = 0) ... 
    //		m_images_position.x = topsizer->GetSize().x;
    //		std::cout<<"m_im_px (topsizer)  = "<<m_images_position.x<<std::endl;
    // This works on both systems :
    m_images_position.x = ctrlpanel->GetSize().x+10;
    m_images_position.y = 0;
    //=====================================================================================================
		
		
    //=====================================================================================================
    // Color map
    m_color_map = new PaletteMultilinear(PaletteMultilinear::gray,false,m_value_min,m_value_max);
    //=====================================================================================================
		
    m_zoom_combo->SetSelection(m_zoom-1);
    Fit();
		
    if (m_is3D) {
      m_xslice_ctrl->SetRange(0,m_isize(1)-1);
      m_yslice_ctrl->SetRange(0,m_isize(2)-1);
      m_zslice_ctrl->SetRange(0,m_isize(3)-1);
      m_xslice_ctrl->SetValue(m_isize(1)/2);
      m_yslice_ctrl->SetValue(m_isize(2)/2);
      m_zslice_ctrl->SetValue(m_isize(3)/2);
    }
		
    m_vmin_slider->SetRange((int)m_value_min, (int)m_value_max);
    m_vmin_slider->SetValue((int)m_value_min);
    m_vmax_slider->SetRange((int)m_value_min, (int)m_value_max);
    m_vmax_slider->SetValue((int)m_value_max);
    m_color_map->set( m_value_min, m_value_max );
		
    m_mouse_in_image = false;
		
    m_xslice_changed = m_yslice_changed = m_zslice_changed = m_scale_changed = m_set_changed = 
      m_zoom_changed = m_palette_changed = true;
		
    wxLogMessage(_T("Interface init ok"));
    return true;
  }
  //=======================================================================================
	
	
	
  //=======================================================================================
  void wxScaleSetsBrowser::Fit() 
  {
    m_vsize(1) = m_isize(1)*m_zoom;
    m_vsize(2) = m_isize(2)*m_zoom;
    m_vsize(3) = m_isize(3)*m_zoom;				
		
    if (m_is3D) {
			
      m_xslice_wp = wxPoint(m_images_position.x+m_vsize(1),m_images_position.y);
      m_yslice_wp = wxPoint(m_images_position.x,m_images_position.y);
      m_zslice_wp = wxPoint(m_images_position.x,m_images_position.y+m_vsize(3));
      m_image_frame_size.x = m_vsize(1)+m_vsize(2);
      m_image_frame_size.y = m_vsize(2)+m_vsize(3);
    }
    else {
      m_xslice_wp = wxPoint(m_images_position.x,m_images_position.y);
      m_image_frame_size.x = m_vsize(1);
      m_image_frame_size.y = m_vsize(2);
			
    }

    this->SetClientSize(m_images_position.x+m_image_frame_size.x,
			m_images_position.y+m_image_frame_size.y);
		
    wxLogMessage(_T("Fit ok"));

  }
  //=======================================================================================
	
	
	
	
	
	
  //=======================================================================================
  // MENU
  //=======================================================================================
	
	
	
  //=======================================================================================
  void wxScaleSetsBrowser::OnMenuSave (wxCommandEvent& event) 
  {
    wxString filename = wxFileSelector(_T("Select image scale-sets file"),_T(""),_T(""),_T("iss"),_T("Image scale-sets files (*.iss)|*.iss"),wxSAVE);
    if ( !filename ) return;
		
    if ( !m_sxs->save(lgl::wx2std(filename),lgl::Filename(""))) {
      wxLogError(_T("Error saving '%s'."), filename.c_str());
      return;	
    }
  }
  //=======================================================================================
	
	
  //=======================================================================================
  void wxScaleSetsBrowser::OnMenuSaveLabels (wxCommandEvent& event) 
  {
    wxString filename = wxFileSelector(_T("Select label image file"),_T(""),_T(""),_T("hdr"),_T("Label image files (*.hdr)|*.hdr"),wxSAVE);
    if ( !filename ) return;
    int res = wxMessageBox(_T("Save mean values of regions instead of label ?"),_T("Save current cut"), wxYES_NO |  wxCANCEL );

    if (res==wxCANCEL) return;

    lgl::pImageRam label_image;
    // allocation of label image by procreation
    label_image = m_sxs->getBaseImage().procreate();

    m_sxs->getSection( getCurrentScale(), label_image);

    lgl::pImageRam output = label_image;
    if (res==wxYES) {
      lgl::pImageRam average =  m_image->procreate();
      averageOnRegions(*m_image,*label_image,average);
      output = average;
    }

    if ( !lgl::writeImageAnalyze( lgl::wx2std(filename), *output)) {
      wxLogError(_T("Error saving '%s'."), filename.c_str());
      delete label_image;
      if (res==wxYES) delete output;
      return;	
    }
    delete label_image;
    if (res==wxYES) delete output;
  }
  //=======================================================================================
	
  //=======================================================================================	
  void wxScaleSetsBrowser::OnMenuAbout (wxCommandEvent& event) 
  {
    wxString msg;
    msg.Printf( _T("Scale-Sets Browser\n") 
		_T(" (c) Laurent Guigues 2004 (laurent.guigues@ign.fr)\n")
		);
    wxMessageBox(msg, _T("About"), wxOK | wxICON_INFORMATION, this);
  }
  //=======================================================================================
	

  //=======================================================================================	
  void wxScaleSetsBrowser::OnMenuQuit (wxCommandEvent& event) 
  {
    Close();
  }
  //=======================================================================================
	
	
	
	
	
	
	
  //=======================================================================================
  //=======================================================================================
  //=======================================================================================
  //=======================================================================================
  // EVENTS
  //=======================================================================================
  //=======================================================================================
  //=======================================================================================
  //=======================================================================================
	
	
  //=======================================================================================
	
  void wxScaleSetsBrowser::OnZoomChange (wxCommandEvent& event) 
  {
    m_zoom = m_zoom_combo->GetSelection()+1;
    Fit();
    m_zoom_changed = true;
    updateView();
  }
  //=======================================================================================
	
  //=======================================================================================
  void wxScaleSetsBrowser::OnColormapChange (wxCommandEvent& event) 
  {
    m_color_map->set( m_color_maps_id[m_colormap_combo->FindString(m_colormap_combo->GetValue())] );
		
    m_palette_changed = true;
    updateView();
  }
  //=======================================================================================
	
	
  //=======================================================================================
	
  void wxScaleSetsBrowser::OnColormapInverseCheck (wxCommandEvent& event) 
  {
    m_color_map->setInverse( m_colormap_inverse_check->IsChecked() );
		
    m_palette_changed = true;
    updateView();
  }
  //=======================================================================================
	
	
  //=======================================================================================	
  void wxScaleSetsBrowser::OnContrastMinChange (wxScrollEvent& event) 
  {
    double vmin = m_vmin_slider->GetValue(); 
    double vmax = m_vmax_slider->GetValue(); 
    if (vmin>vmax) { m_vmax_slider->SetValue((int)vmin); vmax = vmin; }
    //double ivmin = m_value_min + vmin*(m_value_max-m_value_min)/100.; 
    //double ivmax = m_value_min + vmax*(m_value_max-m_value_min)/100.; 
    m_color_map->set( vmin,vmax );
		
    m_palette_changed = true;
    updateView();
  }
  //=======================================================================================
	
  //=======================================================================================
	
  void wxScaleSetsBrowser::OnContrastMaxChange (wxScrollEvent& event) 
  {
    double vmin = m_vmin_slider->GetValue(); 
    double vmax = m_vmax_slider->GetValue(); 
    if (vmax<vmin) { m_vmin_slider->SetValue((int)vmax); vmin = vmax; }
    //double ivmin = m_value_min + vmin*(m_value_max-m_value_min)/100.; 
    //double ivmax = m_value_min + vmax*(m_value_max-m_value_min)/100.; 
    m_color_map->set( vmin,vmax );
		
    m_palette_changed = true;
    updateView();
  }
  //=======================================================================================
	
	
  //=======================================================================================	
  void wxScaleSetsBrowser::OnXSliceCtrl ( wxSpinEvent& event) 
  {
    m_xslice_changed = true;
    updateView();
  }
  //=======================================================================================
	
  //=======================================================================================
  void wxScaleSetsBrowser::OnYSliceCtrl ( wxSpinEvent& event) 
  {
    m_yslice_changed = true;
    updateView();
  }
  //=======================================================================================

  //=======================================================================================	
  void wxScaleSetsBrowser::OnZSliceCtrl ( wxSpinEvent& event) 
  {
    m_zslice_changed = true;
    updateView();
  }
  //=======================================================================================
	
	
  //=======================================================================================
  void wxScaleSetsBrowser::OnTicksCheck ( wxCommandEvent& event) 
  {
    // Check on : draw
    if (m_ticks_check->IsChecked()) drawTicks();
    // Check off : clears ticks by redrawing bitmaps
    else drawSliceBitmaps();
  }
  //=======================================================================================
	
	
  //=======================================================================================	
  void wxScaleSetsBrowser::OnScaleSlider (wxScrollEvent& event) 
  {
    lgl::F32 ls = m_scale_slider->GetValue()*1./m_scale_slider->GetMax();
    m_scale = invlogscale(ls);
		
    wxLogMessage(_T("logs  = %f"),ls);
    wxLogMessage(_T("ilogs = %f"),m_scale);
		
    OnScaleChange();
  }
  //=======================================================================================
	
  //=======================================================================================
  void wxScaleSetsBrowser::OnScaleCtrl (wxCommandEvent& event) 
  {
    double v;
    m_scale_ctrl->GetValue().ToDouble( &v );
    m_scale = v;
    if (m_scale<0) m_scale = 0;
    if (m_scale>m_scale_max) m_scale = m_scale_max;
    OnScaleChange();
  }
  //=======================================================================================
	
  //=======================================================================================
  void wxScaleSetsBrowser::OnSetDrawCheck ( wxCommandEvent& event) 
  {
    m_set_changed = true;
    updateView();
  }
  //=======================================================================================
	
  //=======================================================================================
  void wxScaleSetsBrowser::OnSetColorChange(wxCommandEvent& event)
  {
    m_cur_set_color = 	m_set_color[m_set_color_combo->GetSelection()];
    m_palette_changed = true;
    updateView();
  }
  //=======================================================================================
	
	
  //=======================================================================================
  void wxScaleSetsBrowser::OnSetPersistenceCtrl(wxCommandEvent& event)
  {
    double v;
    m_set_persistence_ctrl->GetValue().ToDouble(&v);
    m_set_persistence = v; 
    wxLogMessage(_T("per=%f"),m_set_persistence);
  }
  //=======================================================================================
	
	
  //=======================================================================================
  void wxScaleSetsBrowser::OnScaleChange () 
  {
    // udpates slider and ctrl
    wxString v;
    v.Printf(_T("%f"),m_scale);
    m_scale_ctrl->SetValue(v);
    lgl::F32 ls = logscale(m_scale);
		
    wxLogMessage(_T("logs = %f"),ls);
		
    m_scale_slider->SetValue((int)(ls*m_scale_slider->GetMax()));
    m_scale_changed = true;
    if (m_image_style_radio->GetSelection()==2) {
      m_xslice_changed = m_yslice_changed = m_zslice_changed = true;
    }
    updateView();
  }
  //=======================================================================================
	
	
  //=======================================================================================
	
  void wxScaleSetsBrowser::OnImageStyleChange(wxCommandEvent& event)
  {
    //	GetSelection();
    m_xslice_changed = m_yslice_changed = m_zslice_changed = true;
    updateView();
  }
  //=======================================================================================
	
  //=======================================================================================
  void wxScaleSetsBrowser::OnSectionStyleChange(wxCommandEvent& event)
  {
    m_scale_changed = true;
    updateView();
  }
  //=======================================================================================
	
	
  //=======================================================================================
  void wxScaleSetsBrowser::OnSectionColorChange(wxCommandEvent& event)
  {
    m_contours_color = 	m_set_color[m_section_color_combo->GetSelection()];
    m_palette_changed = true;
    updateView();
  }
  //=======================================================================================
	
	
	
  //=======================================================================================
  lgl::BOOL wxScaleSetsBrowser::mouseToSpace(wxMouseEvent& ev, lgl::ImageSite& p)
  {
    int x = ev.m_x;
    int y = ev.m_y;
    if (m_is3D) {
      // in the xslice
      if ((x>=m_xslice_wp.x)&&
	  (y>=m_xslice_wp.y)&&
	  (x<m_xslice_wp.x+m_xslice_bmp.GetWidth())&&
	  (y<m_xslice_wp.y+m_xslice_bmp.GetHeight())) {
	p(1) = m_xslice_ctrl->GetValue();
	p(2) = (x-m_xslice_wp.x)/m_zoom ;
	p(3) = (m_xslice_wp.y+m_xslice_bmp.GetHeight()-1-y)/m_zoom;
	wxString s;
	s.Printf(_T("%d"),p(3));
	SetStatusText(s,0);
	return true;
      }
      // in the yslice
      if ((x>=m_yslice_wp.x)&&
	  (y>=m_yslice_wp.y)&&
	  (x<m_yslice_wp.x+m_yslice_bmp.GetWidth())&&
	  (y<m_yslice_wp.y+m_yslice_bmp.GetHeight())) {
	p(1) = (x-m_yslice_wp.x)/m_zoom;
	p(2) = m_yslice_ctrl->GetValue();
	p(3) = (m_yslice_wp.y+m_yslice_bmp.GetHeight()-1-y)/m_zoom;
	return true;		
      }
      // in the zslice
      if ((x>=m_zslice_wp.x)&&
	  (y>=m_zslice_wp.y)&&
	  (x<m_zslice_wp.x+m_zslice_bmp.GetWidth())&&
	  (y<m_zslice_wp.y+m_zslice_bmp.GetHeight())) {
	p(1) = (x-m_zslice_wp.x)/m_zoom;
	p(2) = (y-m_zslice_wp.y)/m_zoom;
	p(3) = m_zslice_ctrl->GetValue();
	return true;
      }
    }
    else {
      // 2D
      if ((x>=m_xslice_wp.x)&&
	  (y>=m_xslice_wp.y)&&
	  (x<m_xslice_wp.x+m_xslice_bmp.GetWidth())&&
	  (y<m_xslice_wp.y+m_xslice_bmp.GetHeight())) {
	p(1) = (x-m_xslice_wp.x)/m_zoom;
	p(2) = (y-m_xslice_wp.y)/m_zoom;
	p(3) = 0;
	return true;
      }
    }
    return false;
  }
  //=======================================================================================
	
	
  //=======================================================================================
 // COM projet

//COM projet 
  void wxScaleSetsBrowser::OnMouse(wxMouseEvent& ev)
  {
    lgl::ImageSite pos(0,0,0,0);
    if (!mouseToSpace(ev,pos)) {
      // mouse exits the image 
      if (m_mouse_in_image) {
	m_mouse_in_image = false;
	//wxSetCursor(*wxSTANDARD_CURSOR);
      }
      return;
    }
    // mouse has entered the image
    if (!m_mouse_in_image) {
      m_mouse_in_image = true;
    }
    //wxSetCursor(wxCursor(wxCURSOR_IBEAM));
		
    // gets drawing context 
    wxClientDC dc( this );
    // Pen 
    wxPen pen(wxColor(m_cur_set_color.r,m_cur_set_color.g,m_cur_set_color.b),2,wxSOLID);
    dc.SetPen(pen);
		
    // position
    wxString stat;
    if (m_is3D) stat.Printf(_T("%d,%d,%d"),pos(1),pos(2),pos(3));
    else stat.Printf(_T("%d,%d"),pos(1),pos(2));
    SetStatusText(stat,0);
    // image value
    int v = (int)(m_image->get(pos));
    stat.Printf(_T("%d"),v);
    SetStatusText(stat,1);
    // clic 
    int x = ev.m_x;
    int y = ev.m_y;


    if (m_is3D) {
      // clic in the xslice
      if ((x>=m_xslice_wp.x)&&
	  (y>=m_xslice_wp.y)&&
	  (x<m_xslice_wp.x+m_xslice_bmp.GetWidth())&&
	  (y<m_xslice_wp.y+m_xslice_bmp.GetHeight())) {
	// change slice
	if (ev.LeftDown()) {
	  m_yslice_ctrl->SetValue( (x-m_xslice_wp.x)/m_zoom );
	  m_zslice_ctrl->SetValue( (m_xslice_wp.y+m_xslice_bmp.GetHeight()-1-y)/m_zoom );
	  m_yslice_changed = true;
	  m_zslice_changed = true;
	  // update
	  updateView();
	}
	// set selection 
	else if (ev.RightDown()) {
	  // memo position
	  m_last_sel_x = x;
	  m_last_sel_y = y;
	  ImageSite pos( 0, (x-m_xslice_wp.x)/m_zoom, (y-m_xslice_wp.y)/m_zoom ); 
	  int lab =  (int)m_xslice_base->get( pos );
	  pSet s = (*m_sxs)[lab]; 
	  m_cur_sets.clear();
	  m_cur_sets.insert(s);
	}
	// set selection 
	else if (ev.RightIsDown()) {
	  ImageSite pos( 0, (x-m_xslice_wp.x)/m_zoom, (y-m_xslice_wp.y)/m_zoom ); 
	  int lab =  (int)m_xslice_base->get( pos );
	  pSet s = (*m_sxs)[lab]; 
	  m_cur_sets.insert(s);
	  // Draws the selection 
	  if (true) {
	    dc.DrawLine(m_last_sel_x,m_last_sel_y,x,y);
	    m_last_sel_x = x;
	    m_last_sel_y = y;
	  }
	}
	// set selection 
	else if (ev.RightUp()) {
	  // Draws the selection 
	  if (true) {
	    dc.DrawLine(m_last_sel_x,m_last_sel_y,x,y);
	    m_last_sel_x = x;
	    m_last_sel_y = y;
	  }
	  // changes slice (same as left down)
	  m_yslice_ctrl->SetValue( (x-m_xslice_wp.x)/m_zoom );
	  m_zslice_ctrl->SetValue( (m_xslice_wp.y+m_xslice_bmp.GetHeight()-1-y)/m_zoom );
	  m_yslice_changed = true;
	  m_zslice_changed = true;
	  //
	  ImageSite pos( 0, (x-m_xslice_wp.x)/m_zoom, (y-m_xslice_wp.y)/m_zoom ); 
	  int lab =  (int)m_xslice_base->get( pos );
	  pSet s = (*m_sxs)[lab]; 
	  m_cur_sets.insert(s);
	  pSet sup;
	  getSupremum(*m_sxs,m_cur_sets,sup);
	  m_cur_sets.clear();
	  m_cur_sets.insert(sup);
					
	  m_set_changed = true;
	  updateView();
	}
      }
      // clic in the yslice
      else if ((x>=m_yslice_wp.x)&&
	       (y>=m_yslice_wp.y)&&
	       (x<m_yslice_wp.x+m_yslice_bmp.GetWidth())&&
	       (y<m_yslice_wp.y+m_yslice_bmp.GetHeight())) {
	// change slice
	if (ev.LeftDown()) {
	  m_xslice_ctrl->SetValue( (x-m_yslice_wp.x)/m_zoom );
	  m_zslice_ctrl->SetValue( (m_yslice_wp.y+m_yslice_bmp.GetHeight()-1-y)/m_zoom );
	  m_xslice_changed = true;
	  m_zslice_changed = true;
	  // update
	  updateView();
	}
	// set selection 
	else if (ev.RightDown()) {
	  // memo position
	  m_last_sel_x = x;
	  m_last_sel_y = y;
	  ImageSite pos( 0, (x-m_yslice_wp.x)/m_zoom, (y-m_yslice_wp.y)/m_zoom ); 
	  int lab =  (int)m_yslice_base->get( pos );
	  pSet s = (*m_sxs)[lab]; 
	  m_cur_sets.clear();
	  m_cur_sets.insert(s);
	}
	// set selection 
	else if (ev.RightIsDown()) {
	  ImageSite pos( 0, (x-m_yslice_wp.x)/m_zoom, (y-m_yslice_wp.y)/m_zoom ); 
	  int lab =  (int)m_yslice_base->get( pos );
	  pSet s = (*m_sxs)[lab]; 
	  m_cur_sets.insert(s);
	  // Draws the selection 
	  if (true) {
	    dc.DrawLine(m_last_sel_x,m_last_sel_y,x,y);
	    m_last_sel_x = x;
	    m_last_sel_y = y;
	  }
					
	}
	// set selection 
	else if (ev.RightUp()) {
	  // Draws the selection 
	  if (true) {
	    dc.DrawLine(m_last_sel_x,m_last_sel_y,x,y);
	    m_last_sel_x = x;
	    m_last_sel_y = y;
	  }
	  ImageSite pos( 0, (x-m_yslice_wp.x)/m_zoom, (y-m_yslice_wp.y)/m_zoom ); 
	  int lab =  (int)m_yslice_base->get( pos );
	  pSet s = (*m_sxs)[lab]; 
	  m_cur_sets.insert(s);
	  pSet sup;
	  getSupremum(*m_sxs,m_cur_sets,sup);
	  m_cur_sets.clear();
	  m_cur_sets.insert(sup);
					
	  m_set_changed = true;
	  updateView();
	}
      }
      // clic in the xslice
      else if ((x>=m_zslice_wp.x)&&
	       (y>=m_zslice_wp.y)&&
	       (x<m_zslice_wp.x+m_zslice_bmp.GetWidth())&&
	       (y<m_zslice_wp.y+m_zslice_bmp.GetHeight())) {
	// left clic = change slice
	if (ev.LeftDown()) {
	  m_xslice_ctrl->SetValue( (x-m_zslice_wp.x)/m_zoom );
	  m_yslice_ctrl->SetValue( (y-m_zslice_wp.y)/m_zoom );
	  m_xslice_changed = true;
	  m_yslice_changed = true;
	  // update
	  updateView();
	}
	// set selection 
	else if (ev.RightDown()) {
	  // memo position
	  m_last_sel_x = x;
	  m_last_sel_y = y;
	  ImageSite pos( 0, (x-m_zslice_wp.x)/m_zoom, (y-m_zslice_wp.y)/m_zoom ); 
	  int lab =  (int)m_zslice_base->get( pos );
	  pSet s = (*m_sxs)[lab]; 
	  m_cur_sets.clear();
	  m_cur_sets.insert(s);
	}
	else if (ev.RightIsDown()) {
	  // Draws the selection 
	  if (true) {
	    dc.DrawLine(m_last_sel_x,m_last_sel_y,x,y);
	    m_last_sel_x = x;
	    m_last_sel_y = y;
	  }
	  ImageSite pos( 0, (x-m_zslice_wp.x)/m_zoom, (y-m_zslice_wp.y)/m_zoom ); 
	  int lab =  (int)m_zslice_base->get( pos );
	  pSet s = (*m_sxs)[lab]; 
	  m_cur_sets.insert(s);
	}
	// set selection 
	else if (ev.RightUp()) {
	  // Draws the selection 
	  if (true) {
	    dc.DrawLine(m_last_sel_x,m_last_sel_y,x,y);
	    m_last_sel_x = x;
	    m_last_sel_y = y;
	  }
	  ImageSite pos( 0, (x-m_zslice_wp.x)/m_zoom, (y-m_zslice_wp.y)/m_zoom ); 
	  int lab =  (int)m_zslice_base->get( pos );
	  pSet s = (*m_sxs)[lab]; 
	  m_cur_sets.insert(s);
	  pSet sup;
	  getSupremum(*m_sxs,m_cur_sets,sup);
	  m_cur_sets.clear();
	  m_cur_sets.insert(sup);
	  m_set_changed = true;
	  updateView();
	}
      }
    }
    else {
      //=========================================
      // 2D
      if ((x>=m_xslice_wp.x)&&
	  (y>=m_xslice_wp.y)&&
	  (x<m_xslice_wp.x+m_xslice_bmp.GetWidth())&&
	  (y<m_xslice_wp.y+m_xslice_bmp.GetHeight())) {
	// change slice
	/*
	  if (ev.LeftDown()) {
	  m_yslice_ctrl->SetValue( (x-m_xslice_wp.x)/m_zoom );
	  m_zslice_ctrl->SetValue( (m_xslice_wp.y+m_xslice_bmp.GetHeight()-1-y)/m_zoom );
	  m_yslice_changed = true;
	  m_zslice_changed = true;
	  // update
	  updateView();
	  } */
	// set selection 
	if (ev.RightDown()) {
	  // memo position
	  m_last_sel_x = x;
	  m_last_sel_y = y;
	  ImageSite pos( 0, (x-m_xslice_wp.x)/m_zoom, (y-m_xslice_wp.y)/m_zoom ); 
	  int lab =  (int)m_xslice_base->get( pos );
	  pSet s = (*m_sxs)[lab]; 
	  m_cur_sets.clear();
	  m_cur_sets.insert(s);
	}
	// set selection 
	else if (ev.RightIsDown()) {
	  ImageSite pos( 0, (x-m_xslice_wp.x)/m_zoom, (y-m_xslice_wp.y)/m_zoom ); 
	  int lab =  (int)m_xslice_base->get( pos );
	  pSet s = (*m_sxs)[lab]; 
	  m_cur_sets.insert(s);
	  // Draws the selection 
	  if (true) {
	    dc.DrawLine(m_last_sel_x,m_last_sel_y,x,y);
	    m_last_sel_x = x;
	    m_last_sel_y = y;
	  }
	}
	// set selection 
	else if (ev.RightUp()) {
	  // Draws the selection 
	  if (true) {
	    dc.DrawLine(m_last_sel_x,m_last_sel_y,x,y);
	    m_last_sel_x = x;
	    m_last_sel_y = y;
	  }
	  // changes slice (same as left down)
	  /*
	    m_yslice_ctrl->SetValue( (x-m_xslice_wp.x)/m_zoom );
	    m_zslice_ctrl->SetValue( (m_xslice_wp.y+m_xslice_bmp.GetHeight()-1-y)/m_zoom );
	    m_yslice_changed = true;
	    m_zslice_changed = true;
 */
	  //
	  ImageSite pos( 0, (x-m_xslice_wp.x)/m_zoom, (y-m_xslice_wp.y)/m_zoom ); 
	  int lab =  (int)m_xslice_base->get( pos );
	  pSet s = (*m_sxs)[lab]; 
	  m_cur_sets.insert(s);
	  pSet sup;
	  getSupremum(*m_sxs,m_cur_sets,sup);
	  m_cur_sets.clear();
	  m_cur_sets.insert(sup);
					
	  m_set_changed = true;
	  updateView();
	}
      }
    }	


    // Set change
    if (ev.GetWheelRotation()!=0) {    
      std::set<ImageScaleSets::Set*> old;
      std::set<ImageScaleSets::Set*>::iterator i;
      pSet m_cur;
      if (ev.GetWheelRotation()>0) {    
	wxLogMessage(_T("UP"));
	old = m_cur_sets;
	m_cur_sets.clear();
	for (i=old.begin();i!=old.end();++i) {
	  m_cur_sets.insert((*i)->father());
	}
	m_set_changed = true;
	updateView();
      }
      else {
	wxLogMessage(_T("HOME"));
	old = m_cur_sets;
	m_cur_sets.clear();
	std::set<ImageScaleSets::Set*>::iterator i;
	for (i=old.begin();i!=old.end();++i) {
	  m_cur = (*i)->father();
	  while ((!m_cur->isRoot())&&(m_cur->persistence()<m_set_persistence)) {
	    m_cur = m_cur->father();
	  }
	  m_cur_sets.insert(m_cur);
	}
	m_set_changed = true;
	updateView();
      }
    }
    
  }
  //=======================================================================================


  //=======================================================================================
  void wxScaleSetsBrowser::OnKeyPress(wxKeyEvent& ev)
  {
    std::set<ImageScaleSets::Set*> old;
    std::set<ImageScaleSets::Set*>::iterator i;
    pSet m_cur;
    switch (ev.GetKeyCode()) {
    case WXK_UP :
      wxLogMessage(_T("UP"));
      old = m_cur_sets;
      m_cur_sets.clear();
      for (i=old.begin();i!=old.end();++i) {
	m_cur_sets.insert((*i)->father());
      }
      m_set_changed = true;
      updateView();
      break;
    case WXK_HOME :
      wxLogMessage(_T("HOME"));
      old = m_cur_sets;
      m_cur_sets.clear();
      std::set<ImageScaleSets::Set*>::iterator i;
      for (i=old.begin();i!=old.end();++i) {
	m_cur = (*i)->father();
	while ((!m_cur->isRoot())&&(m_cur->persistence()<m_set_persistence)) {
	  m_cur = m_cur->father();
	}
	m_cur_sets.insert(m_cur);
      }
      m_set_changed = true;
      updateView();
      break;
    }
  }
  //=======================================================================================


  //=======================================================================================

  lgl::F32 wxScaleSetsBrowser::getCurrentScale()
  {
    return m_scale;
  }
  //=======================================================================================






  //=======================================================================================
  // VIEW UPDATES
  //=======================================================================================





  //=======================================================================================
  void wxScaleSetsBrowser::getXSliceImage() 
  {
    if (!m_comp_ximg) return;
	
    if (m_is3D) {
      if (m_image_style_radio->GetSelection()==0) {
	// zero image
	m_xslice_img->resize( lgl::ImageSite(m_image->size(0),m_image->size(2),m_image->size(3)) );
	m_xslice_img->mset(0);
      }
      else {
	// extracts values
	ImageRam* tmp = 0;
	m_image->getSlice( lgl::ImageSite(0,1), m_xslice_ctrl->GetValue(), tmp );
	verticalFlip ( *tmp, m_xslice_img );
	delete tmp;
      }
    }
    else {
      if (m_image_style_radio->GetSelection()==0) {
	// zero image
	m_xslice_img->resize( m_image->size() );
	m_xslice_img->mset(0);
      }
      else {
	if (m_xslice_img) delete m_xslice_img;
	m_xslice_img = m_image->clone();
      }
    }
  }
  //=======================================================================================

  //=======================================================================================
  void wxScaleSetsBrowser::getYSliceImage() 
  {
    if (!m_comp_yimg) return;
	
    if (m_image_style_radio->GetSelection()==0) {
      // zero image
      m_yslice_img->resize( lgl::ImageSite(m_image->size(0),m_image->size(1),m_image->size(3)) );
      m_yslice_img->mset(0);
    }
    else {
      ImageRam* tmp = 0;
      m_image->getSlice( lgl::ImageSite(0,0,1), m_yslice_ctrl->GetValue(), tmp);
      verticalFlip ( *tmp, m_yslice_img );
      delete tmp;
    }
  }
  //=======================================================================================


  //=======================================================================================
  void wxScaleSetsBrowser::getZSliceImage() 
  {
    if (!m_comp_zimg) return;
	
    if (m_image_style_radio->GetSelection()==0) {
      // zero image
      m_zslice_img->resize( lgl::ImageSite(m_image->size(0),m_image->size(1),m_image->size(2)) );
      m_zslice_img->mset(0);
    }
    else {
      m_image->getSlice( lgl::ImageSite(0,0,0,1), m_zslice_ctrl->GetValue(), m_zslice_img );
    }
  }
  //=======================================================================================






  //=======================================================================================
  void wxScaleSetsBrowser::getXSliceBase() {
    if (!m_comp_xbase) return;
	
    if (m_is3D) {
      ImageRam* tmp = 0;
      m_sxs->getBaseImage().getSlice(lgl::ImageSite(0,1), m_xslice_ctrl->GetValue(), tmp );
      verticalFlip ( *tmp, m_xslice_base ); 
      delete tmp;
    }
    else {
      m_xslice_base = &m_sxs->getBaseImage();
    }
    

  }
  //=======================================================================================

  //=======================================================================================
  void wxScaleSetsBrowser::getYSliceBase() {
    if (!m_comp_ybase) return;
	
    ImageRam* tmp = 0;
    m_sxs->getBaseImage().getSlice(lgl::ImageSite(0,0,1), m_yslice_ctrl->GetValue(), tmp );
    verticalFlip ( *tmp, m_yslice_base ); 
    delete tmp;
  }
  //=======================================================================================

  //=======================================================================================
  void wxScaleSetsBrowser::getZSliceBase() {
    if (!m_comp_zbase) return;
	
    m_sxs->getBaseImage().getSlice(lgl::ImageSite(0,0,0,1), m_zslice_ctrl->GetValue(), m_zslice_base);
  }
  //=======================================================================================





  //=======================================================================================
  void wxScaleSetsBrowser::getXSliceSection() {
    if (!m_comp_xsec) return;
	
    m_sxs->getSection( *m_xslice_base , getCurrentScale(), m_xslice_lab);


  }
  //=======================================================================================

  //=======================================================================================
  void wxScaleSetsBrowser::getYSliceSection() {
    if (!m_comp_ysec) return;
	
    m_sxs->getSection( *m_yslice_base , getCurrentScale(), m_yslice_lab );
  }
  //=======================================================================================

  //=======================================================================================
  void wxScaleSetsBrowser::getZSliceSection() {
    if (!m_comp_zsec) return;
	
    m_sxs->getSection( *m_zslice_base, getCurrentScale(), m_zslice_lab );
  }
  //=======================================================================================






  //=======================================================================================
  void wxScaleSetsBrowser::getCurrentSetsBaseMapping() {
    if (!m_set_changed) return;
	
    ScaleSets::Set* set = (*m_cur_sets.begin());
    wxLogMessage(_T("... base mapping %d "),set->index());
	
    // get base 
    std::vector<ScaleSets::Set*> sbase;
    getBase(set,sbase);
    // mapping 
    m_cur_sets_base_map.clear();
    std::vector<ScaleSets::Set*>::iterator i;
    for (i=sbase.begin();i!=sbase.end();++i) {
      m_cur_sets_base_map.insert(std::pair<int,int>((*i)->index(),1));
      //		wxLogMessage(_T("base mapping : %d -> 1"),(*i)->index());
    }
    wxLogMessage(_T("base mapping : card = %d"),m_cur_sets_base_map.size());


    wxString mess;
    mess.Printf(_T("%d"),set->index());
    SetStatusText(mess,2);
    mess.Printf(_T("%d"),m_cur_sets_base_map.size());
    SetStatusText(mess,3);
    mess.Printf(_T("%f"),set->scaleOfAppearance());
    SetStatusText(mess,4);
    mess.Printf(_T("%f"),set->scaleOfDisappearance());
    SetStatusText(mess,5);
    if (set->scaleOfAppearance()!=0) {
      mess.Printf(_T("%f"),set->persistence());
      SetStatusText(mess,6);
    }
    else {
      mess.Printf(_T("+oo"));
      SetStatusText(mess,6);
    }
  }
  //=======================================================================================


  //=======================================================================================
  void wxScaleSetsBrowser::getXSliceSets() {
    if (!m_comp_xset) return;
	
    lgl::map ( *m_xslice_base, m_cur_sets_base_map, m_xslice_set, 0 );

  }
  //=======================================================================================

  //=======================================================================================
  void wxScaleSetsBrowser::getYSliceSets() {
    if (!m_comp_yset) return;
	
    lgl::map ( *m_yslice_base, m_cur_sets_base_map, m_yslice_set, 0 );
  }
  //=======================================================================================

  //=======================================================================================
  void wxScaleSetsBrowser::getZSliceSets() {
    if (!m_comp_zset) return;
	
    lgl::map ( *m_zslice_base, m_cur_sets_base_map, m_zslice_set, 0 );
  }
  //=======================================================================================





  //=======================================================================================
  void wxScaleSetsBrowser::getXSliceBitmap() {
    if (!m_comp_xbmp) return;
	
    // Mean values image requested
    if (m_image_style_radio->GetSelection()==2) {
      averageOnRegions(*m_xslice_img,*m_xslice_lab,m_xslice_img);
    }
    // contours
	ImageRGB tmp(lgl::ImageSite(3,3,3)); 
    if (m_xslice_img->channels()==1) {
      if (m_section_style_radio->GetSelection()==0) {
	MonoChannelImageToImageRGB ( *m_xslice_img, tmp, *m_color_map, m_zoom ) ;
      }
      else if (m_section_style_radio->GetSelection()==1) {
	MonoChannelImageAndLabelsToImageRGB ( *m_xslice_img, *m_xslice_lab, tmp, *m_color_map, m_contours_color , m_zoom );
      }
      else if (m_section_style_radio->GetSelection()==2) {
	MonoChannelImageToImageRGB ( *m_xslice_lab, tmp, m_palette_regions, m_zoom ) ;
      }
    }
    else {
      lgl::PaletteId pal;
      if (m_section_style_radio->GetSelection()==0) {
	ImageToImageRGB ( *m_xslice_img, tmp, pal, m_zoom ) ;
      }
      else if (m_section_style_radio->GetSelection()==1) {
	ImageAndLabelsToImageRGB ( *m_xslice_img, *m_xslice_lab, tmp, pal, m_contours_color , m_zoom );
      }
      else if (m_section_style_radio->GetSelection()==2) {
	ImageToImageRGB ( *m_xslice_lab, tmp, m_palette_regions, m_zoom ) ;
      }

    }
    // set
    if (m_set_draw_check->IsChecked()) {
      drawContours( *m_xslice_set, m_cur_set_color, m_zoom, tmp );
    }

    // bmp
    m_xslice_bmp = wxBitmap ( tmp );
	
  }
  //=======================================================================================

  //=======================================================================================
  void wxScaleSetsBrowser::getYSliceBitmap() {
    if (!m_comp_ybmp) return;
	
    // Mean values image requested
    if (m_image_style_radio->GetSelection()==2) {
      averageOnRegions(*m_yslice_img,*m_yslice_lab,m_yslice_img);
    }
	
    // contours 
    ImageRGB tmp;
    if (m_section_style_radio->GetSelection()==0) {
      MonoChannelImageToImageRGB ( *m_yslice_img, tmp, *m_color_map, m_zoom ); 
    }
    else if (m_section_style_radio->GetSelection()==1) {
      MonoChannelImageAndLabelsToImageRGB ( *m_yslice_img, *m_yslice_lab, tmp, *m_color_map, m_contours_color , m_zoom);
    }
    else if (m_section_style_radio->GetSelection()==2) {
      MonoChannelImageToImageRGB ( *m_yslice_lab, tmp, m_palette_regions, m_zoom ) ;
    }
	
    // set
    if (m_set_draw_check->IsChecked()) {
      drawContours( *m_yslice_set, m_cur_set_color, m_zoom, tmp );
    }
    // bmp
    m_yslice_bmp = wxBitmap ( tmp );
  }
  //=======================================================================================

  //=======================================================================================
  void wxScaleSetsBrowser::getZSliceBitmap() {
    if (!m_comp_zbmp) return;
	
    // Mean values image requested
    if (m_image_style_radio->GetSelection()==2) {
      averageOnRegions(*m_zslice_img,*m_zslice_lab,m_zslice_img);
    }
	
    // contours
    ImageRGB tmp;
    if (m_section_style_radio->GetSelection()==0) {
      MonoChannelImageToImageRGB ( *m_zslice_img, tmp, *m_color_map, m_zoom); 
    }
    else if (m_section_style_radio->GetSelection()==1) {
      MonoChannelImageAndLabelsToImageRGB ( *m_zslice_img, *m_zslice_lab, tmp, *m_color_map, m_contours_color , m_zoom );
    }
    else if (m_section_style_radio->GetSelection()==2) {
      MonoChannelImageToImageRGB ( *m_zslice_lab, tmp, m_palette_regions, m_zoom ) ;
    }
	
    // set
    if (m_set_draw_check->IsChecked()) {
      drawContours( *m_zslice_set, m_cur_set_color, m_zoom, tmp );
    }
    // bmp
    m_zslice_bmp = wxBitmap ( tmp );
  }
  //=======================================================================================




  //======================================================================================= 	
  void wxScaleSetsBrowser::drawSliceBitmaps() 
  {
    drawXSliceBitmap();
    drawYSliceBitmap();
    drawZSliceBitmap();
  }
  //=======================================================================================

  //=======================================================================================
  void wxScaleSetsBrowser::drawXSliceBitmap() 
  {
    wxClientDC dc( this );
    dc.DrawBitmap( m_xslice_bmp, m_xslice_wp.x, m_xslice_wp.y );
    drawXTicks();
  }
  //=======================================================================================

  //=======================================================================================
  void wxScaleSetsBrowser::drawYSliceBitmap() 
  {
    if (!m_is3D) return;	
    wxClientDC dc( this );
    dc.DrawBitmap( m_yslice_bmp, m_yslice_wp.x, m_yslice_wp.y );
    drawYTicks();
  }
  //=======================================================================================

  //=======================================================================================
  void wxScaleSetsBrowser::drawZSliceBitmap() 
  {
    if (!m_is3D) return;	
    wxClientDC dc( this );
    dc.DrawBitmap( m_zslice_bmp, m_zslice_wp.x, m_zslice_wp.y );
    drawZTicks();
  }
  //=======================================================================================





  //=======================================================================================
  void wxScaleSetsBrowser::drawTicks ( ) 
  {
    drawXTicks();
    drawYTicks();
    drawZTicks();
  }
  //=======================================================================================

  //=======================================================================================
  void wxScaleSetsBrowser::drawXTicks ( ) 
  {
	
    if (!m_is3D) return;	
    if (!m_ticks_check->IsChecked()) return;

    wxClientDC dc( this );
    dc.SetPen(wxPen(*wxRED,1,wxLONG_DASH));
    // xslice 
    dc.DrawLine(m_xslice_wp.x+m_zoom*m_yslice_ctrl->GetValue()+m_zoom/2,
		m_xslice_wp.y,
		m_xslice_wp.x+m_zoom*m_yslice_ctrl->GetValue()+m_zoom/2,
		m_xslice_wp.y+m_xslice_bmp.GetHeight());
    dc.DrawLine(m_xslice_wp.x,
		m_xslice_wp.y+m_xslice_bmp.GetHeight()-m_zoom*m_zslice_ctrl->GetValue()-m_zoom/2,
		m_xslice_wp.x+m_xslice_bmp.GetWidth(),
		m_xslice_wp.y+m_xslice_bmp.GetHeight()-m_zoom*m_zslice_ctrl->GetValue()-m_zoom/2);
  }
  //=======================================================================================


  //=======================================================================================
  void wxScaleSetsBrowser::drawYTicks ( ) 
  {
    if (!m_is3D) return;	
    if (!m_ticks_check->IsChecked()) return;

    wxClientDC dc( this );
    dc.SetPen(wxPen(*wxRED,1,wxLONG_DASH));
    // xslice 
    dc.DrawLine(m_yslice_wp.x+m_zoom*m_xslice_ctrl->GetValue()+m_zoom/2,
		m_yslice_wp.y,
		m_yslice_wp.x+m_zoom*m_xslice_ctrl->GetValue()+m_zoom/2,
		m_yslice_wp.y+m_yslice_bmp.GetHeight());
    dc.DrawLine(m_yslice_wp.x,
		m_yslice_wp.y+m_yslice_bmp.GetHeight()-m_zoom*m_zslice_ctrl->GetValue()-m_zoom/2,
		m_yslice_wp.x+m_yslice_bmp.GetWidth(),
		m_yslice_wp.y+m_yslice_bmp.GetHeight()-m_zoom*m_zslice_ctrl->GetValue()-m_zoom/2);
	
  }
  //=======================================================================================


  //=======================================================================================
  void wxScaleSetsBrowser::drawZTicks ( ) 
  {
    if (!m_is3D) return;	
    if (!m_ticks_check->IsChecked()) return;

    wxClientDC dc( this );
    dc.SetPen(wxPen(*wxRED,1,wxLONG_DASH));
    // xslice 
    dc.DrawLine(m_zslice_wp.x+m_zoom*m_xslice_ctrl->GetValue()+m_zoom/2,
		m_zslice_wp.y,
		m_zslice_wp.x+m_zoom*m_xslice_ctrl->GetValue()+m_zoom/2,
		m_zslice_wp.y+m_zslice_bmp.GetHeight());
    dc.DrawLine(m_zslice_wp.x,
		m_zslice_wp.y+m_zoom*m_yslice_ctrl->GetValue()+m_zoom/2,
		m_zslice_wp.x+m_zslice_bmp.GetWidth(),
		m_zslice_wp.y+m_zoom*m_yslice_ctrl->GetValue()+m_zoom/2);
	
  }
  //=======================================================================================



  //=======================================================================================
  void wxScaleSetsBrowser::OnPaint(wxPaintEvent& ev ) 
  {

    wxPaintDC dc( this );
	
    dc.DrawBitmap( m_xslice_bmp, m_xslice_wp.x, m_xslice_wp.y );
    if (m_is3D) {
      dc.DrawBitmap( m_yslice_bmp, m_yslice_wp.x, m_yslice_wp.y );
      dc.DrawBitmap( m_zslice_bmp, m_zslice_wp.x, m_zslice_wp.y );
    }
    drawTicks();
  }
  //=======================================================================================


  //======================================================================================= 	
  void wxScaleSetsBrowser::resolveChanges() 
  {
	
    m_comp_ximg = m_xslice_changed && (m_image_style_radio->GetSelection()>0);
    m_comp_yimg = m_is3D && m_yslice_changed && (m_image_style_radio->GetSelection()>0);
    m_comp_zimg = m_is3D && m_zslice_changed && (m_image_style_radio->GetSelection()>0);
	
    m_comp_xsec = ( m_xslice_changed || m_scale_changed ) && ( (m_section_style_radio->GetSelection()>0) || (m_image_style_radio->GetSelection()==2) );
    m_comp_ysec = m_is3D && ( m_yslice_changed || m_scale_changed ) && ( (m_section_style_radio->GetSelection()>0) || (m_image_style_radio->GetSelection()==2) );
    m_comp_zsec = m_is3D && ( m_zslice_changed || m_scale_changed ) && ( (m_section_style_radio->GetSelection()>0) || (m_image_style_radio->GetSelection()==2) );
	
    m_comp_xset = (m_set_draw_check->IsChecked()) && ( m_xslice_changed || m_set_changed );
    m_comp_yset = m_is3D && (m_set_draw_check->IsChecked()) && ( m_yslice_changed || m_set_changed );
    m_comp_zset = m_is3D && (m_set_draw_check->IsChecked()) && ( m_zslice_changed || m_set_changed );
	
    m_comp_xbase = m_comp_xsec || m_comp_xset ;
    m_comp_ybase = m_is3D && ( m_comp_ysec || m_comp_yset );
    m_comp_zbase = m_is3D && ( m_comp_zsec || m_comp_zset );
	
    m_comp_xbmp = ( m_xslice_changed || m_scale_changed || m_set_changed || m_zoom_changed || m_palette_changed );
    m_comp_ybmp = m_is3D && ( m_yslice_changed || m_scale_changed || m_set_changed || m_zoom_changed || m_palette_changed );
    m_comp_zbmp = m_is3D && ( m_zslice_changed || m_scale_changed || m_set_changed || m_zoom_changed || m_palette_changed );
	
  }
  //======================================================================================= 	

  //======================================================================================= 	
  void wxScaleSetsBrowser::updateView() 
  {
	
    wxLogMessage(_T("========================================="));
    
    
    
    resolveChanges();
    
    wxStopWatch sw;
    
    getXSliceImage();
    getYSliceImage();
    getZSliceImage();
	
    wxLogMessage(_T("Image slicing : %ldms "),sw.Time());
    //	wxMessageBox("OK");
	
    sw.Start();
    getXSliceBase();
    getYSliceBase();
    getZSliceBase();
    wxLogMessage(_T("Base slicing : %ldms "),sw.Time());
    //	wxMessageBox("OK");
	
    sw.Start();
    getXSliceSection();
    getYSliceSection();
    getZSliceSection();
    wxLogMessage(_T("Section : %ldms "),sw.Time());
    //	wxMessageBox("OK");
	
    sw.Start();
    getCurrentSetsBaseMapping();
    getXSliceSets();
    getYSliceSets();
    getZSliceSets();
    wxLogMessage(_T("Sets : %ldms "),sw.Time());
    //	wxMessageBox("OK");
	
    sw.Start();
    getXSliceBitmap();
    getYSliceBitmap();
    getZSliceBitmap();
    wxLogMessage(_T("Bitmaps : %ldms "),sw.Time());
    //	wxMessageBox("OK");
	
    sw.Start();
    drawXSliceBitmap();
    drawYSliceBitmap();
    drawZSliceBitmap();
    wxLogMessage(_T("Drawing : %ldms "),sw.Time());
    //	wxMessageBox("OK");
	
    m_xslice_changed = m_yslice_changed = m_zslice_changed = m_scale_changed = m_set_changed = false;
    m_zoom_changed = m_palette_changed = false;
  }
  //=======================================================================================
}
#endif

