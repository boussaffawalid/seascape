/* 

SxS : A C++ implementation of the scale climbing algorithm for multiscale image segmentation.
� 2004 Laurent Guigues (laurent.guigues@ign.fr)

This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.

You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
#ifndef __sxsScaleSetsAlgorithms_h__
#define __sxsScaleSetsAlgorithms_h__

//===========================================================
// namespace	: sxs
/*! \file 
	\brief Freehand template algorithms on ScaleSets : section,base,supremum ...
*/
//===========================================================
// author		: Laurent Guigues
// history		: 
//	12/07/04		creation
//===========================================================
#include "sxsGeneral.h"
#include "sxsScaleSets.h"
#include "lglImageRam.h"
#include <map>
#include <set>

namespace sxs 
{

  typedef ScaleSets::Set* pSet;
  //===========================================================
  /// Pushes back in the container C the elements of the 
  /// ScaleSets S which constitute its section (horizontal cut) of scale s.
  /// C must be a container of ScaleSets::Set*.
  template <class pSetContainer> 
  void getSection ( const ScaleSets& S, lgl::F32 s, pSetContainer& C); 
  //===========================================================

  //===========================================================
  /// Pushes back in the container out the elements of the ScaleSets S 
  /// which constitute the base of the pSet in
  template <class pSetContainer>
  void getBase ( pSet in, pSetContainer& out); //IMP
  //===========================================================


  //===========================================================
  /// Pushes back in the container out the elements of the ScaleSets S 
  /// which constitute the base of the elements of container in (just applies the single input pSet method to all the elements of in). 
  template <class pSetContainer1, class pSetContainer2 >
  void getBase ( const ScaleSets& S, const pSetContainer1& in, pSetContainer2& out); // IMP
  //===========================================================

  //===========================================================
  /// Returns the supremum of the Sets of the container in 
  template <class pSetContainer>
  void getSupremum ( ScaleSets& S, const pSetContainer& in, pSet& sup); // IMP
  //===========================================================

  //===========================================================
  /// Maps the Sets of the pSetContainer to their maximal ancestors 
  /// whose scale is lower than scale
  template <class pSetContainer>
  void getMaximalAncestors ( const pSetContainer& in, lgl::F32 scale, std::map<pSet,pSet>& out);
  //===========================================================

  //===========================================================
  /// Transforms the label image in (whose labels correspond to 
  /// Sets index in the ScaleSets S) into the label image corresponding to the 
  /// indices of the maximal ancestors of the initial labels.
  /// Nota : The ScaleSets is not const because the algorithm needs to allocate a Slot, however the Slot is freed and the ScaleSets is unchanged after the algorithm
  template <class T>
  void getMaximalSection ( const lgl::ImageRamT<T>& in, ScaleSets& S, lgl::F32 scale, lgl::ImageRamT<T>& out);	 // OK
  //===========================================================
  

#include "sxsScaleSetsAlgorithms_code.h"

}
//===========================================================
// EO namespace sxs
//===========================================================

//===========================================================
// EOF
//===========================================================
#endif


