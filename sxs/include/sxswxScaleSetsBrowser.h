/* 

SxS : A C++ implementation of the scale climbing algorithm for multiscale image segmentation.
� 2004 Laurent Guigues (laurent.guigues@ign.fr)

This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.

You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
#ifndef __sxswxScaleSetsBrowser_h__
#define __sxswxScaleSetsBrowser_h__


//===========================================================
// namespace	: lgl (wx)
// class		: ScaleSetsBrowser
//===========================================================
// author		: Laurent Guigues
// history		: 
//	20/09/04	: creation
/*! \file 
	\brief A wx*-based viewer of ImageScaleSets for 2D or 3D images
*/
//===========================================================
#ifdef __SXSWX__
#include "sxsGeneral.h"
#include <wx/spinctrl.h>
//#include <wx/minifram.h>
#include "lglImageRam.h"
#include "lglImageManip.h"
#include "lglImageIO.h"
#include "sxsImageScaleSets.h"
#include "sxsScaleSetsAlgorithms.h"
#include <limits>
#include <set>

namespace sxs {

	

  //=====================================================================================
  /// \brief A wx*-based browser of an ImageScaleSets
  /// 
  class wxScaleSetsBrowser : public wxFrame
    {
      
    public:
      wxScaleSetsBrowser(wxFrame *parent, ImageScaleSets& scale_sets, bool delete_scale_sets_on_closing, const wxString& title=(wxChar*)"Scale-Sets Browser" );
      ~wxScaleSetsBrowser();
      
      lgl::BOOL InitBrowser();
      lgl::BOOL InitInterface();
      void Fit();
      
      
      void OnMenuSave(wxCommandEvent& event); 
      void OnMenuSaveLabels(wxCommandEvent& event);
      void OnMenuQuit(wxCommandEvent& event);
      void OnMenuAbout(wxCommandEvent& event);
      
      void OnXSliceCtrl(wxSpinEvent& event);
      void OnYSliceCtrl(wxSpinEvent& event);
      void OnZSliceCtrl(wxSpinEvent& event);
      
      void OnImageStyleChange(wxCommandEvent& event);
      void OnZoomChange(wxCommandEvent& event);
      void OnColormapChange(wxCommandEvent& event);
      void OnColormapInverseCheck(wxCommandEvent& event);
      void OnContrastMinChange(wxScrollEvent& event);
      void OnContrastMaxChange(wxScrollEvent& event);
      void OnTicksCheck(wxCommandEvent& event);
      
      void OnSectionStyleChange(wxCommandEvent& event);
      void OnSectionColorChange(wxCommandEvent& event);
      
      void OnScaleSlider(wxScrollEvent& event);
      void OnScaleCtrl(wxCommandEvent& event);
      void OnScaleChange();
      
      void OnSetDrawCheck(wxCommandEvent& event);
      void OnSetColorChange(wxCommandEvent& event);
      void OnSetPersistenceCtrl(wxCommandEvent& event);
      
      void OnPaint(wxPaintEvent& ev );
      void OnMouse(wxMouseEvent& ev);
      void OnKeyPress(wxKeyEvent& ev);
      
      lgl::F32 getCurrentScale();
      /// Slices the 3D image (computes the ImageRams)
      void getSliceImages();
      void getXSliceImage();
      void getYSliceImage();
      void getZSliceImage();
      /// Slices of the base segmentation
      void getXSliceBase();
      void getYSliceBase();
      void getZSliceBase();
      /// Slices the 3D sections at current scale (computes the ImageRams)
      void getSliceSections();
      void getXSliceSection();
      void getYSliceSection();
      void getZSliceSection();
      /// Computes the label images of the current set at current slicing point
      void getCurrentSetsBaseMapping();
      void getSliceSets();
      void getXSliceSets();
      void getYSliceSets();
      void getZSliceSets();
      /// Transform the ImageRams to Bitmaps
      void getSliceBitmaps();
      void getXSliceBitmap();
      void getYSliceBitmap();
      void getZSliceBitmap();
      /// Draws the slices bitmaps
      void drawSliceBitmaps();
      void drawXSliceBitmap();
      void drawYSliceBitmap();
      void drawZSliceBitmap();
      
      /// Draws the ticks
      void drawTicks();
      void drawXTicks();
      void drawYTicks();
      void drawZTicks();
      
      /// Makes all 
      void updateView();
      
      // computes the 3D position corresponding to the mouse coordinates
      // returns true iff the mouse is in a slice view
      lgl::BOOL mouseToSpace(wxMouseEvent& ev, lgl::ImageSite& p);
      
      // Returns the label in the current section of the site s : the slice images must be up to date 
      int labelOfSite(lgl::ImageSite& s);
      
      /// Log mapping of the scales into [0,1]
      inline lgl::F32 logscale( lgl::F32 l ) { lgl::F32 nl = l/m_scale_min; if (nl<1) nl=1; return log(nl)/m_logscale_max; }
      /// Inverse mapping
      inline lgl::F32 invlogscale( lgl::F32 s ) { return m_scale_min * exp ( s * m_logscale_max ); }
      
      
      /// Determines what must be recomputed
      void resolveChanges();
      
    protected:
      //==================================================
      /// A pointer on the ImageScaleSets
      ImageScaleSets*		m_sxs;
      /// must the ScaleSets be deleted by the browser on closing ? 
      lgl::BOOL			m_delete_sxs;
      /// The maximum scale (scale of the root)
      lgl::F32			m_scale_max;
      /// The minimum scale (for log scale) 
      lgl::F32			m_scale_min;
      /// The maximum value of logscale
      lgl::F32			m_logscale_max;
      /// The current scale
      lgl::F32			m_scale;
      
      /// Input image parameters
      /// A pointer on the image;
      lgl::ImageRam*		m_image;
      /// Is 3D ?
      lgl::BOOL			m_is3D;
      /// Min/max value of the image
      double				m_value_min, m_value_max;
      /// The image size
      lgl::ImageSite		m_isize;
      /// The view of the image size (=m_isize*m_zoom)
      lgl::ImageSite		m_vsize;
      /// The size of the image frame (containing all three slices)
      wxSize			m_image_frame_size;
      
      /// The current slice position
      lgl::ImageSite		m_slice_position;
      /// Draw ticks on slices ?
      lgl::BOOL			m_ticks_on;
      
      
      //=========================================================
      /// Menu
      wxMenuBar *menuBar;
      wxMenu *menuFile;
      wxMenu *menuHelp;
      //=========================================================
      /// Widgets			
      // top level sizer
      wxBoxSizer *topsizer;		
      /// Slice coordinate spinners
      wxSpinCtrl *m_xslice_ctrl, *m_yslice_ctrl, *m_zslice_ctrl;
      /// zoom
      wxComboBox *m_zoom_combo;
      /// colormap
      wxComboBox *m_colormap_combo;
      /// colormap inverse
      wxCheckBox *m_colormap_inverse_check;
      /// Contrast adjustment sliders
      wxSlider *m_vmin_slider, *m_vmax_slider;
      /// Image display style
      wxRadioBox *m_image_style_radio;
      /// Section display style
      wxRadioBox *m_section_style_radio;
      /// Section color 
      wxComboBox *m_section_color_combo;
      
      /// Ticks
      wxCheckBox *m_ticks_check;
      
      /// The scale slider 
      wxSlider *m_scale_slider;
      /// The scale text control
      wxTextCtrl *m_scale_ctrl;
      
      /// Set 
      /// Draw ?
      wxCheckBox *m_set_draw_check;
      /// Color
      wxComboBox *m_set_color_combo;
      /// Minimum persistence
      wxTextCtrl *m_set_persistence_ctrl; 
      //=========================================================
      // COLORS
      // color map  
      // TO DO : general colormap
      lgl::PaletteMultilinear* m_color_map;
      // Contours color
      lgl::RGBpoint m_contours_color;
      // Current set color
      lgl::RGBpoint m_cur_set_color;
      // Set colors
      lgl::RGBpoint m_set_color[6];
      // Regions palette = random colors
      lgl::PaletteLUT m_palette_regions;
      //=========================================================
      /// Color maps
      wxString	m_color_maps[9];
      lgl::PaletteMultilinear::Type m_color_maps_id[9];
      //=========================================================
      
      //=========================================================
      /// View change events
      lgl::BOOL m_xslice_changed, m_yslice_changed, m_zslice_changed, m_scale_changed;
      lgl::BOOL m_zoom_changed, m_palette_changed;
      lgl::BOOL m_set_changed;
      /// Recomputation variables 
      lgl::BOOL m_comp_ximg, m_comp_yimg, m_comp_zimg;
      lgl::BOOL m_comp_xbase, m_comp_ybase, m_comp_zbase;
      lgl::BOOL m_comp_xsec, m_comp_ysec, m_comp_zsec;
      lgl::BOOL m_comp_xset, m_comp_yset, m_comp_zset;
      lgl::BOOL m_comp_xbmp, m_comp_ybmp, m_comp_zbmp;
      //=========================================================
      
      //=========================================================
      // x,y and z slices images 
      lgl::ImageRam		*m_xslice_img, *m_yslice_img, *m_zslice_img;
      // x,y and z slices base label images 
      lgl::ImageRam		*m_xslice_base, *m_yslice_base, *m_zslice_base;
      // x,y and z slices section label images 
      lgl::ImageRam		*m_xslice_lab, *m_yslice_lab, *m_zslice_lab;
      // x,y and z slices label images of the current sets (0 : background)
      lgl::ImageRam		*m_xslice_set, *m_yslice_set, *m_zslice_set;
      //=========================================================
      // current bitmaps (image(+contours)(+set))
      wxBitmap			m_xslice_bmp, m_yslice_bmp, m_zslice_bmp;
      //=========================================================
      
      
      //=========================================================
      // zoom
      int				m_zoom;
      //=========================================================
      
      //=========================================================
      // position of images in window
      wxPoint		m_images_position;
      wxPoint		m_xslice_wp, m_yslice_wp, m_zslice_wp;
      //=========================================================
      
      //=========================================================
      // is the mouse in image ?
      lgl::BOOL		m_mouse_in_image;
      // Coord of the last point hitted in selection mode (right clic usually)
      int m_last_sel_x;
      int m_last_sel_y;
      //=========================================================
      
      //=========================================================
      // Variables for interactive set selection
      // Current sets selected
      std::set<ImageScaleSets::Set*> m_cur_sets;
      // Current sets base mapping
      std::map<int,int> m_cur_sets_base_map;
      // Minimum persistence of a valid set (pageup...)
      lgl::F32 m_set_persistence;
      // The 3D points hit by the mouse during dragging
      //std::vector<lgl::ImageSite> m_set_image_points;
      //=========================================================
      
      
      //=========================================================
      // IDs for the events
      enum
	{
	  idMenuSave,
	  idMenuSaveLabels,
	  idMenuQuit,
	  idMenuLog,
	  // it is important for the id corresponding to the "About" command to have
	  // this standard value as otherwise it won't be handled properly under Mac
	  // (where it is special and put into the "Apple" menu)
	  idMenuAbout = wxID_ABOUT,
	  
	  idStatusBar,
	  
	  idXSliceCtrl,
	  idYSliceCtrl,
	  idZSliceCtrl,
	  idTicksCheck,
	  
	  idImageStyleRadio,
	  idZoomCombo,
	  idColormapCombo,
	  idColormapInverseCheck,
	  
	  idVminSlider,
	  idVmaxSlider,
	  
	  idSectionStyleRadio,
	  idSectionColorCombo,
	  
	  idScaleSlider,
	  idScaleCtrl,
	  
	  idSetDrawCheck,
	  idSetColorCombo,
	  idSetPersistenceCtrl
	};
      
      DECLARE_EVENT_TABLE()
	};
  //=======================================================================================
  
  
  
  
};
//==========================================================================================
// EO namespace sxs
//==========================================================================================


//==========================================================================================
// EOF
//==========================================================================================

// TEST BROWSER
//wxScaleSetsBrowser *m_sxsia = new wxScaleSetsBrowser(NULL,"SXS Scale Set Browser");	
//m_sxsia->Show(TRUE);
#endif
#endif




