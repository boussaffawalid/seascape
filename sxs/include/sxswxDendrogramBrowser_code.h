/* 

SxS : A C++ implementation of the scale climbing algorithm for multiscale image segmentation.
� 2004 Laurent Guigues (laurent.guigues@ign.fr)

This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.

You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
/*! \file 
	\brief Code for the template class DendrogramBrowser
*/

#include "struct_base/btfifo.h"
#include <deque>


////////////////////////////////////////////////////////////////////
/// Calcule le niveau des noeuds dans la hierarchie (z).
/// Renvoie le niveau max.
template <class SxS>
Int4 DendrogramBrowser<SxS>::TailleBase()
{
  Int4 T=0;
  NodeIt nit,enit=endNodes();
  for (nit=Nodes();nit!=enit;++nit) {
    if ((*nit)._getsysFlag(BaseSlot)==1) T++;
  }
  return T;
}
////////////////////////////////////////////////////////////////////
/// Calcule le niveau des noeuds dans la hierarchie (z).
/// Renvoie le niveau max.
template <class SxS>
Int4 DendrogramBrowser<SxS>::FindLevels()
{
//  FindOrder();
  hauteur = 0;
  NodeIt nit,enit=endNodes();
  for (nit=Nodes();nit!=enit;++nit) {
    (*nit).val().y = 0;
    (*nit).val()._N = 1;
  }
  typename TList<Node>::iterator lit,elit=Order.end();
  for (lit=Order.begin();lit!=elit;++lit) {
    //Node n = *lit;
    // Si noeud de base : suivant
    if ((*lit)._getsysFlag(BaseSlot)==1) continue;
    (*lit).val()._N = 0;
    IOEdgeIt eit,eeit=(*lit).endOut();
    for (eit=(*lit).Out();eit!=eeit;++eit) {
      (*lit).val()._N += (*eit).To().val()._N;
      int y = (int)(1+(*eit).To().val().y);
      if (y>(*lit).val().y) (*lit).val().y = y; 
      if (y>hauteur) hauteur=y;
    }
  }
  return hauteur;
}
////////////////////////////////////////////////////////////////////
template <class SxS>
void DendrogramBrowser<SxS>::_FP_rec_D1st(Node n)
{
  // Si noeud de base : assigne pos
  if (n._getsysFlag(BaseSlot)==1) {
    n.val().x = _FP_x;
    _FP_x++;
  }
  // Sinon appel recursif
  else {
    IOEdgeIt ite,eite=n.endOut();
    for (ite = n.Out(); ite!=eite; ++ite) {
      _FP_rec_D1st((*ite).To());
    }
  }
}

////////////////////////////////////////////////////////////////////
/// Calcule les positions "demelees" des elements pour affichage planaire
/// (Assignation Depth First Search pour la base et moyennage en remontee)
template <class SxS>
void DendrogramBrowser<SxS>::FindPositions()
{
//  FindOrder();
  // 1 : DFS
  _FP_x = 0;
  _FP_rec_D1st ( Apex);
  // 2 : Remontee
  typename TList<Node>::iterator lit,elit=Order.end();
  for (lit=Order.begin();lit!=elit;++lit) {
    //Node n = *lit;
    // Si noeud de base : suivant
    if ((*lit)._getsysFlag(BaseSlot)==1) continue;
    // Sinon moyennage sur fils
    Float4 nf = 0;
    (*lit).val().x = 0; 
    IOEdgeIt eit,eeit=(*lit).endOut();
    for (eit=(*lit).Out();eit!=eeit;++eit) {
      (*lit).val().x += (*eit).To().val().x;
      nf++;
    }
    (*lit).val().x /= nf;
  }
}
////////////////////////////////////////////////////////////////////
// Petite structure pour le tri de noeuds selon _N
template <class SxS>
class _FP2_Node
{
  public :
    typedef typename M::Node Node;
    Node _n;
    _FP2_Node( const Node& n = M::notaNode() ) : _n(n) {}
    bool operator > ( const _FP2_Node<M>& n )  
    {
      return (_n.val()._N > n._n.val()._N );
    }
};
////////////////////////////////////////////////////////////////////
template <class SxS>
void DendrogramBrowser<SxS>::_FP2_rec_D1st(Node n)
{
  n.val().x = n.val()._xmin + (n.val()._N - 1.)/2.;
  // tri des fils par ordre de taille
  TList< _FP2_Node<M> > fils;
  IOEdgeIt ite,eite=n.endOut();
  for (ite = n.Out(); ite!=eite; ++ite) {
    fils.insortdec ( _FP2_Node<M>((*ite).To()) );
  }
  Int4 xmin = n.val()._xmin;
  typename TList< _FP2_Node<M> >::iterator lit,elit=fils.end();
  for (lit=fils.begin();lit!=elit;++lit) {
    (*lit)._n.val()._xmin = xmin;
    xmin += (*lit)._n.val()._N;
  }
  // Appel recursif
  for (lit=fils.begin();lit!=elit;++lit) {
    _FP2_rec_D1st((*lit)._n);
  }
}
////////////////////////////////////////////////////////////////////
/// Calcule les positions "demelees" des elements pour affichage planaire
/// Version 2 : Tri sur la taille des ensembles en descente et assignation 
/// milieu des intervalles.
template <class SxS>
void DendrogramBrowser<SxS>::FindPositions2()
{
  Apex.val()._xmin = 0;
  _FP2_rec_D1st ( Apex);
}
////////////////////////////////////////////////////////////////////
// Petite structure pour le tri de noeuds selon id
template <class SxS>
class _FP3_Node
{
  public :
    typedef typename M::Node Node;
    Node _n;
    _FP3_Node( const Node& n = M::notaNode() ) : _n(n) {}
    bool operator > ( const _FP3_Node<M>& n )  
    {
      return (_n.val().Apparition() < n._n.val().Apparition());
	      //_n.getid() > n._n.getid() );
    }
};
////////////////////////////////////////////////////////////////////
template <class SxS>
void DendrogramBrowser<SxS>::_FP3_rec_D1st(Node n)
{
  n.val().x = n.val()._xmin + (n.val()._N - 1.)/2.;
  // tri des fils par ordre de taille
  TList< _FP3_Node<M> > fils;
  IOEdgeIt ite,eite=n.endOut();
  for (ite = n.Out(); ite!=eite; ++ite) {
    fils.insortdec ( _FP3_Node<M>((*ite).To()) );
  }
  Float4 xmin = n.val()._xmin;
  typename TList< _FP3_Node<M> >::iterator lit,elit=fils.end();
  for (lit=fils.begin();lit!=elit;++lit) {
    (*lit)._n.val()._xmin = xmin;
    xmin += (Float4)(*lit)._n.val()._N;
  }
  // Appel recursif
  for (lit=fils.begin();lit!=elit;++lit) {
    _FP3_rec_D1st((*lit)._n);
  }
}
////////////////////////////////////////////////////////////////////
/// Calcule les positions "demelees" des elements pour affichage planaire
/// Version 2 : Tri sur la taille des ensembles en descente et assignation 
/// milieu des intervalles.
template <class SxS>
void DendrogramBrowser<SxS>::FindPositions3()
{
  Apex.val()._xmin = 0;
  _FP3_rec_D1st ( Apex);
}
////////////////////////////////////////////////////////////////////
/// Calcule les parametres de dessin dans un rectangle donne
/// Si recalcul = true, recalcule les niveaux et positions
template <class SxS>
void DendrogramBrowser<SxS>::InitDrawing( const TRectangle<int>& R, bool recalcul)
{
  if (recalcul) {
    FindOrder();
    FindLevels();
    FindPositions();
  }
  //
  rect = R;
  xmin=1e30;
  xmax=-1e30;
  ymin=1e30;
  ymax=-1e30;
  NodeIt nit,enit=endNodes();
  for (nit=Nodes();nit!=enit;++nit) {
    Float8 x = (Float8)(*nit).val().x;
    if (x<xmin) xmin=x;
    if (x>xmax) xmax=x;
    Float8 y = (Float8)(*nit).val().y;
    if (y<ymin) ymin=y;
    if (y>ymax) ymax=y;
  }
  xfac = ((Float8)(R.p2.x-R.p1.x-20))/(xmax-xmin);
  yfac = ((Float8)(R.p2.y-R.p1.y-20))/(ymax-ymin);
  x0w = R.p1.x+10;
  y0w = R.p1.y+10;
}
////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////
/// Dessine la hierarchie en dendogramme dans le rectangle donne.
template <class SxS>
void DendrogramBrowser<SxS>::DrawDendogram( GraphicsOut& f, bool draw_rect, bool draw_nodes )
{
  //
  Float4 fpx0,fpy0,fpx1,fpy1;
  Int4 px0,py0,px1,py1;
  f.SetCouleur(0,0,0);
  if (draw_rect) f.DrawRect(rect,TripletRGB(0,0,0));
  EdgeIt eit,eeit=endEdges();
  for (eit=Edges(); eit!=eeit; ++eit) {
    DrawingPosition((*eit).From(),fpx0,fpy0);
    DrawingPosition((*eit).To(),fpx1,fpy1);
    px0 = (int)(nint(fpx0));
    py0 = (int)(nint(fpy0));
    px1 = (int)(nint(fpx1));
    py1 = (int)(nint(fpy1));
    if ((px0!=px1)||(py0!=py1)) {
      f.DrawLine(px0,py0,px1,py0);
      f.DrawLine(px1,py0,px1,py1);
    }
  }
  if (draw_nodes) {
    NodeIt nit,enit=endNodes();
    for (nit=Nodes();nit!=enit;++nit) {
      DrawingPosition((*nit),fpx0,fpy0);
      px0 = (int)(nint(fpx0));
      py0 = (int)(nint(fpy0));
      f.DrawCircle(px0,py0,2);
    }
  }
}



////////////////////////////////////////////////////////////////////
/// Calcule les y des noeuds
template <class SxS>
void DendrogramBrowser<SxS>::CalculOrdonnees(int mode)
{
  NodeIt hnit,ehnit=endNodes();
  // ordonnees = niveau
  if (mode==0) {
    FindLevels();
    _minord = 0;
    _maxord = hauteur;
    for (hnit=Nodes();hnit!=ehnit;++hnit) {
      Float4 y = (*hnit).val().y; 
      (*hnit).val().y = Ord2Unit( y );
    }
  }
  // ordonnees = echelle d'apparition
  else if (mode==1) {
    _minord = (Float4)1e30;
    _maxord = (Float4)-1e30;
    for (hnit=Nodes();hnit!=ehnit;++hnit) {
      if ((*hnit).val().Apparition()<_minord) {
	_minord = (*hnit).val().Apparition();
      }
      if ((*hnit).val().Apparition()>_maxord) {
	_maxord = (*hnit).val().Apparition();
      }
    }
    for (hnit=Nodes();hnit!=ehnit;++hnit) {
      Float4 a = (*hnit).val().Apparition();
      (*hnit).val().y = Ord2Unit( a );
    }
  }
  // ordonnees = cout
  else if (mode==2) {
    _minord = (Float4)1e30;
    _maxord = (Float4)-1e30;
    for (hnit=Nodes();hnit!=ehnit;++hnit) {
      /*
      Affine_Morceaux& F = (*hnit).val().CostOptLambda();
      int lm = F._M.size()-1;
      Float4 c = F._M[lm].z;
      */
      Float4 c = (*hnit).val().CostGeo()*(*hnit).val().CostGradient();
      if (c<_minord) {
	_minord = c;
      }
      if (c>_maxord) {
	_maxord = c;
      }
    }
    for (hnit=Nodes();hnit!=ehnit;++hnit) {
      /*
      Affine_Morceaux& F = (*hnit).val().CostOptLambda();
      int lm = F._M.size()-1;
      Float4 a = F._M[lm].z;
      */
      Float4 a = _maxord - (*hnit).val().CostGeo()*(*hnit).val().CostGradient();
      (*hnit).val().y = Ord2Unit( a );
    }
  }
  // ordonnees = deviation
  else if (mode==3) {
    _minord = (Float4)1e30;
    _maxord = (Float4)-1e30;
    for (hnit=Nodes();hnit!=ehnit;++hnit) {
      /*
      Affine_Morceaux& F = (*hnit).val().CostOptLambda();
      int lm = F._M.size()-1;
      Float4 c = F._M[lm].y - (*hnit).val().Apparition()*F._M[lm].z;
      //      c /= (*hnit).val().N();
      */
      Float4 c = (*hnit).val().CostRadio();
      if (c<_minord) {
	_minord = c;
      }
      if (c>_maxord) {
	_maxord = c;
      }
    }
    for (hnit=Nodes();hnit!=ehnit;++hnit) {
      /*
      Affine_Morceaux& F = (*hnit).val().CostOptLambda();
      int lm = F._M.size()-1;
      Float4 a = F._M[lm].y - (*hnit).val().Apparition()*F._M[lm].z;
      // a /= (*hnit).val().N();
      */
      Float4 a = (*hnit).val().CostRadio();
      (*hnit).val().y = Ord2Unit( a );
    }
  }
}
////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////
/// Unit <-> Ord
/// Mapping des ordonnees vers [0,1] et reciproquement
/// SetScaling fixe une fonction de mapping de parametre s
/// mode / s
/// 0 : lineaire / s : N/A
/// 1 : log / s : calcule automatiquement
/// 2 : e(-sx)    
template <class SxS>
void DendrogramBrowser<SxS>::SetScaling( int mode, float s )
{
  _ymap_mode = mode;
  _ymap_coef = s;
}

////////////////////////////////////////////////////////////////////
/// Unit <-> Ord
template <class SxS>
float DendrogramBrowser<SxS>::y2unit( float a )
{
  float b=0;
  if (_ymap_mode==0) {
    b = (a-_minord)/(_maxord-_minord);
  }
  else if (_ymap_mode==1) {
    if (a>_ymap_coef) {
      b = log(a/_ymap_coef) / log(_maxord/_ymap_coef);
    }
  }
  else if (_ymap_mode==2) {
    b = 1 - exp(-_ymap_coef*a);
  }
  return b;
}
//=============================================================

//=============================================================
template <class SxS>
float DendrogramBrowser<SxS>::unit2y( Float4 a )
{
 float b=0;
  if (_ymap_mode==0) {
    b = _minord + a*(_maxord-_minord);
  }
  else if (_ymap_mode==1) {
    if (a>0) {
      b = _ymap_coef * exp(a * log(_maxord/_ymap_coef));
    }
  }
  else if (_ymap_mode==2) {
    b = log ( 1. / ( 1.-a) ) / _ymap_coef;
  }
  return b;
}
//=============================================================


//=============================================================
template <class SxS>
void DendrogramBrowser<SxS>::DrawGraduations( GraphicsOut& GO, int nstep, int fsize) 
{
#ifdef __USE_INTERFACE__
  Float4 step = 1;
  Fonte2D font(fsize);
  if (nstep==0) {
    nstep = (int)(_maxord-_minord);
  }
  else {
    step = (_maxord-_minord)/(Float4)nstep;
  }
  Float4 prec = (Float4)(1./step);
  Float4 l=_minord;
  for (int i=0; i<=nstep; ++i,l+=step ) {
    Float4 yh = Ord2Unit(l); 
    Float4 xh=0,xd,yd;
    Hier2Win(xh,yh,xd,yd);
    GO.DrawLine(rect.p1.x-5,(int)yd,rect.p1.x,(int)yd);
    Float4 ll = nint(l*prec)/prec;
    TChaine leg(ll);
    int lleg = font.LongueurTexte(leg);
    font.Draw(GO,rect.p1.x-10-lleg,(int)yd+5,leg);
  }  
  // dessin origine
  l=0;
  Float4 yh = Ord2Unit(l); 
  Float4 xh=0,xd,yd;
  Hier2Win(xh,yh,xd,yd);
  GO.DrawLine(rect.p1.x-5,(int)yd,rect.p1.x+5,(int)yd);
  Float4 ll = nint(l*prec)/prec;
  TChaine leg(ll);
  int lleg = font.LongueurTexte(leg);
  font.Draw(GO,rect.p1.x-10-lleg,(int)yd+5,leg);
#endif
}
//=============================================================



////////////////////////////////////////////////////////////////////
template <class SxS>
void DendrogramBrowser<SxS>::Win2Hier ( Float4 x, Float4 y, Float4& hx, Float4& hy )
{
  hx = (Float4)xmin + ((Float4)(x-x0w))/xfac;
  hy = (Float4)ymax - ((Float4)(y-y0w))/yfac;
}
////////////////////////////////////////////////////////////////////
template <class SxS>
void DendrogramBrowser<SxS>::Hier2Win ( Float4 x, Float4 y, Float4& hx, Float4& hy )
{
  hx = (Float4)x0w + xfac*(x-xmin);
  hy = (Float4)y0w + yfac*(ymax-y); 
}
////////////////////////////////////////////////////////////////////
template <class SxS>
void DendrogramBrowser<SxS>::DrawingPosition ( const Node& n, Float4& x, Float4& y )
{
  Float4 xx = (Float4)n.val().x;
  Float4 yy = (Float4)n.val().y;
  x = (Float4)x0w + xfac*(xx-xmin);
  y = (Float4)y0w + yfac*(ymax-yy); 
}
////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////
template <class SxS>
void DendrogramBrowser<SxS>::InvDrawingPosition ( Float4 x, Float4 y, Node& n )
{
  NodeIt nit,enit=endNodes();
  Float8 dmin = 1e30;
  for (nit=Nodes();nit!=enit;++nit) {
    Float4 xx,yy;
    DrawingPosition( (*nit), xx, yy );
    Float4 d = lgl::SQR(xx-x) + lgl::SQR(yy-y);
    if (d<dmin) {
      dmin = d;
      n = *nit;
    }
  }
}
////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////
/// Calcule les emprises _xmin,_xmax en coordonnees hierarchie 
/// de la representation de chaque hierarchie partielle issue des noeuds
/// Pre : les positions doivent avoir ete calculees par une des methodes FindPositions*
template <class SxS>
void DendrogramBrowser<SxS>::FindEmprisesPartielles()
{
  typename TList<Node>::iterator lit,elit=Order.end();
  for (lit=Order.begin();lit!=elit;++lit) {
    (*lit).val()._xmin = (*lit).val()._xmax = (Int4)(*lit).val().x;
    IOEdgeIt eit,eeit=(*lit).endOut();
    for (eit=(*lit).Out();eit!=eeit;++eit) {
      Int4 sxmin = (Int4)(*eit).To().val()._xmin; 
      Int4 sxmax = (Int4)(*eit).To().val()._xmax; 
      if ( sxmin < (*lit).val()._xmin )  (*lit).val()._xmin = sxmin;
      if ( sxmax > (*lit).val()._xmax )  (*lit).val()._xmax = sxmax;
    }
  }
}
////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////
/// Classe de "pallier" pour dessin coupe
class _Pallier
{
 public:
  Float4 _x0,_x1,_y;
  _Pallier( Float4 x0=0, Float4 x1=0, Float4 y=0 ) : _x0(x0), _x1(x1), _y(y) {}
  // tri sur _x0
  bool operator < ( const _Pallier& p ) const { return (_x0 < p._x0); }  
  bool operator > ( const _Pallier& p ) const { return (_x0 > p._x0); }  
}; 
////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////
inline Nostream& operator << (Nostream& o, const _Pallier& p ) 
{
  o<<"["<<p._x0<<","<<p._x1<<","<<p._y<<"]";
  return o;
}
////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////
/// Dessine une Lambda-coupe
template <class SxS>
void DendrogramBrowser<SxS>::DrawLambdaCut ( GraphicsOut& f, Float4& Lambda )
{
  // Trouve la coupe
  PILE<Node> Cut;
  Float8 CutCost;
  LambdaCut( Lambda, Cut, CutCost );
  // Calcule les palliers
  PILE<_Pallier> Pal(Cut.size());
  Int4 i;
  for (i=0;i<Cut.size();i++) {
    Float4 x0,x1,yn;
    Hier2Win ( Cut[i].val()._xmin, Cut[i].val().y, x0, yn );
    Hier2Win ( Cut[i].val()._xmax, Cut[i].val().y, x1, yn );
    // Interpole lineairement y avec position du pere
    Float4 y = yn-5;
    if ( Cut[i].InDegree()>0 ) {
      Node Fat = (*Cut[i].In()).To();
      Float4 ln = Cut[i].val().Apparition();
      Float4 lf = Fat.val().Apparition();
      Float4 xf,yf;
      Hier2Win ( Fat.val().x, Fat.val().y, xf, yf );
      y = yn + (yf-yn)*(Lambda-ln)/(lf-ln);
    }
    //
    Pal << _Pallier( x0, x1, y ); 
  }
  // Les trie
  QuickSort(Pal);
  // Les dessine
  f.DrawLine( (int)nint(Pal[0]._x0) , (int)nint(Pal[0]._y), 
	      (int)nint(Pal[0]._x1) , (int)nint(Pal[0]._y) );
  for (i=1;i<Pal.size();i++) {
    // interpole
    DrawCosinus ( f, 
		  (int)nint(Pal[i-1]._x1), (int)nint(Pal[i-1]._y), 
		  (int)nint(Pal[i]._x0), (int)nint(Pal[i]._y) );
    f.DrawLine( (int)nint(Pal[i]._x0), (int)nint(Pal[i]._y), 
		(int)nint(Pal[i]._x1), (int)nint(Pal[i]._y) );
  }
} 
////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////
/// Dessine un cosinus d'interpolation entre deux palliers (x0,y0)->(x1,y1)
/// en coord fenetre
template <class SxS>
void DendrogramBrowser<SxS>::DrawCosinus ( GraphicsOut& f, Int4 x0, Int4 y0, Int4 x1, Int4 y1 )
{
  Int4 i;
  Int4 dx = x1-x0;
  Int4 dy = y1-y0;
  Int4 x = x0;
  Int4 y = y0;
  for (i=0;i<dx;i++) {
    Int4 ny = y0 + (Int4)( (Float8)dy*(1.-cos( 3.14159*(x+1-x0)/(Float8)dx))/2. );
    f.DrawLine(x,y,x+1,ny);
    x++;
    y = ny;
  }
}
////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////
/// Dessine une coupe quelconque 
/// ratio donne le lieu de coupe entre apparition et disparition de chaque noeud
template <class SxS>
void DendrogramBrowser<SxS>::DrawCut ( GraphicsOut& f, const PILE<Node>& Cut, float ratio)
{
  // Calcule les palliers
  PILE<_Pallier> Pal(Cut.size());
  Int4 i;
  for (i=0;i<Cut.size();i++) {
    Float4 x0,x1,yn;
    Hier2Win ( Cut[i].val()._xmin, Cut[i].val().y, x0, yn );
    Hier2Win ( Cut[i].val()._xmax, Cut[i].val().y, x1, yn );
    // Si apex
    Float4 y = yn-5;
    // Sinon interpole lineairement y avec position du pere
    if ( Cut[i].InDegree()>0 ) {
      Node Fat = (*Cut[i].In()).To();
      Float4 xf,yf;
      Hier2Win ( Fat.val().x, Fat.val().y, xf, yf );
      y = yn + (yf-yn)*ratio;
    }
    //
    Pal << _Pallier( x0, x1, y ); 
  }
  // Les trie
  QuickSort(Pal);
  // Les dessine
  f.DrawLine( (int)nint(Pal[0]._x0) , (int)nint(Pal[0]._y), 
	      (int)nint(Pal[0]._x1) , (int)nint(Pal[0]._y) );
  for (i=1;i<Pal.size();i++) {
    // interpole
    DrawCosinus ( f, 
		  (int)nint(Pal[i-1]._x1), (int)nint(Pal[i-1]._y), 
		  (int)nint(Pal[i]._x0), (int)nint(Pal[i]._y) );
    f.DrawLine( (int)nint(Pal[i]._x0), (int)nint(Pal[i]._y), 
		(int)nint(Pal[i]._x1), (int)nint(Pal[i]._y) );
  }
}
//////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////
/// Dessine une coupe quelconque 
/// ratio donne le lieu de coupe entre apparition et disparition de chaque noeud
template <class SxS>
void DendrogramBrowser<SxS>::drawCut ( GraphicsOut& f, const PILE<Node>& Cut, const PILE<float>& ratio)
{
  // Calcule les palliers
  PILE<_Pallier> Pal(Cut.size());
  Int4 i;
  for (i=0;i<Cut.size();i++) {
    Float4 x0,x1,yn;
    Hier2Win ( Cut[i].val()._xmin, Cut[i].val().y, x0, yn );
    Hier2Win ( Cut[i].val()._xmax, Cut[i].val().y, x1, yn );
    // Si apex
    Float4 y = yn-5;
    // Sinon interpole lineairement y avec position du pere
    if ( Cut[i].InDegree()>0 ) {
      Node Fat = (*Cut[i].In()).To();
      Float4 xf,yf;
      Hier2Win ( Fat.val().x, Fat.val().y, xf, yf );
      y = yn + (yf-yn)*ratio[i];
    }
    //
    Pal << _Pallier( x0, x1, y ); 
  }
  // Les trie
  QuickSort(Pal);
  // Les dessine
  f.DrawLine( (int)nint(Pal[0]._x0) , (int)nint(Pal[0]._y), 
	      (int)nint(Pal[0]._x1) , (int)nint(Pal[0]._y) );
  for (i=1;i<Pal.size();i++) {
    // interpole
    DrawCosinus ( f, 
		  (int)nint(Pal[i-1]._x1), (int)nint(Pal[i-1]._y), 
		  (int)nint(Pal[i]._x0), (int)nint(Pal[i]._y) );
    f.DrawLine( (int)nint(Pal[i]._x0), (int)nint(Pal[i]._y), 
		(int)nint(Pal[i]._x1), (int)nint(Pal[i]._y) );
  }
}
//////////////////////////////////////////////////////////////////////////////

