/* 

SxS : A C++ implementation of the scale climbing algorithm for multiscale image segmentation.
� 2004 Laurent Guigues (laurent.guigues@ign.fr)

This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.

You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
#ifndef __sxswxScaleSetsBrowser_h__
#define __sxswxScaleSetsBrowser_h__
//===========================================================
// namespace	: lgl (wx)
// class		: ScaleSetsBrowser
//===========================================================
// author		: Laurent Guigues
// history		: 
//	20/09/04	: creation
/*! \file 
	\brief A wx*-based viewer of ImageScaleSets for 2D or 3D images
*/
//===========================================================
#ifdef __SXSWX__



#include "sxsGeneral.h"
#include "sxs.h"
#include <wx/frame.h>
#include <wx/spinctrl.h>
#include <wx/splitter.h>
#include <wx/spinctrl.h>
#include <wx/log.h>
#include <wx/listctrl.h>
//#include <wx/minifram.h>
#include "../../lgl/include/lglImageRam.h"
#include "../../lgl/include/lglImageManip.h"
#include "../../lgl/include/lglImageIO.h"
#include "sxsImageScaleSets.h"
#include "sxsScaleSetsAlgorithms.h"
//#include "sxs3DViewer.h"
#include <limits>
#include <set>

namespace sxs {
class DrawCanvas;
class LeftWindow;
class PointSizeDialog;
class Move3DConfig;
class LogWindow;

//=======================================================================================
// Definition of the various algorithms used
// All ScaleClimbingUsers (SCU) inherit from N_wxProgressDialog_User in order to handle the wxProgressDialog while running
//=======================================================================================

//=======================================================================================
// Mumford-Shah piece-wise constant model

// ScaleClimbingUser
class MS_ScaleClimbing_User : 
public virtual N_wxProgressDialog_User,
public virtual E_MumfordShah_User
{};

class wxScaleSetsBrowser : public wxFrame
    {
      
    public:
      wxScaleSetsBrowser(wxFrame *parent, const wxString& title="Scale-Sets Browser" );
      ~wxScaleSetsBrowser();
      
      void OnMenuSave(wxCommandEvent& event);
	  void OnMenuOpen(wxCommandEvent& event);
      void OnMenuQuit(wxCommandEvent& event);
	  void OnMenuLog(wxCommandEvent& event);
      void OnMenuAbout(wxCommandEvent& event);

	  void OnMenu3DWindow(wxCommandEvent& event);
	  void OnMenu3DpointSize(wxCommandEvent& event); 
	  void OnMenu3DMoveConfig(wxCommandEvent& event);
	  void OnMenuSegment (wxCommandEvent& event);
	  void OnMenu3DScrollEnslavement(wxCommandEvent& event);
	  

	  void CreateScene();
	  void Display();
	 
	  LeftWindow *m_left_windows;
	  DrawCanvas *m_draw_canvas;
//	  sxs3DViewer *m_3D_Viewer;

//	  PointSet *m_points;
//	  Scene *m_scene;

	  bool m_scale_set_loaded;
	  bool p_is_3D_Window_Created;
	  bool p_scoll_slaved;
    
    protected:
      
      //=========================================================
      /// Menu
      wxMenuBar *menuBar;
      wxMenu *menuFile;
      wxMenu *menuHelp;
	  wxMenu *menu3D;
	  wxMenu* menuAlgo;
      
	  LogWindow *p_log_window;
     
	
      
      /// A pointer on the ImageScaleSets
      ImageScaleSets*		m_sxs;
      /// must the ScaleSets be deleted by the browser on closing ? 
      lgl::BOOL			m_delete_sxs;
      
      //=========================================================
      // IDs for the events
      enum
	{
	  idMenu3DEnslavement,
	  idMenuSegment,
	  idMenuOpen,
	  idMenuSave,
	  idMenuQuit,
	  idMenuLog,
	  idMenu3DWindow,
	  idMenu3DPointSize,
	  idMenu3DMoveConfig,
	  // it is important for the id corresponding to the "About" command to have
	  // this standard value as otherwise it won't be handled properly under Mac
	  // (where it is special and put into the "Apple" menu)
	  idMenuAbout = wxID_ABOUT,
	  
	  idStatusBar
	};
      
      DECLARE_EVENT_TABLE()
	};
	
//=======================================================================================
class PointSizeDialog:public wxDialog
{
public:
		PointSizeDialog(wxScaleSetsBrowser *parent);
		void OnExit(wxCloseEvent & event);
		wxSpinCtrl *m_spinctrl;
		DECLARE_EVENT_TABLE()
};

class Move3DConfig:public wxDialog
{
public:
		Move3DConfig(wxScaleSetsBrowser *parent);
		void OnExit(wxCloseEvent & event);

		wxTextCtrl *m_Eye_Rate_Entry;
		wxTextCtrl *m_COI_Rate_Entry;
		wxTextCtrl *m_Up_Rate_Entry;
		wxTextCtrl *m_Down_Rate_Entry;
		wxTextCtrl *m_Left_Rate_Entry;
		wxTextCtrl *m_Right_Rate_Entry;
		wxTextCtrl *m_Zoom_Rate_Entry;


		DECLARE_EVENT_TABLE()
};

class LogWindow : public wxLogWindow {
	public:
		LogWindow( wxFrame *parent, bool show, bool pass, const wxSize& size);
		  
		virtual bool OnFrameClose ( wxFrame* f );
};

class DrawCanvas: public wxScrolledWindow{
	public:
		DrawCanvas(wxWindow *parent, wxScaleSetsBrowser *owner );

	    void OnPaint(wxPaintEvent& ev );
        void OnMouse(wxMouseEvent& ev);
        void OnKeyPress(wxKeyEvent& ev);
		void OnScroll(wxScrollWinEvent & event);
		void FollowScroll();

	  lgl::F32 getCurrentScale();
      /// Slices the 3D image (computes the ImageRams)
  
      void getSliceImage();
      /// Slices of the base segmentation
      void getSliceBase();

      void getSliceSection();
      /// Computes the label images of the current set at current slicing point
      void getCurrentSetsBaseMapping();
      void getSliceSets();
      /// Transform the ImageRams to Bitmaps
      void getSliceBitmap();
	void getSlicePoints(bool forced=false);

lgl::PaletteLUT getPalette() {return m_palette_regions;}


      /// Makes all 
      void updateView();

      
      // Returns the label in the current section of the site s : the slice images must be up to date 
      int labelOfSite(lgl::ImageSite& s);
      
      /// Log mapping of the scales into [0,1]
      inline lgl::F64 logscale( lgl::F32 l ) { lgl::F64 nl = l/m_scale_min; if (nl<1) nl=1; return log(nl)/m_logscale_max; }
      /// Inverse mapping
      inline lgl::F64 invlogscale( lgl::F64 s ) { return m_scale_min * exp ( s * m_logscale_max ); }
      
      
      /// Determines what must be recomputed
      void resolveChanges();
		
      
		
       //==================================================
      /// A pointer on the ImageScaleSets
      ImageScaleSets*		m_sxs;
      /// must the ScaleSets be deleted by the browser on closing ? 
      lgl::BOOL			m_delete_sxs;
      /// The maximum scale (scale of the root)
      //lgl::F32			m_scale_max;
	  lgl::F64			m_scale_max;
      /// The minimum scale (for log scale) 
      //lgl::F32			m_scale_min;
	  lgl::F64			m_scale_min;
      /// The maximum value of logscale
      //lgl::F32			m_logscale_max;
	  lgl::F64			m_logscale_max;
      /// The current scale
      //lgl::F32			m_scale;
	  lgl::F64			m_scale;
      
      /// Input image parameters
      /// A pointer on the image;
      lgl::ImageRam*		m_image;
  
      /// Min/max value of the image
      double				m_value_min, m_value_max;
      /// The image size
      lgl::ImageSite		m_isize;
      /// The view of the image size (=m_isize*m_zoom)
      lgl::ImageSite		m_vsize;
      /// The size of the image frame (containing all three slices)
      wxSize			m_image_frame_size;
      
      /// The current slice position
      lgl::ImageSite		m_slice_position;
      
      
       //=========================================================
      // COLORS
      // color map  
      // TO DO : general colormap
      lgl::PaletteMultilinear* m_color_map;
      // Contours color
      lgl::RGBpoint m_contours_color;
      // Current set color
      lgl::RGBpoint m_cur_set_color;
      // Set colors
      lgl::RGBpoint m_set_color[6];
      // Regions palette = random colors
      lgl::PaletteLUT m_palette_regions;
      //=========================================================
      /// Color maps
      wxString	m_color_maps[9];
      lgl::PaletteMultilinear::Type m_color_maps_id[9];
      //=========================================================
      
      //=========================================================
      /// View change events
      lgl::BOOL m_xslice_changed, m_scale_changed;
      lgl::BOOL m_zoom_changed, m_palette_changed;
      lgl::BOOL m_set_changed;
      /// Recomputation variables 
      lgl::BOOL m_comp_ximg;
      lgl::BOOL m_comp_xbase;
      lgl::BOOL m_comp_xsec;
      lgl::BOOL m_comp_xset;
      lgl::BOOL m_comp_xbmp;
      //=========================================================
      
      //=========================================================
      // x,y and z slices images 
      lgl::ImageRam		*m_xslice_img;
      // x,y and z slices base label images 
      lgl::ImageRam		*m_xslice_base;
      // x,y and z slices section label images 
      lgl::ImageRam		*m_xslice_lab;
      // x,y and z slices label images of the current sets (0 : background)
      lgl::ImageRam		*m_xslice_set;
      //=========================================================
      // current bitmaps (image(+contours)(+set))
      wxBitmap			m_xslice_bmp;
      //=========================================================
      
      //=========================================================
      // zoom
      int				m_zoom;
      //=========================================================
      
      
      //=========================================================
      // is the mouse in image ?
      lgl::BOOL		m_mouse_in_image;
      // Coord of the last point hitted in selection mode (right clic usually)
      int m_last_sel_x;
      int m_last_sel_y;
      //=========================================================
      
      //=========================================================
      // Variables for interactive set selection
      // Current sets selected
      std::set<ImageScaleSets::Set*> m_cur_sets;
      // Current sets base mapping
      std::map<int,int> m_cur_sets_base_map;
      // Minimum persistence of a valid set (pageup...)
      //lgl::F32 m_set_persistence;
	  lgl::F64 m_set_persistence;
      // The 3D points hit by the mouse during dragging
      //std::vector<lgl::ImageSite> m_set_image_points;
      //=========================================================
      
	  bool m_has_to_fit;
	  bool m_mouse_test;

       DECLARE_EVENT_TABLE()
    private:
		wxScaleSetsBrowser *m_owner;
		wxMemoryDC p_mem_dc;
};

class LeftWindow : public wxWindow
{
public:
	LeftWindow::LeftWindow(wxWindow *parent, DrawCanvas *draw_canvas,wxScaleSetsBrowser *owner);
	~LeftWindow();
	  lgl::BOOL InitBrowser();
      void InitInterface();
      void Fit();

     void OnImageStyleChange(wxCommandEvent& event);
      void OnZoomChange(wxCommandEvent& event);

      void OnSectionStyleChange(wxCommandEvent& event);
      void OnSectionColorChange(wxCommandEvent& event);
      
      void OnScaleSlider(wxCommandEvent& event);
      void OnScaleCtrl(wxCommandEvent& event);
      void OnScaleChange();
      
      void OnSetDrawCheck(wxCommandEvent& event);
      void OnSetColorChange(wxCommandEvent& event);
      void OnSetPersistenceCtrl(wxCommandEvent& event);
      
        /// Image display style
      wxRadioBox *m_image_style_radio;

	   /// Section display style
      wxRadioBox *m_section_style_radio;

	  /// Draw ?
      wxCheckBox *m_set_draw_check;

	  //=========================================================
      /// Widgets			
      // top level sizer
      wxBoxSizer *topsizer;		

      /// zoom
      wxComboBox *m_zoom_combo;
   
      /// Section color 
      wxComboBox *m_section_color_combo;   
   
      /// The scale slider 
      wxSlider *m_scale_slider;
      /// The scale text control
      wxTextCtrl *m_scale_ctrl;

	  DrawCanvas *m_draw_canvas;
	  wxScaleSetsBrowser *m_owner;
      
      /// Set 
      
      /// Color
      wxComboBox *m_set_color_combo;
      /// Minimum persistence
      wxTextCtrl *m_set_persistence_ctrl; 

	  wxListCtrl *m_object_list;

	  enum{
	    idObjectList,
		idImageStyleRadio,
		idZoomCombo,
	  
		idSectionStyleRadio,
		idSectionColorCombo,
	  
		idScaleSlider,
		idScaleCtrl,
	  
		idSetDrawCheck,
		idSetColorCombo,
		idSetPersistenceCtrl
	  };
	   DECLARE_EVENT_TABLE()
};
  
};
//==========================================================================================
// EO namespace sxs
//==========================================================================================


//==========================================================================================
// EOF
//==========================================================================================
#endif
#endif



