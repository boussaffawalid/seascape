/* 

SxS : A C++ implementation of the scale climbing algorithm for multiscale image segmentation.
� 2004 Laurent Guigues (laurent.guigues@ign.fr)

This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.

You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
//===========================================================
/*! \file 
	\brief Includes all the headers of the sxs library 
*/
//===========================================================
// author		: Laurent Guigues
// history		: 
//	12/07/04		creation
//===========================================================
#include "sxsGeneral.h"

#include "sxsC_BoundaryMeasure.h"
#include "sxsC_Constant.h"
#include "sxsC_PolygonalBoundary.h"
#include "sxsD_Covariance.h"
#include "sxsD_ImageCovariance.h"
#include "sxsE_MumfordShah.h"
#include "sxsE_PureImageCovariance.h"
#include "sxsE_PolygonalBoundaryImageCovariance.h"
#include "sxsImageScaleClimbing.h"
#include "sxsImageScaleClimbingUser.h"
#include "sxsImageScaleSets.h"
#include "sxsN_Verbose_User.h"
#include "sxsN_wxProgressDialog_User.h"
#include "sxsPiecewiseAffineFunction.h"
#include "sxsScaleClimbing.h"
#include "sxsScaleClimbingGraph.h"
#include "sxsScaleClimbingUser.h"
#include "sxsScaleSets.h"
#include "sxsScaleSetsAlgorithms.h"
#include "sxsVersion.h"
#include "sxswxScaleSetsBrowser.h"
