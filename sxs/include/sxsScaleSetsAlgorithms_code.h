/* 

SxS : A C++ implementation of the scale climbing algorithm for multiscale image segmentation.
� 2004 Laurent Guigues (laurent.guigues@ign.fr)

This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.

You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
/*! \file 
	\brief Code of the template methods declared in sxsScaleSetsAlgorithms. Included from sxsScaleSetsAlgorithms.h
*/

//===========================================================
/// Pushes back in the container C the elements of the 
/// ScaleSets S which constitute its section (horizontal cut) of scale scale.
/// C must be a container of ScaleSets<T>::pSet (pointers on Sets).
template <class pSetContainer> 
void getSection ( const ScaleSets& S, lgl::F32 scale, pSetContainer& C);
//===========================================================




//===========================================================
/// Pushes back in the container out the pointers on the Sets 
/// which constitute the base of the Set* in
template <class pSetContainer>
void getBase ( pSet in, pSetContainer& out)
{
	std::list<pSet> Q;
	Q.push_back(in);
	while (Q.size()>0) {
		ScaleSets::Set* s = Q.front();
		Q.pop_front();
		if (s->isBase()) {
			out.push_back(s);
		}
		else {
			std::vector<ScaleSets::Set*>::iterator i;
			for (i=s->sons().begin();i!=s->sons().end();++i) Q.push_back(*i);
		}
	}
}
//===========================================================


//===========================================================
/// Pushes back in the container out the elements of the ScaleSets S 
/// which constitute the base of the elements of container in
template <class pSetContainer1, class pSetContainer2>
void getBase ( const ScaleSets& S, const pSetContainer1& in, pSetContainer2& out)
{
	typename pSetContainer1::const_iterator i;
	for (i=in.begin();i!=in.end();++i) getBase(*i,out);
}
//===========================================================




//===========================================================
/// Returns the supremum of the Sets of the container in
template <class pSetContainer>
void getSupremum ( ScaleSets& S, const pSetContainer& in, pSet& sup)
{
	ScaleSets::Slot branch;
	if (!S.allocateSlot(branch,1)) {
		sxsERROR("getSupremum : unable to allocate a 1 bit Slot on the ScaleSets !");
	}
	typename pSetContainer::const_iterator i = in.begin();
	// marks first branch
	pSet cur = *i;
	while (cur->getMark(branch)==0) {
		cur->setMark(branch,1);
		cur = cur->father();
	}
	sup = *i;
	// climbs other branches
	for (++i;i!=in.end();++i) {
		cur = *i;
		while (cur->getMark(branch)==0) {
			cur->setMark(branch,1);
			cur = cur->father();
		}
		// meeting point upper than current sup ?
		if (sup->scaleOfAppearance()<cur->scaleOfAppearance()) sup = cur;
	}
	// Unmarks the tree 
	for (i=in.begin();i!=in.end();++i) {
		cur = *i;
		while (cur->getMark(branch)==1) {
			cur->setMark(branch,0);
			cur = cur->father();
		}
	}
	// frees the Slot
	S.freeSlot(branch);
}
//===========================================================




//===========================================================
/// Maps the elements of the pSetContainer to their maximal ancestors 
/// whose scale is lower than scale
//template <class pSetContainer>
//void getMaximalAncestors ( const pSetContainer& in, lgl::F32 scale, std::map<ScaleSets::Set*,ScaleSets::Set*>& out);
//===========================================================


//===========================================================
/// Maps the elements of the pSetContainer to their maximal ancestors 
/// whose scale is lower than scale
template <class pSetContainer>
void getMaximalAncestors ( const pSetContainer& in, lgl::F32 scale, std::map<ScaleSets::Set*,ScaleSets::Set*>& out)
{
	typename pSetContainer::const_iterator i = in.begin();

	for (;i!=in.end();++i) {
		pSet cur = *i;
		if (cur->scaleOfAppearance()<scale)
		{
			while ((!cur->isRoot())&&(cur->father()->scaleOfAppearance()<scale)) { cur = cur->father(); }
			typename std::map<ScaleSets::Set*,ScaleSets::Set*>::iterator it = out.find(cur);
			if (it == out.end()) out.insert(std::pair<ScaleSets::Set*,ScaleSets::Set*> (cur,(*i)));
		}
		else
		{
			std::vector<ScaleSets::Set*> psc = cur->sons();
			for (unsigned int ak=0;ak<psc.size();++ak)
			{
				if (psc[ak]->scaleOfAppearance()<scale)
				{
					typename std::map<ScaleSets::Set*,ScaleSets::Set*>::iterator it = out.find(psc[ak]);
					if (it == out.end()) out.insert(std::pair<ScaleSets::Set*,ScaleSets::Set*> (psc[ak],(*i)));
				}
				else
				{
					for(unsigned int bk = 0;bk<psc[ak]->sons().size();++bk)
						psc.push_back(psc[ak]->sons()[bk]);
				}
			}
		}
	}

}












//===========================================================
/// Transforms the label image in (whose labels correspond to 
/// Sets index in the ScaleSets S) into the label image corresponding to the 
/// indices of the maximal ancestors of the initial labels.
template <class T>
void getMaximalSection ( const lgl::ImageRamT<T>& in, ScaleSets& S, lgl::F32 scale, lgl::ImageRamT<T>& out)
{
	lglLOG("getMaxSection "<<ENDL);
	// maps in labels (indices) to the section sets indices
	std::map<int,int> lmap;
	// marks indices already seen
	typename ScaleSets::Slot seen;
	if (!S.allocateSlot ( seen )) {
		lglERROR("getMaximalSection(image_in,sxs,scale,image_out) : unable to allocate a 1 bit Slot on the ScaleSets !");
		return;
	}
	//
	typename lgl::ImageRamT<T>::const_iterator init;
	for (init=in.begin();init!=in.end();++init) {
		typename ScaleSets::Set* b = S[*init];
		if (!b) {
			lglERROR("getMaximalSection(image_in,sxs,scale,image_out) : incoherence between label image and ScaleSets indices");
		}
		// already seen
		if (b->getMark(seen)==1) {
			if ((*(lmap.find(*init))).first!=*init) lglLOG("Label "<<*init<<" seen but not mapped !!"<<std::endl);
			continue;
		}
		b->setMark(seen,1);
		// index of its maximal ancestor of lower scale than scale
		int lmax = b->getMaximalAncestor(scale)->index();
		// map insertion
		lmap.insert(std::pair<T,T>(*init,lmax)); 
	}
	lglLOG("mapping ok "<<ENDL);
	// unmarks the mapped elements 
	typename std::map<T,T>::const_iterator mit;
	std::set<T> sec;
	for (mit=lmap.begin();mit!=lmap.end();++mit) {
		S[(*mit).first]->setMark(seen,0);
		sec.insert((*mit).second);
	}
	// frees the Slot seen
	S.freeSlot ( seen );
	// image out construction
	out = in;
	typename lgl::ImageRamT<T>::iterator outit;
	for (init=in.begin(),outit=out.begin();init!=in.end();++init,++outit) {
		*outit = (*(lmap.find(*init))).second;
	}
}
//===========================================================


