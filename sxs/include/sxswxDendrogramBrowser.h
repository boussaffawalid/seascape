/* 

SxS : A C++ implementation of the scale climbing algorithm for multiscale image segmentation.
� 2004 Laurent Guigues (laurent.guigues@ign.fr)

This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.

You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
#ifndef __sxswxDendrogramBrowser_h__
#define __sxswxDendrogramBrowser_h__
//===========================================================
// namespace	: sxs (wx)
// class		: DendrogramViewer
//===========================================================
// author		: Laurent Guigues
// history		: 
//	20/09/04	: creation
/*! \file 
\brief A wxFrame drawing a ScaleSets as a dendrogram and allowing various interactions
*/
//===========================================================
#ifdef __SXSWX__

#include "sxsGeneral.h"
#include <wx/spinctrl.h>
#include "lglRamImage.h"
#include "lglImageManip.h"
#include "lglImageIO.h"
#include <limits>

namespace sxs {
	
	
	//=====================================================================================
	/// \brief A listener of the events occuring in a DendrogramBrowser
	//=====================================================================================
	class DendrogramBrowserListener
	{
	public:
		virtual void OnSetChange(pSet set) {}
		virtual void OnSectionChange(F16b scale) {}
	};
	//=====================================================================================
	
	//=====================================================================================
	/// \brief A wxFrame drawing a ScaleSets as a dendrogram and allowing various interactions
	lass DendrogramBrowser : public wxFrame 
	{
		public:
			DendrogramBrowser ( wxFrame *parent, const wxString& title, ScaleSets& scale_sets, DendrogramBrowserListener& listen );
			
			/// \brief Initializes the browser on the ScaleSets s
			///
			/// Computes all necessary (LevelsAndSizes, X, Y);
			void setScaleSets( ScaleSets& scale_sets );
			
			
			/// \brief Performs a Bottom-Up traversal of the hierarchy in order to initialize the levels, the size and xmin/xmax of the Sets.
			void bottomUpInitialization();
			
			/// \brief Computes the abcissa of the Sets in order to obtain a planar representation. 
			/// 
			/// Computation made by : 
			///  - a Depth First Search Top-Down propagation for computation of the abcissa of the base
			///  - a Hierarchical Order Bottom-Up propagation for the other sets
			/// The Top-Down traversal depends on an ordering function on the sons of a Set. 
			/// Here, the order is the scale of appearance. 
			/// The X assignment during the Bottom-Up traversal is made at (xmax-xmin)/2
			void computeX();

			/// \brief Assigns y-coordinates to the sets. 
			/// 
			/// y are normalized between 0 (base sets) and 1 (root)
			/// \param mode :
			///		0 : y prop. to the levels
			///		1 : y prop. to the scales of appearance
			void computeY(int mode=0);

			/// \brief Assigns a scaling style for the y (i.e. a mapping from .  
			/// Mapping des ordonnees vers [0,1] et reciproquement
			/// SetScaling fixe une fonction de mapping de parametre s
			/// mode / s
			/// 0 : lineaire / s : N/A
			/// 1 : log(sx) si sx>=1; 0 sinon (ecrase le bas de l'echelle) 
			/// 2 : e(-sx)    
			void setScaling( int mode, float s ); 
			/// 
			float Ord2Unit( float a );
			float Unit2Ord( float a );
			/// Calcule les parametres de dessin dans un rectangle donne
			/// Si recalcul = true, recalcule l'ordre, les niveaux et positions
			void fit( const TRectangle<int>& R, bool recalcul = true );

			// Passage des coordonnees fenetre aux coord hierarchie
			void gc2sxs ( Float4 x, Float4 y, Float4& hx, Float4& hy );
			// Passage des coordonnees fenetre aux coord hierarchie
			void sxs2gc ( Float4 x, Float4 y, Float4& hx, Float4& hy );
			/// Calcule la position d'un noeud dans le dessin courant
			void drawingPosition ( const Node& n, Float4& x, Float4& y );
			/// Calcule le noeud du graphe le plus proche d'un point du dessin courant
			void invDrawingPosition ( Float4 x, Float4 y, Node& n );
			
			/// Calcule les emprises _xmin,_xmax en coordonnees hierarchie 
			/// de la representation de chaque hierarchie partielle issue des noeuds
			/// Pre : les positions doivent avoir ete calculees par une des methodes FindPositions*
			void findPartial();
			
			/// \name Drawing methods
			/// 
			/// \{
			/// Dessine la hierarchie en dendogramme pour les param de dessin courants
			/// dans une fenetre
			void draw( WXInterface::IdFen& f );
			
			/// Dessine la hierarchie en dendogramme pour les param de dessin courants
			/// dans un GraphicsOut
			void drawDendrogram( wxGC& gc& f,
				bool draw_rect = false, 
				bool draw_nodes = false );
			/// Dessine les graduations de l'axe y
			/// nstep : nombre de graduations (si==0 : pas entiers (pour mapping niveaux)) 
			/// fsize : taille de fonte
			void drawGraduations( wxGC& gc, int nstep=0, int fsize=12);
			
			/// Dessine une Lambda-coupe
			/// Pre : FindEmprisesPartielles
			void drawSection ( wxGC& gc f, Float4& Lambda ); 
			/// Dessine un cosinus d'interpolation entre deux palliers (x0,y0)->(x1,y1)
			/// en coord fenetre
			void drawCosinus ( wxGC& gc, Int4 x0, Int4 y0, Int4 x1, Int4 y1 );
			/// Dessine une famille de lambda coupes
			void drawSections ( );
			/// Dessine une coupe quelconque 
			/// ratio donne le lieu de coupe entre apparition et disparition de chaque noeud
			void drawCut ( wxGC& gc const PILE<Node>& Cut, float ratio = 0.5);
			/// Dessine une coupe quelconque 
			/// ratio donne le lieu de coupe entre apparition et disparition de chaque noeud
			/// Different pour chaque noeud
			void drawCut ( wxGC& gc, const PILE<Node>& Cut, const PILE<float>& ratio);
			//////////////////////////////////////////////////////////////////////////////
			/// \}

		protected:
			ScaleSets* m_sxs;

			// the data on each set needed for drawing
			typedef struct {
				// the set's coordinates
				F16 x,y;
				// level in the hierarchy (l(base)=0; l(x)=1+max{l(y)|y in sons(x)} ) 
				UI16b l;
				// size 
				UI16b size;
				// x min/ x max for recursive sort
				F16b xmin,xmax;
			}
			set_data;
			// The data on the sets, the indices match those of the list m_list of the ScaleSets
			// (remember that the field m_self_position of a Set is an iterator on the Set's position in this list)
			std::vector<set_data> m_data;
			// 

			 // Routine de collecte recursive pour FindPositions
     void _FP_rec_D1st(Node n);
     // Routine de collecte recursive pour FindPositions2
     void _FP2_rec_D1st(Node n);
     // Routine de collecte recursive pour FindPositions3
     void _FP3_rec_D1st(Node n);
     // Stockage de position courante
     Int4 _FP_x;
	 // 
     // Hauteur de la hierarchie
     Int4 hauteur;
     // Parametres de dessin courant
     // origine du dessin dans la fenetre (x0w,y0w)
     // emprise de la hierarchie (xmin,ymin)(xmax,ymax)
     // facteurs d'echelle xfac,yfac
     Float8 x0w,y0w,xmin,xmax,ymin,ymax,xfac,yfac;
     // rectangle de dessin
     TRectangle<Int4> rect;
     // Ordonnee min/max
     float _minord,_maxord;
     // mapping y <-> [0,1]
     int _ymap_mode;
     float _ymap_coef;
	};
	//==========================================================================================
	// EO DendrogramBrowser
	//==========================================================================================
	
	
};
//==========================================================================================
// EO namespace sxs
//==========================================================================================


//==========================================================================================
// EOF
//==========================================================================================
#endif
#endif
