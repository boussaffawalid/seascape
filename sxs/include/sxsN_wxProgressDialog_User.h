/* 

SxS : A C++ implementation of the scale climbing algorithm for multiscale image segmentation.
� 2004 Laurent Guigues (laurent.guigues@ign.fr)

This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.

You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
#ifndef __sxsN_wxProgressDialog_User_h__
#define __sxsN_wxProgressDialog_User_h__
//===========================================================
// namespace	: sxs (wx)
// class		: N_wxProgressDialog_User
/*! \file 
	\brief Descendant of ImageScaleClimbingUser which overloads the notification callbacks in order to update a wxProgressDialog during the algorithm. 
*/
//===========================================================
// author		: Laurent Guigues
// history		: 
//	22/09/04		creation
//===========================================================
#ifdef __SXSWX__
#include "sxsGeneral.h"
#include "sxsImageScaleClimbingUser.h"
#include "wx/progdlg.h"
#include <cmath>

namespace sxs 
{


//==================================================================================
// WARNINGS/ERRORS
//==================================================================================
#if defined(__SXS_PDSCU_COMPILE_ERROR__)
//==================================================================================
/// Displays a warning message
#define sxswxPDSCU_WARNING(A) { wxLogWarning("[sxs]" << A ); }
/// Displays an error message
#define sxswxPDSCU_ERROR(A) { wxLogError( "[sxs]" << A ); }
//==================================================================================
#else
//==================================================================================
#define sxswxPDSCU_ERROR(A) 
#define sxswxPDSCU_WARNING(A) 
//==================================================================================
#endif
//==================================================================================

  //===========================================================
  ///  \brief Descendant of ImageScaleClimbingUser which overloads the notification callbacks in order to update a wxProgressDialog during the algorithm. 
  class N_wxProgressDialog_User : public virtual ImageScaleClimbingUser
    {
    public:
      
      //===========================================================
      /// ctor
      N_wxProgressDialog_User() : m_pd(0), m_max(100), m_continue(true) {}
      /// dtor
      virtual ~N_wxProgressDialog_User() {}
      //===========================================================
      
      //===========================================================
      void setProgressDialog ( wxProgressDialog* p, int maximum ) { m_pd = p; m_max = maximum; }
      //===========================================================
      
      
      //===========================================================
      // Ponctual callbacks notifying the current algorithm state, progress...
      //===========================================================
      /// Called back before the algorithm runs (beginning of run())
      virtual void SCCB_N_preRun() { 
	m_watch.Start();
	  }
      /// Called back when the algorithm has finished (end of run())
      virtual void SCCB_N_postRun() { 
	if (m_pd) m_pd->Update(m_max);	
	m_pd = 0;
	lglLOG( "Total time = "<<m_watch.Time()<<" ms"<<ENDL);
      }
      /// Called back before the computation base segmentation (beginning of computeBaseSegmentation())
      virtual void SCCB_N_preComputeBaseSegmentationNotification() {
	if (m_pd) m_pd->Update(m_max/2,_T("Base segmention"));
      }
      /// Called back after the computation base segmentation (end of computeBaseSegmentation())
      virtual void SCCB_N_postComputeBaseSegmentationNotification() {
      }	
      /// Called back before the graph contruction (beginning of buildGraph())
      virtual void SCCB_N_preBuildGraphNotification() {
	if (m_pd) m_pd->Update(0,_T("Building Graph"));
      }
      /// Called back after the graph contruction (end of buildGraph())
      virtual void SCCB_N_postBuildGraphNotification() {
      }		
      /// Called back before the ScaleSets's Base contruction (beginning of buildBase())
      virtual void SCCB_N_preBuildBaseNotification() { 
	if (m_pd) m_pd->Update(0,_T("Building Base"));
      } 
      /// Called back after the ScaleSets's Base contruction (end of buildBase())
      virtual void SCCB_N_postBuildBaseNotification() { 
      } 
      /// Called back before the Heap construction (beginning of buildHeap())
      virtual void SCCB_N_preBuildHeapNotification() { 
	if (m_pd) m_pd->Update(0,_T("Building Heap"));
      }
      /// Called back after the Heap construction (end of buildHeap())
      virtual void SCCB_N_postBuildHeapNotification() { 
      }
      /// Called back before climbing starts (beginning of climb())
      virtual void SCCB_N_preClimbingNotification() { 
	if (m_pd) m_pd->Update(0,_T("Scale Climbing"));
      }
      /// Called back after climbing (end of climb())
      virtual void SCCB_N_postClimbingNotification() { 
      }
      /// Called back before post processing starts (beginning of postProcess())
      virtual void SCCB_N_prePostprocessingNotification() { 
	if (m_pd) m_pd->Update(m_max/2,_T("Post Treatment"));
      }	
      /// Called back after post processing starts (end of postProcess())
      virtual void SCCB_N_postPostprocessingNotification() { 
      }	
      /// Called back before each ScaleClimbing step (beginning of step())
      virtual void SCCB_N_preStepNotification() { }
      /// Called back after each ScaleClimbing step (end of step())
      virtual void SCCB_N_postStepNotification() { } 
      /// Called back at the beginning of each functionalDynamicProgrammingStep(e) 
      virtual void SCCB_N_preFDP(Edge e) { }
      /// Called back at the end of each functionalDynamicProgrammingStep(e) 
      virtual void SCCB_N_postFDP(Edge e) { }
      //===========================================================
      /// Gives a progress indicator for the current task 
      virtual bool SCCB_N_progressIndicator( int stage, int number_of_stages) 
	{
	  if (m_pd) {
		  if(number_of_stages<m_max) return true; // by AAE, avoid dividing by 0.
	    if (stage % ( number_of_stages / m_max ) == 0) {
	      return m_pd->Update(stage*(m_max-1)/number_of_stages);
	    }
	    else return true; 
	  }
	  return true;
	}
      //===========================================================
      
      
      //===========================================================
      // Warning/Error callbacks
      //===========================================================
      /// Warning : algorithm can continue but might give strange results.
      /// If the CB returns TRUE then continues else stops. 
      virtual lgl::BOOL SCCB_warning(int number, lgl::String& description) { 
	sxswxPDSCU_WARNING ( description )  
	  return true; 
      }
      /// Fatal error : algorithm cannot continue (in fact stops just after). 
      virtual void SCCB_error(const lgl::String& description) { 
	sxswxPDSCU_ERROR ( description )  
	  }
      //===========================================================
      
    protected:
      wxProgressDialog* m_pd;
      int m_max;
      bool m_continue;
      wxStopWatch m_watch;
      
    };
  //===========================================================
  // EO class N_wxProgressDialog_User
  //===========================================================
  
	

}
//===========================================================
// EO namespace sxs
//===========================================================

//===========================================================
// EOF
//===========================================================
#endif
#endif
