/* 

lGl : A C++ library needed by the multiscale image segmentation library SxS
� 2004 Laurent Guigues (laurent.guigues@ign.fr)

This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.

You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
/**	\file	
	\brief A 3D image viewer based on a wxFrame
**/
#ifndef __lglwxImage3DViewer_h__
#define __lglwxImage3DViewer_h__
//===========================================================
// namespace	: lgl (wx)
// class		: Image3DViewer
//===========================================================
// author		: Laurent Guigues
// history		: 
//	01/09/04	: creation
//===========================================================
#ifdef __LGLWX__
#include <wx/spinctrl.h>
#include "lglImageRam.h"
#include "lglImageManip.h"
#include "lglImageIO.h"
#include "lglType.h"
#include <limits>

namespace lgl {


/*
	//=====================================================================================
	/// A 3D image viewer based on a wxFrame
	template<class T> class Image3DViewer : public wxFrame 
	{
		
		public:
			Image3DViewer(wxFrame *parent, const wxString& title="3D Image Viewer");
			Image3DViewer(wxFrame *parent, const wxString& title, const ImageRam& image);
			~Image3DViewer();

			bool InitControls();
			bool Open( const ImageRam& image );
			void CloseImage();

			void OnMenuOpen(wxCommandEvent& event);
			void OnMenuClose(wxCommandEvent& event);
			void OnMenuQuit(wxCommandEvent& event);
			void OnMenuAbout(wxCommandEvent& event);
			
			void OnXSliceCtrl(wxSpinEvent& event);
			void OnYSliceCtrl(wxSpinEvent& event);
			void OnZSliceCtrl(wxSpinEvent& event);
			void OnZoomChange(wxCommandEvent& event);
			void OnColormapChange(wxCommandEvent& event);
			void OnColormapInverseCheck(wxCommandEvent& event);
			void OnContrastMinChange(wxCommandEvent& event);
			void OnContrastMaxChange(wxCommandEvent& event);
			void OnTicksCheck(wxCommandEvent& event);
			
			void OnPaint(wxPaintEvent& ev );
			void OnMouse(wxMouseEvent& ev);

			void Fit();
			/// Slices the 3D image (computes the ImageRams)
			void getSliceImages();
			void getXSliceImage();
			void getYSliceImage();
			void getZSliceImage();
			/// Transform the ImageRams to Bitmaps
			void getSliceBitmaps();
			void getXSliceBitmap();
			void getYSliceBitmap();
			void getZSliceBitmap();
			/// Draws the slices bitmaps
			void drawSliceBitmaps();
			void drawXSliceBitmap();
			void drawYSliceBitmap();
			void drawZSliceBitmap();
			/// Makes all 
			void updateSlices();
			void updateXSlice();
			void updateYSlice();
			void updateZSlice();

			/// 
			void drawTicks();
			void drawXTicks();
			void drawYTicks();
			void drawZTicks();
			// computes the 3D position corresponding to the mouse coordinates
			// returns true iff the mouse is in a slice view
			bool mouseToSpace(wxMouseEvent& ev, ImageSite& p);

		protected:
			/// A pointer on the image currently viewed 
			const ImageRam*	m_image;
			/// Is an image opened ?
			bool			m_image_opened;
			/// must the image be deleted by the viewer ? 
			bool			m_delete_image;
			/// Min/max value of the image
			T				m_value_min, m_value_max;
			/// The image size
			ImageSite		m_isize;
			/// The view of the image size (=m_isize*m_zoom)
			ImageSite		m_vsize;
			/// The size of the image frame (containing all three slices)
			wxSize			m_image_frame_size;

			/// The current slice position
			ImageSite		m_slice_position;
			/// Draw ticks on slices ?
			bool			m_ticks_on;

			/// Menu
			wxMenuBar *menuBar;
			wxMenu *menuFile;
			wxMenu *menuHelp;
	
			
			/// Widgets
			// top level sizer
			wxBoxSizer *topsizer;		
			/// Slice coordinate spinners
			wxSpinCtrl *m_xslice_ctrl, *m_yslice_ctrl, *m_zslice_ctrl;
			/// zoom
			wxComboBox *m_zoom_combo;
			/// colormap
			wxComboBox *m_colormap_combo;
			/// colormap inverse
			wxCheckBox *m_colormap_inverse_check;
			/// Contrast adjustment sliders
			wxSlider *m_vmin_slider, *m_vmax_slider;
			/// Ticks
			wxCheckBox *m_ticks_check;
			/// Color maps
			wxString	m_color_maps[9];
			PaletteMultilinear::Type m_color_maps_id[9];

			// x,y and z slices
			ImageRamT<T>	m_xslice_img,m_yslice_img,m_zslice_img;
			wxBitmap	m_xslice_bmp,m_yslice_bmp,m_zslice_bmp;

			// color map 
			PaletteMultilinear* m_color_map;

			// zoom
			int				m_zoom;

			// position of images in window
			wxPoint		m_images_position;
			wxPoint		m_xslice_wp, m_yslice_wp, m_zslice_wp;

			// is the mouse in image ?
			bool		m_mouse_in_image;

			// IDs for the events
			enum
			{
				idMenuOpen,
				idMenuClose,
				idMenuQuit,
				// it is important for the id corresponding to the "About" command to have
				// this standard value as otherwise it won't be handled properly under Mac
				// (where it is special and put into the "Apple" menu)
				idMenuAbout = wxID_ABOUT,
				idStatusBar,
				idXSliceCtrl,
				idYSliceCtrl,
				idZSliceCtrl,
				idZoomCombo,
				idColormapCombo,
				idColormapInverseCheck,
				idVminSlider,
				idVmaxSlider,
				idTicksCheck,
				idImageFrame
			};

			DECLARE_EVENT_TABLE()
	};
	//=======================================================================================



	


	//=======================================================================================
	// EVENT TABLE 
	BEGIN_EVENT_TABLE(Image3DViewer<class T>, wxFrame ) 
		EVT_PAINT( Image3DViewer<T>::OnPaint ) 
	EVT_MENU ( idMenuOpen, Image3DViewer<T>::OnMenuOpen ) 
	EVT_MENU ( idMenuClose, Image3DViewer<T>::OnMenuClose ) 
	EVT_MENU ( idMenuQuit, Image3DViewer<T>::OnMenuQuit ) 
	EVT_MENU ( idMenuAbout, Image3DViewer<T>::OnMenuAbout ) 
	EVT_SPINCTRL ( idXSliceCtrl, Image3DViewer<T>::OnXSliceCtrl ) 
	EVT_SPINCTRL ( idYSliceCtrl, Image3DViewer<T>::OnYSliceCtrl ) 
	EVT_SPINCTRL ( idZSliceCtrl, Image3DViewer<T>::OnZSliceCtrl ) 
	EVT_COMBOBOX ( idZoomCombo, Image3DViewer<T>::OnZoomChange ) 
	EVT_COMBOBOX ( idColormapCombo, Image3DViewer<T>::OnColormapChange) 
	EVT_CHECKBOX ( idColormapInverseCheck, Image3DViewer<T>::OnColormapInverseCheck ) 
	EVT_CHECKBOX ( idTicksCheck, Image3DViewer<T>::OnTicksCheck ) 
	EVT_COMMAND_SCROLL ( idVminSlider, Image3DViewer<T>::OnContrastMinChange) 
	EVT_COMMAND_SCROLL ( idVmaxSlider, Image3DViewer<T>::OnContrastMaxChange) 
	EVT_MOUSE_EVENTS( Image3DViewer<T>::OnMouse ) 
END_EVENT_TABLE() 
	//=======================================================================================
IMAGE3DVIEWER_EVENT_TABLE(unsigned char)
IMAGE3DVIEWER_EVENT_TABLE(short)
IMAGE3DVIEWER_EVENT_TABLE(int)

	//=======================================================================================

	//=======================================================================================
	template <class T>	
	Image3DViewer<T>::Image3DViewer(wxFrame *parent, const wxString& title)
		: wxFrame(parent,-1, _T(title), wxPoint(0,0), wxDefaultSize, wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxSYSTEM_MENU | wxCAPTION  ),
		m_image_opened(false)
	{
		wxLogMessage("Image3DViewer::Image3DViewer(...)");
		InitControls();
	}
	//=======================================================================================


	//=======================================================================================
	template <class T>	
		Image3DViewer<T>::Image3DViewer(wxFrame *parent, const wxString& title, const ImageRam& image)
		: wxFrame(parent,-1, _T(title), wxPoint(0,0), wxDefaultSize, wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxSYSTEM_MENU | wxCAPTION  ),
		m_image_opened(false)
	{
		wxLogMessage("Image3DViewer::Image3DViewer(Image)");
		InitControls();
		Open(image);
	}
	//=======================================================================================


	//=======================================================================================
	template <class T>	
		Image3DViewer<T>::~Image3DViewer()
	{
		CloseImage();
		delete m_color_map;
	}
	//=======================================================================================


	//=======================================================================================
	template <class T>	
		bool Image3DViewer<T>::InitControls()
	{
		wxLogMessage("Image3DViewer::InitControls()");
		m_image = NULL;
		m_isize = ImageSite(0,0,0,0);
		m_delete_image = false;

		//SetBackgroundColour(wxColour(0,0,0));
		//=========================================
		// Menu
#if wxUSE_MENUS
		// create the File menu bar
		menuFile = new wxMenu;
		menuFile->Append(idMenuOpen, _T("&Open image\tAlt-O"), _T("Opens an image file"));
		menuFile->Append(idMenuClose, _T("&Close image\tAlt-C"), _T("Closes the image"));
		menuFile->Append(idMenuQuit, _T("E&xit\tAlt-X"), _T("Quit this program"));

		// the "About" item should be in the help menu
		menuHelp = new wxMenu;
		menuHelp->Append(idMenuAbout, _T("&About...\tF1"), _T("Show about dialog"));
    
		// now append the freshly created menu to the menu bar...
		menuBar = new wxMenuBar();
		menuBar->Append(menuFile, _T("&File"));
		menuBar->Append(menuHelp, _T("&Help"));

		// ... and attach this menu bar to the frame
		SetMenuBar(menuBar);
#endif // wxUSE_MENUS

#if wxUSE_STATUSBAR
		CreateStatusBar(4);
		SetStatusText(_T("Welcome"));
#endif // wxUSE_STATUSBAR

		//=========================================
		// Controls
		// top level sizer
		topsizer = new wxBoxSizer( wxHORIZONTAL ); 
		// Menu box
		wxStaticBox* tmp = new wxStaticBox(this,-1,"");
		//tmp->SetBackgroundColour(wxColour(255,255,255));
		wxStaticBoxSizer* ctrlsizer = new wxStaticBoxSizer( tmp, wxVERTICAL );
		// Adds slices sizer to the ctrlsizer
		// Slice box 
		wxStaticBoxSizer* slicesizer = new wxStaticBoxSizer( new wxStaticBox(this,-1,"Slices"), wxVERTICAL );
		
		wxBoxSizer* slicectrl = new wxBoxSizer(wxHORIZONTAL);
		m_xslice_ctrl = new wxSpinCtrl ( this, idXSliceCtrl, wxEmptyString, wxDefaultPosition, wxSize(50,20), wxSP_ARROW_KEYS, 0, 128, 64 );
		m_yslice_ctrl = new wxSpinCtrl ( this, idYSliceCtrl, wxEmptyString, wxDefaultPosition, wxSize(50,20), wxSP_ARROW_KEYS, 0, 128, 64 );
		m_zslice_ctrl = new wxSpinCtrl ( this, idZSliceCtrl, wxEmptyString, wxDefaultPosition, wxSize(50,20), wxSP_ARROW_KEYS, 0, 128, 64 );

		slicectrl->Add( new wxStaticText(this,-1,"x"),0.5,wxALIGN_CENTER|wxLEFT,10);
		slicectrl->Add( m_xslice_ctrl,0.5,wxALIGN_CENTRE|wxLEFT,5);
		slicectrl->Add( new wxStaticText(this,-1,"y"),0.5,wxALIGN_CENTER|wxLEFT,10);
		slicectrl->Add( m_yslice_ctrl,0.5,wxALIGN_CENTRE|wxLEFT,5);
		slicectrl->Add( new wxStaticText(this,-1,"z"),0.5,wxALIGN_CENTER|wxLEFT,10);
		slicectrl->Add( m_zslice_ctrl,0.5,wxALIGN_CENTRE|wxLEFT,5);
		slicesizer->Add( slicectrl, 0.5,wxCENTER|wxBOTTOM,5) ;
		
		// Tickw
		m_ticks_check = new wxCheckBox( this, idTicksCheck, "Draw slicing lines");	
		m_ticks_check->SetValue(true);
		slicesizer->Add( m_ticks_check,0.5,wxALIGN_CENTER) ;
		
		ctrlsizer->Add( slicesizer, 0.5,wxEXPAND) ;
		
		// View box 
		wxStaticBoxSizer* viewsizer = new wxStaticBoxSizer( new wxStaticBox(this,-1,"View"), wxVERTICAL );
		// Zoom
		wxBoxSizer* zoomsizer = new wxBoxSizer( wxHORIZONTAL ); 
		wxString zchoice[4];
		zchoice[0] = "x1";
		zchoice[1] = "x2";
		zchoice[2] = "x3";
		zchoice[3] = "x4";
		m_zoom_combo = new wxComboBox( this, idZoomCombo, "", wxDefaultPosition, 
			wxDefaultSize, 4, zchoice );
		m_zoom_combo->SetSelection(1);
		zoomsizer->Add( new wxStaticText(this,-1,"Zoom"),0.5,wxALIGN_CENTER|wxLEFT,10);
		zoomsizer->Add( m_zoom_combo,0.5,wxALIGN_CENTRE|wxLEFT,5);
		viewsizer->Add( zoomsizer,0.5,wxALIGN_CENTRE|wxBOTTOM,10) ;
		// Colormap
		wxBoxSizer* cmapsizer = new wxBoxSizer( wxHORIZONTAL ); 
		m_color_maps[0] = "gray";
		m_color_maps[1] = "red-blue";
		m_color_maps[2] = "black-red-white";
		m_color_maps[3] = "black-blue-white";
		m_color_maps[4] = "black-yellow-white";
		m_color_maps[5] = "blue-white";
		m_color_maps[6] = "black body";
		m_color_maps[7] = "spectrum";

		m_color_maps_id[0] = Fun_doubleToRGBpoint_Multilinear::gray;
		m_color_maps_id[1] = Fun_doubleToRGBpoint_Multilinear::red_blue;
		m_color_maps_id[2] = Fun_doubleToRGBpoint_Multilinear::black_red_white;
		m_color_maps_id[3] = Fun_doubleToRGBpoint_Multilinear::black_blue_white;
		m_color_maps_id[4] = Fun_doubleToRGBpoint_Multilinear::black_yellow_white;
		m_color_maps_id[5] = Fun_doubleToRGBpoint_Multilinear::blue_white;
		m_color_maps_id[6] = Fun_doubleToRGBpoint_Multilinear::black_body;
		m_color_maps_id[7] = Fun_doubleToRGBpoint_Multilinear::spectrum;

		m_colormap_combo = new wxComboBox( this, idColormapCombo, "", wxDefaultPosition, 
			wxDefaultSize, 8, m_color_maps );
		m_colormap_combo->SetSelection(0);
		cmapsizer->Add( new wxStaticText(this,-1,"Palette"),0.5,wxALIGN_CENTER|wxLEFT,0);
		cmapsizer->Add( m_colormap_combo,0.5,wxALIGN_CENTRE|wxLEFT,10);
		m_colormap_inverse_check = new wxCheckBox( this, idColormapInverseCheck, "Inverse");	
		m_colormap_inverse_check->SetValue(false);
		cmapsizer->Add( m_colormap_inverse_check,0.5,wxALIGN_CENTER|wxLEFT,10) ;
		viewsizer->Add( cmapsizer,0.5,wxALIGN_CENTRE|wxBOTTOM,5) ;
		
		ctrlsizer->Add( viewsizer,0.5,wxEXPAND); 

		// Contrast box 
		wxStaticBoxSizer* contrastsizer = new wxStaticBoxSizer( new wxStaticBox(this,-1,"Value range"), wxVERTICAL );
		m_vmin_slider = new wxSlider( this, idVminSlider,(std::numeric_limits::min)(),(std::numeric_limits::min)(),(std::numeric_limits::max)(), wxDefaultPosition, wxSize(200,10), wxSL_LABELS );
		m_vmax_slider = new wxSlider( this, idVmaxSlider,(std::numeric_limits::max)(),(std::numeric_limits::min)(),(std::numeric_limits::max)(), wxDefaultPosition, wxSize(200,10), wxSL_LABELS  );
		contrastsizer->Add( m_vmin_slider,0.5,wxALIGN_CENTRE) ;
		contrastsizer->Add( m_vmax_slider,0.5,wxALIGN_CENTRE) ;
		ctrlsizer->Add( contrastsizer,0.5,wxEXPAND); 

		// Adds the ctrlsizer to the topsizer 
		topsizer->Add( ctrlsizer,0,wxALIGN_LEFT|wxALIGN_TOP);

		// sets the topsizer
		SetSizer( topsizer );  
		topsizer->SetSizeHints( this ); 

		m_images_position.x = topsizer->GetSize().x+10;
		m_images_position.y = 10;

		// Color map
		m_color_map = new Fun_doubleToRGBpoint_Multilinear(Fun_doubleToRGBpoint_Multilinear::gray,false,m_value_min,m_value_max);

		
		CloseImage();
		return TRUE;
	}
	//=======================================================================================

	//=======================================================================================
	template <class T>	
		bool Image3DViewer<T>::Open(const ImageRam& image) 
	{
		CloseImage();

		m_image = &image;
		m_image_opened = true;
		m_isize = m_image->size();
		m_zoom = 2;
		m_zoom_combo->SetSelection(m_zoom-1);
		Fit();

		m_slice_position = ImageSite(0, m_isize(1)/2, m_isize(2)/2, m_isize(3)/2);
		m_xslice_ctrl->SetRange(0,m_isize(1)-1);
		m_yslice_ctrl->SetRange(0,m_isize(2)-1);
		m_zslice_ctrl->SetRange(0,m_isize(3)-1);
		m_xslice_ctrl->SetValue(m_isize(1)/2);
		m_yslice_ctrl->SetValue(m_isize(2)/2);
		m_zslice_ctrl->SetValue(m_isize(3)/2);

		// Min/max value of the image
		m_value_min = (std::numeric_limits::max)();
		m_value_max = (std::numeric_limits::min)();
		ImageRam::const_iterator it;
		for (it=m_image->begin();it!=m_image->end();++it) {
			if (*it<m_value_min) m_value_min = *it;
			if (*it>m_value_max) m_value_max = *it;
		}
		wxLogMessage("Min/max = %d,%d",m_value_min,m_value_max);

		m_vmin_slider->SetRange(m_value_min, m_value_max);
		m_vmin_slider->SetValue(m_value_min);
		m_vmax_slider->SetRange(m_value_min, m_value_max);
		m_vmax_slider->SetValue(m_value_max);
		m_color_map->set( m_value_min, m_value_max );
		
		updateSlices();

		m_mouse_in_image = false;

		// Enables controls
		menuFile->Enable(idMenuClose,true);
		m_xslice_ctrl->Enable();
		m_yslice_ctrl->Enable();
		m_zslice_ctrl->Enable();
		m_zoom_combo->Enable();
		m_colormap_combo->Enable();
		m_colormap_inverse_check->Enable();
		m_ticks_check->Enable();
		m_vmin_slider->Enable();
		m_vmax_slider->Enable();

		return TRUE;
	}
	//=======================================================================================

	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::CloseImage() 
	{
		if (m_delete_image) {
			delete m_image;
			m_delete_image = false;
		}
		topsizer->SetSizeHints( this ); 
		
		// Disables controls		
		menuFile->Enable(idMenuClose,false);
		m_xslice_ctrl->Disable();
		m_yslice_ctrl->Disable();
		m_zslice_ctrl->Disable();
		m_zoom_combo->Disable();
		m_colormap_combo->Disable();
		m_colormap_inverse_check->Disable();
		m_ticks_check->Disable();
		m_vmin_slider->Disable();
		m_vmax_slider->Disable();
		
		m_image_opened = false;
	}
	//=======================================================================================



	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::OnMenuOpen (wxCommandEvent& event) 
	{
		wxString filename = wxFileSelector(_T("Select image file"),"","","hdr","Analyze headers files (*.hdr)|*.hdr");
		if ( !filename ) return;

		lgl::ImageRam* image = new lgl::ImageRam();

		if ( !lgl::readImageAnalyze(lgl::String(filename.c_str()), *image) ) {
			wxLogError(_T("Couldn't load image from '%s'."), filename.c_str());
			return;
		}

		m_delete_image = true;
		Open(*image);

		wxLogMessage(wxString("Image : ")<<image->size(0)<<","<<image->size(1)<<","<<image->size(2)<<","<<image->size(3));
		wxLog::FlushActive();
	}
	//=======================================================================================

	

	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::OnMenuClose (wxCommandEvent& event) 
	{
		CloseImage();
	}
	//=======================================================================================
	
	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::OnMenuAbout (wxCommandEvent& event) 
	{
		wxString msg;
		msg.Printf( _T("3D Image Viewer\n") 
					_T("Laurent Guigues 09/2004") );
		wxMessageBox(msg, _T("About"), wxOK | wxICON_INFORMATION, this);
	}
	//=======================================================================================

	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::OnMenuQuit (wxCommandEvent& event) 
	{
		CloseImage();
		Close();
	}
	//=======================================================================================

	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::OnZoomChange (wxCommandEvent& event) 
	{
		m_zoom = m_zoom_combo->GetSelection()+1;
		Fit();
		getSliceBitmaps();
		drawSliceBitmaps();
	}
	//=======================================================================================

	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::OnColormapChange (wxCommandEvent& event) 
	{
		m_color_map->set( m_color_maps_id[m_colormap_combo->FindString(m_colormap_combo->GetValue())] );
		getSliceBitmaps();
		drawSliceBitmaps();
	}
	//=======================================================================================


	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::OnColormapInverseCheck (wxCommandEvent& event) 
	{
		m_color_map->setInverse( m_colormap_inverse_check->IsChecked() );
		getSliceBitmaps();
		drawSliceBitmaps();
	}
	//=======================================================================================


	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::OnContrastMinChange (wxCommandEvent& event) 
	{
		double vmin = m_vmin_slider->GetValue(); 
		double vmax = m_vmax_slider->GetValue(); 
		if (vmin>vmax) { m_vmax_slider->SetValue(vmin); vmax = vmin; }
		//double ivmin = m_value_min + vmin*(m_value_max-m_value_min)/100.; 
		//double ivmax = m_value_min + vmax*(m_value_max-m_value_min)/100.; 
		m_color_map->set( vmin,vmax );
		
		getSliceBitmaps();
		drawSliceBitmaps();
	}
	//=======================================================================================
			
	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::OnContrastMaxChange (wxCommandEvent& event) 
	{
		double vmin = m_vmin_slider->GetValue(); 
		double vmax = m_vmax_slider->GetValue(); 
		if (vmax<vmin) { m_vmin_slider->SetValue(vmax); vmin = vmax; }
		//double ivmin = m_value_min + vmin*(m_value_max-m_value_min)/100.; 
		//double ivmax = m_value_min + vmax*(m_value_max-m_value_min)/100.; 
		m_color_map->set( vmin,vmax );

		getSliceBitmaps();
		drawSliceBitmaps();
	}
	//=======================================================================================


	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::OnXSliceCtrl ( wxSpinEvent& event) 
	{
		updateXSlice();
		if (m_ticks_check->IsChecked()) {
			drawYSliceBitmap();
			drawZSliceBitmap();
		}
	}
	//=======================================================================================

	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::OnYSliceCtrl ( wxSpinEvent& event) 
	{
		updateYSlice();
		if (m_ticks_check->IsChecked()) {
			drawXSliceBitmap();
			drawZSliceBitmap();
		}
	}
	//=======================================================================================
	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::OnZSliceCtrl ( wxSpinEvent& event) 
	{
		updateZSlice();
		if (m_ticks_check->IsChecked()) {
			drawXSliceBitmap();
			drawYSliceBitmap();
		}
	}
	//=======================================================================================


	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::OnTicksCheck ( wxCommandEvent& event) 
	{
		m_ticks_on = m_ticks_check->IsChecked();
		if (m_ticks_on) drawTicks();
		/// Clears ticks by redrawing bitmaps
		else drawSliceBitmaps();
	}
	//=======================================================================================

	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::drawTicks ( ) 
	{
		if (!m_image_opened) return;
		
		drawXTicks();
		drawYTicks();
		drawZTicks();
	}
	//=======================================================================================
		
	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::drawXTicks ( ) 
	{
		if (!m_image_opened) return;
		
		wxClientDC dc( this );
		dc.SetPen(wxPen(*wxRED,1,wxLONG_DASH));
		// xslice 
		dc.DrawLine(m_xslice_wp.x+m_zoom*m_yslice_ctrl->GetValue(),
					m_xslice_wp.y,
					m_xslice_wp.x+m_zoom*m_yslice_ctrl->GetValue(),
					m_xslice_wp.y+m_xslice_bmp.GetHeight());
		dc.DrawLine(m_xslice_wp.x,
					m_xslice_wp.y+m_xslice_bmp.GetHeight()-1-m_zoom*m_zslice_ctrl->GetValue(),
					m_xslice_wp.x+m_xslice_bmp.GetWidth(),
					m_xslice_wp.y+m_xslice_bmp.GetHeight()-1-m_zoom*m_zslice_ctrl->GetValue());
	}
	//=======================================================================================
	

	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::drawYTicks ( ) 
	{
		if (!m_image_opened) return;
		
		wxClientDC dc( this );
		dc.SetPen(wxPen(*wxRED,1,wxLONG_DASH));
		// xslice 
		dc.DrawLine(m_yslice_wp.x+m_zoom*m_xslice_ctrl->GetValue(),
					m_yslice_wp.y,
					m_yslice_wp.x+m_zoom*m_xslice_ctrl->GetValue(),
					m_yslice_wp.y+m_yslice_bmp.GetHeight());
		dc.DrawLine(m_yslice_wp.x,
					m_yslice_wp.y+m_yslice_bmp.GetHeight()-1-m_zoom*m_zslice_ctrl->GetValue(),
					m_yslice_wp.x+m_yslice_bmp.GetWidth(),
					m_yslice_wp.y+m_yslice_bmp.GetHeight()-1-m_zoom*m_zslice_ctrl->GetValue());

	}
	//=======================================================================================


	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::drawZTicks ( ) 
	{
		if (!m_image_opened) return;
		
		wxClientDC dc( this );
		dc.SetPen(wxPen(*wxRED,1,wxLONG_DASH));
		// xslice 
		dc.DrawLine(m_zslice_wp.x+m_zoom*m_xslice_ctrl->GetValue(),
					m_zslice_wp.y,
					m_zslice_wp.x+m_zoom*m_xslice_ctrl->GetValue(),
					m_zslice_wp.y+m_zslice_bmp.GetHeight());
		dc.DrawLine(m_zslice_wp.x,
					m_zslice_wp.y+m_zoom*m_yslice_ctrl->GetValue(),
					m_zslice_wp.x+m_zslice_bmp.GetWidth(),
					m_zslice_wp.y+m_zoom*m_yslice_ctrl->GetValue());

	}
	//=======================================================================================

	
	//=======================================================================================
	template <class T>	
		bool Image3DViewer<T>::mouseToSpace(wxMouseEvent& ev, ImageSite& p)
	{
		if (!m_image_opened) return false;

		int x = ev.m_x;
		int y = ev.m_y;

		// in the xslice
		if ((x>=m_xslice_wp.x)&&
			(y>=m_xslice_wp.y)&&
			(x<m_xslice_wp.x+m_xslice_bmp.GetWidth())&&
			(y<m_xslice_wp.y+m_xslice_bmp.GetHeight())) {
			p(1) = m_xslice_ctrl->GetValue();
			p(2) = (x-m_xslice_wp.x)/m_zoom ;
			p(3) = (m_xslice_wp.y+m_xslice_bmp.GetHeight()-1-y)/m_zoom;
			wxString s;
			s.Printf("%d",p(3));
			SetStatusText(s,0);
			return true;
		}
		// in the yslice
		if ((x>=m_yslice_wp.x)&&
			(y>=m_yslice_wp.y)&&
			(x<m_yslice_wp.x+m_yslice_bmp.GetWidth())&&
			(y<m_yslice_wp.y+m_yslice_bmp.GetHeight())) {
			p(1) = (x-m_yslice_wp.x)/m_zoom;
			p(2) = m_yslice_ctrl->GetValue();
			p(3) = (m_yslice_wp.y+m_yslice_bmp.GetHeight()-1-y)/m_zoom;
			return true;		
		}
		// in the zslice
		if ((x>=m_zslice_wp.x)&&
			(y>=m_zslice_wp.y)&&
			(x<m_zslice_wp.x+m_zslice_bmp.GetWidth())&&
			(y<m_zslice_wp.y+m_zslice_bmp.GetHeight())) {
			p(1) = (x-m_zslice_wp.x)/m_zoom;
			p(2) = (y-m_zslice_wp.y)/m_zoom;
			p(3) = m_zslice_ctrl->GetValue();
			return true;
		}
		return false;
	}
	//=======================================================================================
	
	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::OnMouse(wxMouseEvent& ev)
	{
		if (!m_image_opened) return;

		ImageSite pos(0,0,0,0);
		if (!mouseToSpace(ev,pos)) {
			// mouse exits the image 
			if (m_mouse_in_image) {
				m_mouse_in_image = false;
				//wxSetCursor(*wxSTANDARD_CURSOR);
			}
			return;
		}
		// mouse has entered the image
		if (!m_mouse_in_image) {
			m_mouse_in_image = true;
		}
		//wxSetCursor(wxCursor(wxCURSOR_IBEAM));
		
		// position
		wxString stat;
		stat.Printf("%d,%d,%d",pos(1),pos(2),pos(3));
		SetStatusText(stat,2);
		// image value
		int v = m_image->iget(pos);
		stat.Printf("%d",v);
		SetStatusText(stat,3);
		// clic 
		if (ev.GetButton()!=1) return;
		int x = ev.m_x;
		int y = ev.m_y;

		// clic in the xslice
		if ((x>=m_xslice_wp.x)&&
			(y>=m_xslice_wp.y)&&
			(x<m_xslice_wp.x+m_xslice_bmp.GetWidth())&&
			(y<m_xslice_wp.y+m_xslice_bmp.GetHeight())) {
			m_yslice_ctrl->SetValue( (x-m_xslice_wp.x)/m_zoom );
			m_zslice_ctrl->SetValue( (m_xslice_wp.y+m_xslice_bmp.GetHeight()-1-y)/m_zoom );
			if (m_ticks_check->IsChecked()) drawXSliceBitmap();
			updateYSlice();
			updateZSlice();
		}
		// clic in the yslice
		if ((x>=m_yslice_wp.x)&&
			(y>=m_yslice_wp.y)&&
			(x<m_yslice_wp.x+m_yslice_bmp.GetWidth())&&
			(y<m_yslice_wp.y+m_yslice_bmp.GetHeight())) {
			m_xslice_ctrl->SetValue( (x-m_yslice_wp.x)/m_zoom );
			m_zslice_ctrl->SetValue( (m_yslice_wp.y+m_yslice_bmp.GetHeight()-1-y)/m_zoom );
			updateXSlice();
			if (m_ticks_check->IsChecked()) drawYSliceBitmap();
			updateZSlice();
		}
		// clic in the xslice
		if ((x>=m_zslice_wp.x)&&
			(y>=m_zslice_wp.y)&&
			(x<m_zslice_wp.x+m_zslice_bmp.GetWidth())&&
			(y<m_zslice_wp.y+m_zslice_bmp.GetHeight())) {
			m_xslice_ctrl->SetValue( (x-m_zslice_wp.x)/m_zoom );
			m_yslice_ctrl->SetValue( (y-m_zslice_wp.y)/m_zoom );
			updateXSlice();
			updateYSlice();
			if (m_ticks_check->IsChecked()) drawZSliceBitmap();
		}

	}
	//=======================================================================================

	
	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::Fit() 
	{
		if (!m_image_opened) return;
		
		m_vsize(1) = m_isize(1)*m_zoom;
		m_vsize(2) = m_isize(2)*m_zoom;
		m_vsize(3) = m_isize(3)*m_zoom;				

		m_xslice_wp = wxPoint(m_images_position.x+m_vsize(2),m_images_position.y);
		m_yslice_wp = wxPoint(m_images_position.x,m_images_position.y);
		m_zslice_wp = wxPoint(m_images_position.x,m_images_position.y+m_vsize(3));

		m_image_frame_size.x = m_vsize(1)+m_vsize(2);
		m_image_frame_size.y = m_vsize(2)+m_vsize(3);
		
		this->SetClientSize(	m_images_position.x+m_image_frame_size.x,
										m_images_position.y+m_image_frame_size.y);

	}
	//=======================================================================================


	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::getSliceImages() {
		if (!m_image_opened) return;
		getXSliceImage();
		getYSliceImage();
		getZSliceImage();
	}
	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::getXSliceImage() {
		if (!m_image_opened) return;
		m_image->getSlice( ImageSite(0,1), m_xslice_ctrl->GetValue(), m_xslice_img );
	}
	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::getYSliceImage() {
		if (!m_image_opened) return;
		m_image->getSlice( ImageSite(0,0,1), m_yslice_ctrl->GetValue(), m_yslice_img );
	}
	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::getZSliceImage() {
		if (!m_image_opened) return;
		m_image->getSlice( ImageSite(0,0,0,1), m_zslice_ctrl->GetValue(), m_zslice_img );
	}
	//=======================================================================================


	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::getSliceBitmaps() {
		if (!m_image_opened) return;
		getXSliceBitmap(); 
		getYSliceBitmap(); 
		getZSliceBitmap(); 
	}
	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::getXSliceBitmap() {
		if (!m_image_opened) return;
		ImageRGB tmp;
		if (!ImageToImageRGB ( m_xslice_img, tmp, *m_color_map, FPoint(m_zoom,0,0,-m_zoom) )) 
			wxLogError("ImageToImageRGB error");
		m_xslice_bmp = tmp.ConvertToBitmap();	
	}
	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::getYSliceBitmap() {
		if (!m_image_opened) return;
		ImageRGB tmp;
		if (!ImageToImageRGB ( m_yslice_img, tmp, *m_color_map, FPoint(m_zoom,0,0,-m_zoom) ))
			wxLogError("ImageToImageRGB error");	
		m_yslice_bmp = tmp.ConvertToBitmap();	
	}
	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::getZSliceBitmap() {
		if (!m_image_opened) return;
		ImageRGB tmp;
		if (!ImageToImageRGB ( m_zslice_img, tmp, *m_color_map, FPoint(m_zoom,0,0,m_zoom) ))
			wxLogError("ImageToImageRGB error");
		m_zslice_bmp = tmp.ConvertToBitmap();	
	}
	//=======================================================================================

	

	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::updateSlices() {
		if (!m_image_opened) return;
		updateXSlice();
		updateYSlice();
		updateZSlice();
	}
	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::updateXSlice() {
		if (!m_image_opened) return;
		getXSliceImage();
		getXSliceBitmap();
		drawXSliceBitmap();
	}
	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::updateYSlice() {
		if (!m_image_opened) return;
		getYSliceImage();
		getYSliceBitmap();
		drawYSliceBitmap();
	}
	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::updateZSlice() {
		if (!m_image_opened) return;
		getZSliceImage();
		getZSliceBitmap();
		drawZSliceBitmap();
	}
	//=======================================================================================
	



	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::drawSliceBitmaps() {
		if (!m_image_opened) return;
		drawXSliceBitmap();
		drawYSliceBitmap();
		drawZSliceBitmap();
	}
	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::drawXSliceBitmap() {
		if (!m_image_opened) return;
		wxClientDC dc( this );
		dc.DrawBitmap( m_xslice_bmp, m_xslice_wp.x, m_xslice_wp.y );
		if (m_ticks_check->IsChecked()) drawXTicks();
	}
	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::drawYSliceBitmap() {
		if (!m_image_opened) return;
		wxClientDC dc( this );
		dc.DrawBitmap( m_yslice_bmp, m_yslice_wp.x, m_yslice_wp.y );
		if (m_ticks_check->IsChecked()) drawYTicks();
	}
	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::drawZSliceBitmap() {
		if (!m_image_opened) return;
		wxClientDC dc( this );
		dc.DrawBitmap( m_zslice_bmp, m_zslice_wp.x, m_zslice_wp.y );
		if (m_ticks_check->IsChecked()) drawZTicks();
	}
	//=======================================================================================
	//=======================================================================================
	template <class T>	
		void Image3DViewer<T>::OnPaint(wxPaintEvent& ev ) 
	{
		wxPaintDC dc( this );
		if (!m_image_opened) return;
		dc.DrawBitmap( m_xslice_bmp, m_xslice_wp.x, m_xslice_wp.y );
		dc.DrawBitmap( m_yslice_bmp, m_yslice_wp.x, m_yslice_wp.y );
		dc.DrawBitmap( m_zslice_bmp, m_zslice_wp.x, m_zslice_wp.y );
		if (m_ticks_check->IsChecked()) drawTicks();
	}
	//=======================================================================================


};
*/
//==========================================================================================
// EO namespace lgl
//==========================================================================================


//==========================================================================================
// EOF
//==========================================================================================
#endif
#endif



