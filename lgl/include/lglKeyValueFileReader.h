/* 

lGl : A C++ library needed by the multiscale image segmentation library SxS
© 2004 Laurent Guigues (laurent.guigues@ign.fr)

This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.

You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
/*! \file 
	\brief A class which allows to read ascii files in which each line is a couple Key Value and to store the Values in specific variables. 
*/
#ifndef __lglKeyValueFileReader_h__
#define __lglKeyValueFileReader_h__
//===========================================================
// namespace	: lgl
// classe		: KeyValueFileReader
// 
//===========================================================
// author		: Laurent Guigues
// history		: 
//	26/01/05	: creation
//===========================================================
#include "lglGeneral.h"
#include <vector>

namespace lgl
{

  //=================================================================================
  /// \brief Reads an ascii file in which each line is a couple : Key Value 
  /// and stores the Value corresponding to each recognized Key into a specific variable. 
  class KeyValueFileReader
    {
    public:
      //=================================================================================
      /// \brief Abstract couple (Key,Value)
      class KeyValue
	{
	public:
	  /// \brief Virutal dtor
	  virtual ~KeyValue() {}
	  /// \brief Tests wether key corresponds to the personnal key. If so stores the Value and returns true. Else returns false
	  virtual bool Test( const String& key, const String& value, bool verbose = false ) = 0;
	};
      //=================================================================================

      //=================================================================================
      /// \brief Couple (Key,Value) whose Value is a Boolean 
      class BoolValue : public KeyValue
	{
	public:
	  BoolValue ( const String& key, bool& var, bool default_value = false) : m_key(key), m_adr(&var) { var = default_value; }

	  bool Test( const String& key, const String& value, bool verbose = false ) {
	    if (key==m_key) {
	      if ((value==String("on"))||(value==String("ON"))||(value==String("1"))) *m_adr = true;
	      else *m_adr = false;
	      if (verbose) std::cout << "[" << key << "] = <" << *m_adr <<"> (Bool)" << std::endl;
	      return true;
	    }
	    return false;
	  }

	protected:
	  String m_key;
	  bool* m_adr;
	};
      //=================================================================================
      
   
   
      //=================================================================================
      /// \brief Couple (Key,Value) whose Value is a 32 bits integer
      class I32Value : public KeyValue
	{
	public:
	  I32Value ( const String& key, I32& var, I32 default_value = 0) : m_key(key), m_adr(&var) { var = default_value; }

	  bool Test( const String& key, const String& value, bool verbose = false ) {
	    if (key==m_key) {
	      sscanf((const char*)value.c_str(),"%d",m_adr);
	      if (verbose) std::cout << "[" << key << "] = <" << *m_adr <<"> (I32)" <<std::endl;
	      return true;
	    }
	    return false;
	  }

	protected:
	  String m_key;
	  I32* m_adr;
	};
      //=================================================================================


    //=================================================================================
      /// \brief Couple (Key,Value) whose Value is a 32 bits float
      class F32Value : public KeyValue
	{
	public:
	  F32Value ( const String& key, F32& var, F32 default_value = 0) : m_key(key), m_adr(&var) { var = default_value; }

	  bool Test( const String& key, const String& value, bool verbose = false ) {
	    if (key==m_key) {
	      sscanf((const char*)value.c_str(),"%f",m_adr);
	      if (verbose) std::cout << "[" << key << "] = <" << *m_adr <<"> (F32)" <<std::endl;
	      return true;
	    }
	    return false;
	  }

	protected:
	  String m_key;
	  F32* m_adr;
	};
      //=================================================================================
    //=================================================================================
      /// \brief Couple (Key,Value) whose Value is a 64 bits float
      class F64Value : public KeyValue
	{
	public:
	  F64Value ( const String& key, F64& var, F64 default_value = 0) : m_key(key), m_adr(&var) { var = default_value; }

	  bool Test( const String& key, const String& value, bool verbose = false ) {
	    if (key==m_key) {
	      sscanf((const char*)value.c_str(),"%lf",m_adr);
	      if (verbose) std::cout << "[" << key << "] = <" << *m_adr <<"> (F64)" <<std::endl;
	      return true;
	    }
	    return false;
	  }

	protected:
	  String m_key;
	  F64* m_adr;
	};
      //=================================================================================

    //=================================================================================
      /// \brief Couple (Key,Value) whose Value is a String
      class StringValue : public KeyValue
	{
	public:
	  StringValue ( const String& key, String& var, String default_value = String("")) : m_key(key), m_adr(&var) { var = default_value; }

	  bool Test( const String& key, const String& value, bool verbose = false ) {
	    if (key==m_key) {
	      *m_adr = value;
	      if (verbose) std::cout << "[" << key << "] = \"" << *m_adr <<"\" (String)" <<std::endl;
	      return true;
	    }
	    return false;
	  }

	protected:
	  String m_key;
	  String* m_adr;
	};
      //=================================================================================





      KeyValueFileReader() {}
      ~KeyValueFileReader() { clear(); }

      /// \brief Adds a KeyValue to the list of known KeyValues. v should have been allocated on the heap and will be deleted by the object.
      void add(KeyValue* v) { m_kv.push_back(v); }

      bool read( const Filename& f, bool verbose = false ); 
      void clear(); 

      
    protected:
      std::vector < KeyValue* > m_kv;

      

    };
  // EO class KeyValueFileReader
  //=================================================================================


}
//=================================================================================
// EO namespace lgl
//=================================================================================

//=================================================================================
// EOF
//=================================================================================
#endif
