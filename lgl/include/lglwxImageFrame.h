#ifndef __lglImageFrame_h__
#define __lglImageFrame_h__
//===========================================================
// namespace	: lgl
// classe		: ImageFrame
//===========================================================
// author		: Laurent Guigues
// history		: 
//	26/08/04	: creation
//===========================================================

//===========================================================
// 
#ifdef __LGLWX__ 

#include "lglwxImageRGB.h"
//#include "wx/image.h"

namespace lgl
{

	// ----------------------------------------------------------------------------
class ImageFrame : public wxFrame
{
public:
    ImageFrame(wxFrame *parent, const wxString& title, const wxBitmap& bitmap)
        : wxFrame(parent, -1, _T(title),
                  wxDefaultPosition, wxDefaultSize,
                  wxCAPTION | wxSYSTEM_MENU),
                  m_bitmap(bitmap)
    {
        SetClientSize(bitmap.GetWidth(), bitmap.GetHeight());
    }

    void OnPaint(wxPaintEvent& WXUNUSED(event))
    {
        wxPaintDC dc( this );
        //TRUE for masked images
        dc.DrawBitmap( m_bitmap, 0, 0, TRUE );
    }

    void OnSave(wxCommandEvent& WXUNUSED(event))
    {
        wxImage image = m_bitmap.ConvertToImage();

        
        wxString savefilename = wxFileSelector( wxT("Save Image"),
                                                wxT(""),
                                                wxT(""),
                                                (const wxChar *)NULL,
                                            wxT("BMP files (*.bmp)|*.bmp|")
                                            wxT("PNG files (*.png)|*.png|")
                                            wxT("JPEG files (*.jpg)|*.jpg|")
                                            wxT("GIF files (*.gif)|*.gif|")
                                            wxT("TIFF files (*.tif)|*.tif|")
                                            wxT("PCX files (*.pcx)|*.pcx|")
                                            wxT("ICO files (*.ico)|*.ico|")
                                            wxT("CUR files (*.cur)|*.cur"),
                                                wxSAVE);

        if ( savefilename.empty() )
            return;

        // This one guesses image format from filename extension
        // (it may fail if the extension is not recognized):
        bool ok = image.SaveFile(savefilename);

        if ( !ok )
            wxMessageBox(_T("No handler for this file type."),
                         _T("File was not saved"),
                         wxOK|wxCENTRE, this);
    }

private:
    wxBitmap m_bitmap;

    DECLARE_EVENT_TABLE()
};
// ----------------------------------------------------------------------------



BEGIN_EVENT_TABLE(ImageFrame, wxFrame)
  EVT_PAINT(ImageFrame::OnPaint)
  EVT_LEFT_DCLICK(ImageFrame::OnSave)
END_EVENT_TABLE()

};
//==========================================================================================
// EO namespace lgl
//==========================================================================================


//==========================================================================================
#endif /// #ifdef _LGLWX
//==========================================================================================
// EOF
//==========================================================================================
#endif


