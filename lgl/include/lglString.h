/* 

lGl : A C++ library needed by the multiscale image segmentation library SxS
� 2004 Laurent Guigues (laurent.guigues@ign.fr)

This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.

You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
/**	\file	
	\brief lGl Strings and Filenames (today wxString/wxFilename)
**/
#ifndef __lglString_h__
#define __lglString_h__
//===========================================================
// namespace	: lgl
//===========================================================
// author		: Laurent Guigues
// class		: String
// history		: 
//	26/07/04		creation
//===========================================================
#ifndef __LGLWX__
					   //ERROR : lGl MUST BE COMPILED WITH __LGLWX__ DEFINED (lgl::String is wxString) 
#endif
/*
#if (__GNUC__) && (__GNUC__ <3)
#include <strstream.h>
#else
#include <sstream>
#endif
#include <iostream>
*/

//=============================================================
// lgl::Filename is based on wxFileName
// lgl::String is based on wxString
#ifdef __LGLWX__
#include "wx/string.h"
#include "wx/filename.h"
#endif
					   //#else 
//=============================================================
#include <string>
					   //#endif

namespace lgl
{

  //#ifdef __LGLWX__
  //typedef wxFileName	Filename;
  //  typedef wxString	String;
  //#else
  typedef std::string Filename;
  typedef std::string String;
  //#endif
  //typedef std::string	String;
  
  /*
  class String : public wxString
  {
  public:
    String() {}
    String( char* c) : wxString((const wxChar*)c) {}
      String( const char* c) : wxString((const wxChar*)c) {}
	String( const wxString& s ) : wxString(s) {}
  };
*/
  //====================================================================
  inline String getExtension(const Filename& filename);

  //====================================================================
  inline String removeExtension(const Filename& filename);

  //====================================================================
  inline void setExtension(Filename& filename, const String& extension);
//========================================================================

//========================================================================
  inline void Trim(String& s);
//========================================================================

#ifdef __LGLWX__
  inline wxString std2wx(std::string s){
    wxString wx;
    const char* my_string=s.c_str();
    wxMBConvUTF8 *wxconv= new wxMBConvUTF8();
    wx=wxString(wxconv->cMB2WC(my_string),wxConvUTF8);
    delete wxconv;
    // test if conversion works of not. In case it fails convert from Ascii
    if(wx.length()==0)
      wx=wxString(wxString::FromAscii(s.c_str()));
    return wx;
  }
  
  inline std::string wx2std(wxString s){
    std::string s2;
    if(s.wxString::IsAscii()) {
      s2=s.wxString::ToAscii();
    } else {
      const wxWX2MBbuf tmp_buf = wxConvCurrent->cWX2MB(s);
      const char *tmp_str = (const char*) tmp_buf;
      s2=std::string(tmp_str, strlen(tmp_str));
    }
    return s2;
  }
#endif

  String getExtension(const Filename& filename) {
    unsigned int position = filename.find_last_of(".");
    if (position == filename.npos) return std::string("");
    return filename.substr(position+1,filename.size()-position);
  } ////
  //========================================================================
  
  //========================================================================
  String removeExtension(const Filename& filename) {
    unsigned int position = filename.find_last_of(".");
    if (position == filename.npos) return filename; //std::string("");
    return filename.substr(0, position);
  } ////
  //========================================================================
  
  //========================================================================
  void setExtension(Filename& filename, const String& extension) {
    unsigned int position = filename.find_last_of(".");  
    if (position == filename.npos) {
      filename += ".";
    }
    else {
      filename = filename.substr(0,position+1);
    }
    filename += extension;
    
  } ////
//========================================================================

//========================================================================
  inline void Trim(String& s) {}
  //========================================================================


}
//===========================================================
// EO namespace lgl
//===========================================================


//===========================================================
// EOF
//===========================================================
#endif
