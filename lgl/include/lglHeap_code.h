/* 

lGl : A C++ library needed by the multiscale image segmentation library SxS
� 2004 Laurent Guigues (laurent.guigues@ign.fr)

This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.

You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
/*! \file 
	\brief Code of Heap
*/
//============================================================================
template <class T, class CT>
std::ostream& operator << (std::ostream& s, const Heap<T,CT>& t)
{
  s << "[";
  for (I32 i=0; i<t.size(); i++) s << t[i] << " "  ;
  s << "]";
  return s;
}
//============================================================================


//===========================================================
template <class T, class CT>
Heap<T,CT>::Heap ( const CT& c) : m_c(&c) {}
//===========================================================

//===========================================================
template <class T, class CT>
void Heap<T,CT>::set ( const CT& c ) 
{ m_c = &c; }
//===========================================================


//===========================================================
template <class T, class CT>
I32 Heap<T,CT>::insert(T t)
{
  m_p.push_back(t);
  return upsort(size()-1);
}
//===========================================================


//===========================================================
template <class T, class CT>
T& Heap<T,CT>::top()
{
  lglASSERT( size() > 0)
    return m_p.front();
}
//===========================================================


//===========================================================
template <class T, class CT>
const T& Heap<T,CT>::top() const
{
  lglASSERT( size() > 0)
    return m_p.front();
}
//===========================================================


//===========================================================
template <class T, class CT>
T Heap<T,CT>::remove_top()
{
  lglASSERT( size() > 0 ) 
    T f(m_p[0]);
  T last = m_p.back();
  m_p.pop_back();
  if (m_p.size()>0) {
    m_p[0] = last;
    downsort(0);
  }
  return f;
}
//============================================================================



//============================================================================
template <class T, class CT>
T Heap<T,CT>::remove(I32 n)
{
  lglASSERT ( (n>=0)&&(n<size()) ) 
    T f(m_p[n]);
  T last = m_p.back();
  m_p.pop_back();
  if (m_p.size()>0) {
    m_p[n] = last;
    downsort(n);
  }
  return f;
}
//============================================================================



//============================================================================
template <class T, class CT>
I32  Heap<T,CT>::father( I32  i) const
{
  return ((i-1)/2);
}
//============================================================================

//============================================================================
template <class T, class CT>
I32  Heap<T,CT>::rightson( I32  i) const
{ 
  return (i*2+2);
}
//============================================================================

//============================================================================
template <class T, class CT>
I32  Heap<T,CT>::leftson( I32  i) const
{ 
  return (i*2+1);
}
//============================================================================


//============================================================================
template <class T, class CT>
void Heap<T,CT>::swap(I32  i, I32  j)
{
  T tmp = m_p[i];
  m_p[i] = m_p[j];
  m_p[j] = tmp;
}
//============================================================================


//============================================================================
template <class T, class CT>
I32  Heap<T,CT>::upsort(I32  i)
{
  //if (i==0) return i;
  I32  j = father(i);
  while ((i>0)&&(*m_c)(m_p[i],m_p[j])) {
    swap(i,j);
    i = j;
    j = father(j);
  }
  return i;
}
//============================================================================


//============================================================================
template <class T, class CT>
I32  Heap<T,CT>::downsort(I32  i)
{
  do {
    I32  ls = leftson(i);
    if (ls<m_p.size()) {
      bool lc = ((*m_c)(m_p[i],m_p[ls]));
      I32  rs = ls + 1;
      bool rc = true;
      if (rs<m_p.size()) rc = ((*m_c)(m_p[i],m_p[rs]));
      if  ( !lc ) { 
	if ( !rc ) { 
	  if ((*m_c)(m_p[ls],m_p[rs])) { 
	    swap(i,ls);
	    i = ls;
	  }
	  else { 
	    swap(i,rs);
	    i = rs;
	  }
	}
	else {
	  swap(i,ls);
	  i = ls;
	}
      }
      else if ( !rc ) { 
	swap(i,rs);
	i = rs;
      }
      else return i;
    } 
    else return i;
  }
  while (true);
  return i;
}
//============================================================================


//============================================================================
// EOF
//============================================================================
