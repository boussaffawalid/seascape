/* 

lGl : A C++ library needed by the multiscale image segmentation library SxS
� 2004 Laurent Guigues (laurent.guigues@ign.fr)

This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.

You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
/*! \file 
	\brief Includes all the headers of lgl.
*/
//===========================================================
// namespace	: lgl
//  includes all the headers of the library
//===========================================================
// author		: Laurent Guigues
// history		: 
//	11/07/04		creation
//===========================================================
// General inclusions, preprocessor definitions, and so on
#include "lglGeneral.h"
// little/big endian machine handler
#include "lglMachine.h"
// general templates for unary/binary/ternary functions
#include "lglFunction.h"

// Mathematical and physical constants 
#include "lglConstant.h"

// pseudo-random numbers generators
#include "lglRandom.h"

// 2D points 
#include "lglPoint2D.h"
// 2D rectangles
#include "lglRectangle.h"
// 0 to 4D points 
#include "lglPoint4D.h"
// abstract collections of objets 
#include "lglCollection.h"
// vectors (linear algebra objects inherited from std::vector STL objects)
#include "lglVector.h"
// abstract arrays 
#include "lglArray.h"
// 2D arrays (inherited from BinaryFunction and Array)
#include "lglArray2D.h"
// Matrices (inherited from Array2D)
#include "lglMatrix.h"

// Bitwise operations and bit allocation/desallocation
#include "lglBitManager.h"

// Priority queues implemented as binary trees 
#include "lglHeap.h"
// Priority queues implemented as binary trees allowing fast element location in the queue (for priority change)
#include "lglIndexedHeap.h"

// Arbitrary (and dynamical) size arrays allowing random indices (sparsity) while keeping a constant time random access   
// (basically it is a efficient implementation of a STL map<int,T>).
// Handled by a 4 levels dynamic tree of arity 256.
#include "lglSparseArray.h"


// Generic IO
// Key Value file parser
#include "lglKeyValueFileReader.h"


// Abstract image definition
#include "lglImage.h"
// 2d/3d Images stored in RAM (inherited from Image)
#include "lglImageRam.h"
// Various functions for image manipulation
#include "lglImageManip.h"
// Image loading/saving methods
#include "lglImageIO.h"
// Handler for the Analyze medical image format 
#include "lglAnalyzeHeader.h"

/// IMAGE ALGORITHMS
/// Basics : connected components, local optima
#include "lglImageBasicAlgorithms.h"
// Image partitioning using the generalized Voronoi tesselation approach of Arbelaez and Cohen
#include "lglVoronoiImageTesselation.h"
// Boundary tracker
#include "lglInterPixelBoundaryTracker.h"

// A node/edge + edge linking-based attributed graph structure implementation (template attributes)
#include "lglGraph.h"
// Graphs whose nodes are image sites and edges are adjacency relationships between neighboring image sites 
#include "lglPixelGraph.h"
// Methods for building region adjacency graphs (RAG) from label images
#include "lglLabelImageToGraph.h"


// CLASSES/METHODS USING wxWidgets
// Color images (3 channels byte images) based on wxImage (inherits wxImage,ImageInOut)
#include "lglwxImageRGB.h"
// A wxFrame-based class implementing a 3D image viewer (slicing...)
#include "lglwxImage3DViewer.h"
// A log window (based on wxLogWindow)  
#include "lglwxLogWindow.h"
// A basic frame for drawing a RGBImage
#include "lglwxImageFrame.h"


// INTERFACE WITH IGN/MATIS's "NOYAU" LIBRARY
// one must define __NOYAU__
//#include "lglNoyau.h"
