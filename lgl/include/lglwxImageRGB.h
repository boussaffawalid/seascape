/* 

lGl : A C++ library needed by the multiscale image segmentation library SxS
� 2004 Laurent Guigues (laurent.guigues@ign.fr)

This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.

You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
/**	\file	
	\brief Classical 2D RGB images based on wxImage
**/
#ifndef __lglImageRGB_h__
#define __lglImageRGB_h__
//===========================================================
// namespace	: lgl
// classe		: ImageRGB
// Classical 2D RGB images based on wxImage
//===========================================================
// author		: Laurent Guigues
// history		: 
//	26/08/04	: creation
//===========================================================

//===========================================================
// 
#ifdef __LGLWX__ 

#include "lglImage.h"
#include "wx/image.h"

namespace lgl
{

	//==========================================================================================
	/// Red, green, blue structure
	class RGBpoint
	{
		public :
			unsigned char r,g,b;
			RGBpoint(unsigned char rr=0, unsigned char gg=0, unsigned char bb=0) : r(rr), g(gg), b(bb) {}

			inline BOOL operator == ( const RGBpoint& x ) const { return (r==x.r)&&(g==x.g)&&(b==x.b); }
			inline BOOL operator != ( const RGBpoint& x ) const { return (r!=x.r)||(g!=x.g)||(b!=x.b); }

			inline RGBpoint operator + ( const RGBpoint& x ) const { return RGBpoint(r+x.r,g+x.g,b+x.b); } 
			inline RGBpoint operator - ( const RGBpoint& x ) const { return RGBpoint(r-x.r,g-x.g,b-x.b); } 
			inline RGBpoint operator * ( double v ) const { return RGBpoint((unsigned char)(r*v),(unsigned char)(g*v),(unsigned char)(b*v)); } 
			inline void operator += ( const RGBpoint& x ) { r+=x.r; g+=x.g; b+=x.b; }
			inline void operator -= ( const RGBpoint& x ) { r-=x.r; g-=x.g; b-=x.b; }

			static RGBpoint WHITE() { return RGBpoint(255,255,255); }
			static RGBpoint BLACK() { return RGBpoint(0,0,0); }
			static RGBpoint RED() { return RGBpoint(255,0,0); }
			static RGBpoint GREEN() { return RGBpoint(0,255,0); }
			static RGBpoint BLUE() { return RGBpoint(0,0,255); }
			static RGBpoint YELLOW() { return RGBpoint(255,255,0); }
	};
	//==========================================================================================
	
	//==========================================================================================
	class ImageRGB;
	
	//==========================================================================================
	/// Iterator of ImageRGB
	template <class RefT>
	class ImageRGB_iterator
	{
			FRIEND_CLASS ImageRGB;
		public :
			ImageRGB_iterator() : m_ptr(NULL) {}

			RefT operator*() { return *m_ptr; }

			BOOL operator==(const ImageRGB_iterator<RefT>& i) const { return m_ptr==i.m_ptr; }
			BOOL operator!=(const ImageRGB_iterator<RefT>& i) const { return m_ptr!=i.m_ptr; }


			ImageRGB_iterator<RefT>& operator++() { m_ptr++; return *this; }

		protected :
			UI8* m_ptr;
			ImageRGB_iterator( UI8* ptr) : m_ptr(ptr) {}


	};	
	//==========================================================================================

	
	//==========================================================================================
	/// Classical 2D RGB images 
	class ImageRGB : virtual public ImageInOut, public wxImage
	{
	public :
		/// iterator
		typedef ImageRGB_iterator<UI8&> iterator;
		/// const_iterator
		typedef ImageRGB_iterator<const UI8&> const_iterator;

		///@name Construction and destruction
		///@{
		/// Default ctor
		ImageRGB() { Create(1,1); }
		/// Ctor with size and initial value 
		ImageRGB(const ImageSite& size) { build(size); }
		/// Copy ctor
		ImageRGB(const ImageRGB& src) : wxImage(src) {}
		/// Destructor
		~ImageRGB() {}
		/// 
		BOOL build(const ImageSite& p) {
			if (p==size()) return TRUE;
			if (p.dimension()!=3) return FALSE;
			if (p(0)!=3) return FALSE; 
			Create(p(1),p(2)); 
			return TRUE;
		}
		///@}		
		
		

		///@name Size, #dimensions, #channels
		///@{
		/// Returns the size of the Image.
		///  - size()(0) : number of channels
		///  - size()(1) : number of points in x coordinate
		///  - size()(2) : number of points in y coordinate
		///  - size()(3) : number of points in z coordinate
		inline ImageSite	size() const { return ImageSite(3,GetWidth(),GetHeight()); }
		/// Returns the size of the Image in the ith dimension
		inline I32	size(I32 i) const { return size()(i); }
		/// Returns the dimension of the Image
		inline I32	dimension() const { return 2; }
		/// Returns the number of channels the Image
		inline I32	channels() const { return 3; }
		/// Returns the domain of the image
		inline ImageBloc domain() const { return ImageBloc(ImageSite(0,0,0),size()); }
		/// Resizes the image. returns TRUE if ok.
		virtual BOOL resize( const ImageSite& p ) { return build(p); }
		/// returns the code of the BasicType of the Image (@see BasicType)
		virtual TypeCode type() const { return BasicType::code((unsigned char)0); }
	  /// Returns the spacing of the Image
	  virtual ImageSpacing spacing() const { return ImageSpacing(1,1,1,1); }

		///@}		
		
		
		///@name Values access
		///@{
		/// Returns the value of the image at point p as a 32 bits float
		inline virtual F32  get( const ImageSite& p ) const { return (F32)(GetData()[m_offset(p)]); }
		/// Sets the value of the image at point p as a 32 bits float
		inline virtual void set( const ImageSite& p, F32 v ) { GetData()[m_offset(p)] = (UI8)v; }
		/// Sets all the values of the image to v 
		virtual void mset( F32 v ) { unsigned char c = (unsigned char)v; memset(GetData(),c,3*GetWidth()*GetHeight()); }
		/// Returns the RGB value of the image at point (x,y)
		inline RGBpoint get( int x, int y) const { UI8* d=GetData()+m_offset(x,y); return RGBpoint(*d,*(d+1),*(d+2)); }
		/// Sets the RGB value of the image at point (x,y)
		inline void set( int x, int y, const RGBpoint& v) const { UI8* d=GetData()+m_offset(x,y); *d=v.r; *(d+1)=v.g; *(d+2)=v.b; }

		/// Returns a pointer on the data 
		unsigned char* getData() { return GetData(); }
		/// Returns a const pointer on the data 
		const unsigned char* getData() const { return GetData(); }

		///@}


		/// Returns a reference on the value at site p
		unsigned char& operator() (const ImageSite& p) { return GetData()[m_offset(p)];  }
		/// Returns a const ref on the value at site p
		const unsigned char& operator() (const ImageSite& p) const { return GetData()[m_offset(p)];  }
		/// Returns a ref on the red value at site p(c,x,y)
		unsigned char& operator() (I32 x, I32 y) { return GetData()[m_offset(x,y)];  }
		/// Returns a const ref on the red value at site p(c,x,y)
		const unsigned char& operator() (I32 x, I32 y) const { return GetData()[m_offset(x,y)];  }
		/// Returns a ref on the value at site p(c,x,y)
		unsigned char& operator() (I32 c, I32 x, I32 y) { return GetData()[m_offset(c,x,y)];  }
		/// Returns a const ref on the value at site p(c,x,y)
		const unsigned char& operator() (I32 c, I32 x, I32 y) const { return GetData()[m_offset(c,x,y)];  }
		

		/// \name Methods inherited from lgl::Collection<double> for auto-iteration 
		/// \{
		/// Restarts the iteration to the first value
		inline void start() { m_cur = GetData(); }
		/// Returns true when the iteration is past the end
		inline BOOL stop() { return (m_cur >= m_get_data_end() ); }
		/// Returns the current value as a double
		inline const double& operator * () const { static double d=0; d=(double)(*m_cur); return d; }				
		/// Steps to the next value
		inline void operator++() { m_cur++; }
		/// \}

		///@name Iteration
		///@{
		iterator begin() { return iterator(GetData()); }
		iterator end() { return iterator(m_get_data_end()); }
		const_iterator begin() const { return const_iterator(GetData()); }
		const_iterator end() const { return const_iterator(m_get_data_end()); }
		iterator begin(const ImageSite& p) { return iterator(GetData()+m_offset(p)); }
		iterator end(const ImageSite& p) { return iterator(GetData()+m_offset(p)+1); }
		const_iterator begin(const ImageSite& p) const { return const_iterator(GetData()+m_offset(p)); }
		const_iterator end(const ImageSite& p) const { return const_iterator(GetData()+m_offset(p)+1); }
		///@}

	protected:
		/// Converts the image site p into a memory offset 
		inline long m_offset( const ImageSite& p) const { return p(0)+3*(p(1)+GetWidth()*p(2)); } 
		/// Converts the image coord x,y into a memory offset (first channel)
		inline long m_offset( int x, int y ) const { return 3*(x+GetWidth()*y); } 
		/// Converts the image coord x,y into a memory offset (first channel)
		inline long m_offset( int c, int x, int y ) const { return c + 3*(x+GetWidth()*y); } 
		/// Returns a pointer on the end of the data+1
		inline unsigned char* m_get_data_end() const { return GetData()+3*GetWidth()*GetHeight()+1; }
		/// Auto-iterator
		unsigned char* m_cur;
		
	};
	//==========================================================================================
	// EO class ImageRGB
	//==========================================================================================




};
//==========================================================================================
// EO namespace lgl
//==========================================================================================


//==========================================================================================
#endif /// #ifdef _LGLWX
//==========================================================================================
// EOF
//==========================================================================================
#endif


