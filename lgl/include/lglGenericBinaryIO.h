/* 

lGl : A C++ library needed by the multiscale image segmentation library SxS
� 2004 Laurent Guigues (laurent.guigues@ign.fr)

This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.

You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
/*! \file 
	\brief Template methods for generic stream IO, i.e. which properly cast input type into output type and swap bytes according to the machine type in order to always get back to Intel coding 
*/
#ifndef __lglGenericBinaryIO_h__
#define __lglGenericBinaryIO_h__
//===========================================================
// namespace	: lgl
// classe		: -
// template methods for generic stream IO, i.e. which properly cast input type into output type and swap bytes according to the machine type in order to always get back to Intel coding 
//===========================================================
// author		: Laurent Guigues
// history		: 
//	28/07/04	: creation
//===========================================================
#include "lglSystem.h"
#include <fstream>

//====================================================================
/// Read in f a value of type Tin and casts it in type Tout. Handles little big/endians.
/// The Machine::Test() must have been done.
inline template <class Tin, class Tout> 
void gb_read ( std::ifstream& f, Tin, Tout& v)
{
	Tin in;
	f.read((char*)&in,sizeof(in));
#ifdef __MAC__
	System::swapEndians(in);
#endif
	v = (Tout)in;
}
//====================================================================


//====================================================================
/**
* Lit dans f nb valeurs de type Tin et la cast en Tout 
* Gere les little/big indians 
*/
template <class Tin, class Tout> 
void gb_read ( std::ifstream& f, Tin, Tout* v, int nb)
{
	if (InfoType<Tin>::NomType()==InfoType<Tout>::NomType()) {
		f.read((char*)v,nb*sizeof(Tout));
#ifdef __MAC__
		for (int i=0;i<nb;++i,++v) System::swapEndians(*v);
#endif
	}
	else {
		Tin* in = new Tin[nb];
        Tin* min = in;
		f.read((char*)in,nb*sizeof(Tin));
		for (int i=0;i<nb;++i,++v,++in) {
#ifdef __MAC__
			System::swapEndians(*in);
#endif
			*v = (Tout)*in;
		}
		delete[] min;
	}
}
//====================================================================


//====================================================================
/**
* Lit dans f une valeur de type eT et la cast en Tout 
* Gere les little/big indians 
*/
template <class Tout> 
void gb_read_et ( std::ifstream& f, enumType eT, Tout& v)
{
	switch (eT) {
		//case ETBool :  { bool  a=0; gb_read(f,a,v); break; }
		case ETChar :  { Char  a=0; gb_read(f,a,v); break; }
		case ETInt1 :  { Int1  a=0; gb_read(f,a,v); break; } 
		case ETUInt1 : { UInt1 a=0; gb_read(f,a,v); break; }
		case ETInt2 :  { Int2  a=0; gb_read(f,a,v); break; } 
		case ETUInt2 : { UInt2 a=0; gb_read(f,a,v); break; } 
		case ETInt4 :  { Int4  a=0; gb_read(f,a,v); break; } 
		case ETUInt4 : { UInt4 a=0; gb_read(f,a,v); break; } 
		case ETFloat4 :{ Float4 a=0; gb_read(f,a,v); break; }
		case ETFloat8 :{ Float8 a=0; gb_read(f,a,v); break; }
		default : {
			TChaine mess = TChaine("gb_read(f,")+InfoType<Tout>::NomType(eT)+TChaine(",")+InfoType<Tout>::NomType()+TChaine(") impossible");
			THROWN(Erreur,mess); 
				  }
	}
}
//====================================================================



//====================================================================
/**
* Lit dans f nb valeurs de type eT et les caste en Tout 
* Gere les little/big indians 
*/
template <class Tout> 
void gb_read_et ( std::ifstream& f, enumType eT, Tout* v, int nb)
{
	switch (eT) {
		//case ETBool : { gb_read<Bool,Tout>(f,v,nb); break; }
		case ETChar : { gb_read<Char,Tout>(f,v,nb); break; }
		case ETInt1 : { gb_read<Int1,Tout>(f,v,nb); break; } 
		case ETUInt1 : { gb_read<UInt1,Tout>(f,v,nb); break; }
		case ETInt2 : { gb_read<Int2,Tout>(f,v,nb); break; } 
		case ETUInt2 : { gb_read<UInt2,Tout>(f,v,nb); break; } 
		case ETInt4 : { gb_read<Int4,Tout>(f,v,nb); break; } 
		case ETUInt4 : { gb_read<UInt4,Tout>(f,v,nb); break; } 
		case ETFloat4 : { gb_read<Float4,Tout>(f,v,nb); break; }
		case ETFloat8 : { gb_read<Float8,Tout>(f,v,nb); break; }
		default : {
			TChaine mess = TChaine("gb_read(f,")+InfoType<Tout>::NomType(eT)+TChaine(",")+InfoType<Tout>::NomType()+TChaine(",")+TChaine(nb)+TChaine(") impossible");
			THROWN(Erreur,mess); 
				  }
	}
}
//====================================================================


//====================================================================
/**
* Ecrit dans f une valeur de type Tin castee en Tout 
* Gere les little/big indians 
*/
template <class Tin, class Tout> 
void gb_write ( std::ofstream& f, Tin v, Tout)
{
	Tout out = (Tout)v;
#ifdef __MAC__
	System::swapEndians(out);
#endif
	f.write((char*)&out,sizeof(out));
}
//====================================================================


//====================================================================
/**
* Ecrit dans f nb valeurs de type Tin castees en Tout 
* Gere les little/big indians 
*/
template <class Tin, class Tout> 
void gb_write ( std::ofstream& f, Tin* v, int nb, Tout)
{
	if (InfoType<Tin>::NomType()==InfoType<Tout>::NomType()) {
#ifdef __MAC__
		Tout* vv = new Tout[nb];
		Tout* mvv = vv;
        for (int i=0;i<nb;++i,++v,++mvv) {
			*mvv = (Tout)*v;
            System::swapEndians(*mvv);
		}
        f.write((char*)vv,nb*sizeof(Tout));
        delete[] vv;
#else
		f.write((char*)v,nb*sizeof(Tin));
#endif
	}
	else {
		Tout* vv = new Tout[nb];
        Tout* mvv = vv;
		for (int i=0;i<nb;++i,++v,++mvv) {
			*mvv = (Tout)*v;
#ifdef __MAC__
			System::swapEndians(*mvv);
#endif
		}
		f.write((char*)vv,nb*sizeof(Tout));
		delete[] vv;
	}
}
//====================================================================



//====================================================================
/**
* Ecrit dans f une valeur de type Tin castee dans le type correspondant � eT 
* Gere les little/big indians 
*/
template <class Tin> 
void gb_write_et ( std::ofstream& f, Tin v, enumType eT)
{
	switch (eT) {
	//case ETBool :  { bool a=0; gb_write(f,v,a); break; }
	case ETChar :  { Char a=0; gb_write(f,v,a); break; }
	case ETInt1 :  { Int1 a=0; gb_write(f,v,a); break; } 
	case ETUInt1 : { UInt1 a=0; gb_write(f,v,a); break; }
	case ETInt2 :  { Int2 a=0; gb_write(f,v,a); break; } 
	case ETUInt2 : { UInt2 a=0; gb_write(f,v,a); break; } 
	case ETInt4 :  { Int4 a=0; gb_write(f,v,a); break; } 
	case ETUInt4 : { UInt4 a=0; gb_write(f,v,a); break; } 
	case ETFloat4 :{ Float4 a=0; gb_write(f,v,a); break; }
	case ETFloat8 :{ Float8 a=0; gb_write(f,v,a); break; }
	default : {
		TChaine mess = TChaine("gb_write(f,")+InfoType<Tin>::NomType()+TChaine(",")+InfoType<Tin>::NomType(eT)+TChaine(") impossible");
		THROWN(Erreur,mess); 
			  }
	}
}
//====================================================================



//====================================================================
/**
* Ecrit dans f nb valeurs de type Tin castees dans le type correspondant � eT 
* Gere les little/big indians 
*/
template <class Tin> 
void gb_write_et ( std::ofstream& f, Tin* v, enumType eT, int nb)
{
	switch (eT) {
	//case ETBool :  { bool a=0; gb_write(f,v,nb,a); break; }
	case ETChar :  { Char a=0; gb_write(f,v,nb,a); break; }
	case ETInt1 :  { Int1 a=0; gb_write(f,v,nb,a); break; } 
	case ETUInt1 : { UInt1 a=0; gb_write(f,v,nb,a); break; }
	case ETInt2 :  { Int2 a=0; gb_write(f,v,nb,a); break; } 
	case ETUInt2 : { UInt2 a=0; gb_write(f,v,nb,a); break; } 
	case ETInt4 :  { Int4 a=0; gb_write(f,v,nb,a); break; } 
	case ETUInt4 : { UInt4 a=0; gb_write(f,v,nb,a); break; } 
	case ETFloat4 :{ Float4 a=0; gb_write(f,v,nb,a); break; }
	case ETFloat8 :{ Float8 a=0; gb_write(f,v,nb,a); break; }
	default : {
		TChaine mess = TChaine("gb_write(f,")+InfoType<Tin>::NomType()+TChaine(",")+InfoType<Tin>::NomType(eT)+TChaine(",")+TChaine(nb)+TChaine(") impossible");
		THROWN(Erreur,mess); 
		  }
	}
}
//====================================================================

#endif
