/* 

lGl : A C++ library needed by the multiscale image segmentation library SxS
© 2004 Laurent Guigues (laurent.guigues@ign.fr)

This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.

You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
/**	\file	
	\brief A polygonal approximation algorithm 
**/
#ifndef __lglPolygonalSimplification_h__
#define __lglPolygonalSimplification_h__

//===========================================================
// namespace	        : lgl
// class	        : PolygonalSimplification<T>
//===========================================================
// author		: Laurent Guigues
// history		: 
//	13/12/04	: creation
//===========================================================
#include "lglGeneral.h"
#include <vector>
#include "lglIndexedHeap.h"

namespace lgl
{

  //#include "outils_base/matrice.h"
  //#include "recipes/jacobi.h"



  //========================================================================================================
  /// \brief A polygonal approximation algorithm based on iterated segment merging following a criterion 
  /// of variance in the orthogonal direction to the line. The class is template on the point type which must 
  /// own an operator[] to access the ith coordinate. 
  /// 
  /// * Naturally manages closed lines (the result independent of the starting point)
  /// * Does not delocalise angles 
  /// * Fast : Worst case ~ n.log(n) / Average ~ n 
  template <class T>
    class PolygonalSimplification
    {
      
      /// \brief Internal structure : a point of a polyline to be simplified 
      class Point
	{
    public:
      typedef PolygonalSimplification<T> self;
	public:
	  /// Ctor 
	  Point() : m_heapid(-1), m_vectorid(0), m_succ(0), m_pred(0), m_cost(-1), 
	    m_N(0), m_SX(0), m_SY(0), m_SXX(0), m_SYY(0), m_SXY(0), m_X(0), m_Y(0) {} 
	  /// Initialization
	  inline void init( int vectorid, Point* pred, Point* succ, F32 X, F32 Y ) {
	    m_vectorid=vectorid; m_pred=pred; m_succ=succ; 
	    m_N=1; m_X=m_SX=X; m_Y=m_SY=Y; m_SXX=X*X; m_SYY=Y*Y; m_SXY=X*Y; }
	  
	  /// Computes the cost of deleting this point
	  F32 computeCost(BOOL center_on_chord); 
	  
	  /// Suppresses this point from the line (its successor is updated)
	  inline void suppress() {
	    m_succ->m_SX += m_SX;
	    m_succ->m_SY += m_SY;
	    m_succ->m_SXX += m_SXX;
	    m_succ->m_SYY += m_SYY;
	    m_succ->m_SXY += m_SXY;
	    m_succ->m_N   += m_N;
	    if (m_pred) m_pred->m_succ = m_succ;
	    m_succ->m_pred = m_pred;
	  }
	  
	  //	protected:
	  // heap index 
	  I32 m_heapid;
	  // vector index
	  I32 m_vectorid;
	  // successor, predecessor
	  Point *m_succ,*m_pred;
	  // merging cost 
	  F32 m_cost;

	  // Statistics on the points hold by the point (previous points, itself included)
	  // number
	  I32 m_N;
	  // sums, quadratic sums of components
	  F32 m_SX,m_SY,m_SXX,m_SYY,m_SXY;
	  // coordinates
	  F32 m_X,m_Y;
	};
      // EO class Point
      
    public:
      typedef PolygonalSimplification<T> self;

      /// Ctor
      PolygonalSimplification() {}
      
      /// Type of lines
      typedef enum { Open=0, Closed=1, AutoDetect=2 } LineType;
      
      /// \brief Returns out, the approximation of the polyline in at a maximal distance distance. 
      /// 
      /// \param center_on_chord tells wether the deviation is computed from the middle of the chord or form the barycenter of the points. 
      /// \param type gives the type of line among  Open, Closed or AutoDetect (end points coincide)
      static void Approximate( const std::vector<T>& in, 
			       F32 distance, 
			       std::vector<T>& out,
			       BOOL center_on_chord = TRUE,
			       LineType type = AutoDetect );

           
      //      static self& StaticInstance() { static self S; return S; }
      
      static inline bool IsClosed( const std::vector<T>& p );
      
    protected:
      /// Comparator for the IndexedHeap
      class Comparator
	{
	public:
	  Comparator() {}
	  /// Comparison function for the IndexedHeap used in the algorithm
	  BOOL operator() ( Point* e1, Point* e2) const { return e1->m_cost < e2->m_cost; } 
	};
     /// Indexer for the IndexedHeap
      class Indexer
	{
	public:
	  Indexer() {}
	  /// Comparison function for the IndexedHeap used in the algorithm
	  I32& operator() ( Point* e ) const { return e->m_heapid; }
	};
    };
  // EO class PolygonalSimplification
  //========================================================================================================

  /*
	  // heap index 
	  I32 m_heapid;
	  // vector index
	  I32 m_vectorid;
	  // successor, predecessor
	  MergingPoint* m_succ, m_pred;
	  // merging cost 
	  F32 m_cost;

	  // Statistics on the points hold by the point (previous points, itself included)
	  // number
	  I32 m_N;
	  // sums, quadratic sums of components
	  F32 m_SX,m_SY,m_SXX,m_SYY,m_SXY;
	  // coordinates
	  F32 m_X,m_Y;
  */
  //========================================================================================================
  template <class T>
    F32 PolygonalSimplification<T>::Point::computeCost(BOOL center_on_chord)
    {
      // chord
      F32 m2x = m_succ->m_X - m_pred->m_X;
      F32 m2y = m_succ->m_Y - m_pred->m_Y;
      F32 n2 = sqrt(m2x*m2x+m2y*m2y);
      if (n2==0) return 2;
      // loop ? 
      if (m_pred->m_pred != 0) {
        if ( (m_succ->m_X - m_pred->m_pred->m_X==0) &&
             (m_succ->m_Y - m_pred->m_pred->m_Y==0) ) return 2; 
      }  
      // The fusion point must project on the chord
      F32 m1x = m_X - m_pred->m_X;
      F32 m1y = m_Y - m_pred->m_Y;
      F32 n1 = sqrt(SQR(m1x)+SQR(m1y));
      F32 scal = (m1x*m2x+m1y*m2y)/(n1*n2);
      if ((scal<-0.01)||(scal>1.01)) return 2;
      // union
      F32 sumX = m_SX + m_succ->m_SX;
      F32 sumY = m_SY + m_succ->m_SY;
      F32 sumX2 = m_SXX + m_succ->m_SXX;
      F32 sumY2 = m_SYY + m_succ->m_SYY;
      F32 sumXY = m_SXY + m_succ->m_SXY;
      F32 n = m_N + m_succ->m_N;
      // quadratic deviation
      F32 a,b,c;
      if (center_on_chord) {
        // centered on the chord 
        F32 cx = 0.5* ( m_pred->m_X + m_succ->m_X);
        F32 cy = 0.5* ( m_pred->m_Y + m_succ->m_Y);
        a = sumX2 + cx * ( n*cx - 2*sumX );
	b = sumXY + n*cx*cy - cy*sumX - cx*sumY;
	c = sumY2 + cy * ( n*cy - 2*sumY );
      }
      else {
        // centered on the barycenter 
        a = sumX2 - (sumX*sumX)/n;
	b = sumXY - (sumX*sumY)/n;
	c = sumY2 - (sumY*sumY)/n;
      }
      // Quadratic dev in the orthogonal dir to the chord 
      F32 dx = - m2y / n2;
      F32 dy =   m2x / n2;
      // Inverse var/covar
      F32 det = b*b-a*c;
      // V = 1 / tX*S-1*X
      F32 xa = -c*dx + b*dy;
      F32 ya = b*dx - a*dy;
      F32 div = ( dx*xa + dy*ya );
      F32 val = 0;
      if (div!=0) val = det / div;

      return val ;
    }
  //========================================================================================================

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//template <class T>
//void PQ_Regression_Polygonale<T>::Simplifie ( 
//  PILE<T>& P0, F32 dist, PILE<T>& P1, bool corde, TypeLigne type)


  //========================================================================================================
  template <class T>
   void PolygonalSimplification<T>::Approximate( const std::vector<T>& in, 
					 F32 distance, 
					 std::vector<T>& out,
					 BOOL center_on_chord,
					 LineType type)
    {
      
      bool circular = false;
      if (type==Closed) circular = true;
      else if (type==AutoDetect) circular = IsClosed( in );
      
      //    distance *= 2;
      int i,n = in.size();
      int npmin = 3;
      if (circular) npmin = 4;
      if (n<npmin) { 
	out = in;
	return;
      } 
      // Tableau de points de fusion
      std::vector < Point > P(n);
      // Remplissage
      for (i=0; i<n; ++i) {
	Point* pred = NULL;
	if (i>0) pred = &(P[i-1]);
	Point* succ = NULL;
	if (i<n-1) succ = &(P[i+1]);
	P[i].init( i, pred, succ, in[i][0] / distance, in[i][1] / distance );
      } 
      // Mode circulaire : bouclage de la liste
      int deb = 1;
      int fin = n-1;
      if (circular) {
	// Premier noeud 
	Point* pred = &P[fin];
	Point* succ = &P[1];
	P[0].init( 0, pred, succ, in[0][0] / distance, in[0][1] / distance);
	pred = &P[fin-1];
	succ = &P[0];
	P[fin].init( fin, pred, succ, in[fin][0] / distance, in[fin][1] / distance );
	deb = 0;
	fin = n;
      }  
      // Nombre d'elements restant
      int nb = fin-deb;
      // Priority Queue
      Comparator comparator;
      Indexer indexer;
      IndexedHeap < Point*, Comparator, Indexer > Queue(comparator,indexer);
      // Remplissage
      for (i=deb; i<fin; ++i) {
	F32 c = P[i].computeCost(center_on_chord);
	if (c<1) {
	  P[i].m_cost = c;
	  Point * pf = &(P[i]);
	  Queue.insert(pf);
	}
      }
      Point * First = &(P[deb]);
      // Vidage
      //      PILE<T> PS;
      while ((Queue.size()!=0)&&(nb>=npmin-2)) {
	Point * F = Queue.remove_top();
	if (F->m_cost > 1) break;
	if (First==F) {
	  if (F->m_succ!=NULL) First = F->m_succ;
	  else First = F->m_pred;
	}
	F->suppress();
	nb--;
	// Maj pred et succ
	F32 cost;
	int pos;
	Point * G = F->m_pred;
	if ((G!=0)&&((G->m_heapid>0)||(circular))) {
	  pos = G->m_heapid;
	  cost = G->m_cost;
	  G->m_cost = G->computeCost(center_on_chord);
	  if (pos>=0) {
	    if ( cost > 1 ) Queue.remove(pos);
	    else if ( cost < G->m_cost ) Queue.downsort(pos);
	    else Queue.upsort(pos);
	  }
	  else if ( G->m_cost < 1) Queue.insert(G);
	}
	G = F->m_succ;
	if (G->m_succ!=0) {
	  pos = G->m_heapid;
	  cost = G->m_cost;
	  G->m_cost = G->computeCost(center_on_chord);
	  if (pos>=0) {
  	    if ( cost > 1 ) Queue.remove(pos);
	    else if ( cost < G->m_cost ) Queue.downsort(pos);
	    else Queue.upsort(pos);
	  }
	  else if ( G->m_cost < 1) Queue.insert(G);
	}    
      }
      // Construction ligne approx
      out.clear();
      if (!circular) out.push_back(in[0]);
      Point * pCur=First;
      for (i=0;i<nb;i++) {
	out.push_back(in[pCur->m_vectorid]);
	pCur=pCur->m_succ;
      }
      if (!circular) {
	out.push_back(in[n-1]);
      }  
      else {
	if (in[0]==in[n-1]) {
	  if (out.size()>0) {
	    T tmp(out[0]);
	    out.push_back(tmp);
	  }  
	  else {
	    out.push_back(in[0]);
	  }  
	}  
      }
    }
  //========================================================================================================


  //========================================================================================================
  template <class T>
    bool PolygonalSimplification<T>::IsClosed (  const std::vector<T>& in ) 
    {
      if (in.size()<2) return false;     
      return ((bool)((in[0][0]==in[in.size()-1][0])&&(in[0][1]==in[in.size()-1][1])));
    }
  //========================================================================================================
 

}
// EO namespace lgl
#endif
