
namespace lgl {


void Machine::testEndians () 
{
  int n = 1 ;
  char * p = (char *) & n ;
  m_Intel = (*p == (char) 1) ;
}

void Machine::testBusSize () 
{
  if (sizeof(void *) == 4) 
    {
      m_machine32 = true ;
      m_machine64 = false ;
    } 
  else if (sizeof(void *) == 8) 
    {
      m_machine32 = false ;
      m_machine64 = true ;
      std::cout << "A 64 bits machine ??? Waooo !!! Does it exist ???" << std::endl ;
    } 
  else 
    std::cout << "lgl::Machine::testBusSize : neither a 32 bits nor a 64 bits machine... Is it a MD12 of 1968 ?" << std::endl ;
}


void Machine::swapEndians (unsigned short & us) {
  unsigned char * p = (unsigned char *) & us ;
  unsigned char r ;
  r = p[0] ;
  p[0] = p[1] ;
  p[1] = r ;
}

void Machine::swapEndians (short & us) {
  char * p = (char *) & us ;
  char r ;
  r = p[0] ;
  p[0] = p[1] ;
  p[1] = r ;
}

void Machine::swapEndians (unsigned int & us) {
  unsigned char * p = (unsigned char *) & us ;
  unsigned char r ;
  r = p[0] ;
  p[0] = p[3] ;
  p[3] = r ;
  r= p[1] ;
  p[1] = p[2] ;
  p[2] = r ;
}

void Machine::swapEndians (int & us) {
   char * p = ( char *) & us ;
   char r ;
  r = p[0] ;
  p[0] = p[3] ;
  p[3] = r ;
  r= p[1] ;
  p[1] = p[2] ;
  p[2] = r ;
}

void Machine::swapEndians (unsigned long & us) 
{
  unsigned char * p = (unsigned char *) & us ;
  unsigned char r ;
  r = p[0] ;
  p[0] = p[3] ;
  p[3] = r ;
  r= p[1] ;
  p[1] = p[2] ;
  p[2] = r ;
}

void Machine::swapEndians (long & us) 
{
  unsigned char * p = (unsigned char *) & us ;
  unsigned char r ;
  r = p[0] ;
  p[0] = p[3] ;
  p[3] = r ;
  r= p[1] ;
  p[1] = p[2] ;
  p[2] = r ;
}

void Machine::swapEndians (float & us) 
{
  unsigned char * p = (unsigned char *) & us ;
  unsigned char r ;
  r = p[0] ;
  p[0] = p[3] ;
  p[3] = r ;
  r= p[1] ;
  p[1] = p[2] ;
  p[2] = r ;
}

void Machine::swapEndians (double & us) 
{
  unsigned char * p = (unsigned char *) & us ;
  unsigned char r ;
  r = p[0] ;
  p[0] = p[7] ;
  p[7] = r ;
  r= p[1] ;
  p[1] = p[6] ;
  p[6] = r ;
  r= p[2] ;
  p[2] = p[5] ;
  p[5] = r ;
  r= p[3] ;
  p[3] = p[4] ;
  p[4] = r ;
}
}