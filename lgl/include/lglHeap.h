/* 

lGl : A C++ library needed by the multiscale image segmentation library SxS
� 2004 Laurent Guigues (laurent.guigues@ign.fr)

This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.

You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
/*! \file 
	\brief Priority queues handled by binary trees
*/
#ifndef __lglHeap_h__
#define __lglHeap_h__
//===========================================================
// namespace	: lgl
// class		: Heap
//	Priority queues handled by binary trees 
//	Allow :
//		log(n) insertion
//		constant time acces to the first element
//		log(n) removal of the first element
//		log(n) priority change of a random element 
// Comparator must be a BinaryFunction<T,T,Bool> returning TRUE 
// iff x has a greater priority than y (see e.g. Less<T> in lglFunction.h).
//===========================================================
// author		: Laurent Guigues
// history		: 
//	04/10/99		first release at IGN/MATIS
//	09/07/04		reshaped
//===========================================================

#include "lglGeneral.h"
//#include "lglFunction.h"
#include <vector>
#include <functional>
#include <algorithm>


namespace lgl {
  
  template < class T, class Comparator > class Heap ;
  template < class T, class Comparator > std::ostream& operator << (std::ostream&, const Heap<T,Comparator>& );
  
  
  
  //==========================================================================================
  // Heap 
  //==========================================================================================
  /// \brief Priority queues handled by binary trees 
  ///
  ///	Allows :
  ///		- log(n) insertion
  ///		- constant time acces to the first element
  ///		- log(n) removal of the first element
  ///		- log(n) priority change of a random element 
  ///
  /// Comparator must be a BinaryFunction<T,T,Bool> returning TRUE 
  /// iff x has a greater priority than y (see e.g. Less<T> in lglFunction.h).
  template <class T, class Comparator = Less<T> > class Heap 
    {
      public :
      //friend std::ostream& operator << (std::ostream&, const Heap<T,Comparator>& );

      //===========================================================================
      /// Default constructor 
      Heap() : m_c(NULL) {}
      /// Constructor with a comparator 
      Heap( const Comparator& c ) ;
      /// Destructor  
      ~Heap() {}
      /// sets the Comparator  
      void set(const Comparator& c); 
      //===========================================================================

      //===========================================================================
      /// inserts an element in the Heap and returns its position 
      I32 insert(T);
      /// return a reference on the first element of the Heap 
      T& top(); 
      /// return a constant reference on the first element of the Heap 
      const T& top() const;  
      /// removes and returns the first element of the Heap 
      T remove_top();
      /// removes and returns the nth element 
      T remove(I32 n);
      /// returns the size of the Heap 
      inline I32 size() const {return m_p.size(); }
      /// empties the Heap 
      void clear() { m_p.clear(); }
      //===========================================================================
    
      //===========================================================================
      /// returns a constant reference on the stack of elements 
      const std::vector<T> & stack() const {return m_p;}
      /// returns a reference to the ith element of the stack 
      T& operator [] (I32 i) { lglASSERT( (i>=0)&&(i<size()) ) return m_p[i];}
      /// returns a constant reference to the ith element of the stack 
      const T& operator [] (I32 i) const { lglASSERT( (i>=0)&&(i<size()) ) return m_p[i];}
      //===========================================================================

      //===========================================================================
      /// returns the position of the father of i 
      inline I32 father( I32 i ) const;
      /// returns the position of the right son of i 
      inline I32 rightson( I32 i ) const;
      /// returns the position of the leftson of i 
      inline I32 leftson( I32 i ) const;
      /// swaps the ith and jth elements 
      inline void swap(I32 i, I32 j);
      /// moves up the ith element while it is not upward sorted. returns its final position.
      inline I32 upsort(I32);
      /// moves down the ith element while it is not downward sorted. returns its final position.
      inline I32 downsort(I32);
      //===========================================================================

      //===========================================================================
      private : 
      /// binary tree handled by a vector 
      std::vector<T> m_p;
      /// comparator 
      const Comparator* m_c;
      //===========================================================================

    };
  //==========================================================================================
  // EO class Heap 
  //==========================================================================================



  //==========================================================================================
  // template codes inclusion
  //==========================================================================================
#include "lglHeap_code.h"


};
//==========================================================================================
// EO namespace lgl
//==========================================================================================


//==========================================================================================
// EOF
//==========================================================================================
#endif


