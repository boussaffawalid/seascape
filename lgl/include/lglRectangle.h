/* 

lGl : A C++ library needed by the multiscale image segmentation library SxS
� 2004 Laurent Guigues (laurent.guigues@ign.fr)

This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.

You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
/**	\file	
	\brief 2D points
**/
#ifndef __lglRectangle_h__
#define __lglRectangle_h__
//===========================================================
// namespace	: lgl
// classe		: Rectangle<T>
// 2D rectangles
//===========================================================
// author		: Laurent Guigues
// history		: 
//	28/07/04	: creation
//===========================================================
#include "lglGeneral.h"
#include <iostream>

namespace lgl
{
  

  
  
  //==========================================================================================
  /// Rectangles
  template <class T> class Rectangle
    {
    public:
      /// Builds the rectangle x1 y1 x2 y2 
      Rectangle( T x1=0, T y1=0, T x2=0, T y2=0 ) { m_v[0]=Point2D<T>(x1,y1); m_v[1]=Point2D<T>(x2,y2); }
      /// Builds the rectangle v1 v2
      Rectangle( const Point2D<T> v0, const Point2D<T>& v1 ) { m_v[0]=v0; m_v[1]=v1; }
      /// Copy ctor
      Rectangle( const Rectangle& p ) { m_v[0]=p.m_v[0]; m_v[1]=p.m_v[1]; }
      /// Operator =
      inline Rectangle& operator = ( const Rectangle& p ) { m_v[0]=p.m_v[0]; m_v[1]=p.m_v[1]; return *this; }
      /// Operator = with a value : sets all coordinates of the rectangle to v. 
      inline Rectangle& operator = ( T v) { m_v[0] = m_v[1] = v; return *this; }
      /// Operator == 
      inline BOOL operator == ( const Rectangle& p ) const { return ((m_v[0]==p.m_v[0])&&(m_v[1]==p.m_v[1])); }
      /// Operator != 
      inline BOOL operator != ( const Rectangle& p ) const { return ((m_v[0]!=p.m_v[0])||(m_v[1]!=p.m_v[1])); }
      /// Operator < : tests wether all coordinates of *this are inferior to those of p. 
      inline BOOL operator < ( const Rectangle& p ) const { return ((m_v[0]<p.m_v[1])&&(m_v[1]<p.m_v[1])); }
      /// Operator < : tests wether all coordinates of *this are inferior or equal to those of p. 
      inline BOOL operator <= ( const Rectangle& p ) const { return ((m_v[0]<=p.m_v[1])&&(m_v[1]<=p.m_v[1])); }
      /// Operator < : tests wether all coordinates of *this are superior to those of p. 
      inline BOOL operator > ( const Rectangle& p ) const { return ((m_v[0]>p.m_v[1])&&(m_v[1]>p.m_v[1])); }
      /// Operator < : tests wether all coordinates of *this are superior or equal to those of p. 
      inline BOOL operator >= ( const Rectangle& p ) const { return ((m_v[0]>=p.m_v[1])&&(m_v[1]>=p.m_v[1])); }
      
      /// returns the rectangle containing *this and p (the coordinates must be ordered)
      inline Rectangle<T> operator | (const Rectangle<T>& p) const { return Rectangle<T>(min(m_v[0](0),p.m_v[0](0)),
											 min(m_v[0](1),p.m_v[0](1)),
											 max(m_v[1](0),p.m_v[1](0)),
											 max(m_v[1](1),p.m_v[1](1))); }
      /// returns the intersection between *this and p (the coordinates must be ordered)
      inline Rectangle<T> operator & (const Rectangle<T>& p) const { return Rectangle<T>(max(m_v[0](0),p.m_v[0](0)),
											 max(m_v[0](1),p.m_v[0](1)),
											 min(m_v[1](0),p.m_v[1](0)),
											 min(m_v[1](1),p.m_v[1](1))); }
      
      /// returns the sum of the two points (sum of coordinates)
      inline Rectangle<T> operator + (const Rectangle<T>& p) const { return Rectangle<T>(m_v[0]+p.m_v[0],m_v[1]+p.m_v[1]); }
      /// adds the point p to this (sum of coordinates)
      inline void operator += (const Rectangle<T>& p) { m_v[0]+=p.m_v[0]; m_v[1]+=p.m_v[1]; }
      /// returns the difference of the two points (sum of coordinates)
      inline Rectangle<T> operator - (const Rectangle<T>& p) const { return Rectangle<T>(m_v[0]-p.m_v[0],m_v[1]-p.m_v[1]); }
      /// substracts the point p to this (substraction of coordinates)
      inline void operator -= (const Rectangle<T>& p) { m_v[0]-=p.m_v[0]; m_v[1]-=p.m_v[1]; }

      /// multiplies the rectangle coordinates by x
      inline void operator *= (T x) { m_v[0]*=x; m_v[1]*=x; }
      
      /// Return a ref on the ith point of the rectangle
      inline Point2D<T>& operator () ( I32 i ) { return m_v[i]; }
      /// Return a const ref on the ith point of the rectangle
      inline const Point2D<T>& operator () ( I32 i ) const { return m_v[i]; }
      
      
    protected:
      /// coordinates 
      Point2D<T>	m_v[2];
    };
  //==========================================================================================
  // EO class Rectangle
  //==========================================================================================
  
  template <class T>
    std::ostream& operator << (std::ostream& s, const Rectangle<T>& p ) {
    s << "(" << p(0) << "," << p(1) <<")";
    return s;
  }
  
  /// Integer point
  typedef Rectangle<I32>	IRectangle;
  /// Float point
  typedef Rectangle<F32>	FRectangle;
  
  
};
//==========================================================================================
// EO namespace lgl
//==========================================================================================


//==========================================================================================
// EOF
//==========================================================================================
#endif


