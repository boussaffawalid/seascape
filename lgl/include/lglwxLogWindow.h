/* 

lGl : A C++ library needed by the multiscale image segmentation library SxS
� 2004 Laurent Guigues (laurent.guigues@ign.fr)

This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.

You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
/**	\file	
	\brief Log window (based on wxLogWindow) associated with a Frame
**/
#ifndef __lglwxLogWindow_h__
#define __lglwxLogWindow_h__
//===========================================================
// namespace	: lglwx
// classe		: LogWindow
//===========================================================
// author		: Laurent Guigues
// history		: 
//	09/09/04	: creation 
//===========================================================
#ifdef __LGLWX__

#include "lglGeneral.h"

namespace lgl
{

  class LogWindowOfFrame;

  class FrameWithLog : public wxFrame
  {
  public:
    inline FrameWithLog( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_FRAME_STYLE, const wxString& name = _T("frame"));

    inline virtual void OnLogOpenClose(wxCommandEvent& event) {}
    inline wxLogWindow* getLogWindow();  
  protected :
    /// Log window 
    LogWindowOfFrame *m_log;
  };


  // ----------------------------------------------------------------------------
  // Log window
  class LogWindowOfFrame : public wxLogWindow
  {
  public:
    LogWindowOfFrame( FrameWithLog *parent, const wxString& title, BOOL show, BOOL pass, const wxSize& size) : 
      wxLogWindow(parent,title,show,pass), m_parent(parent) 
      { 
	GetFrame()->SetClientSize(size);
      } 
		  
      //
      virtual bool OnFrameClose ( wxFrame* f ) { 
	wxCommandEvent e;
	m_parent->OnLogOpenClose(e);
	return true;
      }
		  
      FrameWithLog* m_parent;
  };
  // ----------------------------------------------------------------------------

  FrameWithLog::FrameWithLog( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style, const wxString& name) 
    : wxFrame(parent,id,title,pos,size,style,name) 
    {
      m_log = new LogWindowOfFrame(this,_T("log"),TRUE,FALSE,wxSize(400,150));
    }

    wxLogWindow* FrameWithLog::getLogWindow() { return m_log; }
    
}
//===========================================================
// EO namespace lgl
//===========================================================

#endif
// #ifdef __LGLWX__
//===========================================================
// EOF
//===========================================================
#endif


