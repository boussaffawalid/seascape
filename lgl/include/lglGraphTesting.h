/* 

lGl : A C++ library needed by the multiscale image segmentation library SxS
� 2004 Laurent Guigues (laurent.guigues@ign.fr)

This program is free software; you can use, modify and/or redistribute it 
under the terms of the CeCILL license, which is a French law-Compatible and 
GNU GPL-Compatible free software license published by CEA, CNRS and INRIA.

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited liability.
See the CeCILL license for more details.

You should have received a copy of the CeCILL license along with this software
(files CeCILL_LICENCE_V1-fr.txt (french) and CeCILL_LICENSE_V1-en.txt (english)).
If not visit http://www.cecill.info.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

*/
/*! \file 
	\brief Freehand methods performing various tests on graphs
*/
#ifndef __sxsGraphTesting_h__
#define __sxsGraphTesting_h__
//=============================================================================
// namespace : sxs
// class     : -
// freehand methods performing various tests on graphs :
//  - GraphHasLoops
//  - GraphIsSimple
// author    : Laurent Guigues
// history   :
//  09/07/2004 Creation
//=============================================================================
#include "lglGeneral.h"
#include "lglBitManager.h"
#include "lglSparseArray.h"

namespace lgl
{
	// Returns true iff the graph has loops (edges starting and ending from the same node)
	template <class G>
	Bool GraphHasLoops( const G& g );

	// Returns true iff the graph has no multiple edge (more than one edge connecting the same nodes)
	template <class G>
	Bool GraphIsSimple( const G& g );

}
#include "lglGraphTesting_code.h"
//==========================================================================================
// EO namespace lgl
//==========================================================================================


//==========================================================================================
// EOF
//==========================================================================================
#endif


