/*! \file 

\brief Class ThISTimer : measure the time
*/
/*! 
  \class ThISTimer
  \ingroup 
*/
#ifndef __lglTimer__h__
#define __lglTimer__h__

#include <ctime>
#if defined(_WIN32) || defined(WIN32) || defined(__NT__)
	#include <time.h>
	#include <resource.h>
#else 
	#include <sys/time.h>
	#include <sys/resource.h>
#endif
#include <iostream>

namespace lgl {
//====================================================================
///  \brief Class to measure time
class Timer
{
public:

  //====================================================================
  Timer();
  void Start();
  void Stop(bool accumulate=true);
  void Reset();
  void Print(std::ostream & os) const;
  //====================================================================
  
  //====================================================================
  double GetTimeInMicroSecond()     const { return mElapsed; }
  double GetMeanTimeInMicroSecond() const { return mElapsed/mNumberOfCall; }
  int GetNumberOfCall()             const { return mNumberOfCall; }
  //====================================================================

protected:
#if defined(_WIN32) || defined(WIN32) || defined(__NT__)

#else
  rusage mBegin; 
  rusage mEnd;
#endif
  double mElapsed;
  int mNumberOfCall;
};
// EO class Timer
//====================================================================
}
#endif

