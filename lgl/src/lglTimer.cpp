/*! \file 
  \brief Implementation of Timer
*/

#ifndef lglTIMER_CC
#define lglTIMER_CC

#include "../include/lglTimer.h"

namespace lgl {
//====================================================================
/// Constructs the class 
Timer::Timer() { 
  Reset(); 
}
//====================================================================

//====================================================================
void Timer::Start() 
{
#if defined(_WIN32) || defined(WIN32) || defined(__NT__)

#else
  getrusage(RUSAGE_SELF, &mBegin);
  mNumberOfCall++;
#endif
}
//====================================================================

//====================================================================
void Timer::Stop(bool accumulate) 
{
#if defined(_WIN32) || defined(WIN32) || defined(__NT__)

#else
  getrusage(RUSAGE_SELF, &mEnd);
  if (accumulate) {
	mElapsed += (mEnd.ru_utime.tv_usec - mBegin.ru_utime.tv_usec)+
	  (mEnd.ru_utime.tv_sec - mBegin.ru_utime.tv_sec)*1000000;
  }
  else {
	mNumberOfCall--;
  }
#endif
}
//====================================================================

//====================================================================
void Timer::Print(std::ostream & os) const
{
  os << "Timer #     = " << mNumberOfCall << std::endl;
  os << "Timer total = " << mElapsed << " usec" << std::endl;
  os << "Timer mean  = " << mElapsed/mNumberOfCall << " usec" << std::endl;
}
//====================================================================

//====================================================================
void Timer::Reset() 
{
  mNumberOfCall = 0;
  mElapsed = 0;
}
//====================================================================
}
#endif

