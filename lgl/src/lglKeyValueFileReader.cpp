#include "../include/lglKeyValueFileReader.h"

namespace lgl
{
  
  bool KeyValueFileReader::read( const Filename& f, bool verbose )
  {
    if (verbose) {
      std::cout << "-------------------------------------------------------"<<std::endl;
      std::cout << "Parsing file : "<<f<<std::endl;
    }
    std::ifstream fic((const char *)f.c_str(),PARAM_IFSTREAM_TEXT);
    if (!fic.good()) {
      if (verbose) {
	std::cout << "ERROR OPENING THE FILE !"<<std::endl;
      }
      return false;
    }
    //    int l=1;
    while (!fic.eof()) {
      char line[1024];
      sprintf(line,"\n");
      fic.getline(line,1024,'\n');
      char c1[1024];
      sprintf(c1,"\n");
      sscanf(line,"%s",c1);
      String s1(c1);
      String s2(line + s1.size());
      /*
TO DO : Add right Triming
      s2.Trim(false);
      s2.Trim(true);
      */
      //    if (verbose) std::cout << "l." << l++ << " ";
      std::vector < KeyValue* >::iterator i;
      for (i=m_kv.begin();i!=m_kv.end();++i) {
	if ((*i)->Test(s1,s2,verbose)) break;
      }
      //if (verbose) std::cout << std::endl;
    }
    
    
    fic.close(); 
    std::cout << "-------------------------------------------------------"<<std::endl;
   return true;
  }


  void KeyValueFileReader::clear()
  {
    std::vector < KeyValue* >::iterator i;
    for (i=m_kv.begin();i!=m_kv.end();++i) {
      delete *i;
    }
    m_kv.clear();
  }




}


