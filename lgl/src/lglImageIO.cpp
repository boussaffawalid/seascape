#include "../include/lglImageIO.h"
#include "../include/lglAnalyzeHeader.h"
#include "../include/lglwxImageRGB.h"
#include "../include/lglImageManip.h"

namespace lgl {

  //============================================================
  /// General image loading
  BOOL readImage ( const lgl::Filename& filename, pImageRam& im )
  {
    // hdr image
    if ( filename[filename.length()-1]=='r') {
      if ( !readImageAnalyze( filename, im) ) {
	//	wxLogError(_T("Couldn't load image from '%s'."), fn.c_str());
	return false;
      }
      return true;
    }
    // other 
    else {
#ifdef __LGLWX__
      lgl::ImageRGB image;
      wxString s = lgl::std2wx(filename);
      if ( !image.LoadFile( s ) ){
	//	wxLogError(_T("Couldn't load image from '%s'."), filename.c_str());
	return false;
      }
      // conversion into an ImageRam
      ImageRamT<unsigned char>* ima = new ImageRamT<unsigned char>;
      ImageRGBToImageRam(image,*ima);
      im = ima;
      return true;
#else
      return false;
#endif
    }
  }
  //============================================================


  //============================================================
  template <class T>
  void TreadImageRaw( T, const lgl::Filename& filename, ImageSite size, pImageRam& image, 
		      BOOL rightEndian, BOOL& result )
  {
    //  lglLOG("TreadImageRaw : sizeof(t)="<<sizeof(T)<<ENDL);

    result = false;
    ImageRamT<T>* im = new ImageRamT<T>;
    if (!im->resize(size)) { delete im; return; }

    FILE* f_src = fopen((char *)filename.c_str(),"rb");
    if (!f_src) { delete im; return; }

    result = im->rawRead( f_src, rightEndian );

    fclose(f_src);

    if (!result) delete im;
    else image=im;
  }
  //============================================================

  //============================================================
  BOOL readImageAnalyze( const lgl::Filename& hdr_filename, pImageRam& image )
  {
    AnalyzeHeader hdr;
    if (!hdr.read(hdr_filename)) return false;
    lgl::Filename img_filename(hdr_filename);
    setExtension(img_filename,String("img"));

    // Type switch
    //TypeCode ty = AnalyzeHeader::analyzeCodeToBasicTypeCode(hdr.getData().ic.datatype);
    BOOL result; 
    /// Assumes that if the header is coded with a different Endian convention 
    /// than current machine then so is the data...
    lglSwitchOnTypeFunctionCall6( hdr.basicTypeCode(), TreadImageRaw, img_filename, hdr.size(), image, !hdr.rightEndian(), result ); 
	
    image->setSpacing( hdr.spacing() );

    return result;
  }
  //============================================================


  //============================================================
  BOOL writeImageRaw( const lgl::Filename& filename, const ImageRam& image )
  {
    FILE* f_src = fopen((char *)filename.c_str(),"wb");
    if( f_src == NULL) return false;

    bool result = image.rawWrite( f_src );

    fclose(f_src);
    return result;
  }
  //============================================================


  //============================================================
  BOOL writeImageAnalyze( const lgl::Filename& hdr_filename, const ImageRam& image )
  {
    AnalyzeHeader hdr;
    if (!hdr.write(image,hdr_filename)) return false;

    lgl::Filename img_filename(hdr_filename);
    setExtension(img_filename,String("img"));

    if (!writeImageRaw(img_filename,image)) return false;
    return true;
  }
  //============================================================



#ifdef __LGLWX__

  //============================================================
  BOOL wxOpenImageAnalyze( pImageRam& im )
  {
    wxString filename = wxFileSelector(_T("Open image"),_T(""),_T(""),
				       _T("hdr"),
				       _T("Analyze headers files (*.hdr)|*.hdr"));
    if ( !filename ) return false;

    if ( !readImageAnalyze( lgl::wx2std(filename), im) ) {
      wxLogError(_T("Couldn't load image from '%s'."), filename.c_str());
      return false;
    }
    return true;
  }
  //============================================================


  //============================================================
  BOOL wxSaveImageAnalyze( const ImageRam& im )
  {
    wxString filename = wxFileSelector(_T("Save image"),_T(""),_T(""),
				       _T("hdr"),
				       _T("Analyze headers files (*.hdr)|*.hdr"));
    if ( !filename ) return false;

    if ( !writeImageAnalyze( lgl::wx2std(filename), im) ) {
      wxLogError(_T("Couldn't save image to '%s'."), filename.c_str());
      return false;
    }
    return true;
  }
  //============================================================


  //============================================================
  /// Interactive (wxOpenDialog based) image loading
  // MODIFIED BY AAE, to return chosen filename
  wxString wxOpenImage( pImageRam& im )
  {
    wxString filename = wxFileSelector(_T("Open image"),_T(""),_T(""),
				       _T(""),_T("*.*"));
    //		"BMP files (*.bmp)|*.bmp|GIF files (*.gif)|*.gif|TIF files (*.tif)|*.tif|JPEG files (*.jpg)|*.jpg|PNG files (*.png)|*.png|PCX files (*.pcx)|*.pcx|PNM files (*.pnm)|*.pnm|XPM files (*.xpm)|*.xpm|Analyze headers files (*.hdr)|*.hdr");
    if ( !filename ) return wxT("NULL");

    // hdr image
    if (filename.Last()=='r') {
      if ( !readImageAnalyze( lgl::wx2std(filename), im) ) {
	wxLogError(_T("Couldn't load image from '%s'."), filename.c_str());
	return wxT("NULL");
      }
      return filename;
    }
    // other 
    else {
      lgl::ImageRGB image;
      if ( !image.LoadFile(filename) ){
	wxLogError(_T("Couldn't load image from '%s'."), filename.c_str());
	return wxT("NULL");
      }
      // conversion into a RamImage
      ImageRamT<unsigned char>* ima = new ImageRamT<unsigned char>;
      ImageRGBToImageRam(image,*ima);
      im = ima;
      return filename;
    }
  }
  //============================================================

  //============================================================
  /// Interactive (wxOpenDialog based) image saving
  BOOL wxSaveImage( const ImageRam& im )
  {
    wxString filename = wxFileSelector(_T("Save image"),_T(""),_T(""),
				       _T("hdr"),
				       _T("Analyze headers files (*.hdr)|*.hdr"));
    if ( !filename ) return false;

    if ( !writeImageAnalyze( lgl::wx2std(filename), im) ) {
      wxLogError(_T("Couldn't save image to '%s'."), filename.c_str());
      return false;
    }
    return true;
  }
  //============================================================



#endif
  // __LGLWX__
}
//============================================================
// EO namespace lgl
//============================================================
// EOF
//============================================================
