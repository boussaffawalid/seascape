// By: Anton Albajes-Eizagirre
// Language loader/dealer class. Loads program strings for many languages from an xml file
// It delivers strings to client identified by ID of string and language
#ifndef _languageLoader_h_
#define _languageLoader_h_

#include "tinyxml.h"
#include <map>
#include <vector>
#include <fstream>
#include <string>
using namespace std;

typedef map<string,string> dictionary;

class languageDealer{
private:
	map<string,dictionary*> languages;
	int loadLanguages(TiXmlElement *_elem);
	dictionary* loadXMLLanguage(TiXmlElement *_elem);
public:
	int loadLanguages(const char *_path);
	string getStringById( string _language, string _id);
	vector< string> * getLanguagesVector();
};

#endif // _languageLoader_h_
