// By Anton Albajes-Eizagirre
// XML loader for interface parameters
#ifndef _interfaceParams_h_
#define _interfaceParams_h_

#include "tinyxml.h"
#include <map>
#include <string>

using namespace std;
typedef map<string,string> mapParameters;

class interfaceParamsLoader{
private:
	mapParameters m_params;
	string m_errorString;//
	void loadInterfaceParams(const char *_path);
	
public:
	interfaceParamsLoader(const char *_path);
	string getParameter(string _id);
	string getErrorString();
};

#endif // _interfaceParams_h_