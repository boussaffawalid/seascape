// By: Anton Albajes-Eizagirre
// Location Loader class. Loads specie classes and groups from an excel XLS file
// and delivers them to the client

/*

  FORMAT SPECIFICATION:
  ------ -------------
	Code Group	Species Id
Grup 1 Sponges		
Acanthella acuta	S	1
Agelas oroides	        S	2
Aplysina cavernicola	S	3
Axinella damicornis	S	4
Axinella verrucosa	S	5
Clathrina clathrus	S	6
Clathrina coriacea	S	7
Crambe crambe	        S	8
Chondrosia reniformis	S	9
Dysidea avara	        S	10
Haliclona mediterranea	S	11
Hemimycale columella	S	12
Ircinia oros	        S	13
Ircinia variabilis	S	14
Oscarella lobularis	S	15
Oscarella tuberculata	S	16
Petrosia ficiformis	S	17
Phorbas tenacior	S	18
Pleraplysilla spinifera	S	19
Raspaciona aculeata	S	20
Reniera fulva	        S	21
Reniera mucosa	        S	22
Spirastrella cunctatrix	S	23
Sponge orange canals	S	26
Sponge brown-pale 	S	27
Sponge orange sp2	S	29
Sponge blue-white	S	30
Grup 2 Corals		
Alcyonium acaule	C	33
Hoplangia durotrix	C	34
Corallium rubrum	C	35
Eunicella singularis	C	36
Leptosammia pruvoti	C	37

*/

#ifndef _location_loader_h_
#define _location_loader_h_

//#include "../tinyxml/tinyxml.h"
#include <map>
#include <vector>
#include <string>
#include <sstream>
#include "BasicExcel.hpp"

using namespace std;

class Specie;

typedef map< string, vector< Specie* > > dictGroups;
typedef map< string, dictGroups* > dictLocations;
typedef map< string, Specie* > dictSpecies;
typedef map< int, Specie* > mapSpecies;

class Specie{
public:
	string name;
	string code;
	int id;
	int color;
};

class LocationLoader{
private:
	dictLocations locations;
	dictSpecies species;
	mapSpecies allSpecies ;

public:
	int nLocations;
	int loadLocations(const char* _path);
	bool saveLocations(const char* _path);
	void printLocations(); // for debugging
	vector<string >* getLocations();
	vector<string >* getGroups(string _location);
	vector<Specie* >* getSpecies(string _location, string _group);
	bool deleteSpecies(string _location, string _group, string _specie);
	Specie* getSpecieByName(string _specie);
	Specie* getSpecieByColor(int _color);
	Specie* getSpecieById(int _id);
	int getNSpecies();
	void deleteSpecies(vector<string> *_species);
};

#endif // _location_loader_h_
