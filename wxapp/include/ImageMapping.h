#ifndef __IMAGE_MAPPING__
#define __IMAGE_MAPPING__

class MapClass;
class MapImage;
class ImageMapping;


class ImageMapping 
{
private:
	std::vector<MapClass*> *m_classifMap;
	MapImage *m_MapImage;
public:
	int *m_colormap_red, *m_colormap_green, *m_colormap_blue;

	ImageMapping();
	~ImageMapping();
	void saveClassification(wxString filename);
	// Parse le fichier contenant la description des classes.
	void parseClassification(wxString filename);
	void writeClassification(wxString filename);
	// Alloue de l'espace m�moire pour un char*.
	char* charFactory();
	// Getter/Setter pour la MapImage
	void setMapImage(MapImage* image);
	MapImage* getMapImage();
	MapImage* createMapImage(int width, int height, wxString filename);
	std::vector<MapClass*>* getMapClassVector() {return m_classifMap;}
	unsigned int AddClassToMap(char* name, char* group, char* site, int red, int green, int blue);
	unsigned int getRandId();
	void SetColorMaps(int *red, int *green, int *blue);
	void GetColorMaps(int *red, int *green, int *blue);
};

class MapImage
{
private:
	std::vector<unsigned int> *m_pixel;
	wxString m_filename;
	int m_height, m_width;

public:
	MapImage(int width, int height, wxString filename);
	~MapImage();

	void saveImage(wxString filename);
	void readImage();
	std::vector<unsigned int>* getPixelList() {return m_pixel;}

};

class MapClass
{
private:
	char* name;
	char* group;
	char* site;
	unsigned int id;
	int red;
	int green;
	int blue;

public:
	MapClass(char* name, char* group, char* site, unsigned int id, int red, int green, int blue);
	MapClass(std::vector<char *> *class_desc);
	MapClass();
	~MapClass();

	void setName(char* name);
	void setGroup(char *group);
	void setSite(char *site);
	void setId(unsigned int id);
	void setRed(int red);
	void setGreen(int green);
	void setBlue(int blue);

	char* getName();
	char* getGroup();
	char* getSite();
	unsigned int getId();
	int getRed();
	int getGreen();
	int getBlue();
};

#endif //__IMAGE_MAPPING__