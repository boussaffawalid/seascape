#ifndef _mpolygons_h_
#define _mpolygons_h_

#include <set>
#include <vector>


// POLYGON RELATED TYPES
// point type
typedef std::pair<int,int> mPoint;
// point set comparer
struct PointSetComparer{
  bool operator()(mPoint* const &lhs, mPoint* const &rhs) const {
    // sort by x coord and then y coord
    if(lhs->first==rhs->first)
      return lhs->second<rhs->second;
    return lhs->first<rhs->first;
  }
};
// region, set of points
typedef std::set<mPoint*,PointSetComparer> mRegion;

/* class for modeling a polygon
   contains a set of pixels, 
   area of the polygon,
   perimeter of the polygon,
   labeled class of the polygon,
   and the length of the pixel side in cm
   (the pixel is considered to be squared)
 */
class MPolygon{
 private:
  mRegion *m_pixels;
  double m_area;
  double m_perimeter;
  double m_side_pixel_length;
  int m_classId;
  double ComputeArea();
  double ComputePerimeter();
  int m_Id;
  static int m_lastId;

 public:

  MPolygon();
  ~MPolygon();
  void SetPixelSideLength(double _length) { m_side_pixel_length = _length; };
  void SetPixels(mRegion *_pixels);
  void SetClassId(int _class) { m_classId = _class; };
  static int nextId() { return ++m_lastId; };
  mRegion *GetPixels() { return m_pixels; };
  double GetArea() { return ComputeArea(); };
  void UpdatePerimeter() { this->ComputePerimeter(); };
  double GetPerimeter() { return m_perimeter*m_side_pixel_length; };
  int GetClassId() { return m_classId; };
  int GetId() { return m_Id; };

};

/* Splits the first connected area found in _region and create
   a polygon with the pixels/points. CAUTION: the pixels/points
   droped at the resulting polygon will be deleted from _region
 */
MPolygon* SplitPolygons(mRegion *_region);

/* moves _north point north of _from
   returns the pointer to _north
   WARNING: DOES'NT ALLOC _north
 */
mPoint* MoveNorth(mPoint *_from, mPoint *_north);

/* moves _south point south of _from
   returns the pointer to _south
   WARNING: DOES'NT ALLOC _south
 */
mPoint* MoveSouth(mPoint *_from, mPoint *_south);

/* moves _east point east of _from
   returns the pointer to _east
   WARNING: DOES'NT ALLOC _east
 */
mPoint* MoveEast(mPoint *_from, mPoint *_east);

/* moves _west point west of _from
   returns the pointer to _west
   WARNING: DOES'NT ALLOC _west
 */
mPoint* MoveWest(mPoint *_from, mPoint *_west);

mPoint* MoveNorthWest(mPoint *, mPoint *);
mPoint* MoveSouthWest(mPoint *, mPoint *);
mPoint* MoveNorthEast(mPoint *, mPoint *);
mPoint* MoveSouthEast(mPoint *, mPoint *);

#endif // _mpolygons_h_
