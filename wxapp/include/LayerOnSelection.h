#ifndef __LAYER_ON_SELECTION__
#define __LAYER_ON_SELECTION__
void LayersOnSelectedRegions( const ImageRam& in, const ImageRam& lab, const std::set <int>& slab, pImageRam& out, pImageRam& layer, ImageMapping *image_map);
template <class T>
void TLayersOnSelectedRegions( T, const ImageRam& in, const ImageRam& lab, const std::set <int>& slab, pImageRam& out, pImageRam& layer, ImageMapping *image_map);
template <class T, class L>
void TLLayersOnSelectedRegions( L, T, const ImageRam& rin, const ImageRam& rlab, const std::set <int>& slab, pImageRam& rout, pImageRam& rlayer, ImageMapping *image_map);

void UnLayersOnSelectedRegions( const ImageRam& in, const ImageRam& lab, const std::set <int>& slab, pImageRam& out, pImageRam& layer, ImageMapping *image_map);
template <class T>
void TUnLayersOnSelectedRegions( T, const ImageRam& in, const ImageRam& lab, const std::set <int>& slab, pImageRam& out, pImageRam& layer, ImageMapping *image_map);
template <class T, class L>
void TLUnLayersOnSelectedRegions( L, T, const ImageRam& rin, const ImageRam& rlab, const std::set <int>& slab, pImageRam& rout, pImageRam& rlayer, ImageMapping *image_map);

void InitLayersOnSelectedRegions( const ImageRam& in, const ImageRam& lab, pImageRam& out, pImageRam& layer, ImageMapping *image_map);
template <class T>
void TInitLayersOnSelectedRegions( T, const ImageRam& in, const ImageRam& lab, pImageRam& out, pImageRam& layer, ImageMapping *image_map);
template <class T, class L>
void TLInitLayersOnSelectedRegions( L, T, const ImageRam& rin, const ImageRam& rlab, pImageRam& rout, pImageRam& rlayer, ImageMapping *image_map);

void ImageToImageRGBmed ( const ImageRam& in, ImageRGB& out, const Palette& pal, int zoom);
template <class T>
void TImageToImageRGBmed ( T, const ImageRam& rin, ImageRGB& out, const Palette& pal, int zoom);

void ImageAndLabelsToImageRGBmed ( const ImageRam& in, const ImageRam& lab, ImageRGB& out, const Palette& pal, RGBpoint contours_color, int zoom);
template <class L>
void LImageAndLabelsToImageRGBmed ( L, const ImageRam& in, const ImageRam& lab, ImageRGB& out, const Palette& pal, RGBpoint contours_color, int zoom);
template <class T, class L>
void TLImageAndLabelsToImageRGBmed ( T, L, const ImageRam& rin, const ImageRam& rlab,ImageRGB& out, const Palette& pal, RGBpoint contours_color, int zoom);

class LayerOnSelectionDesc
{
private:
	int red;
	int blue;
	int green;
	float opacity;

public:
	LayerOnSelectionDesc();
	~LayerOnSelectionDesc();
	
	static LayerOnSelectionDesc* setLayerOnSelection(int l_red, int l_green, int l_blue, float l_opacity);
	static LayerOnSelectionDesc* getLayerOnSelection();
	static LocationLoader* getLocationLoader();
	static void setLocationLoader(LocationLoader *_locationDealer);
	static void setPixelsSpecies(classPixels *_pixelsSpecies);
	static classPixels* getPixelsSpecies();
	static void setVectorPolygons(vector<MPolygon*> *_pols);
	static vector<MPolygon *>* getVectorPolygons();

	int getRed();
	int getGreen();
	int getBlue();
	float getOpacity();
};

#endif // __LAYER_ON_SELECTION__
