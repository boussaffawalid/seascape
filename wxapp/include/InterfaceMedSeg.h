//-------------------------------
// Instructions de compilation
//-------------------------------
#ifndef __INTERFACEMEDSEG__
#define __INTERFACEMEDSEG__

//-------------------------------
// Inclusions de fichiers
//-------------------------------
#include "sxsGeneral.h"
#include <wx/spinctrl.h>
#include <wx/colordlg.h>
//#include <wx/minifram.h>
#include "lglImageRam.h"
#include "lglImageManip.h"
#include "lglImageIO.h"
#include "sxsImageScaleSets.h"
#include "sxsScaleSetsAlgorithms.h"
#include <limits>
#include <set>
#include <fstream>
#include <vector>

#include "ImageMapping.h"

#include "sxs.h"
#include <wx/frame.h>
#include <wx/splitter.h>
#include <wx/spinctrl.h>
#include <wx/log.h>
#include <wx/listctrl.h>
#include <wx/dcbuffer.h>
#include <wx/odcombo.h>
#include <wx/button.h>
#include <wx/grid.h>
//#include <wx/kbdstate.h>

//#include <wx/xrc/xh_odcombo.h>
//#include <wx/setup.h>
//#include <wx/msw/setup.h>
//#include <wx/msw/setup0.h>

#include "sxs.h"
#include "lglwxLogWindow.h"

// for language and location loadage functions
#include "languageLoader.h"
#include "locationLoader.h"
#include "interfaceParams.h"
#include "BasicExcel.hpp"

// for polygon functions
#include "mpolygons.h"

//------------------------------------------------------------------------------
// On utilise les espaces de nom sxs et lgl afin d'�viter les sxs:: et lgl::
//------------------------------------------------------------------------------
using namespace sxs;
using namespace lgl;

//-------------------------------
// Definition de constantes
//-------------------------------
#define SCROLLWIN_ID 200;

//------------------------------------------------------------------------------------------------------------------------------------
// Prototypes des classes (�vite d'avoir � placer InterfaceMedSeg en tout dernier apr�s toutes les autres classes qu'elle utilise)
//------------------------------------------------------------------------------------------------------------------------------------
class BitmapButtonMedSeg;
class ScrollWinMedSeg;
class PanelMedSeg;
class LeftWindow;
class PointSizeDialog;
class LogWindow;
class ComboClassMedSeg;
class AddClassMedSeg;
class ManageClassMedSeg;
class ManageColorMaps;
class SegmentationParametersWindow;
class TableWindow;
class NavigationWindow;

typedef std::map<int,int> classPixels;

//-------------------------------
// Fenetre principale
//-------------------------------
class InterfaceMedSeg : public FrameWithLog
{
private:
	DECLARE_EVENT_TABLE()

protected:
	PanelMedSeg *Panel;
	ManageClassMedSeg *m_manage_frame;
	ManageColorMaps *m_colormaps_frame;
	SegmentationParametersWindow *m_segparam_frame;
	AddClassMedSeg *m_add_frame;
	std::vector< string >* _languages;
	void setLanguage(string _currentLanguage);
	classPixels _pixelsSpecies;
	vector<MPolygon*> *_polygons;
public:
	ScrollWinMedSeg *ImageFrame;
	InterfaceMedSeg(wxFrame *parent, bool delete_scale_sets_on_closing, const wxString& title=(wxChar*)"MedSeg" );
	~InterfaceMedSeg();

// D�clar� en tant que public, car le code r�alis� auparavant n'utilise aucun accesseur. 
// TODO : A corriger.
	LeftWindow *OptionFrame;
	string currentLanguage;
	string m_image_filename;
	string m_image_path;
	wxColour *background_color;
	interfaceParamsLoader *_paramsLoader;

	void ImageFrameInit();

	void OnSize( wxSizeEvent& event );

	languageDealer *_languageDealer;
	LocationLoader *_locationDealer;
	
	static int m_colormap_red[256];// = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,9,13,16,20,25,29,32,36,41,45,48,52,57,61,64,68,73,77,80,84,89,93,96,100,105,109,112,116,121,125,128,132,137,141,144,148,153,157,160,164,169,173,176,180,185,189,192,196,201,205,208,212,217,221,224,228,233,237,240,244,249,253,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255};
	static int m_colormap_green[256];// = {255,251,247,242,239,235,230,226,223,219,215,210,207,203,198,194,191,187,183,178,175,171,166,162,159,155,151,146,143,139,134,130,127,123,119,114,111,106,102,98,95,91,87,82,79,74,70,66,63,59,55,50,47,42,38,34,31,27,23,18,15,10,6,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,6,10,14,19,23,27,31,34,38,42,46,51,55,59,63,66,70,74,78,83,87,91,95,98,102,106,110,115,119,123,127,130,134,138,142,147,151,155,159,162,166,170,174,179,183,187,191,194,198,202,206,211,215,219,223,226,230,234,238,243,247,251,255};
	static int m_colormap_blue[256];// = {255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,253,249,245,241,236,232,228,224,221,217,213,209,204,200,196,192,189,185,181,177,172,168,164,160,157,153,149,145,140,136,132,128,125,121,117,113,108,104,100,96,93,89,85,81,76,72,68,64,61,57,53,49,44,40,36,32,29,25,21,17,12,8,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

//_________________________________________________________________________________________
	//void OnFocus(wxFocusEvent& event);
	void OnMenuSave(wxCommandEvent& event);
	void OnMenuOpen(wxCommandEvent& event);
	void OnMenuQuit(wxCommandEvent& event);
	void OnMenuLog(wxCommandEvent& event);
	void OnMenuAbout(wxCommandEvent& event);
	void OnMenuLanguage1(wxCommandEvent& event);
	void OnMenuLanguage2(wxCommandEvent& event);
	void OnMenuLanguage3(wxCommandEvent& event);
	void OnMenuLanguage4(wxCommandEvent& event);
	void OnMenuLanguage5(wxCommandEvent& event);
	void OnMenuLanguage6(wxCommandEvent& event);
	void OnMenuLanguage7(wxCommandEvent& event);
	void OnMenuLanguage8(wxCommandEvent& event);
	void OnMenuLanguage9(wxCommandEvent& event);
	void OnMenuLanguage10(wxCommandEvent& event);

	void OnMenu3DWindow(wxCommandEvent& event);
	void OnMenu3DpointSize(wxCommandEvent& event); 
	void OnMenu3DMoveConfig(wxCommandEvent& event);
	void OnMenuSegment (wxCommandEvent& event);
	void OnMenuAdd (wxCommandEvent& event);
	void OnMenuColorMaps (wxCommandEvent& event);
	void OnMenuExtract (wxCommandEvent& event);
	void OnMenuExtractXLS (wxCommandEvent& event);
	void OnMenuShowPolygon(wxCommandEvent& ev);
	void OnMenuImport (wxCommandEvent& event);
	void OnMenuExport (wxCommandEvent& event);
	void OnMenu3DScrollEnslavement(wxCommandEvent& event);
	void DoSegmentation(ImageScaleClimbingParameters* param );

	void CreateScene();
	void Display();
	vector<MPolygon*>* GetPolygons();
	void KeyDown(wxKeyEvent &ev);

	void OnWheelRotation2i(wxScrollEvent& ev);
	void OnWheelRotationi(wxMouseEvent& ev);
	void OnScrolli(wxScrollWinEvent & event);
	void OnListBoxButtons(wxCommandEvent &event);
	void UpdateClassGrid(wxGrid *classGrid);
	void ShowTableWindow(int highlight = -1);
	void UpdateStatusBarScaling();

	void AddClassClose();
//	PointSet *m_points;
//	Scene *m_scene;

	bool m_scale_set_loaded;
	bool p_is_3D_Window_Created;
	bool p_scoll_slaved;

	double m_pixel_side_length;
	int m_nlevels;

protected:

	//=========================================================
	/// Menu
	wxMenuBar *menuBar;
	wxMenu *menuFile;
	wxMenu *menuHelp;
	wxMenu *menu3D;
	wxMenu* menuAlgo;
	wxMenu* menuLanguages;




	/// A pointer on the ImageScaleSets
	ImageScaleSets*		m_sxs;
	/// must the ScaleSets be deleted by the browser on closing ? 
	lgl::BOOL			m_delete_sxs;

	//=========================================================
	// IDs for the events
	enum
	{
		idMenu3DEnslavement,
		idMenuSegment,
		idMenuAdd,
		idMenuColorMaps,
		idMenuExtract,
		idMenuExtractXLS,
		idMenuShowPolygon,
		idMenuOpen,
		idMenuImport,
		idMenuExport,
		idMenuLanguage1,
		idMenuLanguage2,
		idMenuLanguage3,
		idMenuLanguage4,
		idMenuLanguage5,
		idMenuLanguage6,
		idMenuLanguage7,
		idMenuLanguage8,
		idMenuLanguage9,
		idMenuLanguage10,
		idMenuSave,
		idMenuQuit,
		idMenuLog,
		idMenu3DWindow,
		idMenu3DPointSize,
		idMenu3DMoveConfig,
		// it is important for the id corresponding to the "About" command to have
		// this standard value as otherwise it won't be handled properly under Mac
		// (where it is special and put into the "Apple" menu)
		idMenuAbout = wxID_ABOUT,

		idStatusBar,
		idColorButton,
		idButtonOk,
		idButtonCancel
	};
//_________________________________________________________________________________________


};


//-------------------------------
// Fenetre contenant l'image
//-------------------------------
class PanelMedSeg : public wxPanel
{

public:
	PanelMedSeg(wxWindow* parent, wxWindowID id = -1, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize);
	~PanelMedSeg();

};


//-------------------------------
// Fenetre contenant l'image
//-------------------------------
class ScrollWinMedSeg : public wxScrolledWindow
{
private:
	ImageMapping *m_map_image;
	bool m_bIsScroll;
	int m_iLastSliceSection;
	bool m_bShouldDraw;

	// TRUE si une image a �t� charg�e, FALSE sinon.
	bool m_bActive;

	// R�affichage des Scrollbars?
	bool m_bShoulScrollEx;

	bool m_shouldScroll;

public:
	InterfaceMedSeg *m_owner;
	
	ScrollWinMedSeg(wxWindow* parent, InterfaceMedSeg *owner, wxWindowID id = -1, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxHSCROLL | wxVSCROLL, const wxString& name = (wxChar*)"scrolledWindow");
	~ScrollWinMedSeg();

	void InitLayer();
	ImageMapping* getImageMapping() {return m_map_image;}

	ImageRam* getGoodSliceSection(); 
	std::vector<ImageRam*> *m_vSliceSection;

	void OnPaint(wxPaintEvent& ev );
	void OnMouse(wxMouseEvent& ev);
	void OnKeyPress(wxKeyEvent& ev);
	void OnScroll(wxScrollWinEvent & event);
	void OnWheelRotation2(wxScrollEvent& ev);
	void OnWheelRotation(wxMouseEvent& ev);
	void OnMouseMove(wxMouseEvent &event);
	void FollowScroll();
	void ShowPolygon(ImageSite &pos);
	void GatherSegmentScale();
	void dcClear();

	lgl::F32 getCurrentScale();
	/// Slices the 3D image (computes the ImageRams)

	void getSliceImage();
	/// Slices of the base segmentation
	void getSliceBase();

	void getSliceSection();
	/// Computes the label images of the current set at current slicing point
	void getCurrentSetsBaseMapping();
	void getSliceSets();
	/// Transform the ImageRams to Bitmaps
	void getSliceBitmap();
	void getSlicePoints(bool forced=false);

	lgl::PaletteLUT getPalette() {return m_palette_regions;}


	/// Makes all 
	void updateView();


	// Returns the label in the current section of the site s : the slice images must be up to date 
	int labelOfSite(lgl::ImageSite& s);

	/// Log mapping of the scales into [0,1]
	inline lgl::F64 logscale( lgl::F32 l ) { lgl::F64 nl = l/m_scale_min; if (nl<1) nl=1; return log(nl)/m_logscale_max; }
	/// Inverse mapping
	inline lgl::F64 invlogscale( lgl::F64 s ) { return m_scale_min * exp ( s * m_logscale_max ); }


	/// Determines what must be recomputed
	void resolveChanges();

	void SetActive() {m_bActive =true;}

	//==================================================
	/// A pointer on the ImageScaleSets
	ImageScaleSets*		m_sxs;
	/// must the ScaleSets be deleted by the browser on closing ? 
	lgl::BOOL			m_delete_sxs;
	/// The maximum scale (scale of the root)
	//lgl::F32			m_scale_max;
	lgl::F64			m_scale_max;
	/// The minimum scale (for log scale) 
	//lgl::F32			m_scale_min;
	lgl::F64			m_scale_min;
	/// The maximum value of logscale
	//lgl::F32			m_logscale_max;
	lgl::F64			m_logscale_max;
	/// The current scale
	//lgl::F32			m_scale;
	lgl::F64			m_scale;

	/// Input image parameters
	/// A pointer on the image;
	lgl::ImageRam*		m_image;

	/// Min/max value of the image
	double				m_value_min, m_value_max;
	/// The image size
	lgl::ImageSite		m_isize;
	/// The view of the image size (=m_isize*m_zoom)
	lgl::ImageSite		m_vsize;
	/// The size of the image frame (containing all three slices)
	wxSize			m_image_frame_size;

	/// The current slice position
	lgl::ImageSite		m_slice_position;


	//=========================================================
	// COLORS
	// color map  
	// TO DO : general colormap
	lgl::PaletteMultilinear* m_color_map;
	// Contours color
	lgl::RGBpoint m_contours_color;
	// Current set color
	lgl::RGBpoint m_cur_set_color;
	// Set colors
	lgl::RGBpoint m_set_color[6];
	// Regions palette = random colors
	lgl::PaletteLUT m_palette_regions;
	//=========================================================
	/// Color maps
	wxString	m_color_maps[9];
	lgl::PaletteMultilinear::Type m_color_maps_id[9];
	//=========================================================

	//=========================================================
	/// View change events
	lgl::BOOL m_xslice_changed, m_scale_changed;
	lgl::BOOL m_zoom_changed, m_palette_changed;
	lgl::BOOL m_set_changed;
	/// Recomputation variables 
	lgl::BOOL m_comp_ximg;
	lgl::BOOL m_comp_xbase;
	lgl::BOOL m_comp_xsec;
	lgl::BOOL m_comp_xset;
	lgl::BOOL m_comp_xbmp;
	//=========================================================

	//=========================================================
	// x,y and z slices images 
	lgl::ImageRam		*m_xslice_img;
	lgl::ImageRam		*m_xslice_mask;
	lgl::ImageRam		*m_xslice_layer;
	// x,y and z slices base label images 
	lgl::ImageRam		*m_xslice_base;
	// x,y and z slices section label images 
	lgl::ImageRam		*m_xslice_lab;
	lgl::ImageRam		*m_xslice_labtest;
	// x,y and z slices label images of the current sets (0 : background)
	lgl::ImageRam		*m_xslice_set;
	//=========================================================
	// current bitmaps (image(+contours)(+set))
	wxBitmap			m_xslice_bmp;
	wxBitmap			m_xslice_bmpmask;
	wxBitmap			m_xslice_bmplayer;
	//=========================================================
	// Mask test
	wxMask				*m_super_mask;
	bool				m_bShould_save;
	wxString			m_sExFilename;

	LeftWindow *m_option;

	//=========================================================
	// zoom
	int				m_zoom;
	//=========================================================


	//=========================================================
	// is the mouse in image ?
	lgl::BOOL		m_mouse_in_image;
	
	bool m_controlKey;
	bool m_scaling;
	bool m_scaling_changed;
	int m_scaling_x0, m_scaling_x1, m_scaling_y0, m_scaling_y1;

	// Coord of the last point hitted in selection mode (right clic usually)
	int m_last_sel_x;
	int m_last_sel_y;
	//=========================================================

	//=========================================================
	// Variables for interactive set selection
	// Current sets selected
	std::set<ImageScaleSets::Set*> m_cur_sets;
	// Current sets base mapping
	std::map<int,int> m_cur_sets_base_map;
	// Minimum persistence of a valid set (pageup...)
	//lgl::F32 m_set_persistence;
	lgl::F64 m_set_persistence;
	// The 3D points hit by the mouse during dragging
	//std::vector<lgl::ImageSite> m_set_image_points;
	//=========================================================

	bool m_has_to_fit;
	bool m_mouse_test;

	DECLARE_EVENT_TABLE()
private:
	wxMemoryDC p_mem_dc;

};



//=======================================================================================
class PointSizeDialog : public wxDialog
{
public:
	PointSizeDialog(InterfaceMedSeg *parent);
	void OnExit(wxCloseEvent & event);
	wxSpinCtrl *m_spinctrl;
	DECLARE_EVENT_TABLE()
};


class LogWindow : public wxLogWindow {
public:
	LogWindow( wxFrame *parent, bool show, bool pass, const wxSize& size);

	virtual bool OnFrameClose ( wxFrame* f );
};


class LeftWindow : public wxWindow
{
public:
	LeftWindow(wxWindow *parent, ScrollWinMedSeg *draw_canvas,InterfaceMedSeg *owner);
	~LeftWindow();
	lgl::BOOL InitBrowser();
	void InitInterface();
	void Fit();

	void OnImageStyleChange(wxCommandEvent& event);
	void OnZoomChange(wxCommandEvent& event);

	void OnSectionStyleChange(wxCommandEvent& event);
	void OnAffichClassifCheck(wxCommandEvent& event);
	void OnSectionColorChange(wxCommandEvent& event);

	void OnScaleSlider(wxScrollEvent& envent);
	void OnScaleCtrl(wxCommandEvent& event);
	void OnScaleChange();

	void OnSetDrawCheck(wxCommandEvent& event);
	void OnSetColorChange(wxCommandEvent& event);
	void OnSetPersistenceCtrl(wxCommandEvent& event);

	void OnClickButtons(wxCommandEvent& event);
	void OnChangeSiteOrGroup(wxCommandEvent& event);
	void OnShowTable(wxCommandEvent &ev);
	void OnShowNavigationWindow(wxCommandEvent &ev);
	// AAE: while change image sizes on text ctrls
	void OnChangeImageSizes(wxCommandEvent& event);
	void OnButtonSegmentSize(wxCommandEvent &ev);
	void OnChangeImagePixelsCm(wxCommandEvent &ev);
	void UpdateImageSizeCtrls();

	// AAE: textboxes for image size
	wxTextCtrl *m_image_height;
	wxTextCtrl *m_image_width;
	wxStaticText *m_image_height_label;
	wxStaticText *m_image_width_label;
	wxStaticText *m_image_pixels_cm_label;
	wxTextCtrl *m_image_pixels_cm;
	bool m_image_ready;
	bool m_image_size_propagate;

	// AAE: classGrid for listing pixels classified for each specie
	wxGrid *classGrid;
	// AAE: class members for widgets to be able to reset labels
	wxStaticBoxSizer* viewsizer;
	wxStaticBoxSizer* sectionsizer;
	wxStaticBoxSizer* scalesizer;
	wxStaticBoxSizer *setsizer;
	wxStaticBoxSizer *sizerStaticAreaSize;
	wxStaticBoxSizer *sizerStaticAreaTable;
	wxStaticBoxSizer *sizerStaticAreaButtonNavigation;

	wxButton *buttonShowTable;
	wxButton *buttonShowNavigationWindow;
	wxButton *buttonSegmentSize;

	/// Image display style
	wxRadioBox *m_image_style_radio;

	/// Section display style
	wxRadioBox *m_section_style_radio;

	/// Draw ?
	wxCheckBox *m_set_draw_check;
	wxCheckBox *m_affich_classif_cb;

	//=========================================================
	/// Widgets			
	// top level sizer
	wxBoxSizer *topsizer;		

	/// zoom
	wxComboBox *m_zoom_combo;

	/// Section color 
	//wxComboBox *m_section_color_combo;   

	/// The scale slider 
	wxSlider *m_scale_slider;
	/// The scale text control
	wxTextCtrl *m_scale_ctrl;

	ScrollWinMedSeg *ImageFrame;
	InterfaceMedSeg *m_owner;
	NavigationWindow *m_navigationWindow;

	/// Set 

	/// Color
	wxComboBox *m_set_color_combo;
	/// Minimum persistence
	wxTextCtrl *m_set_persistence_ctrl; 

//	wxListCtrl *m_object_list;

	wxComboBox *m_color_sel;
	BitmapButtonMedSeg *m_bm_contour_col;
	wxGauge *m_gauge;
	wxButton *m_but_new_class;

	//wxOwnerDrawnComboBox m_class_sel;
	wxComboBox *m_site_sel;
	wxComboBox *m_group_sel;
	ComboClassMedSeg *m_class_sel;

	enum{
		idObjectList,
		idImageStyleRadio,
		idZoomCombo,

		idSectionStyleRadio,
		idSectionColorCombo,
		idAffichClassifCb,

		idScaleSlider,
		idScaleSlider2,
		idScaleCtrl,

		idSetDrawCheck,
		idSetColorCombo,
		idSetPersistenceCtrl,

		idColorContour,
		idButtonAddClass,
		idButtonShowTable,
		idButtonShowNavigationWindow,
		idButtonSegmentSize,
		idComboSel,
		idImageSizes1,
		idImageSizes2,
		idImagePixelsCm
	};
	DECLARE_EVENT_TABLE()
};


class ComboClassMedSeg : public wxOwnerDrawnComboBox
{
private:
	ImageMapping *m_mapping_im;
	wxComboBox *m_site_sel;
	wxComboBox *m_group_sel;
	int *m_colormap_red, *m_colormap_green, *m_colormap_blue;

public:
	ComboClassMedSeg(wxWindow *parent, int id, wxString value, wxPoint pos, wxSize size, wxArrayString choices, long style, wxComboBox* site_sel, wxComboBox* group_sel);//, wxValidator validator, wxString name);
	~ComboClassMedSeg();
//	void OnDrawItem(wxDC& dc, wxRect& rect, int item, int flags);

	enum{
		idComboClass
	};

	LocationLoader *_locationDealer;

	DECLARE_EVENT_TABLE()

	//virtual void OnDrawItem(wxDC& dc,const wxRect& rect, int item, int WXUNUSED(flags)) const;
	virtual void OnDrawItem(wxDC& dc,const wxRect& rect, int item, int flags) const;
	virtual void OnDrawBackground(wxDC& dc, const wxRect& rect, int item, int flags ) const;
	virtual wxCoord OnMeasureItem(size_t WXUNUSED(item)) const;
	virtual wxCoord OnMeasureItemWidth(size_t WXUNUSED(item)) const;
public:
	void LoadComboBox(ImageMapping *mapping_im, wxString filename);
	void ReloadComboBox();
	void InitSite();
	void InitGroup();
	void SetColorMaps(int *red, int *green, int *blue);
};

class BitmapButtonMedSeg : public wxBitmapButton
{
private:
	wxColourDialog *m_cd_contour_col;
	wxColour *m_color;
	ScrollWinMedSeg *ImageFrame;

public:
	BitmapButtonMedSeg(wxWindow* parent, wxWindowID id, const wxBitmap& bitmap);
	~BitmapButtonMedSeg();

	void OnClick(wxCommandEvent& event);
	void OnMouse(wxMouseEvent& ev);
	void WriteBitmap(wxColour color);
	wxColour* getColor();
	void setColor(wxColour color);
	void setImageFrame(ScrollWinMedSeg *win);

	enum
	{
		idClickEvent
	};

private:
	DECLARE_EVENT_TABLE()

};


class AddClassMedSeg : public wxFrame
{
private:
	wxFrame *m_parent;
public:
	BitmapButtonMedSeg *m_added_class_col;
	wxButton *m_ok;
	wxButton *m_cancel;
	wxStaticText *m_label_name;
	wxStaticText *m_label_group;
	wxStaticText *m_label_site;
	wxStaticText *m_label_color;
	wxTextCtrl *m_text_name;
	wxTextCtrl *m_text_group;
	wxTextCtrl *m_text_site;

	AddClassMedSeg(wxWindow* parent, wxWindowID id, const wxString& title);
	~AddClassMedSeg();

	void OnClose(wxCloseEvent& event);

private:
	DECLARE_EVENT_TABLE()
};

class ManageColorMaps : public wxFrame
{
private:

public:
	ManageColorMaps(wxWindow* parent, wxWindowID id, const wxString& title);
	~ManageColorMaps();

	void OnClose(wxCloseEvent& event);
	void Undo(wxCommandEvent& event);
	void Save(wxCommandEvent& event);
	void Load(wxCommandEvent& event);
	void UpdateGrid(wxGridEvent &event);
	void SelectColor(wxGridEvent &event);
	void SetGrid();
	void UseColorMap();
	int *m_red, *m_green, *m_blue;
	wxWindow *parent;
	wxGrid *m_grid_colorMaps;
private:
	DECLARE_EVENT_TABLE()

protected:
	enum{
		idButtonUndo,
		idButtonSave,
		idButtonLoad,
		idGrid,
	};
};

class SegmentationParametersWindow : public wxFrame{
 private:
  wxFrame *m_parent;
  wxSlider *m_slider_baseSeg_thrd;
  wxSlider *m_slider_energyComplexity;
  wxStaticText *m_text_baseSeg_thrd;
  wxStaticText *m_text_energyComplexity;
  wxStaticText *m_text_baseSeg_thrd_desc;
  wxStaticText *m_text_energyComplexity_desc;
  wxSlider *m_slider_levels;
  wxStaticText *m_text_levels;
  wxStaticText *m_text_levels_desc;

 public:
  SegmentationParametersWindow(wxWindow *parent, wxWindowID id, const wxString& title);
  ~SegmentationParametersWindow();
  void OnClose(wxCloseEvent& event);
  void OnChangeSlider(wxScrollEvent &event);
  void OnKey(wxKeyEvent &ev);
  void OnButtonClose(wxCommandEvent &ev);

 private:

  enum{
    idSlideBase,
    idSlideEnergy,
    idSlideLevels,
    idButtonClose
  };
  DECLARE_EVENT_TABLE()
    
};

class ManageClassMedSeg : public wxFrame
{
private:
	wxFrame *m_parent;
	wxCheckListBox *m_checkList;
	wxArrayString *speciesArray;
public:

	ManageClassMedSeg(wxWindow* parent, wxWindowID id, const wxString& title);
	~ManageClassMedSeg();

	void OnClose(wxCloseEvent& event);
	void DoSelection(wxCommandEvent &event);
	void DoAll(wxCommandEvent &event);
	void SelectAll(wxCommandEvent &event);
	void SelectNone(wxCommandEvent &event);

private:
	DECLARE_EVENT_TABLE()

protected:
	enum{
		idButtonDo,
		idButtonAll,
		idButtonSelAll,
		idButtonSelNone
	};
};

class TableWindow : public wxFrame{
 private:
  wxFrame *m_parent;
  wxGrid *tableGrid;

 public: 
  TableWindow(wxWindow *parent, wxWindowID id, const wxString& title, int highlight);
  ~TableWindow();

  void OnClose(wxCloseEvent &event);
  void ButtonClose(wxCommandEvent &ev);

 private:
  enum{
    idButtonClose
  };
  DECLARE_EVENT_TABLE()
    
};

class NavigationWindow : public wxFrame{
 private:
  wxFrame *m_parent;
  wxString *_imagePath;
  bool m_flagRefresh;
 public:
  NavigationWindow(wxWindow *parent, wxWindowID id, const wxString &title);
  void OnClose(wxCloseEvent &event);
  void OnPaint(wxPaintEvent &ev);
  void RePaint(wxScrollWinEvent &ev);

 private:
  DECLARE_EVENT_TABLE()

};

class SegmentSizingWindow : public wxFrame{
 private:
  wxFrame *m_parent;
  InterfaceMedSeg *m_root;
  LeftWindow *m_left;
  wxTextCtrl *m_size_text;

 public:
  SegmentSizingWindow(wxWindow *parent, wxWindowID id, const wxString &title);
  void OnClose(wxCloseEvent &ev);
  void OnButtonClose(wxCommandEvent &ev);

 private:
  enum{
    idButtonClose
  };
  DECLARE_EVENT_TABLE()
};

class MyAboutWindow : public wxFrame{
private:
	wxFrame *m_parent;
	InterfaceMedSeg *m_root;

public:
	MyAboutWindow(wxWindow *parent, wxWindowID id, const wxString &title);
	void OnClose(wxCloseEvent &ev);

	DECLARE_EVENT_TABLE()
};

class ExportXLSWindow : public wxFrame{
 private:
  wxFrame *m_parent;
  InterfaceMedSeg *m_root;
  wxCheckBox *_checkRegion;
  wxTextCtrl *_textRegion;
  wxCheckBox *_checkLocality;
  wxTextCtrl *_textLocality;
  wxCheckBox *_checkDate;
  wxTextCtrl *_textDate;
  wxCheckBox *_checkDepth;
  wxTextCtrl *_textDepth;
  wxCheckBox *_checkAscii;
  wxCheckBox *_checkSum;

 public:
  ExportXLSWindow(wxWindow *parent, wxWindowID id, const wxString &title);
  void OnClose(wxCloseEvent &ev);
  void OnSave(wxCommandEvent &ev);

 private:
  enum{
    idButtonSave
  };
  DECLARE_EVENT_TABLE()

};

// DUMMY AUX FUNCTION FOR CONVERSION TO WXSTRING (WHICH IS A PAIN ON THE ASS)

wxString toWxString(string _val);

class wxImagePanel : public wxPanel
    {
        wxImage image;
        wxBitmap resized;
        int w, h;
        
    public:
        wxImagePanel(wxFrame* parent, wxString file, wxBitmapType format);
        
        void paintEvent(wxPaintEvent & evt);
        void paintNow();
        void OnSize(wxSizeEvent& event);
        void render(wxDC& dc);
        
        // some useful events
        /*
         void mouseMoved(wxMouseEvent& event);
         void mouseDown(wxMouseEvent& event);
         void mouseWheelMoved(wxMouseEvent& event);
         void mouseReleased(wxMouseEvent& event);
         void rightClick(wxMouseEvent& event);
         void mouseLeftWindow(wxMouseEvent& event);
         void keyPressed(wxKeyEvent& event);
         void keyReleased(wxKeyEvent& event);
         */
        
        DECLARE_EVENT_TABLE()
    };
 



#endif //__INTERFACEMEDSEG__

