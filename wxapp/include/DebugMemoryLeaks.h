#ifndef INC_DEBUG_MEMORY_LEAKS_H
#define INC_DEBUG_MEMORY_LEAKS_H

#ifdef WIN32
#include <crtdbg.h>
#ifdef _DEBUG
#define new new( _CLIENT_BLOCK, __FILE__, __LINE__)
#endif//_DEBUG

#endif//WIN32

static void EnableDebugMemoryLeaks()
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF|_CRTDBG_LEAK_CHECK_DF);
}

#endif//INC_DEBUG_MEMORY_LEAKS_H
