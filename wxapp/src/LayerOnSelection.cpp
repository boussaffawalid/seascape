#ifndef __SXSWX__
#define __SXSWX__ 1
#endif
#include "InterfaceMedSeg.h"
#include "LayerOnSelection.h"

//========================================================================================
//========================================================================================
//========================================================================================
/// \brief Produces an image out whose values are values of in with red channel shift on the regions of same value of lab. 
/// 
/// out can be in.
template <class T, class L>
void TLLayersOnSelectedRegions( L, T, const ImageRam& rin, const ImageRam& rlab, const std::set <int>& slab, pImageRam& rout, pImageRam& rlayer, ImageMapping *image_map)
{//, int red, int green, int blue ) {
	// downcast 
	const ImageRamT<T>* in = dynamic_cast<const ImageRamT<T>*>(&rin);
	if (!in) return;
	const ImageRamT<L>* lab = dynamic_cast<const ImageRamT<L>*>(&rlab);
	if (!lab) return;

	if (!rout) rout = new ImageRamT<T>;
	rout->resize(in->size());
	ImageRamT<T>* out = dynamic_cast<ImageRamT<T>*>(rout);
	ImageRamT<T>* layer = dynamic_cast<ImageRamT<T>*>(rlayer);

	//
	int nc = in->size(0);
	int x = in->size(1);
	int y = in->size(2);
	int ind=0,i,j;
	typename ImageRamT<L>::const_iterator lit;

	out->resize(in->size());
	layer->resize(in->size());
	typename ImageRamT<T>::iterator oit=out->begin();
	typename ImageRamT<T>::iterator yit=layer->begin();
	typename ImageRamT<T>::const_iterator iit=in->begin();

	LayerOnSelectionDesc *DescLayer = LayerOnSelectionDesc::getLayerOnSelection();
	std::vector<unsigned int> *pixel = image_map->getMapImage()->getPixelList();
	std::vector<unsigned int>::iterator itpix = pixel->begin();

	std::vector<MapClass*> *map_class_vector = image_map->getMapClassVector();
	std::vector<MapClass*>::iterator itmc = map_class_vector->begin();
	MapClass *map_class;
	// AAE: get Map of species and classified pixels
	classPixels *pixelsSpecies = LayerOnSelectionDesc::getPixelsSpecies();
	classPixels::iterator it;
	mRegion *region;
	region = new mRegion;
	vector<MPolygon* > *pols = LayerOnSelectionDesc::getVectorPolygons();
	// AAE: color maps
	int *red = image_map->m_colormap_red;
	int *green = image_map->m_colormap_green;
	int *blue = image_map->m_colormap_blue;
	int current_class;
	for (lit=lab->begin(); lit!=lab->end(); ++lit) {
		int l = (int)*lit;
		i=ind%x;
		j=ind/x;
		++ind;
		if (slab.find(l) != slab.end())
		{
			if (*itpix == 0)
			{
				int redCompLayer;
				//redCompLayer = (int)((((float)DescLayer->getRed())/(255.*(float)(DescLayer->getOpacity()))+1.) * ((float)(*iit)*(((1.-(float)(DescLayer->getOpacity()))+1.))));
				redCompLayer = (int)((float)DescLayer->getOpacity()*(float)DescLayer->getRed()+(float)(-1.*DescLayer->getOpacity()+1.)*(float)(*iit));
				if (redCompLayer < 255)
				{
					*yit = red[redCompLayer]; // new version with color maps
				}
				else
					*yit = 255;
			}
			/*else
			{
				for (itmc=map_class_vector->begin(); itmc!=map_class_vector->end(); ++itmc) 
				{
					map_class = *itmc;
					if (*itpix == map_class->getId())
					{
						*yit = map_class->getRed();
					}
				}
			}*/

			*oit=0;
			++oit;
			++iit;
			++yit;

			if (*itpix == 0)
			{
				int redCompLayer;
				//redCompLayer = (int)((((float)DescLayer->getRed())/(255.*(float)(DescLayer->getOpacity()))+1.) * ((float)(*iit)*(((1.-(float)(DescLayer->getOpacity()))+1.))));
				redCompLayer = (int)((float)DescLayer->getOpacity()*(float)DescLayer->getRed()+(float)(-1.*DescLayer->getOpacity()+1.)*(float)(*iit));
				int greenCompLayer;
				//greenCompLayer = (int)((((float)DescLayer->getGreen())/(255.*(float)(DescLayer->getOpacity()))+1.) * ((float)(*iit)*(((1.-(float)(DescLayer->getOpacity()))+1.))));
				greenCompLayer = (int)((float)DescLayer->getOpacity()*(float)DescLayer->getGreen()+(float)(-1.*DescLayer->getOpacity()+1.)*(float)(*iit));
				if (greenCompLayer < 255)
				{
					*yit = green[redCompLayer]; // new version with colormaps
				}
				else
					*yit = 255;
			}

			*oit=0;
			++oit;
			++iit;
			++yit;

			if (*itpix == 0)
			{
				int redCompLayer;
				//redCompLayer = (int)((((float)DescLayer->getRed())/(255.*(float)(DescLayer->getOpacity()))+1.) * ((float)(*iit)*(((1.-(float)(DescLayer->getOpacity()))+1.))));
				redCompLayer = (int)((float)DescLayer->getOpacity()*(float)DescLayer->getRed()+(float)(-1.*DescLayer->getOpacity()+1.)*(float)(*iit));
				
				int blueCompLayer;			
				//blueCompLayer = (int)((((float)DescLayer->getBlue())/(255.*(float)(DescLayer->getOpacity()))+1.) * ((float)(*iit)*(((1.-(float)(DescLayer->getOpacity()))+1.))));
				blueCompLayer = (int)((float)DescLayer->getOpacity()*(float)DescLayer->getBlue()+(float)(-1.*DescLayer->getOpacity()+1.)*(float)(*iit));
				if (blueCompLayer < 255)
				{			
					*yit = blue[redCompLayer]; // new version with color maps
				}
				else
					*yit = 255;
			}

			*oit=0;
			++oit;
			++iit;
			++yit;

			for (int c=3;c<nc;++c) {				 
				*oit = *iit;
				++oit;
				++iit;
				++yit;
			}

			/* Deprecated, by AAE
			for (itmc=map_class_vector->begin(); itmc!=map_class_vector->end(); ++itmc) 
			{
				map_class = *itmc;
				if (DescLayer->getRed() == map_class->getRed() && DescLayer->getGreen() == map_class->getGreen() && DescLayer->getBlue() == map_class->getBlue())
				{
					if (*itpix == 0)
					{
						*itpix = map_class->getId();
					}
				}
			}
			*/
			/* New version, by AAE
				Using locationDealer for classes
			*/
			LocationLoader *locationDealer = LayerOnSelectionDesc::getLocationLoader();
			Specie* fooSpecie = locationDealer->getSpecieByColor(DescLayer->getRed());
			if(fooSpecie!=NULL && *itpix==0){
				*itpix = fooSpecie->id;
				current_class = *itpix;
				// find the specie with id
				it = pixelsSpecies->find(fooSpecie->id);
				if(it!=pixelsSpecies->end())
					// increment classified pixels counter
					++((*it).second);
				else{
					pixelsSpecies->insert(pair<int,int>(fooSpecie->id,1));
				}
			  // NEW IN VERSION 3.0
			  // USING POLYGON STRUCTURES
			  mPoint *p = new mPoint(i,j);
			  region->insert(p);

			}
		}
		else
		{
			for (int c=0;c<nc;++c) {				 
				//*oit = *iit;
				if (*itpix == 0)
				{
					*oit=255;
					*yit=0;
				}
				++oit;
				++iit;
				++yit;
			}
		}
		// Evite de depasser la position finale du pointeur car une ImageRamT n'a pas réellement la taille de la MapImage.
		if (++itpix==pixel->end())
		{
			itpix--;
		}

	}	
	// we merge all the region sets 
	// of the same class id
	// that is, we merge all the polygons
	// of the same class
	vector<MPolygon*>::iterator itPols;
	mRegion::iterator itReg;
	MPolygon *pol;
	mRegion *fooReg;
	for(itPols=pols->begin();itPols!=pols->end();++itPols){
	  if((*itPols)->GetClassId()!=current_class) continue;
	  fooReg=(*itPols)->GetPixels();
	  for(itReg=fooReg->begin();itReg!=fooReg->end();++itReg){
	    mPoint *p = new mPoint((*itReg)->first, (*itReg)->second);
	    region->insert(p);
	  }
	}
	for(itPols=pols->begin();itPols!=pols->end();++itPols){
	  if((*itPols)->GetClassId()!=current_class) continue;
	  MPolygon *pol = (*itPols);
	  pols->erase(itPols);
	  itPols = --(pols->begin());
	  delete pol;
	}
	while((pol=SplitPolygons(region))!=NULL){
	  pol->SetClassId(current_class);
	  pols->push_back(pol);
	}
}


//========================================================================================
template <class T>
void TLayersOnSelectedRegions( T, const ImageRam& in, const ImageRam& lab, const std::set <int>& slab, pImageRam& out, pImageRam& layer, ImageMapping *image_map)
{//, int red, int green, int blue ) {
	lglSwitchOnTypeFunctionCall8( lab.type(), TLLayersOnSelectedRegions, (T)(0), in, lab, slab, out, layer, image_map);//, red, green, blue );
}


//========================================================================================
void LayersOnSelectedRegions( const ImageRam& in, const ImageRam& lab, const std::set <int>& slab, pImageRam& out, pImageRam& layer, ImageMapping *image_map)//, int red, int green, int blue ) 
{
	lglSwitchOnTypeFunctionCall7( in.type(), TLayersOnSelectedRegions, in, lab, slab, out, layer, image_map);// red, green, blue );
}
//========================================================================================
//========================================================================================
//========================================================================================

//========================================================================================
//========================================================================================
//========================================================================================
/// \brief Produces an image out whose values are values of in with red channel shift on the regions of same value of lab. 
/// 
/// out can be in.
template <class T, class L>
void TLUnLayersOnSelectedRegions( L, T, const ImageRam& rin, const ImageRam& rlab, const std::set <int>& slab, pImageRam& rout, pImageRam& rlayer, ImageMapping *image_map)
{//, int red, int green, int blue ) {
	// downcast 
	const ImageRamT<T>* in = dynamic_cast<const ImageRamT<T>*>(&rin);
	if (!in) return;
	const ImageRamT<L>* lab = dynamic_cast<const ImageRamT<L>*>(&rlab);
	if (!lab) return;

	if (!rout) rout = new ImageRamT<T>;
	rout->resize(in->size());
	ImageRamT<T>* out = dynamic_cast<ImageRamT<T>*>(rout);
	ImageRamT<T>* layer = dynamic_cast<ImageRamT<T>*>(rlayer);

	//
	int nc = in->size(0);
	int x = in->size(1);
	int y = in->size(2);
	int ind=0,i,j;

	typename ImageRamT<L>::const_iterator lit;

	out->resize(in->size());
	layer->resize(in->size());
	typename ImageRamT<T>::iterator oit=out->begin();
	typename ImageRamT<T>::iterator yit=layer->begin();
	typename ImageRamT<T>::const_iterator iit=in->begin();

	LayerOnSelectionDesc *DescLayer = LayerOnSelectionDesc::getLayerOnSelection();
	std::vector<unsigned int> *pixel = image_map->getMapImage()->getPixelList();
	std::vector<unsigned int>::iterator itpix = pixel->begin();

	// AAE: get Map of species and classified pixels
	classPixels *pixelsSpecies = LayerOnSelectionDesc::getPixelsSpecies();
	classPixels::iterator it;
	bool erase = false;
	// NEW FOR VERSION 3.0
	mRegion *region;
	region = new mRegion;
	vector<MPolygon* > *pols = LayerOnSelectionDesc::getVectorPolygons();
	int current_class;
	for (lit=lab->begin(); lit!=lab->end(); ++lit) {
		int l = (int)*lit;
		i=ind%x;
		j=ind/x;
		++ind;
		if (slab.find(l) != slab.end())
		{
			/* New version, by AAE
				Using locationDealer for classes
			*/
			LocationLoader *locationDealer = LayerOnSelectionDesc::getLocationLoader();
			Specie* fooSpecie = locationDealer->getSpecieById(*itpix);
			erase = false;
			if(fooSpecie!=NULL){
				// find the specie with id
				it = pixelsSpecies->find(fooSpecie->id);
				if(it!=pixelsSpecies->end()){
					// increment classified pixels counter
					--((*it).second);
					*itpix = 0;
					*yit = 0;
					erase = true;
					// NEW IN VERSION 3.0
					// USING POLYGON STRUCTURES
					mPoint *p = new mPoint(i,j);
					region->insert(p);
					current_class = fooSpecie->id;
				}
				/*
				else{
					pixelsSpecies->insert(pair<int,int>(fooSpecie->id,1));
				}
				*/
			}

			
/*
			if (*itpix != 0)
			{
				*yit = 0;
			}
*/
			*oit=255;
			++oit;
			++iit;
			++yit;

			if (erase)
			{
				*yit = 0;
			}

			*oit=255;
			++oit;
			++iit;
			++yit;

			if (erase)
			{
				*yit = 0;
			}

			*oit=255;
			++oit;
			++iit;
			++yit;

			for (int c=3;c<nc;++c) {				 
				*oit = *iit;
				++oit;
				++iit;
				++yit;
			}
		}
		else
		{
			for (int c=0;c<nc;++c) {				 
				//*oit = *iit;
				if (*itpix == 0)
				{
					*oit=255;
					*yit=0;
				}
				++oit;
				++iit;
				++yit;
			}
		}
		// Evite de depasser la position finale du pointeur car une ImageRamT n'a pas réellement la taille de la MapImage.
		if (++itpix==pixel->end())
		{
			itpix--;
		}

	}
	// we erase pixels to be substracted
	// from the polygon matching the current_class
	vector<MPolygon*>::iterator itPols;
	mRegion::iterator itReg;
	MPolygon *pol;
	mRegion *fooReg;
	for(itPols=pols->begin();itPols!=pols->end();++itPols){
	  if((*itPols)->GetClassId()!=current_class) continue;
	  fooReg=(*itPols)->GetPixels();
	  for(itReg=region->begin();itReg!=region->end();++itReg){
	    fooReg->erase((*itReg));
	  }
	  (*itPols)->UpdatePerimeter();
	}

}


//========================================================================================
template <class T>
void TUnLayersOnSelectedRegions( T, const ImageRam& in, const ImageRam& lab, const std::set <int>& slab, pImageRam& out, pImageRam& layer, ImageMapping *image_map)
{//, int red, int green, int blue ) {
	lglSwitchOnTypeFunctionCall8( lab.type(), TLUnLayersOnSelectedRegions, (T)(0), in, lab, slab, out, layer, image_map);//, red, green, blue );
}


//========================================================================================
void UnLayersOnSelectedRegions( const ImageRam& in, const ImageRam& lab, const std::set <int>& slab, pImageRam& out, pImageRam& layer, ImageMapping *image_map)//, int red, int green, int blue ) 
{
	lglSwitchOnTypeFunctionCall7( in.type(), TUnLayersOnSelectedRegions, in, lab, slab, out, layer, image_map);// red, green, blue );
}
//========================================================================================
//========================================================================================
//========================================================================================

//========================================================================================
//========================================================================================
//========================================================================================
/// \brief Produces an image out whose values are values of in with red channel shift on the regions of same value of lab. 
/// 
/// out can be in.
template <class T, class L>
void TLInitLayersOnSelectedRegions( L, T, const ImageRam& rin, const ImageRam& rlab, pImageRam& rout, pImageRam& rlayer, ImageMapping *image_map)
{//, int red, int green, int blue ) {
	// downcast 
	const ImageRamT<T>* in = dynamic_cast<const ImageRamT<T>*>(&rin);
	if (!in) return;
	const ImageRamT<L>* lab = dynamic_cast<const ImageRamT<L>*>(&rlab);
	if (!lab) return;

	if (!rout) rout = new ImageRamT<T>;
	rout->resize(in->size());
	ImageRamT<T>* out = dynamic_cast<ImageRamT<T>*>(rout);
	ImageRamT<T>* layer = dynamic_cast<ImageRamT<T>*>(rlayer);

	//
	int nc = in->size(0);
	typename ImageRamT<L>::const_iterator lit;

	out->resize(in->size());
	layer->resize(in->size());
	typename ImageRamT<T>::iterator oit=out->begin();
	typename ImageRamT<T>::iterator yit=layer->begin();
	typename ImageRamT<T>::const_iterator iit=in->begin();

	LayerOnSelectionDesc *DescLayer = LayerOnSelectionDesc::getLayerOnSelection();
	std::vector<unsigned int> *pixel = image_map->getMapImage()->getPixelList();
	std::vector<unsigned int>::iterator itpix = pixel->begin();

	std::vector<MapClass*> *map_class_vector = image_map->getMapClassVector();
	std::vector<MapClass*>::iterator itmc = map_class_vector->begin();
	MapClass *map_class;

	for (lit=lab->begin(); lit!=lab->end(); ++lit) 
	{
		if (*itpix != 0)
		{
			for (itmc=map_class_vector->begin(); itmc!=map_class_vector->end(); ++itmc) 
			{
				map_class = *itmc;
				//wxLogMessage("///////////////////////////// %d // %d",(int)*itpix,(int)map_class->getId());
				if (*itpix == map_class->getId())
				{
					*yit = map_class->getRed();

					*oit=0;
					++oit;
					++iit;
					++yit;

					*yit = map_class->getGreen();

					*oit=0;
					++oit;
					++iit;
					++yit;

					*yit = map_class->getBlue();

					*oit=0;
					++oit;
					++iit;
					++yit;

					for (int c=3;c<nc;++c) 
					{				 
						*oit = *iit;
						++oit;
						++iit;
						++yit;
					}
				}
			}
		}
		else
		{
			for (int c=0;c<nc;++c) 
			{				 
				//*oit = *iit;
				*oit=255;
				*yit=0;
				++oit;
				++iit;
				++yit;
			}
		}
		// Evite de depasser la taille maximale du pointeur car une ImageRamT n'a pas réellement la taille de la MapImage.
		if (++itpix==pixel->end())
		{
			itpix--;
		}	
	}
}


//========================================================================================
template <class T>
void TInitLayersOnSelectedRegions( T, const ImageRam& in, const ImageRam& lab, pImageRam& out, pImageRam& layer, ImageMapping *image_map)
{//, int red, int green, int blue ) {
	lglSwitchOnTypeFunctionCall7( lab.type(), TLInitLayersOnSelectedRegions, (T)(0), in, lab, out, layer, image_map);//, red, green, blue );
}


//========================================================================================
void InitLayersOnSelectedRegions( const ImageRam& in, const ImageRam& lab, pImageRam& out, pImageRam& layer, ImageMapping *image_map)//, int red, int green, int blue ) 
{
	lglSwitchOnTypeFunctionCall6( in.type(), TInitLayersOnSelectedRegions, in, lab, out, layer, image_map);// red, green, blue );
}
//========================================================================================
//========================================================================================
//========================================================================================

static LayerOnSelectionDesc *s_LoSDescription;
static LocationLoader *locationDealer;
static classPixels* pixelsSpecies;
static vector<MPolygon *> *m_pols;

void LayerOnSelectionDesc::setVectorPolygons(vector<MPolygon *> *_pols){
  m_pols = _pols;
}

void LayerOnSelectionDesc::setPixelsSpecies(classPixels *_pixelsSpecies){
	pixelsSpecies = _pixelsSpecies;
}

vector<MPolygon *>* LayerOnSelectionDesc::getVectorPolygons(){
  return m_pols;
}

classPixels* LayerOnSelectionDesc::getPixelsSpecies(){
	return pixelsSpecies;
}

LayerOnSelectionDesc::LayerOnSelectionDesc()
{
}


LayerOnSelectionDesc::~LayerOnSelectionDesc()
{
}

LayerOnSelectionDesc* LayerOnSelectionDesc::setLayerOnSelection(int l_red=0, int l_green=0, int l_blue=0, float l_opacity=1.)
{
	if (s_LoSDescription)
	{
		s_LoSDescription->red = l_red;
		s_LoSDescription->green = l_green;
		s_LoSDescription->blue = l_blue;
		s_LoSDescription->opacity = l_opacity;
	}
	else
	{
		s_LoSDescription = new LayerOnSelectionDesc();
		s_LoSDescription->red = l_red;
		s_LoSDescription->green = l_green;
		s_LoSDescription->blue = l_blue;
		s_LoSDescription->opacity = l_opacity;
	}
	return s_LoSDescription;
}

LayerOnSelectionDesc* LayerOnSelectionDesc::getLayerOnSelection()
{
	if (!s_LoSDescription)
		s_LoSDescription = setLayerOnSelection();

	return s_LoSDescription;
}

LocationLoader* LayerOnSelectionDesc::getLocationLoader(){
	return locationDealer;
}

void LayerOnSelectionDesc::setLocationLoader(	LocationLoader *_locationDealer){
	locationDealer = _locationDealer;
}


int LayerOnSelectionDesc::getRed()
{
	return red;
}

int LayerOnSelectionDesc::getGreen()
{
	return green;
}

int LayerOnSelectionDesc::getBlue()
{
	return blue;
}

float LayerOnSelectionDesc::getOpacity()
{
	return opacity;
}








//======================================================================================
template <class T>
void TImageToImageRGBmed ( T, const ImageRam& rin, 
					   ImageRGB& out,  
					   const Palette& pal, 
					   int zoom)
{

	// dynamic downcast
	const ImageRamT<T>* in = dynamic_cast<const ImageRamT<T>*>(&rin);
	if (!in) return;	

	// test that in is a 2D image
	if (in->dimension()!=2) return;
	// output dimensions 
	ImageSite s(3, zoom*in->size(1), zoom*in->size(2) );
	if (!out.resize(s)) return;

	typename ImageRamT<T>::const_iterator iit=in->begin();
	long line_size = 3*out.size(1);
	unsigned char* po; 
	unsigned char* po2;
	unsigned char* po3;
	unsigned char* co;
	unsigned char* co2;
	int i,j,z;
	for (j=0;j<in->size(2);j++) {
		po = &out(0,0,j*zoom);
		co = po;
		for (i=0;i<in->size(1);i++) {
			co2 = co;
			*co = *iit;
			++co;++iit;
			*co = *iit;
			++co;++iit;
			*co = *iit;
			++co;++iit;
			for (int a=3;a<in->size(0);++a) 
			{				 
				++iit;
			}
			po3 = co2 + 3;
			for (z=0;z<zoom-1;++z) {
				memcpy(po3,co2,3);
				po3 += 3;
			}
			co = po3;
		}
		// line copy
		po2 = po + line_size;
		for (z=0;z<zoom-1;++z) {
			memcpy(po2,po,line_size);
			po2 += line_size;
		}
	}
	//
	/*long line_size = 3*out.size(1);
	unsigned char* po; 
	int i,j,z,c;
	for (j=0;j<in->size(2);j++) {
		const T* pi = &((*in)(0,0,j));
		po = &out(0,0,j*zoom);
		unsigned char* co = po;
		for (i=0;i<in->size(1);i++) {
			// builds the image values vector
			std::vector<double> vec(in->size(0));
			for (c=0;c<in->size(0);++c) { vec[c] = *pi; pi++; }
			// RGB mapping
			RGBpoint col = pal(vec);
			// output
			for (z=0;z<zoom;++z) {
				(*co) = col.r; ++co;
				(*co) = col.g; ++co;
				(*co) = col.b; ++co;
			}
		}
		// line copy
		unsigned char* po2 = po + line_size;
		for (z=0;z<zoom-1;++z) {
			memcpy(po2,po,line_size);
			po2 += line_size;
		}
	}*/
}
  //======================================================================================

  //======================================================================================
  void ImageToImageRGBmed ( const ImageRam& in, 
			 ImageRGB& out,  
			 const Palette& pal, 
			 int zoom)
  {
    lglSwitchOnTypeFunctionCall5( in.type(), TImageToImageRGBmed, in, out, pal, zoom );
  }
  //======================================================================================



  //======================================================================================


  //======================================================================================
  /// Same as ImageToImageRGB but overlays the contours of a label image
  template <class T, class L>
	  void TLImageAndLabelsToImageRGBmed ( T, L, const ImageRam& rin, 
	  const ImageRam& rlab,
	  ImageRGB& out,  
	  const Palette& pal, 
	  RGBpoint contours_color,
	  int zoom)
  {
	  // dynamic downcast
	  const ImageRamT<T>* in = dynamic_cast<const ImageRamT<T>*>(&rin);
	  const ImageRamT<L>* lab = dynamic_cast<const ImageRamT<L>*>(&rlab);


	  // test that in is a 2D image
	  if (in->dimension()!=2) return;
	  // output dimensions 
	  ImageSite s(3, zoom*in->size(1), zoom*in->size(2) );
	  if (!out.resize(s)) return;

	  long line_size = 3*out.size(1);
	  typename ImageRamT<T>::const_iterator iit=in->begin();
	  unsigned char* po; 
	  unsigned char* po2;
	  unsigned char* po3;
	  unsigned char* po4;
	  unsigned char* co;
	  unsigned char* co2;
	  unsigned char* co3;
	  unsigned char* llo;
	  int i,j,z,c;
	  for (j=0;j<in->size(2)-1;j++) {
		  po = &out(0,0,j*zoom);
		  co = po;
		  llo = co + (zoom-1)*line_size;
		  const T* pi = &((*in)(0,0,j));
		  const L* pl0 = &((*lab)(0,0,j));
		  const L* pl1 = pl0+1;
		  const L* pl2 = &((*lab)(0,0,j+1));
		  std::vector<double> vec(in->size(0));
		  for (i=0;i<in->size(1);i++) {
			  // builds the image values vector
			  //for (c=0;c<in->size(0);++c) { vec[c] = *pi; pi++; }
			  for (c=0;c<in->size(0);++c) {pi++;}
			  // RGB mapping
			  //RGBpoint col = pal(vec);

			  if ((*pl0)!=(*pl2) && zoom > 1) {
				  //for (z=0;z<zoom-1;++z) {
				  //(*llo) = contours_color.r; (*co) = col.r; ++co; ++llo;
				  //(*llo) = contours_color.g; (*co) = col.g; ++co; ++llo;
				  //(*llo) = contours_color.b; (*co) = col.b; ++co; ++llo;
				  co2 = co;
				  co3 = llo;
				  *co = *iit;
				  *llo = contours_color.r;
				  ++co;++iit;++llo;
				  *co = *iit;
				  *llo = contours_color.g;
				  ++co;++iit;++llo;
				  *co = *iit;
				  *llo = contours_color.b;
				  ++co;++iit;++llo;
				  for (int a=3;a<in->size(0);++a) 
				  {				 
					  ++iit;
				  }
				  po4 = co3 + 3;
				  po3 = co2 + 3;
				  for (z=0;z<zoom-2;++z) {
					  memcpy(po3,co2,3);
					  memcpy(po4,co3,3);
					  po3 += 3;
					  po4 += 3;
				  }
				  co = po3;
				  llo = po4;
				  // }
			  }
			  else if (zoom > 1) 
			  {
				  co2 = co;
				  co3 = llo;
				  *llo = *co = *iit;
				  ++co;++iit;++llo;
				  *llo = *co = *iit;
				  ++co;++iit;++llo;
				  *llo = *co = *iit;
				  ++co;++iit;++llo;
				  for (int a=3;a<in->size(0);++a) 
				  {				 
					  ++iit;
				  }
				  po4 = co3 + 3;
				  po3 = co2 + 3;
				  for (z=0;z<zoom-2;++z) {
					  memcpy(po3,co2,3);
					  memcpy(po4,co3,3);
					  po3 += 3;
					  po4 += 3;
				  }
				  co = po3;
				  llo = po4;
			  }
			  // last point of the zoomed pixel			  
			  if (zoom > 1)
			  {
				  if ((*pl0) != (*pl1))
				  *co = contours_color.r;
			  else
				  *co = co2[0];
			  if ( ((*pl0) != (*pl2)) || ((*pl0) != (*pl1)) )
				  *llo = contours_color.r;
			  else
				  *llo = co2[0];
			  ++co;++llo;
			  if ((*pl0) != (*pl1))
				  *co = contours_color.g;
			  else
				  *co = co2[1];
			  if ( ((*pl0) != (*pl2)) || ((*pl0) != (*pl1)) )
				  *llo = contours_color.g;
			  else
				  *llo = co2[1];
			  ++co;++llo;
			  if ((*pl0) != (*pl1))
				  *co = contours_color.b;
			  else
				  *co = co2[2];
			  if ( ((*pl0) != (*pl2)) || ((*pl0) != (*pl1)) )
				  *llo = contours_color.b;
			  else
				  *llo = co2[2];
			  ++co;++llo;
			  }
			  else
			  {
				  co2 = co;
				  co3 = llo;
				  if ((*pl0) != (*pl1))
					  *co = contours_color.r;
				  else
					  *co = *iit;
				  if ( ((*pl0) != (*pl2)) || ((*pl0) != (*pl1)) )
					  *llo = contours_color.r;
				  else
					  *llo = *iit;
				  //*llo = *co = *iit;
				  ++co;++iit;++llo;
				  if ((*pl0) != (*pl1))
					  *co = contours_color.g;
				  else
					  *co = *iit;
				  if ( ((*pl0) != (*pl2)) || ((*pl0) != (*pl1)) )
					  *llo = contours_color.g;
				  else
					  *llo = *iit;
				  //*llo = *co = *iit;
				  ++co;++iit;++llo;
				  if ((*pl0) != (*pl1))
					  *co = contours_color.b;
				  else
					  *co = *iit;
				  if ( ((*pl0) != (*pl2)) || ((*pl0) != (*pl1)) )
					  *llo = contours_color.b;
				  else
					  *llo = *iit;
				  //*llo = *co = *iit;
				  ++co;++iit;++llo;
			  }
			  /*RGBpoint llcol = col;
			  if ( ((*pl0) != (*pl2)) || ((*pl0) != (*pl1)) ) llcol = contours_color;
			  if ((*pl0) != (*pl1)) col = contours_color;
			  (*llo) = llcol.r; (*co) = col.r; ++co; ++llo; //++iit;
			  (*llo) = llcol.g; (*co) = col.g; ++co; ++llo; //++iit;
			  (*llo) = llcol.b; (*co) = col.b; ++co; ++llo;	//++iit;
			  //++iit;
			  /*for (int a=0;a<in->size(0);++a) 
			  {				 
				  ++iit;
			  }*/
			  ++pl0;
			  ++pl1;
			  ++pl2;
		  }
		  // last pixel of the line
		  // builds the image values vector
		  //std::vector<double> vec(in->size(0));
		  for (c=0;c<in->size(0);++c) { vec[c] = *pi; pi++; }
		  // RGB mapping
		  RGBpoint col = pal(vec);

		  RGBpoint llcol = col;
		  if ((*pl0) != (*pl2)) llcol = contours_color;
		  for (z=0;z<zoom;++z) {
			  (*llo) = llcol.r; (*co) = col.r; ++co; ++llo;
			  (*llo) = llcol.g; (*co) = col.g; ++co; ++llo;
			  (*llo) = llcol.b; (*co) = col.b; ++co; ++llo;			
		  }
		  // line copy
		  unsigned char* po2 = po + line_size;
		  for (z=0;z<zoom-2;++z) {
			  memcpy(po2,po,line_size);
			  po2 += line_size;
		  }
	  }
	  // last line
	  const T* pi = &((*in)(0,0,j));
	  po = &out(0,0,j*zoom);
	  const L* pl0 = &((*lab)(0,0,j));
	  const L* pl1 = pl0+1;	
	  co = po;
	  for (i=0;i<in->size(1)-1;i++) {
		  // builds the image values vector
		  std::vector<double> vec(in->size(0));
		  for (c=0;c<in->size(0);++c) { vec[c] = *pi; pi++; }
		  // RGB mapping
		  RGBpoint col = pal(vec);

		  for (z=0;z<zoom-1;++z) {
			  (*co) = col.r; ++co;
			  (*co) = col.g; ++co;
			  (*co) = col.b; ++co;
		  }
		  // last point of the zoomed pxl
		  if ((*pl0) != (*pl1)) col = contours_color;
		  (*co) = col.r; ++co; 
		  (*co) = col.g; ++co; 
		  (*co) = col.b; ++co; 	
		  ++pl0;
		  ++pl1;
	  }
	  // last pixel of the line
	  // builds the image values vector
	  std::vector<double> vec(in->size(0));
	  for (c=0;c<in->size(0);++c) { vec[c] = *pi; pi++; }
	  // RGB mapping
	  RGBpoint col = pal(vec);

	  for (z=0;z<zoom;++z) {
		  (*co) = col.r; ++co;
		  (*co) = col.g; ++co;
		  (*co) = col.b; ++co;
	  }

	  // line copy
	  po2 = po + line_size;
	  for (z=0;z<zoom-1;++z) {
		  memcpy(po2,po,line_size);
		  po2 += line_size;
	  }
	  //
	 /* long line_size = 3*out.size(1);
	  unsigned char* po; 
	  int i,j,z,c;
	  for (j=0;j<in->size(2)-1;j++) {
		  po = &out(0,0,j*zoom);
		  unsigned char* co = po;
		  unsigned char* llo = co + (zoom-1)*line_size;
		  const T* pi = &((*in)(0,0,j));
		  const L* pl0 = &((*lab)(0,0,j));
		  const L* pl1 = pl0+1;
		  const L* pl2 = &((*lab)(0,0,j+1));
		  for (i=0;i<in->size(1)-1;i++) {
			  // builds the image values vector
			  std::vector<double> vec(in->size(0));
			  for (c=0;c<in->size(0);++c) { vec[c] = *pi; pi++; }
			  // RGB mapping
			  RGBpoint col = pal(vec);

			  if ((*pl0)!=(*pl2)) {
				  for (z=0;z<zoom-1;++z) {
					  (*llo) = contours_color.r; (*co) = col.r; ++co; ++llo;
					  (*llo) = contours_color.g; (*co) = col.g; ++co; ++llo;
					  (*llo) = contours_color.b; (*co) = col.b; ++co; ++llo;
				  }
			  }
			  else {
				  for (z=0;z<zoom-1;++z) {
					  (*llo) = (*co) = col.r; ++co; ++llo;
					  (*llo) = (*co) = col.g; ++co; ++llo;
					  (*llo) = (*co) = col.b; ++co; ++llo;
				  }
			  }
			  // last point of the zoomed pixel
			  RGBpoint llcol = col;
			  if ( ((*pl0) != (*pl2)) || ((*pl0) != (*pl1)) ) llcol = contours_color;
			  if ((*pl0) != (*pl1)) col = contours_color;
			  (*llo) = llcol.r; (*co) = col.r; ++co; ++llo;
			  (*llo) = llcol.g; (*co) = col.g; ++co; ++llo;
			  (*llo) = llcol.b; (*co) = col.b; ++co; ++llo;			
			  ++pl0;
			  ++pl1;
			  ++pl2;
		  }
		  // last pixel of the line
		  // builds the image values vector
		  std::vector<double> vec(in->size(0));
		  for (c=0;c<in->size(0);++c) { vec[c] = *pi; pi++; }
		  // RGB mapping
		  RGBpoint col = pal(vec);

		  RGBpoint llcol = col;
		  if ((*pl0) != (*pl2)) llcol = contours_color;
		  for (z=0;z<zoom;++z) {
			  (*llo) = llcol.r; (*co) = col.r; ++co; ++llo;
			  (*llo) = llcol.g; (*co) = col.g; ++co; ++llo;
			  (*llo) = llcol.b; (*co) = col.b; ++co; ++llo;			
		  }
		  // line copy
		  unsigned char* po2 = po + line_size;
		  for (z=0;z<zoom-2;++z) {
			  memcpy(po2,po,line_size);
			  po2 += line_size;
		  }
	  }
	  // last line
	  const T* pi = &((*in)(0,0,j));
	  po = &out(0,0,j*zoom);
	  const L* pl0 = &((*lab)(0,0,j));
	  const L* pl1 = pl0+1;	
	  unsigned char* co = po;
	  for (i=0;i<in->size(1)-1;i++) {
		  // builds the image values vector
		  std::vector<double> vec(in->size(0));
		  for (c=0;c<in->size(0);++c) { vec[c] = *pi; pi++; }
		  // RGB mapping
		  RGBpoint col = pal(vec);

		  for (z=0;z<zoom-1;++z) {
			  (*co) = col.r; ++co;
			  (*co) = col.g; ++co;
			  (*co) = col.b; ++co;
		  }
		  // last point of the zoomed pxl
		  if ((*pl0) != (*pl1)) col = contours_color;
		  (*co) = col.r; ++co; 
		  (*co) = col.g; ++co; 
		  (*co) = col.b; ++co; 	
		  ++pl0;
		  ++pl1;
	  }
	  // last pixel of the line
	  // builds the image values vector
	  std::vector<double> vec(in->size(0));
	  for (c=0;c<in->size(0);++c) { vec[c] = *pi; pi++; }
	  // RGB mapping
	  RGBpoint col = pal(vec);

	  for (z=0;z<zoom;++z) {
		  (*co) = col.r; ++co;
		  (*co) = col.g; ++co;
		  (*co) = col.b; ++co;
	  }

	  // line copy
	  unsigned char* po2 = po + line_size;
	  for (z=0;z<zoom-1;++z) {
		  memcpy(po2,po,line_size);
		  po2 += line_size;
	  }*/
  }
  //======================================================================================

  //======================================================================================
  /// Same as ImageToImageRGB but overlays the contours of a label image
  template <class L>
	  void LImageAndLabelsToImageRGBmed ( L, const ImageRam& in, 
	  const ImageRam& lab,
	  ImageRGB& out,  
	  const Palette& pal, 
	  RGBpoint contours_color,
	  int zoom)
  {
	  lglSwitchOnTypeFunctionCall8( in.type(), TLImageAndLabelsToImageRGBmed, (L)0, in, lab, out, pal, contours_color, zoom );
  }
  //======================================================================================
  /// Same as ImageToImageRGB but overlays the contours of a label image
  void ImageAndLabelsToImageRGBmed ( const ImageRam& in, 
	  const ImageRam& lab,
	  ImageRGB& out,  
	  const Palette& pal, 
	  RGBpoint contours_color,
	  int zoom)
  {
	  lglSwitchOnTypeFunctionCall7( lab.type(), LImageAndLabelsToImageRGBmed, in, lab, out, pal, contours_color, zoom );
  }
  //======================================================================================
