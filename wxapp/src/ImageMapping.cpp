/*
*	ImageMapping class is used to contain information about the image to be segmented,
*	and LayerOnSelection class is used to manage the layers to be drawn over the image
*	to be segmented. The layers will contain the colored regions for each classified
*	species.
*/
#ifndef __SXSWX__
#define __SXSWX__ 1
#endif
#include "InterfaceMedSeg.h"
#include "ImageMapping.h"

#define CLASSIF_RAW_SIZE 256

ImageMapping::ImageMapping()
{
	m_classifMap = new std::vector<MapClass*>();
	m_MapImage = NULL;
	//m_pixel = new std::vector<unsigned char>();
	//m_pixel->resize(height*width);
}

ImageMapping::~ImageMapping()
{
}

void ImageMapping::SetColorMaps(int *red, int *green, int *blue){
m_colormap_red = red;
m_colormap_green = green;
m_colormap_blue = blue;
}

void ImageMapping::GetColorMaps(int *red, int *green, int *blue){
red = m_colormap_red;
green = m_colormap_green;
blue = m_colormap_blue;
}

void ImageMapping::saveClassification(wxString filename)
{
	/*	lgl::Filename saveClassif(filename);
	lgl::Filename body(filename);
	body = lgl::removeExtension(body);
	lgl::setExtension(saveClassif,"med");
	std::ofstream f;
	f.open((char*)saveClassif.c_str(),std::ios::out|std::ios::app|std::ios::binary);*/
	parseClassification(filename);
	//f.close();
}

void ImageMapping::parseClassification(wxString filename)
{
  lgl::Filename parseClassif(filename.mb_str());
  lgl::Filename body(filename.mb_str());
	body = lgl::removeExtension(body);
	//lgl::setExtension(parseClassif,"sif");
	std::fstream f;
	f.open("./liste_classes.sif",std::ios::in);
	std::string class_line;
	std::istringstream cut_line;
	char token[CLASSIF_RAW_SIZE];
	char* token_stored;

	if (f.is_open())
	{
		while (!f.eof())
		{
			std::vector<char *> *class_table = new std::vector<char *>();
			std::getline(f,class_line);
			//wxLogMessage("---------LINE : %s",class_line.c_str());
			cut_line.clear();
			cut_line.str(class_line);
			while (cut_line.getline(token, CLASSIF_RAW_SIZE, ';'))// && i<7)
			{
				token_stored = charFactory();
				strcpy(token_stored,token);
				class_table->push_back(token_stored);
				//wxLogMessage(">>>  %s",token);
				//wxLogMessage("---------SIZE : %d",class_table->size());
			}
		MapClass *load_classif = new MapClass(class_table);
		m_classifMap->push_back(load_classif);
		delete class_table;
		}
	}
	f.close();
}

void ImageMapping::writeClassification(wxString filename)
{
  lgl::Filename parseClassif(filename.mb_str());
	//lgl::Filename body(filename);
	//body = lgl::removeExtension(body);
	//lgl::setExtension(parseClassif,"sif");
	std::fstream f;
	f.open("./liste_classes.sif",std::ios::out|std::ios::trunc);
	std::string class_line;
	std::istringstream cut_line;

	if (f.is_open())
	{
		unsigned int i=0;
		while (i<m_classifMap->size())
		{
			if (i+1<m_classifMap->size())
			{
				f << m_classifMap->at(i)->getName() << ";" 
					<< m_classifMap->at(i)->getGroup() << ";" 
					<< m_classifMap->at(i)->getSite() << ";" 
					<< m_classifMap->at(i)->getId() << ";" 
					<< m_classifMap->at(i)->getRed() << ";" 
					<< m_classifMap->at(i)->getGreen() << ";" 
					<< m_classifMap->at(i)->getBlue() << ";\n";
			}
			else
			{
				f << m_classifMap->at(i)->getName() << ";" 
					<< m_classifMap->at(i)->getGroup() << ";" 
					<< m_classifMap->at(i)->getSite() << ";" 
					<< m_classifMap->at(i)->getId() << ";" 
					<< m_classifMap->at(i)->getRed() << ";" 
					<< m_classifMap->at(i)->getGreen() << ";" 
					<< m_classifMap->at(i)->getBlue() << ";";
			}
			i++;
		}
	}
	f.close();
}

char* ImageMapping::charFactory()
{
	char* newchar = (char*)malloc(256*sizeof(char));
	return newchar;
}

MapImage* ImageMapping::createMapImage(int width, int height, wxString filename)
{
	if (m_MapImage)
	{
		delete m_MapImage;
	}
	m_MapImage = new MapImage(width,height,filename);
	return m_MapImage;
}

MapImage* ImageMapping::getMapImage()
{
	return m_MapImage;
}

void ImageMapping::setMapImage(MapImage *image)
{
	m_MapImage = image;
}

unsigned int ImageMapping::AddClassToMap(char* name, char* group, char* site, int red, int green, int blue)
{
	if ( name != "" && group != "" && site != "" && !(red==0 && green==0 && blue==0) )
	{
		unsigned int id;
		id = getRandId();
		MapClass *mc = new MapClass(name,group,site,id,red,green,blue);
		m_classifMap->push_back(mc);
		return id;
	}
	else
	{
		return 0;
	}
}

unsigned int ImageMapping::getRandId()
{
	srand((unsigned)time(0));
	unsigned int rand_id=0;
	unsigned int lowest=1, highest=32000; 
	unsigned int range=(highest-lowest)+1; 
	rand_id = lowest+(unsigned int)(range*rand()/(RAND_MAX + 1.0)); 
	return rand_id;
}

//-------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------

MapImage::MapImage(int width, int height, wxString filename)
{
	m_filename = filename;
	m_height = height;
	m_width = width;
	m_pixel = new std::vector<unsigned int>();
	m_pixel->resize(height*width);
	std::vector<unsigned int>::iterator itp = m_pixel->begin();
	unsigned int test = 0;

	for (; itp!=m_pixel->end(); ++itp) 
	{
		*itp = test;
	}
	readImage();
}

MapImage::~MapImage()
{
}

void MapImage::readImage()
{
  lgl::Filename saveClassif(m_filename.mb_str());
  lgl::Filename body(m_filename.mb_str());
	body = lgl::removeExtension(body);
	lgl::setExtension(saveClassif,"med");
	std::ifstream f;
	f.open((char*)saveClassif.c_str(),std::ios::in|std::ios::binary);
	std::vector<unsigned int>::iterator itp = m_pixel->begin();
	unsigned int test;

	//for (; itp!=m_pixel->end(); ++itp) 
	//{
		while (f.read(reinterpret_cast<char*>( &test ), sizeof (unsigned int)) && !f.eof())
		{
			*itp = test;
			itp++;
			//wxLogMessage(" ------->> %d", test);
		}
	//}
	f.close();
}

void MapImage::saveImage(wxString filename)
{
	m_filename = filename;
	lgl::Filename saveClassif(m_filename.mb_str());
	lgl::Filename body(m_filename.mb_str());
	body = lgl::removeExtension(body);
	lgl::setExtension(saveClassif,"med");
	std::ofstream f;
	f.open((char*)saveClassif.c_str(),std::ios::out|std::ios::binary);
	//wxLogMessage("--------------------------- %s",(char*)saveClassif.c_str());
	std::vector<unsigned int>::iterator itp = m_pixel->begin();
	unsigned int test;
	for (; itp!=m_pixel->end(); ++itp) 
	{
		test = *itp;
		f.write( reinterpret_cast<char*>( &test ), sizeof (unsigned int)); 
		//wxLogMessage(" -------> %d", test);
	}
	f.close();
	
}

//-------------------------------------------------------------------------
//
//
//-------------------------------------------------------------------------

MapClass::MapClass(char* name, char* group, char* site, unsigned int id, int red, int green, int blue)
{
	this->name = (char*)malloc(256*sizeof(char));
	strcpy(this->name,name);
	this->group = (char*)malloc(256*sizeof(char));
	strcpy(this->group,group);
	this->site = (char*)malloc(256*sizeof(char));
	strcpy(this->site,site);
	this->id = id;
	this->red = red;
	this->green = green;
	this->blue = blue;
}

MapClass::MapClass()
{
}

MapClass::MapClass(std::vector<char *> *desc_class)
{
	int i=0;
	this->setName(desc_class->at(i++));
	this->setGroup(desc_class->at(i++));
	this->setSite(desc_class->at(i++));
	this->setId((unsigned int) atoi(desc_class->at(i++)));
	this->setRed(atoi(desc_class->at(i++)));
	this->setGreen(atoi(desc_class->at(i++)));
	this->setBlue(atoi(desc_class->at(i++)));
}

MapClass::~MapClass()
{
}

void MapClass::setName(char* name)
{
	this->name = name;
}

void MapClass::setGroup(char* group)
{
	this->group = group;
}

void MapClass::setSite(char* site)
{
	this->site = site;
}

void MapClass::setId(unsigned int id)
{
	this->id = id;
}
void MapClass::setRed(int red)
{
	this->red = red;
}
void MapClass::setGreen(int green)
{
	this->green = green;
}

void MapClass::setBlue(int blue)
{
	this->blue = blue;
}

char* MapClass::getName()
{
	return name;
}

char* MapClass::getGroup()
{
	return group;
}

char* MapClass::getSite()
{
	return site;
}

unsigned int MapClass::getId()
{
	return id;
}

int MapClass::getRed()
{
	return red;
}

int MapClass::getGreen()
{
	return green;
}

int MapClass::getBlue()
{
	return blue;
}
