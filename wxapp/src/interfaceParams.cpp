#include "interfaceParams.h"

interfaceParamsLoader::interfaceParamsLoader(const char *_path){
	m_errorString = string("Error:Parameter:Id:Not:Found");
	loadInterfaceParams(_path);
}

void interfaceParamsLoader::loadInterfaceParams(const char *_path){
	string _idParameter;
	string _valueParameter;
	string _type;
	TiXmlDocument _doc(_path);
	if(!_doc.LoadFile()) return;
  	TiXmlHandle _hdoc(&_doc);
	TiXmlElement *_element;
	// module elements
	_element=_hdoc.FirstChildElement().Element();
	if(!_element) return;
	_type = _element->Attribute("name");
	if(_type!=string("sxsGuiParameters")){
		return;
	}
	TiXmlHandle _hroot(_element);
	TiXmlElement *_elem;
	_elem=_hroot.FirstChildElement().Element();
	for(_elem;_elem;_elem=_elem->NextSiblingElement()){
		_idParameter = _elem->Attribute("id");
		_valueParameter = _elem->Attribute("value");
		m_params[_idParameter] = _valueParameter;
	}
}

string interfaceParamsLoader::getParameter(string _id){
	mapParameters::iterator it;
	for(it=m_params.begin();it!=m_params.end();++it){
		if((*it).first==_id) return (*it).second;
	}
	return getErrorString();
}

string interfaceParamsLoader::getErrorString(){
	return m_errorString;
}