/**
*	InterfaceMedSeg is the main class and the entry point of the application. This is the
*	class the wx engine will call at the startup of the application (as stated on sxs.cpp). This
*	is the class containing pointers to all other classes needed by the application and the
*	class modeling the GUI interface. It contains a member of the class LeftWindow for
*	the left frame of the application. It contains a ScrollWinMedSeg member for the frame
*	of the application where the image will be drawn
**/


#ifndef __SXSWX__
#define __SXSWX__ 1
#endif
#ifndef __LGLWX__
#define __LGLWX__ 1
#endif

#include "InterfaceMedSeg.h"
#include "LayerOnSelection.h"
#include "ImageMapping.h"
//#include "DebugMemoryLeaks.h"
#include <fstream>
#include <iostream>
//_________________________________________________________
//#include "sxsInterfaceMedSeg.h"
#include "sxsImageScaleClimbing.h"
#include "sxsE_MumfordShah.h"
//#include "../include/lglModel.h"

#define NB_ECHANT_SEG 16.

using namespace YExcel;

class MS_ScaleClimbingUser : 
//	public virtual sxs::N_Verbose_User,
	public virtual sxs::N_wxProgressDialog_User,
	public virtual sxs::E_MumfordShah_User
{};

/*
static languageDealer *_languageDealer;
static string currentLanguage;
*/

/*//========================================================================================
//========================================================================================
//========================================================================================
/// \brief Produces an image out whose values are values of in with red channel shift on the regions of same value of lab. 
/// 
/// out can be in.
template <class T, class L>
void TLLayersOnSelectedRegions( L, T, const ImageRam& rin, const ImageRam& rlab, const std::set <int>& slab, pImageRam& rout ) {
// downcast 
const ImageRamT<T>* in = dynamic_cast<const ImageRamT<T>*>(&rin);
if (!in) return;
const ImageRamT<L>* lab = dynamic_cast<const ImageRamT<L>*>(&rlab);
if (!lab) return;

if (!rout) rout = new ImageRamT<T>;
rout->resize(in->size());
ImageRamT<T>* out = dynamic_cast<ImageRamT<T>*>(rout);

//
int nc = in->size(0);
typename ImageRamT<L>::const_iterator lit;

out->resize(in->size());
typename ImageRamT<T>::iterator oit=out->begin();
typename ImageRamT<T>::const_iterator iit=in->begin();

for (lit=lab->begin(); lit!=lab->end(); ++lit) {
int l = (int)*lit;

if (slab.find(l) != slab.end())
{
*oit = 255;
++oit;
++iit;

for (int c=1;c<nc;++c) {				 
*oit = *iit;
++oit;
++iit;	  
}
}
else
{
for (int c=0;c<nc;++c) {				 
*oit = *iit;
++oit;
++iit;
}
}
}	
}


//========================================================================================
template <class T>
void TLayersOnSelectedRegions( T, const ImageRam& in, const ImageRam& lab, const std::set <int>& slab, pImageRam& out ) {
lglSwitchOnTypeFunctionCall6( lab.type(), TLLayersOnSelectedRegions, (T)(0), in, lab, slab, out );
}


//========================================================================================
void LayersOnSelectedRegions( const ImageRam& in, const ImageRam& lab, const std::set <int>& slab, pImageRam& out ) 
{
lglSwitchOnTypeFunctionCall5( in.type(), TLayersOnSelectedRegions, in, lab, slab, out );
}
//========================================================================================
//========================================================================================
//========================================================================================*/






//_________________________________________________________

//***********************************************************************
// Fen�tre principale de l'interface
//***********************************************************************
int InterfaceMedSeg::m_colormap_red[256] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,9,13,16,20,25,29,32,36,41,45,48,52,57,61,64,68,73,77,80,84,89,93,96,100,105,109,112,116,121,125,128,132,137,141,144,148,153,157,160,164,169,173,176,180,185,189,192,196,201,205,208,212,217,221,224,228,233,237,240,244,249,253,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255};
int InterfaceMedSeg::m_colormap_green[256] = {255,251,247,242,239,235,230,226,223,219,215,210,207,203,198,194,191,187,183,178,175,171,166,162,159,155,151,146,143,139,134,130,127,123,119,114,111,106,102,98,95,91,87,82,79,74,70,66,63,59,55,50,47,42,38,34,31,27,23,18,15,10,6,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,6,10,14,19,23,27,31,34,38,42,46,51,55,59,63,66,70,74,78,83,87,91,95,98,102,106,110,115,119,123,127,130,134,138,142,147,151,155,159,162,166,170,174,179,183,187,191,194,198,202,206,211,215,219,223,226,230,234,238,243,247,251,255};
int InterfaceMedSeg::m_colormap_blue[256] = {255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,253,249,245,241,236,232,228,224,221,217,213,209,204,200,196,192,189,185,181,177,172,168,164,160,157,153,149,145,140,136,132,128,125,121,117,113,108,104,100,96,93,89,85,81,76,72,68,64,61,57,53,49,44,40,36,32,29,25,21,17,12,8,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};


InterfaceMedSeg::InterfaceMedSeg(wxFrame *parent, bool delete_scale_sets_on_closing, const wxString& title )
: FrameWithLog(NULL,-1, wxString(title), wxDefaultPosition, wxDefaultSize, wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxCLOSE_BOX | wxSYSTEM_MENU | wxCAPTION | wxRESIZE_BORDER)
{
	// FIRST OF ALL, LOAD PARAMETERS FROM XML
  char *_pathParams = new char[256]; _pathParams[0]='\0';
#ifndef __WINDOWS__  
  strcat(_pathParams,getenv("HOME"));
  _paramsLoader = new interfaceParamsLoader(strcat((char*)_pathParams,"/.seascape/interfaceparams.xml"));
#else
  _paramsLoader = new interfaceParamsLoader("data/interfaceparams.xml");
#endif
	string _fooColor = _paramsLoader->getParameter(string("backgroundcolor"));
	string _fooLanguage = _paramsLoader->getParameter(string("languagesfile"));
	if(_fooColor!=_paramsLoader->getErrorString()){
	  background_color = new wxColour(toWxString(_fooColor));
	}
	else
		background_color = new wxColour(40,168,255);
	
	// create languageDealer object and load languages from xml file
	_languageDealer = new languageDealer();
	if(_fooLanguage!=_paramsLoader->getErrorString()){
		if(_languageDealer->loadLanguages(_fooLanguage.c_str())<1){
			ofstream outf("debug.InterfaceMedSeg.txt");
			outf<<"Error while loading languages"<<endl;
			outf.close();
		}
	}
	else
		if(_languageDealer->loadLanguages("languages.xml")<1){
			ofstream outf("debug.InterfaceMedSeg.txt");
			outf<<"Error while loading languages"<<endl;
			outf.close();
		}

	
	ImageFrame=NULL;


	// create locationDealer object 
	_locationDealer = new LocationLoader();
	//_locationDealer->loadLocations("locations.xls");

	// set first language available as current language (well, to english directly :P)
	_languages=_languageDealer->getLanguagesVector();
	currentLanguage = "English";

	

	ImageFrameInit();

	//_________________________________________________________________________________________________
	// SetIcon( wxICON(IDI_ICON1) );

	m_scale_set_loaded=false;
	p_is_3D_Window_Created=false;
	p_scoll_slaved=false;

	m_delete_sxs = false;
	// HACK by aae
	m_sxs = new ImageScaleSets;
	// HACK by aae
	ImageFrame->m_sxs = m_sxs;
	ImageFrame->m_delete_sxs = false;

	m_pixel_side_length=1;
	m_nlevels = 10;

	//=========================================
	// Menu
#if wxUSE_MENUS
	// File menu 
	menuFile = new wxMenu;
	//menuFile->Append(idMenuOpen, wxString("&Open scale-sets\tAlt-S"), wxString("Open scale-sets"));
	//menuFile->Append(idMenuSave, wxString("&Save scale-sets\tAlt-S"), wxString("Saves the current scale-sets"));
	menuFile->Append(idMenuOpen, wxString(_languageDealer->getStringById(currentLanguage,"00006").c_str(), wxConvUTF8), wxString(_languageDealer->getStringById(currentLanguage,"00007").c_str(), wxConvUTF8));
	menuFile->Append(idMenuSave, wxString(_languageDealer->getStringById(currentLanguage,"00008").c_str(), wxConvUTF8), wxString(_languageDealer->getStringById(currentLanguage,"00009").c_str(), wxConvUTF8));
	// AAE: open classification
	menuFile->Append(idMenuImport, wxString(_languageDealer->getStringById(currentLanguage,"00027").c_str(), wxConvUTF8), wxString(_languageDealer->getStringById(currentLanguage,"00028").c_str(), wxConvUTF8));
	menuFile->Append(idMenuExport, wxString(_languageDealer->getStringById(currentLanguage,"00029").c_str(), wxConvUTF8), wxString(_languageDealer->getStringById(currentLanguage,"00030").c_str(), wxConvUTF8));
	menuFile->Append(idMenuQuit, wxString(_languageDealer->getStringById(currentLanguage,"00010").c_str(), wxConvUTF8), wxString(_languageDealer->getStringById(currentLanguage,"00011").c_str(), wxConvUTF8));

	//menuFile->AppendCheckItem(idMenuLog, wxString("Show log\tAlt-L"), wxString("Shows the log window"));
	//menuFile->Check(idMenuLog,true);
	//	p_log_window=new LogWindow(this,true,false,wxSize(400,150));
	//	p_log_window->Show(true);

	//menuFile->Append(idMenuQuit, wxString("E&xit\tAlt-X"), wxString("Quit this program"));

	/*menu3D = new wxMenu;
	menu3D->AppendCheckItem( idMenu3DWindow, wxString("View 3D Window") );
	menu3D->Append( idMenu3DPointSize, wxString("Change Points Size") );
	menu3D->Append( idMenu3DMoveConfig, wxString("Configure 3D Moving Speed") );
	menu3D->AppendCheckItem( idMenu3DEnslavement, wxString("ScrollBar Enslavement") );
*/
	// Algorithms menu
	menuAlgo = new wxMenu;
	//menuAlgo->Append(idMenuSegment,wxString("&Analyze image \tAlt-R"), wxString("Computes a Scale-Sets analysis on an image"));
	menuAlgo->Append(idMenuSegment,wxString(_languageDealer->getStringById(currentLanguage,"00012").c_str(), wxConvUTF8), wxString(_languageDealer->getStringById(currentLanguage,"00013").c_str(), wxConvUTF8));
	//menuAlgo->Append(idMenuAdd,wxString(_languageDealer->getStringById(currentLanguage,"00014")), wxString(_languageDealer->getStringById(currentLanguage,"00015")));
	menuAlgo->Append(idMenuColorMaps,wxString(_languageDealer->getStringById(currentLanguage,"00062").c_str(), wxConvUTF8), wxString(_languageDealer->getStringById(currentLanguage,"00063").c_str(), wxConvUTF8));
	menuAlgo->Append(idMenuExtract,wxString(_languageDealer->getStringById(currentLanguage,"00016").c_str(), wxConvUTF8), wxString(_languageDealer->getStringById(currentLanguage,"00017").c_str(), wxConvUTF8));
	menuAlgo->Append(idMenuExtractXLS,wxString(_languageDealer->getStringById(currentLanguage,"00019").c_str(), wxConvUTF8), wxString(_languageDealer->getStringById(currentLanguage,"00020").c_str(), wxConvUTF8));
	menuAlgo->Append(idMenuShowPolygon,wxString(_languageDealer->getStringById(currentLanguage,"00085").c_str(), wxConvUTF8), wxString(_languageDealer->getStringById(currentLanguage,"00085").c_str(), wxConvUTF8));

	// menu languages
	menuLanguages = new wxMenu;
	int contLanguages = _languages->size();
	switch(contLanguages){
		case 10:
			menuLanguages->Append(idMenuLanguage10,wxString((*_languages)[9].c_str(), wxConvUTF8));
		case 9:
			menuLanguages->Append(idMenuLanguage9,wxString((*_languages)[8].c_str(), wxConvUTF8));
		case 8:
			menuLanguages->Append(idMenuLanguage8,wxString((*_languages)[7].c_str(), wxConvUTF8));
		case 7:
			menuLanguages->Append(idMenuLanguage7,wxString((*_languages)[6].c_str(), wxConvUTF8));
		case 6:
			menuLanguages->Append(idMenuLanguage6,wxString((*_languages)[5].c_str(), wxConvUTF8));
		case 5:
			menuLanguages->Append(idMenuLanguage5,wxString((*_languages)[4].c_str(), wxConvUTF8));
		case 4:
			menuLanguages->Append(idMenuLanguage4,wxString((*_languages)[3].c_str(), wxConvUTF8));
		case 3:
			menuLanguages->Append(idMenuLanguage3,wxString((*_languages)[2].c_str(), wxConvUTF8));
		case 2:
			menuLanguages->Append(idMenuLanguage2,wxString((*_languages)[1].c_str(), wxConvUTF8));
		case 1:
			menuLanguages->Append(idMenuLanguage1,wxString((*_languages)[0].c_str(), wxConvUTF8));
	}
	
	// Help menu
	menuHelp = new wxMenu;
	//menuHelp->Append(idMenuAbout, wxString("&About...\tF1"), wxString("Show about dialog"));
	menuHelp->Append(idMenuAbout, wxString(_languageDealer->getStringById(currentLanguage,"00018").c_str(), wxConvUTF8), wxString(_languageDealer->getStringById(currentLanguage,"00018").c_str(), wxConvUTF8));



	// Menu bar
	menuBar = new wxMenuBar();
	menuBar->Append(menuFile, wxString(_languageDealer->getStringById(currentLanguage,"00003").c_str(), wxConvUTF8));
	menuBar->Append(menuAlgo, wxString(_languageDealer->getStringById(currentLanguage,"00004").c_str(), wxConvUTF8));
	menuBar->Append(menuLanguages, wxString(_languageDealer->getStringById(currentLanguage,"00000").c_str(), wxConvUTF8));
	menuBar->Append(menuHelp, wxString(_languageDealer->getStringById(currentLanguage,"00005").c_str(), wxConvUTF8));
	/*menuBar->Append(menuFile, wxString("&File"));
	menuBar->Append(menu3D, wxString("3D View"));
	menuBar->Append(menuAlgo, wxString("&Algorithms"));
	menuBar->Append(menuHelp, wxString("&Help"));*/

	SetMenuBar(menuBar);

	//GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString(_languageDealer->getStringById(currentLanguage,"00004")),wxString(_languageDealer->getStringById(currentLanguage,"00014")) ),FALSE);
	GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString(_languageDealer->getStringById(currentLanguage,"00004").c_str(), wxConvUTF8),wxString(_languageDealer->getStringById(currentLanguage,"00016").c_str(), wxConvUTF8) ),FALSE);
	GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString(_languageDealer->getStringById(currentLanguage,"00004").c_str(), wxConvUTF8),wxString(_languageDealer->getStringById(currentLanguage,"00019").c_str(), wxConvUTF8) ),FALSE);
	GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString(_languageDealer->getStringById(currentLanguage,"00004").c_str(), wxConvUTF8),wxString(_languageDealer->getStringById(currentLanguage,"00012").c_str(), wxConvUTF8) ),FALSE);
	GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString(_languageDealer->getStringById(currentLanguage,"00003").c_str(), wxConvUTF8),wxString(_languageDealer->getStringById(currentLanguage,"00008").c_str(), wxConvUTF8) ),FALSE);
	GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString(_languageDealer->getStringById(currentLanguage,"00003").c_str(), wxConvUTF8),wxString(_languageDealer->getStringById(currentLanguage,"00006").c_str(), wxConvUTF8) ),FALSE);
	GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString(_languageDealer->getStringById(currentLanguage,"00003").c_str(), wxConvUTF8),wxString(_languageDealer->getStringById(currentLanguage,"00029").c_str(), wxConvUTF8) ),FALSE);
	GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString(_languageDealer->getStringById(currentLanguage,"00004").c_str(), wxConvUTF8),wxString(_languageDealer->getStringById(currentLanguage,"00085").c_str(), wxConvUTF8) ),FALSE);

/*	GetMenuBar()->Enable(GetMenuBar()->FindMenuItem( wxString("3D View"),wxString("View 3D Window") ),FALSE);
	GetMenuBar()->Enable(GetMenuBar()->FindMenuItem( wxString("3D View"),wxString("Change Points Size") ),FALSE);
	GetMenuBar()->Enable(GetMenuBar()->FindMenuItem( wxString("3D View"),wxString("Configure 3D Moving Speed") ),FALSE);
	GetMenuBar()->Enable(GetMenuBar()->FindMenuItem( wxString("3D View"),wxString("ScrollBar Enslavement") ),FALSE);*/
#endif // wxUSE_MENUS


#if wxUSE_STATUSBAR
	CreateStatusBar(10);
	int* stw = new int[10];
	stw[0] = 100;
	stw[1] = 60;
	stw[2] = 80;
	stw[3] = 80;
	stw[4] = 120;
	stw[5] = 120;
	stw[6] = 1;
	stw[7] = 150;
	stw[8] = 280;
	stw[9] = 10;
	SetStatusWidths(10,stw);
	delete[] stw;
#endif // wxUSE_STATUSBAR

	wxBoxSizer *sizer= new wxBoxSizer( wxVERTICAL );
	SetSizer(sizer);

	Layout();

	Maximize();

	_polygons = new vector<MPolygon* >;
	//	static int MPolygon::m_lastId = 0);

	LayerOnSelectionDesc::setLayerOnSelection(255,180,0,1);
	LayerOnSelectionDesc::setLocationLoader(_locationDealer);
	LayerOnSelectionDesc::setPixelsSpecies(&_pixelsSpecies);
	LayerOnSelectionDesc::setVectorPolygons(_polygons);

	//_________________________________________________________________________________________________

}

InterfaceMedSeg::~InterfaceMedSeg()
{
}

void InterfaceMedSeg::UpdateStatusBarScaling(){
  wxString stat1(_languageDealer->getStringById(currentLanguage,"00104").c_str(),wxConvUTF8);
  wxString stat2(_languageDealer->getStringById(currentLanguage,"00105").c_str(),wxConvUTF8);
  wxString mess;
  double x,y;
  y = ImageFrame->m_isize(2);
  x = ImageFrame->m_isize(1);

  //  mess.Printf(wxT("%d"),set->index());
  if(ImageFrame->m_scaling==false&&m_scale_set_loaded){
    mess.Printf(wxT("  %f"),1/m_pixel_side_length);
    SetStatusText(stat1+mess,7);
    mess.Printf(wxT("  %f"),m_pixel_side_length*x*y);
    SetStatusText(stat2+mess,8);
  }
  else if(ImageFrame->m_scaling_x0==-1){
    SetStatusText(stat1+wxT(" scaling(1)"),7);
    SetStatusText(stat2+wxT(" scaling(1)"),8);
  }
  else if(ImageFrame->m_scaling_x1==-1){
    SetStatusText(stat1+wxT(" scaling(2)"),7);
    SetStatusText(stat2+wxT(" scaling(2)"),8);
  }
}

void InterfaceMedSeg::ImageFrameInit()
{
	this->SetSize(500,500);
	Panel = new PanelMedSeg( this, wxID_ANY, wxPoint(0, 0), wxSize(-1, -1) );
	ImageFrame = new ScrollWinMedSeg(Panel, this, wxID_ANY, wxPoint(250,0), wxSize(-1,-1), wxHSCROLL | wxVSCROLL, (wxChar*)"ScrollWin");
	ImageFrame->m_delete_sxs = false;
	this->SetOwnBackgroundColour(*background_color);
	Panel->SetBackgroundColour(*background_color);
	//	ImageFrame->SetOwnBackgroundColour(wxColour(0,128,255));
	//	ImageFrame->SetBackgroundStyle(wxBG_STYLE_COLOUR);
	ImageFrame->SetBackgroundColour(*background_color);
	Panel->SetSize(this->GetSize()-wxSize(30,30));//-wxSize(Panel->GetPosition().x,Panel->GetPosition().y));
	ImageFrame->SetSize(Panel->GetSize()-wxSize(ImageFrame->GetPosition().x,ImageFrame->GetPosition().y));
	OptionFrame = new LeftWindow(Panel,ImageFrame,this);
	ImageFrame->m_option = OptionFrame;
	
}

/*BEGIN_EVENT_TABLE(InterfaceMedSeg, wxFrame)
EVT_SIZE(InterfaceMedSeg::OnSize)
END_EVENT_TABLE()*/

void InterfaceMedSeg::OnSize( wxSizeEvent& event )
{
	// Important � v�rifier car la fenetre s'instancie et definit sa taille avant que le panel et la fenetre contenant l'image ne s'instancient !
	if (Panel && ImageFrame)
	{
		Panel->SetSize(this->GetSize()-wxSize(30,80));//-wxSize(Panel->GetPosition().x,Panel->GetPosition().y));
		ImageFrame->SetSize(Panel->GetSize()-wxSize(ImageFrame->GetPosition().x,ImageFrame->GetPosition().y)-wxSize(30,80));	
	}
}

//************************************************************************************
// Panel contenant les autres widgets afin de pouvoir les placer correctement
//************************************************************************************
PanelMedSeg::PanelMedSeg(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size)
: wxPanel(parent, id, pos, size)
{
}

PanelMedSeg::~PanelMedSeg()
{
}


//***********************************************************************
// Fen�tre scrollable contenant l'image
//***********************************************************************
ScrollWinMedSeg::ScrollWinMedSeg(wxWindow* parent, InterfaceMedSeg *owner, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name)
: wxScrolledWindow(parent, id, pos, size, style, wxString(name))
{
	m_owner=owner;
	m_color_map = 0;
	m_xslice_img = 0;
	m_xslice_base = 0;
	//m_xslice_lab = 0;
	m_xslice_set = 0;
	m_has_to_fit=false;
	m_mouse_test=false;
	m_controlKey=false;
	m_scaling=false;
	m_scaling_changed=false;
	m_scaling_x0 = m_scaling_x1 = m_scaling_y0 = m_scaling_y1 = -1;
	/*
	m_xslice_changed = false;
	m_scale_changed = false;
	*/
	m_xslice_changed = false;
	m_scale_changed = false;
	m_set_changed = false;
	
	m_zoom_changed = m_palette_changed = false;
	m_delete_sxs = false;
	m_color_map = false;
	//m_xslice_lab = false;
	m_xslice_set = false;
	m_vSliceSection = NULL;
	
	//SetScrollbars(20, 20, 50, 50, 0 , 0, false);
	//Refresh();
	EnableScrolling(true,true);
	m_bIsScroll = false;

	m_map_image = new ImageMapping();
	m_map_image->SetColorMaps(m_owner->m_colormap_red, m_owner->m_colormap_green, m_owner->m_colormap_blue);
	m_bActive = false;
	m_bShould_save = false;
	m_bShoulScrollEx = false;
	((wxWindow*)this)->SetBackgroundColour(*m_owner->background_color);
}

ScrollWinMedSeg::~ScrollWinMedSeg()
{
}

void ScrollWinMedSeg::dcClear()
{
	wxClientDC dc( this );
	dc.Clear();
}

/*void ScrollWinMedSeg::OnSize()
{
this->getImageFrame()->SetSize(p_panel->GetSize()-wxSize(ImageFrame->GetPosition().x,ImageFrame->GetPosition().y));
}*/









BEGIN_EVENT_TABLE( InterfaceMedSeg, wxFrame ) 
EVT_SIZE(InterfaceMedSeg::OnSize)
//EVT_SET_FOCUS(InterfaceMedSeg::OnFocus)
EVT_MENU ( idMenuSave, InterfaceMedSeg::OnMenuSave ) 
EVT_MENU ( idMenuOpen, InterfaceMedSeg::OnMenuOpen ) 
EVT_MENU ( idMenuLanguage10, InterfaceMedSeg::OnMenuLanguage10 )
EVT_MENU ( idMenuLanguage9, InterfaceMedSeg::OnMenuLanguage9 )
EVT_MENU ( idMenuLanguage8, InterfaceMedSeg::OnMenuLanguage8 )
EVT_MENU ( idMenuLanguage7, InterfaceMedSeg::OnMenuLanguage7 )
EVT_MENU ( idMenuLanguage6, InterfaceMedSeg::OnMenuLanguage6 )
EVT_MENU ( idMenuLanguage5, InterfaceMedSeg::OnMenuLanguage5 )
EVT_MENU ( idMenuLanguage4, InterfaceMedSeg::OnMenuLanguage4 )
EVT_MENU ( idMenuLanguage3, InterfaceMedSeg::OnMenuLanguage3 )
EVT_MENU ( idMenuLanguage2, InterfaceMedSeg::OnMenuLanguage2 )
EVT_MENU ( idMenuLanguage1, InterfaceMedSeg::OnMenuLanguage1 )
EVT_MENU ( idMenuQuit, InterfaceMedSeg::OnMenuQuit ) 
EVT_MENU ( idMenuAbout, InterfaceMedSeg::OnMenuAbout ) 
/*EVT_MENU ( idMenu3DWindow, InterfaceMedSeg::OnMenu3DWindow ) 
EVT_MENU ( idMenu3DPointSize, InterfaceMedSeg::OnMenu3DpointSize ) 
EVT_MENU ( idMenu3DMoveConfig, InterfaceMedSeg::OnMenu3DMoveConfig ) 
EVT_MENU ( idMenuLog, InterfaceMedSeg::OnMenuLog ) */
EVT_MENU ( idMenuSegment, InterfaceMedSeg::OnMenuSegment ) 
//EVT_MENU ( idMenuAdd, InterfaceMedSeg::OnMenuAdd )
EVT_MENU ( idMenuColorMaps, InterfaceMedSeg::OnMenuColorMaps )
EVT_MENU ( idMenuExtract, InterfaceMedSeg::OnMenuExtract )
EVT_MENU ( idMenuExtractXLS, InterfaceMedSeg::OnMenuExtractXLS )
EVT_MENU ( idMenuShowPolygon, InterfaceMedSeg::OnMenuShowPolygon )
EVT_MENU ( idMenuImport, InterfaceMedSeg::OnMenuImport )
EVT_MENU ( idMenuExport, InterfaceMedSeg::OnMenuExport )
EVT_BUTTON ( idButtonOk, InterfaceMedSeg::OnListBoxButtons )
EVT_BUTTON ( idButtonCancel, InterfaceMedSeg::OnListBoxButtons )
//EVT_BUTTON ( idButtonAddClass, InterfaceMedSeg::OnListBoxButtons )

//EVT_MENU ( idMenu3DEnslavement, InterfaceMedSeg::OnMenu3DScrollEnslavement)*/

EVT_SCROLL ( InterfaceMedSeg::OnWheelRotation2i )
EVT_SCROLLWIN( InterfaceMedSeg::OnScrolli )
EVT_MOUSEWHEEL( InterfaceMedSeg::OnWheelRotationi )
EVT_KEY_DOWN( InterfaceMedSeg::KeyDown )
END_EVENT_TABLE()

void InterfaceMedSeg::AddClassClose()
{
	this->Enable(true);
}

void InterfaceMedSeg::KeyDown(wxKeyEvent &ev){
 std:cout<<"InterfaceMedSeg::KeyDown"<<std::endl;
}

void InterfaceMedSeg::OnMenuShowPolygon(wxCommandEvent &ev){
  ImageFrame->m_controlKey = true;
}

void InterfaceMedSeg::OnListBoxButtons( wxCommandEvent &event )
{
	switch (event.GetId())
	{
	case idButtonOk:
		{
			ImageMapping *mapping_im;
			mapping_im = ImageFrame->getImageMapping();
			char citem_id[256];
			//citem_id = mapping_im->AddClassToMap((char *)m_add_frame->m_text_name->GetValue().c_str(),(char *)m_add_frame->m_text_group->GetValue().c_str(),m_add_frame->m_added_class_col->getColor()->Red(),m_add_frame->m_added_class_col->getColor()->Green(),m_add_frame->m_added_class_col->getColor()->Blue());
			int verif = 0;
			verif = (int)mapping_im->AddClassToMap((char *)m_add_frame->m_text_name->GetValue().c_str(),(char *)m_add_frame->m_text_group->GetValue().c_str(),(char *)m_add_frame->m_text_site->GetValue().c_str(),m_add_frame->m_added_class_col->getColor()->Red(),m_add_frame->m_added_class_col->getColor()->Green(),m_add_frame->m_added_class_col->getColor()->Blue());
			sprintf(citem_id,"%d",verif);
			if (verif != 0)
			{
				OptionFrame->m_class_sel->ReloadComboBox();
				//OptionFrame->m_class_sel->AppendString(wxString(citem_id));
			}
			this->Enable(true);
			delete m_add_frame;
			break;
		}
	case idButtonCancel:
		{
			this->Enable(true);
			delete m_add_frame;
			break;
		}
	}
}



void InterfaceMedSeg::OnWheelRotation2i(wxScrollEvent& ev)
{
  wxLogMessage(wxT("testtttttttttttttttttttttttttttttttttttttt 0"));
}

void InterfaceMedSeg::OnWheelRotationi(wxMouseEvent& ev)
{
  wxLogMessage(wxT("testtttttttttttttttttttttttttttttttttttttt 1"));
}

void InterfaceMedSeg::OnScrolli(wxScrollWinEvent & event)
{
  wxLogMessage(wxT("testtttttttttttttttttttttttttttttttttttttt 2"));
}

/*
void InterfaceMedSeg::OnFocus(wxFocusEvent& event)
{
if (ImageFrame)
{
//ImageFrame->updateView();
Refresh();
}
}
*/

// AAE:
void InterfaceMedSeg::OnMenuImport(wxCommandEvent& event){
  m_manage_frame = new ManageClassMedSeg(this, -1, wxString(_languageDealer->getStringById(currentLanguage,"00031").c_str(),wxConvUTF8));
	m_manage_frame->Show(true);
	this->Enable(false);
	GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString(_languageDealer->getStringById(currentLanguage,"00004").c_str(), wxConvUTF8),wxString(_languageDealer->getStringById(currentLanguage,"00012").c_str(), wxConvUTF8) ),TRUE);
	GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString(_languageDealer->getStringById(currentLanguage,"00003").c_str(), wxConvUTF8),wxString(_languageDealer->getStringById(currentLanguage,"00029").c_str(), wxConvUTF8) ),TRUE);
	/*
	OptionFrame->m_class_sel->InitGroup();
	OptionFrame->m_class_sel->InitSite();
	*/
}

void InterfaceMedSeg::OnMenuExport(wxCommandEvent& event){

	wxString mystring(_languageDealer->getStringById(currentLanguage,"00041").c_str(), wxConvUTF8);
	wxString filename = wxFileSelector(mystring,wxT(""),wxT(""),wxT(""),wxString(_languageDealer->getStringById(currentLanguage,"00042").c_str(),wxConvUTF8),wxOVERWRITE_PROMPT|wxSAVE);
	_locationDealer->saveLocations(filename.mb_str());
}

void InterfaceMedSeg::DoSegmentation(ImageScaleClimbingParameters* param ){
	wxLogMessage(wxT("Segmentation in progress.."));
	lgl::ImageRam* tmp = 0;
	// AAE
	wxString _filename= lgl::wxOpenImage(tmp);
	if(_filename==wxT("NULL")) return ;
	m_image_path = _filename.mb_str();
	m_image_filename = m_image_path.substr(m_image_path.rfind("\\")+1,m_image_path.length());
	//m_image_filename = m_image_path;
	//if (! lgl::wxOpenImage(tmp))  return;
	//	lglLOG("Image : "<<tmp->size());
	// Image crop
	//	ImageRam* image= tmp->procreate();
	//	image->crop(*tmp,ImageBloc(lgl::ImageSite(0,32,32,32),lgl::ImageSite(1,64,64,64)));
	//	image->crop(*tmp,ImageBloc(lgl::ImageSite(0,32,32,32),lgl::ImageSite(1,96,96,96)));
	//	image->crop(*tmp,ImageBloc(lgl::ImageSite(0,32,32,32),lgl::ImageSite(1,96,96,150)));
	//	image->crop(*tmp,ImageBloc(lgl::ImageSite(0,0,32,0),lgl::ImageSite(1,128,110,214)));
	//	image->crop(*tmp,ImageBloc(lgl::ImageSite(0,0,63,32),lgl::ImageSite(1,181,217,181)));

	lgl::ImageRam* image=tmp;
	//	subsample(*tmp,4,image);

	//	wxSaveImageAnalyze(*image);
	//	return;

	//	delete tmp;


	//	lglLOG("Image cropped : "<<image->size()<<ENDL);

	/// Allocation of the ImageScaleSets
	//On desactive la fen�tre..
	m_scale_set_loaded=false;

	m_sxs = new ImageScaleSets;
	m_delete_sxs = true;
	/// The scale-climbing user
	//MS_ScaleClimbing_User* user = new MS_ScaleClimbing_User;
	MS_ScaleClimbingUser* user = new MS_ScaleClimbingUser();
	user->setImage(*image);
	// The algorithm
	ImageScaleClimbing  algo;
	algo.setUser(user);
	algo.setScaleSets(*m_sxs);
	algo.setImage(image);
	// Its parameters
	lgl::Neighborhood N;
	if (image->dimension()==2) N.setType(lgl::Neighborhood::d2_4n);
	else N.setType(lgl::Neighborhood::d3_6n);
	param->setNeighborhood(N);
	algo.setParameters(param);

	// Progress dialog 
	wxProgressDialog* pd = new wxProgressDialog(wxT("Scale Climbing"), wxT(""),  1000, this,  
		wxPD_AUTO_HIDE | wxPD_CAN_ABORT | wxPD_ELAPSED_TIME | wxPD_ESTIMATED_TIME | wxPD_REMAINING_TIME);
	user->setProgressDialog(pd,1000);
	//GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString(_languageDealer->getStringById(currentLanguage,"00004")),wxString(_languageDealer->getStringById(currentLanguage,"00014")) ),TRUE);
	GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString(_languageDealer->getStringById(currentLanguage,"00004").c_str(),wxConvUTF8),wxString(_languageDealer->getStringById(currentLanguage,"00016").c_str(),wxConvUTF8) ),TRUE);
	GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString(_languageDealer->getStringById(currentLanguage,"00004").c_str(),wxConvUTF8),wxString(_languageDealer->getStringById(currentLanguage,"00019").c_str(),wxConvUTF8) ),TRUE);
	GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString(_languageDealer->getStringById(currentLanguage,"00003").c_str(),wxConvUTF8),wxString(_languageDealer->getStringById(currentLanguage,"00008").c_str(),wxConvUTF8) ),TRUE);
	GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString(_languageDealer->getStringById(currentLanguage,"00004").c_str(),wxConvUTF8),wxString(_languageDealer->getStringById(currentLanguage,"00085").c_str(),wxConvUTF8) ),TRUE);
	//GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString("&Fichier"),wxString("&Ouvrir") ),FALSE);
	GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString(_languageDealer->getStringById(currentLanguage,"00004").c_str(),wxConvUTF8),wxString(_languageDealer->getStringById(currentLanguage,"00012").c_str(),wxConvUTF8) ),FALSE);
	// If algo run ok : open sxs in a browser 
	if (algo.run()) {
		pd->Destroy();
		Display();
		ImageMapping *mapping_im;
		mapping_im = ImageFrame->getImageMapping();
		MapImage *map_im;// = mapping_im->getMapImage();
		//if (!map_im)
		//{
		map_im = mapping_im->createMapImage(ImageFrame->m_xslice_bmp.GetWidth(),ImageFrame->m_xslice_bmp.GetHeight(),wxT(""));
		//}
		OptionFrame->m_class_sel->_locationDealer = this->_locationDealer;
		OptionFrame->m_class_sel->LoadComboBox(mapping_im, wxT(""));
		map_im->readImage();
		ImageFrame->InitLayer();
		ImageFrame->m_set_changed = true;
		//ImageFrame->getSliceImage();
		ImageFrame->resolveChanges();
		//OptionFrame->m_gauge->SetValue(0);
		ImageFrame->getSliceBase();
		//***Description de la barre repr�sentant l'avancement du chargement des niveaux de segmentation***
		wxPanel temp_panel ( this, -1, wxPoint(ImageFrame->GetPosition().x,ImageFrame->GetPosition().y), wxSize(ImageFrame->GetSize().x,ImageFrame->GetSize().y));
		temp_panel.SetBackgroundColour(*wxBLACK);
		wxStaticText temp_label(&temp_panel, -1, wxT("........................................................................"), wxPoint(20, 20), wxDefaultSize);//, wxALIGN_RIGHT /*| wxST_NO_AUTORESIZE*/);
		temp_panel.SetForegroundColour( *wxRED );
		temp_label.SetOwnForegroundColour( *wxRED );
		temp_label.SetLabel(wxT("Chargement des niveaux de segmentation..."));
		temp_label.Show(true);
		wxGauge temp_gauge(&temp_panel, wxID_ANY, m_nlevels, wxPoint((temp_panel.GetSize().x-160)/2,(temp_panel.GetSize().y-30)/2), wxSize(160,30), wxGA_HORIZONTAL );
		temp_gauge.SetValue(0);
		temp_panel.Show(true);
		temp_panel.SetFocus();
		char clabel[256];
		//************************************************************************************************
		this->Enable(false);
		ImageFrame->m_vSliceSection = new std::vector<ImageRam*>();
		for (double i=0.; i<m_nlevels+1.; i=i+1.)
		{
			//***********Label changeant en fonction du niveau de chargement de la segmentation******
			sprintf(clabel,"Chargement des niveaux de segmentation... %d/%d  ",(int)i,(int)m_nlevels);
			wxString slabel(wxString(std::string(clabel).c_str(),wxConvUTF8));
			temp_label.SetLabel(slabel);
			//***************************************************************************************
			ImageRam* vec_slice_section = new ImageRamT<int>();
			m_sxs->getSection( *ImageFrame->m_xslice_base , ImageFrame->invlogscale(i/m_nlevels), vec_slice_section);
			//m_sxs->getSection( *ImageFrame->m_xslice_base , 0, vec_slice_section);
			ImageFrame->m_vSliceSection->push_back(vec_slice_section);
			temp_gauge.SetValue(temp_gauge.GetValue()+1);
		}
		this->Enable(true);
		// Variable utilis�e pour ne pas essayer de retracer une image vide au chargement de l'application.
		ImageFrame->SetActive();
		ImageFrame->updateView();
	}else {
		pd->Destroy();
		delete m_sxs; 
	}	

}

void InterfaceMedSeg::OnMenuSegment (wxCommandEvent& event) 
{
	if (ImageFrame->m_vSliceSection)
	{
		delete ImageFrame->m_vSliceSection;
		ImageFrame->m_vSliceSection = NULL;
	}

	// SEGMENTATION PARAMETERS WINDOW
	m_segparam_frame = new SegmentationParametersWindow(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00081").c_str(),wxConvUTF8));
	this->Enable(false);
	m_segparam_frame->Show(true);
}


void InterfaceMedSeg::ShowTableWindow(int highlight){
  TableWindow *_tableWindow= new TableWindow(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00087").c_str(),wxConvUTF8),highlight);
  this->Enable(false);
  _tableWindow->Show(true);
}

/*void InterfaceMedSeg::OnMenuLog(wxCommandEvent& event){
if(GetMenuBar()->IsChecked(event.GetId())){
p_log_window->Show(true);
}else{
p_log_window->Show(false);
}
}*/

void InterfaceMedSeg::OnMenuAdd (wxCommandEvent& event) 
{
  m_add_frame = new AddClassMedSeg(this, -1, wxT("Ajouter une nouvelle classe"));
	m_add_frame->SetBackgroundColour(*background_color);
	m_add_frame->Show(true);
	wxBitmap test(20,20);
	m_add_frame->m_added_class_col = new BitmapButtonMedSeg(m_add_frame,idColorButton,test);
	m_add_frame->m_added_class_col->setColor(wxColour(0,0,0));
	this->Enable(false);
	m_add_frame->m_ok = new wxButton(m_add_frame,idButtonOk,wxT("OK"));
	m_add_frame->m_cancel = new wxButton(m_add_frame,idButtonCancel,wxT("Annuler"));
	m_add_frame->m_label_name = new wxStaticText(m_add_frame, -1, wxT("Nom"), wxDefaultPosition, wxSize(40,20));//, wxALIGN_RIGHT /*| wxST_NO_AUTORESIZE*/);
	m_add_frame->m_label_group = new wxStaticText(m_add_frame, -1, wxT("Groupe"), wxDefaultPosition, wxSize(40,20));
	m_add_frame->m_label_site = new wxStaticText(m_add_frame, -1, wxT("Site"), wxDefaultPosition, wxSize(40,20));
	m_add_frame->m_label_color = new wxStaticText(m_add_frame, -1, wxT("Couleur"), wxDefaultPosition, wxSize(40,20));
	m_add_frame->m_label_name->Show(true);
	m_add_frame->m_label_group->Show(true);
	m_add_frame->m_label_site->Show(true);
	m_add_frame->m_label_color->Show(true);
	m_add_frame->m_text_name = new wxTextCtrl(m_add_frame, -1, wxT(""), wxDefaultPosition, wxSize(150,20));
	m_add_frame->m_text_group = new wxTextCtrl(m_add_frame, -1, wxT(""), wxDefaultPosition, wxSize(150,20));
	m_add_frame->m_text_site = new wxTextCtrl(m_add_frame, -1, wxT(""), wxDefaultPosition, wxSize(150,20));

	wxBoxSizer *mainsizer = new wxBoxSizer( wxVERTICAL ); 
	wxBoxSizer *firstsizer = new wxBoxSizer( wxHORIZONTAL );
	wxBoxSizer *secondsizer = new wxBoxSizer( wxVERTICAL );
	wxBoxSizer *secondbissizer = new wxBoxSizer( wxVERTICAL );
	wxBoxSizer *thirdsizer = new wxBoxSizer( wxHORIZONTAL );
	//wxStaticBoxSizer *secondsizer = new wxStaticBoxSizer( new wxStaticBox(add_frame,-1,""), wxVERTICAL );
	//wxStaticBoxSizer *secondbissizer = new wxStaticBoxSizer( new wxStaticBox(add_frame,-1,""), wxVERTICAL );
	//wxStaticBoxSizer *thirdsizer = new wxStaticBoxSizer( new wxStaticBox(add_frame,-1,""), wxHORIZONTAL );

	secondsizer->Add(m_add_frame->m_label_name,0,wxALIGN_RIGHT|wxGROW|wxTOP,13);
	secondsizer->Add(m_add_frame->m_label_group,0,wxALIGN_RIGHT|wxGROW|wxTOP,5);
	secondsizer->Add(m_add_frame->m_label_site,0,wxALIGN_RIGHT|wxGROW|wxTOP,5);
	secondsizer->Add(m_add_frame->m_label_color,0,wxALIGN_RIGHT|wxGROW|wxTOP,8);
	secondbissizer->Add(m_add_frame->m_text_name,0,wxALIGN_LEFT|wxTOP|wxGROW,10);
	secondbissizer->Add(m_add_frame->m_text_group,0,wxALIGN_LEFT|wxTOP|wxGROW,5);
	secondbissizer->Add(m_add_frame->m_text_site,0,wxALIGN_LEFT|wxTOP|wxGROW,5);
	secondbissizer->Add(m_add_frame->m_added_class_col,0,wxALIGN_LEFT|wxTOP,5);
	thirdsizer->Add(m_add_frame->m_ok,0,wxALIGN_RIGHT|wxTOP,5);
	thirdsizer->Add(m_add_frame->m_cancel,0,wxALIGN_LEFT|wxTOP,5);
	firstsizer->Add(secondsizer,0);
	firstsizer->Add(secondbissizer,0,wxLEFT,5);
	mainsizer->Add(firstsizer,0,wxLEFT|wxRIGHT,10);
	mainsizer->Add(thirdsizer,0,wxALIGN_CENTER|wxLEFT|wxBOTTOM|wxRIGHT,10);
	m_add_frame->SetSizer( mainsizer );
	m_add_frame->Layout();
	mainsizer->Fit( m_add_frame );
}
void InterfaceMedSeg::OnMenuColorMaps (wxCommandEvent& event){
  m_colormaps_frame = new ManageColorMaps(this, -1, wxString(_languageDealer->getStringById(currentLanguage,"00064").c_str(),wxConvUTF8));
	m_colormaps_frame->Show(true);
	this->Enable(false);
}

void InterfaceMedSeg::OnMenuExtract (wxCommandEvent& event) 
{
	/*ImageFrame->m_xslice_bmplayer.SaveFile("theImg.bmp",wxBITMAP_TYPE_BMP);
	ImageFrame->m_xslice_bmplayer.SaveFile("theImg.jpg",wxBITMAP_TYPE_JPEG);
	ImageFrame->m_xslice_bmplayer.SaveFile("theImg.png",wxBITMAP_TYPE_PNG);
	ImageFrame->m_xslice_bmplayer.SaveFile("theImg.pcx",wxBITMAP_TYPE_PCX);
	ImageFrame->m_xslice_bmplayer.SaveFile("theImg.pnm",wxBITMAP_TYPE_PNM);
	ImageFrame->m_xslice_bmplayer.SaveFile("theImg.tif",wxBITMAP_TYPE_TIF);
	ImageFrame->m_xslice_bmplayer.SaveFile("theImg.xpm",wxBITMAP_TYPE_XPM);*/
	//wxFileDialog *filesel = new wxFileDialog(this, "Extraire l'image de la classification vers..","","","*.bmp|*.tiff",wxOVERWRITE_PROMPT);
	//filesel->Show(true);
	//wxString filename = wxFileSelector(wxString("Ouvrir une classification"),"","","iss","Fichiers de classification (*.iss)|*.iss",wxFD_OPEN);
	wxString mystring(_languageDealer->getStringById(currentLanguage,"00043").c_str(), wxConvUTF8);
	wxString filename = wxFileSelector(mystring,wxT(""),wxT(""),wxT(""),wxString(_languageDealer->getStringById(currentLanguage,"00044").c_str(),wxConvUTF8),wxOVERWRITE_PROMPT|wxSAVE);
	if ( !filename ) return;
	ImageFrame->m_bShould_save = true;
	ImageFrame->m_sExFilename = filename;
	ImageFrame->m_palette_changed = true;
	ImageFrame->updateView();
}

void InterfaceMedSeg::OnMenuExtractXLS (wxCommandEvent& event) 
{
  ExportXLSWindow *_exportXLSWin = new ExportXLSWindow(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00044").c_str(),wxConvUTF8));
  _exportXLSWin->Show(true);
  this->Enable(false);

	/*ImageFrame->m_xslice_bmplayer.SaveFile("theImg.bmp",wxBITMAP_TYPE_BMP);
	ImageFrame->m_xslice_bmplayer.SaveFile("theImg.jpg",wxBITMAP_TYPE_JPEG);
	ImageFrame->m_xslice_bmplayer.SaveFile("theImg.png",wxBITMAP_TYPE_PNG);
	ImageFrame->m_xslice_bmplayer.SaveFile("theImg.pcx",wxBITMAP_TYPE_PCX);
	ImageFrame->m_xslice_bmplayer.SaveFile("theImg.pnm",wxBITMAP_TYPE_PNM);
	ImageFrame->m_xslice_bmplayer.SaveFile("theImg.tif",wxBITMAP_TYPE_TIF);
	ImageFrame->m_xslice_bmplayer.SaveFile("theImg.xpm",wxBITMAP_TYPE_XPM);*/
	//wxFileDialog *filesel = new wxFileDialog(this, "Extraire l'image de la classification vers..","","","*.bmp|*.tiff",wxOVERWRITE_PROMPT);
	//filesel->Show(true);
	//wxString filename = wxFileSelector(wxString("Ouvrir une classification"),"","","iss","Fichiers de classification (*.iss)|*.iss",wxFD_OPEN);
  
// 	BasicExcel e;
// 	e.New(1);
// 	BasicExcelWorksheet* sheet = e.GetWorksheet("Sheet1");

// 	// message std::string to wxstring
// 	wxString mystring(_languageDealer->getStringById(currentLanguage,"00025").c_str(), wxConvUTF8);
// 	// filename std::string to wxstring
// 	wxString mystring2(m_image_filename.c_str(),wxConvUTF8);
// 	mystring2+=wxT(".xls");
// 	wxString filename = wxFileSelector(mystring,wxT(""),mystring2,wxT(""),wxString(_languageDealer->getStringById(currentLanguage,"00026").c_str(),wxConvUTF8),wxOVERWRITE_PROMPT|wxSAVE);
// 	if ( !filename ) return;
// 	//ImageFrame->updateView();
// 	double surface=0, ximage, yimage, xcm, ycm;
// 	// get image width and height input on text ctrl
// 	if(!OptionFrame->m_image_width->GetValue().ToDouble(&xcm)) xcm = 10;
// 	if(!OptionFrame->m_image_height->GetValue().ToDouble(&ycm)) ycm = 10;
// 	yimage = ImageFrame->m_isize(2);
// 	ximage = ImageFrame->m_isize(1);
// 	sheet->Cell(0,0)->SetString("Image filename:");
// 	sheet->Cell(0,1)->SetString(m_image_filename.c_str());
// 	sheet->Cell(1,0)->SetString("Specie Name");
// 	sheet->Cell(1,1)->SetString("Specie Id");
// 	sheet->Cell(1,2)->SetString("Surface (cm^2)");
// 	sheet->Cell(1,3)->SetString("Pixels");
// 	classPixels::iterator it;
// 	int row=1;
// 	for(it=_pixelsSpecies.begin();it!=_pixelsSpecies.end();++it){
// 		++row;
// 		surface = (double)(*it).second*(xcm/ximage)*(ycm/yimage);
// 		Specie *_specie = _locationDealer->getSpecieById((*it).first);
// 		sheet->Cell(row,0)->SetString(_specie->name.c_str());
// 		sheet->Cell(row,1)->SetInteger((*it).first);
// 		sheet->Cell(row,2)->SetDouble(surface);
// 		sheet->Cell(row,3)->SetInteger((*it).second);
// 	}
// 	e.SaveAs(filename.mb_str());
}

void InterfaceMedSeg::OnMenuOpen (wxCommandEvent& event) {
/*	ImageFrame->m_comp_ximg = true;
	ImageFrame->m_comp_xsec = true;
	ImageFrame->m_comp_xset = true;
	ImageFrame->m_comp_xbase = true;
	ImageFrame->m_comp_xbmp = true;*/
	//wxString filename = wxFileSelector(wxString("Select image scale-sets file"),"","","iss","Image scale-sets files (*.iss)|*.iss");
	if (ImageFrame->m_vSliceSection)
	{
		delete ImageFrame->m_vSliceSection;
		ImageFrame->m_vSliceSection = NULL;
	}
	wxString mystring(_languageDealer->getStringById(currentLanguage,"00045").c_str(), wxConvUTF8);
	wxString filename = wxFileSelector(mystring,wxT(""),wxT(""),wxT("iss"),wxString(_languageDealer->getStringById(currentLanguage,"00046").c_str(),wxConvUTF8),wxFD_OPEN);
	ImageMapping *mapping_im;
	if ( !filename ) return;

	m_sxs = new sxs::ImageScaleSets;
	m_delete_sxs = true;
	if ( !m_sxs->load(wx2std(filename)) ) 
	{
		wxLogError(wxT("Error loading '%s'."), filename.c_str());
		m_scale_set_loaded=false;
		GetMenuBar()->Enable(GetMenuBar()->FindMenuItem( wxT("3D View"),wxT("View 3D Window") ),FALSE);
		delete m_sxs;
		return;	
	}
	else
	{
		Display();
	}
	//GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString(_languageDealer->getStringById(currentLanguage,"00004")),wxString(_languageDealer->getStringById(currentLanguage,"00014")) ),TRUE);
	GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString(_languageDealer->getStringById(currentLanguage,"00004").c_str(),wxConvUTF8),wxString(_languageDealer->getStringById(currentLanguage,"00016").c_str(),wxConvUTF8) ),TRUE);
	GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString(_languageDealer->getStringById(currentLanguage,"00004").c_str(),wxConvUTF8),wxString(_languageDealer->getStringById(currentLanguage,"00019").c_str(),wxConvUTF8) ),TRUE);
	//GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString(_languageDealer->getStringById(currentLanguage,"00003")),wxString(_languageDealer->getStringById(currentLanguage,"00008")) ),TRUE);
	//GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString("&Fichier"),wxString("&Ouvrir") ),FALSE);
	//GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString("&Fonctionnalit�s"),wxString("&Segmenter une image") ),FALSE);
	mapping_im = ImageFrame->getImageMapping();
	MapImage *map_im;// = mapping_im->getMapImage();
	//if (!map_im)
	//{
		map_im = mapping_im->createMapImage(ImageFrame->m_xslice_bmp.GetWidth(),ImageFrame->m_xslice_bmp.GetHeight(),filename);
	//}
	OptionFrame->m_class_sel->LoadComboBox(mapping_im, filename);
	map_im->readImage();
	ImageFrame->InitLayer();
	ImageFrame->m_set_changed = true;
	//ImageFrame->getSliceImage();
	ImageFrame->resolveChanges();
	//OptionFrame->m_gauge->SetValue(0);
	//ImageFrame->getSliceBase();
	//***Description de la barre repr�sentant l'avancement du chargement des niveaux de segmentation***
	wxPanel temp_panel ( this, -1, wxPoint(ImageFrame->GetPosition().x,ImageFrame->GetPosition().y), wxSize(ImageFrame->GetSize().x,ImageFrame->GetSize().y));
	temp_panel.SetBackgroundColour(*wxBLACK);
	wxStaticText temp_label(&temp_panel, -1, wxT("........................................................................"), wxPoint(20, 20), wxDefaultSize);//, wxALIGN_RIGHT /*| wxST_NO_AUTORESIZE*/);
	temp_panel.SetForegroundColour( *wxRED );
	temp_label.SetOwnForegroundColour( *wxRED );
	temp_label.SetLabel(wxString(wxT("Chargement des niveaux de segmentation...")));
	temp_label.Show(true);
	wxGauge temp_gauge(&temp_panel, wxID_ANY, m_nlevels, wxPoint((temp_panel.GetSize().x-160)/2,(temp_panel.GetSize().y-30)/2), wxSize(160,30), wxGA_HORIZONTAL );
	temp_gauge.SetValue(0);
	temp_panel.Show(true);
	temp_panel.SetFocus();
	char clabel[256];
	//************************************************************************************************
	this->Enable(false);
	ImageFrame->m_vSliceSection = new std::vector<ImageRam*>();
	for (double i=0.; i<m_nlevels+1.; i=i+1.)
	{
		//***********Label changeant en fonction du niveau de chargement de la segmentation******
		sprintf(clabel,"Chargement des niveaux de segmentation... %d/%d  ",(int)i,(int)m_nlevels);
		wxString slabel(wxString(std::string(clabel).c_str(),wxConvUTF8));
		temp_label.SetLabel(slabel);
		//***************************************************************************************
		ImageRam* vec_slice_section = new ImageRamT<int>();
		m_sxs->getSection( *ImageFrame->m_xslice_base , ImageFrame->invlogscale(i/m_nlevels), vec_slice_section);
		//m_sxs->getSection( *ImageFrame->m_xslice_base , 0, vec_slice_section);
		ImageFrame->m_vSliceSection->push_back(vec_slice_section);
		temp_gauge.SetValue(temp_gauge.GetValue()+1);
	}
	this->Enable(true);
	// Variable utilis�e pour ne pas essayer de retracer une image vide au chargement de l'application.
	ImageFrame->SetActive();
	ImageFrame->updateView();
}

void InterfaceMedSeg::Display(){
	//GetMenuBar()->Enable(GetMenuBar()->FindMenuItem( wxString("3D View"),wxString("View 3D Window") ),TRUE);
	m_scale_set_loaded=true;
	ImageFrame->m_sxs=m_sxs;
	ImageFrame->m_delete_sxs=m_delete_sxs;
	if (!OptionFrame->InitBrowser()) {
	  wxLogMessage(wxT("Not initialised Correctly"));
		return;
	}


	/*ImageRGB tmp;
	lgl::PaletteId palZ;
	ImageToImageRGB ( *(ImageFrame->m_xslice_img), tmp, palZ, ImageFrame->m_zoom );
	tmp.SaveFile("tmp.tif");
	lglLOG("tmp.tif sauvegardee"<<std::endl)*/

	OptionFrame->OnScaleChange();

}

// functions for languages
void InterfaceMedSeg::setLanguage(string _currentLanguage){
	int foo;
	// modify menu item's labels
	menuBar->SetLabel(idMenuOpen,wxString(_languageDealer->getStringById(_currentLanguage,"00006").c_str(),wxConvUTF8));
	menuBar->SetHelpString(idMenuOpen,wxString(_languageDealer->getStringById(_currentLanguage,"00007").c_str(),wxConvUTF8));
menuBar->SetLabel(idMenuSave,wxString(_languageDealer->getStringById(_currentLanguage,"00008").c_str(),wxConvUTF8));
menuBar->SetHelpString(idMenuSave,wxString(_languageDealer->getStringById(_currentLanguage,"00009").c_str(),wxConvUTF8));
menuBar->SetLabel(idMenuQuit,wxString(_languageDealer->getStringById(_currentLanguage,"00010").c_str(),wxConvUTF8));
menuBar->SetHelpString(idMenuQuit,wxString(_languageDealer->getStringById(_currentLanguage,"00011").c_str(),wxConvUTF8));
menuBar->SetLabel(idMenuSegment,wxString(_languageDealer->getStringById(_currentLanguage,"00012").c_str(),wxConvUTF8));
menuBar->SetHelpString(idMenuSegment,wxString(_languageDealer->getStringById(_currentLanguage,"00013").c_str(),wxConvUTF8));
	//menuBar->SetLabel(idMenuAdd,_languageDealer->getStringById(_currentLanguage,"00014"));
	//menuBar->SetHelpString(idMenuAdd,_languageDealer->getStringById(_currentLanguage,"00015"));
menuBar->SetLabel(idMenuColorMaps,wxString(_languageDealer->getStringById(_currentLanguage,"00062").c_str(),wxConvUTF8));
menuBar->SetHelpString(idMenuColorMaps,wxString(_languageDealer->getStringById(_currentLanguage,"00063").c_str(),wxConvUTF8));
menuBar->SetLabel(idMenuExtract,wxString(_languageDealer->getStringById(_currentLanguage,"00016").c_str(),wxConvUTF8));
menuBar->SetHelpString(idMenuExtract,wxString(_languageDealer->getStringById(_currentLanguage,"00017").c_str(),wxConvUTF8));
menuBar->SetLabel(idMenuExtractXLS,wxString(_languageDealer->getStringById(_currentLanguage,"00019").c_str(),wxConvUTF8));
menuBar->SetHelpString(idMenuExtractXLS,wxString(_languageDealer->getStringById(_currentLanguage,"00020").c_str(),wxConvUTF8));
menuBar->SetLabel(idMenuAbout,wxString(_languageDealer->getStringById(_currentLanguage,"00018").c_str(),wxConvUTF8));
menuBar->SetHelpString(idMenuAbout,wxString(_languageDealer->getStringById(_currentLanguage,"00018").c_str(),wxConvUTF8));
menuBar->SetLabel(idMenuShowPolygon,wxString(_languageDealer->getStringById(_currentLanguage,"00085").c_str(),wxConvUTF8));
menuBar->SetHelpString(idMenuShowPolygon,wxString(_languageDealer->getStringById(_currentLanguage,"00085").c_str(),wxConvUTF8));
	// modify menu's labels
	menuBar->SetLabelTop(GetMenuBar()->FindMenu(wxString(_languageDealer->getStringById(currentLanguage,"00003").c_str(),wxConvUTF8)),wxString(_languageDealer->getStringById(_currentLanguage,"00003").c_str(),wxConvUTF8));
	menuBar->SetLabelTop(GetMenuBar()->FindMenu(wxString(_languageDealer->getStringById(currentLanguage,"00004").c_str(),wxConvUTF8)),wxString(_languageDealer->getStringById(_currentLanguage,"00004").c_str(),wxConvUTF8));
	menuBar->SetLabelTop(GetMenuBar()->FindMenu(wxString(_languageDealer->getStringById(currentLanguage,"00005").c_str(),wxConvUTF8)),wxString(_languageDealer->getStringById(_currentLanguage,"00005").c_str(),wxConvUTF8));
	menuBar->SetLabelTop(GetMenuBar()->FindMenu(wxString(_languageDealer->getStringById(currentLanguage,"00000").c_str(),wxConvUTF8)),wxString(_languageDealer->getStringById(_currentLanguage,"00000").c_str(),wxConvUTF8));
	wxStaticBox *fooBox;
	fooBox = OptionFrame->viewsizer->GetStaticBox();
	fooBox->SetLabel(wxString(_languageDealer->getStringById(_currentLanguage,"00049").c_str(),wxConvUTF8));
	fooBox = OptionFrame->sectionsizer->GetStaticBox();
	fooBox->SetLabel(wxString(_languageDealer->getStringById(_currentLanguage,"00050").c_str(),wxConvUTF8));
	fooBox = OptionFrame->scalesizer->GetStaticBox();
	fooBox->SetLabel(wxString(_languageDealer->getStringById(_currentLanguage,"00051").c_str(),wxConvUTF8));
	fooBox = OptionFrame->setsizer->GetStaticBox();
	fooBox->SetLabel(wxString(_languageDealer->getStringById(_currentLanguage,"00052").c_str(),wxConvUTF8));
	fooBox = OptionFrame->sizerStaticAreaSize->GetStaticBox();
	fooBox->SetLabel(wxString(_languageDealer->getStringById(_currentLanguage,"00061").c_str(),wxConvUTF8));
	fooBox = OptionFrame->sizerStaticAreaTable->GetStaticBox();
	fooBox->SetLabel(wxString(_languageDealer->getStringById(_currentLanguage,"00087").c_str(),wxConvUTF8));
	fooBox = OptionFrame->sizerStaticAreaButtonNavigation->GetStaticBox();
	fooBox->SetLabel(wxString(_languageDealer->getStringById(_currentLanguage,"00088").c_str(),wxConvUTF8));

	OptionFrame->m_affich_classif_cb->SetLabel(wxString(_languageDealer->getStringById(_currentLanguage,"00053").c_str(),wxConvUTF8));
	OptionFrame->m_image_width_label->SetLabel(wxString(_languageDealer->getStringById(_currentLanguage,"00023").c_str(),wxConvUTF8));
	OptionFrame->m_image_height_label->SetLabel(wxString(_languageDealer->getStringById(_currentLanguage,"00024").c_str(),wxConvUTF8));
	OptionFrame->m_image_pixels_cm_label->SetLabel(wxString(_languageDealer->getStringById(_currentLanguage,"00131").c_str(),wxConvUTF8));
	OptionFrame->m_image_pixels_cm->SetLabel(wxString(_languageDealer->getStringById(_currentLanguage,"00132").c_str(),wxConvUTF8));
	OptionFrame->buttonSegmentSize->SetLabel(wxString(_languageDealer->getStringById(_currentLanguage,"00100").c_str(),wxConvUTF8));
	OptionFrame->buttonShowTable->SetLabel(wxString(_languageDealer->getStringById(_currentLanguage,"00086").c_str(),wxConvUTF8));
	OptionFrame->buttonShowNavigationWindow->SetLabel(wxString(_languageDealer->getStringById(_currentLanguage,"00088").c_str(),wxConvUTF8));
	//	OptionFrame->classGrid->SetColLabelValue(0,wxString(_languageDealer->getStringById(_currentLanguage,"00021").c_str(),wxConvUTF8));
	//	OptionFrame->classGrid->SetColLabelValue(1,wxString(_languageDealer->getStringById(_currentLanguage,"00022").c_str(),wxConvUTF8));
	int prevselection = OptionFrame->m_section_style_radio->GetSelection();
	wxString sstyle[3];
	sstyle[0] = wxString(_languageDealer->getStringById(_currentLanguage,"00054").c_str(),wxConvUTF8);
	sstyle[1] = wxString(_languageDealer->getStringById(_currentLanguage,"00055").c_str(),wxConvUTF8);
	// update current language variable
	currentLanguage=_currentLanguage;
}

void InterfaceMedSeg::OnMenuLanguage1 (wxCommandEvent& event){
	string _currentLanguage;
	if(!(_languages->size()<1)){
		_currentLanguage = (*_languages)[0];	
		setLanguage(_currentLanguage);
	}
}
void InterfaceMedSeg::OnMenuLanguage2 (wxCommandEvent& event){
	string _currentLanguage;
	if(!(_languages->size()<2)){
		_currentLanguage = (*_languages)[1];	
		setLanguage(_currentLanguage);
	}

}
void InterfaceMedSeg::OnMenuLanguage3 (wxCommandEvent& event){
	string _currentLanguage;
	if(!(_languages->size()<3)){
		_currentLanguage = (*_languages)[2];
		setLanguage(_currentLanguage);
	}

}
void InterfaceMedSeg::OnMenuLanguage4 (wxCommandEvent& event){
	string _currentLanguage;
	if(!(_languages->size()<4)){
		_currentLanguage = (*_languages)[3];	
	}

}
void InterfaceMedSeg::OnMenuLanguage5 (wxCommandEvent& event){
	string _currentLanguage;
	if(!(_languages->size()<5)){
		_currentLanguage = (*_languages)[4];	
	}

}
void InterfaceMedSeg::OnMenuLanguage6 (wxCommandEvent& event){
	string _currentLanguage;
	if(!(_languages->size()<6)){
		_currentLanguage = (*_languages)[5];	
	}

}
void InterfaceMedSeg::OnMenuLanguage7 (wxCommandEvent& event){
	string _currentLanguage;
	if(!(_languages->size()<7)){
		_currentLanguage = (*_languages)[6];	
	}

}
void InterfaceMedSeg::OnMenuLanguage8 (wxCommandEvent& event){
	string _currentLanguage;
	if(!(_languages->size()<8)){
		_currentLanguage = (*_languages)[7];	
	}

}
void InterfaceMedSeg::OnMenuLanguage9 (wxCommandEvent& event){
	string _currentLanguage;
	if(!(_languages->size()<9)){
		_currentLanguage = (*_languages)[8];	
	}

}
void InterfaceMedSeg::OnMenuLanguage10 (wxCommandEvent& event){
	string _currentLanguage;
	if(!(_languages->size()<10)){
		_currentLanguage = (*_languages)[9];	
	}

}

//=======================================================================================
void InterfaceMedSeg::OnMenuSave (wxCommandEvent& event) 
{
	wxString mystring(_languageDealer->getStringById(currentLanguage,"00047").c_str(), wxConvUTF8);
	wxString filename = wxFileSelector(mystring,wxT(""),wxT(""),wxT("iss"),wxString(_languageDealer->getStringById(currentLanguage,"00048").c_str(),wxConvUTF8),wxFD_SAVE);
	if ( !filename ) return;

	if ( !ImageFrame->m_sxs->save(wx2std(filename),lgl::Filename(""))) {
		wxLogError(wxT("Error saving '%s'."), filename.c_str());
		return;	
	}
	else
	{
		// Rajout du chemin vers le fichier contenant la classification de l'image.
	  lgl::Filename s(filename.mb_str());
	  lgl::Filename body(filename.mb_str());
		body = lgl::removeExtension(body);
		lgl::setExtension(s,"iss");
		std::ofstream f;
		f.open((char*)s.c_str(),std::ios::out|std::ios::trunc);
		if (f.good())
		{
			/*f << "classification \t:";
			lgl::Filename medname(filename);
			medname = body;
			lgl::setExtension(medname,"med");
			f << medname << std::endl;
			f << "liste_classif \t:";
			lgl::Filename sifname(filename);
			sifname = body;
			lgl::setExtension(sifname,"sif");
			f << sifname << std::endl;*/
			f << "Laurent Guigues - 2004" << std::endl;
			f << "Thomas Prelot - 2007" << std::endl;
			wxDateTime now = wxDateTime::Now();
			f << "Derni�re modification le " << now.GetDay() << "/" << now.GetMonth() << "/" <<
				now.GetYear() << " � " << now.GetHour() << ":" << now.GetMinute() << ":" << now.GetSecond() 
				<< std::endl;
		}
		// Rajout du chemin vers le fichier contenant la classification de l'image.
		f.close();
		ImageMapping *mapping_im = ImageFrame->getImageMapping();
		//mapping_im->saveClassification(filename);
		MapImage *map_im = mapping_im->getMapImage();
		map_im->saveImage(filename);
		mapping_im->writeClassification(filename);
		//mapping_im->writeClassification(body);
	}
}

//=======================================================================================

/*void InterfaceMedSeg::OnMenu3DWindow(wxCommandEvent& event) {
if(GetMenuBar()->IsChecked(event.GetId())){
if(!p_is_3D_Window_Created){
m_3D_Viewer = new sxs3DViewer(this);
m_3D_Viewer->m_canvas=new sxs3DCanvas(m_3D_Viewer);
m_3D_Viewer->Show(true);
p_is_3D_Window_Created=true;
CreateScene();
}
GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString("3D View"),wxString("Change Points Size") ),TRUE);
GetMenuBar()->Enable(GetMenuBar()->FindMenuItem( wxString("3D View"),wxString("Configure 3D Moving Speed") ),TRUE);
GetMenuBar()->Enable(GetMenuBar()->FindMenuItem( wxString("3D View"),wxString("ScrollBar Enslavement") ),TRUE);
m_3D_Viewer->Show(true);
}else{
GetMenuBar()->Enable(GetMenuBar()->FindMenuItem(wxString("3D View"),wxString("Change Points Size") ),FALSE);
GetMenuBar()->Enable(GetMenuBar()->FindMenuItem( wxString("3D View"),wxString("Configure 3D Moving Speed") ),FALSE);
GetMenuBar()->Enable(GetMenuBar()->FindMenuItem( wxString("3D View"),wxString("ScrollBar Enslavement") ),FALSE);
m_3D_Viewer->Show(false);
}
}
void InterfaceMedSeg::OnMenu3DScrollEnslavement(wxCommandEvent& event) {
if(GetMenuBar()->IsChecked(event.GetId())){
ImageFrame->FollowScroll();
p_scoll_slaved=true;
}else{
p_scoll_slaved=false;
}
}

void InterfaceMedSeg::OnMenu3DpointSize(wxCommandEvent& event) {
PointSizeDialog *dialog=new PointSizeDialog(this); 
if (dialog->ShowModal() == wxID_OK){
m_points->SetPointSize(dialog->m_spinctrl->GetValue());
m_3D_Viewer->m_canvas->Refresh(false);
}
}*/

/*void InterfaceMedSeg::OnMenu3DMoveConfig(wxCommandEvent& event) {
Move3DConfig *dialog=new Move3DConfig(this); 
if (dialog->ShowModal() == wxID_OK){
double temp;
if(dialog->m_Eye_Rate_Entry->GetValue().ToDouble(&temp)){
m_3D_Viewer->m_canvas->m_eye_rate=(float)temp;
}
if(dialog->m_COI_Rate_Entry->GetValue().ToDouble(&temp)){
m_3D_Viewer->m_canvas->m_coi_rate=(float)temp;
}
if(dialog->m_Up_Rate_Entry->GetValue().ToDouble(&temp)){
m_3D_Viewer->m_canvas->m_up_rate=(float)temp;
}
if(dialog->m_Down_Rate_Entry->GetValue().ToDouble(&temp)){
m_3D_Viewer->m_canvas->m_down_rate=(float)temp;
}
if(dialog->m_Left_Rate_Entry->GetValue().ToDouble(&temp)){
m_3D_Viewer->m_canvas->m_left_rate=(float)temp;
}
if(dialog->m_Right_Rate_Entry->GetValue().ToDouble(&temp)){
m_3D_Viewer->m_canvas->m_right_rate=(float)temp;
}
}
}*/

//=======================================================================================	
void InterfaceMedSeg::OnMenuAbout (wxCommandEvent& event) 
{
	/*
	wxString foo;
	foo = wxString(_languageDealer->getStringById(currentLanguage,"00135").c_str(),wxConvUTF8);
	foo.Replace("\\n","\n",true);
	wxString msg;
	msg.Printf( foo);
	wxMessageBox(msg, wxT("About"), wxOK | wxICON_INFORMATION, this);
	*/
	MyAboutWindow *_about = new MyAboutWindow(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00136").c_str(),wxConvUTF8));
	this->Enable(false);
	_about->Show(true);
}
//=======================================================================================


//=======================================================================================	
void InterfaceMedSeg::OnMenuQuit (wxCommandEvent& event) 
{
	Close();
}
//=======================================================================================

/*void InterfaceMedSeg::CreateScene(){
sgl::initialize();

m_scene=new Scene();

m_points=new PointSet();

std::set<ScaleSets::Set*> Sets;
m_sxs->getSection(ImageFrame->m_scale, Sets);

PaletteLUT pal = ImageFrame->getPalette();
std::vector < lgl::Point4D <float> > liste;


//Recherche de la palette optimal
std::set <ScaleSets::Set*>::const_iterator it=Sets.begin(),fin=Sets.end();

liste = (*it)->attribute()->thePts;

/*
float min, max;
min = max = 0;

for(;it!=fin;++it){
liste = (*it)->attribute()->thePts;
if (liste.size())
{
if(min>(*liste.begin())(0) ){
min=(*liste.begin())(0);
}
if(max<(*liste.begin())(0) ){
max=(*liste.begin())(0);
}
}
}

pal.setRandomPalette(min,max);*/
/*pal.setRandomPalette(0,Sets.size());

it=Sets.begin();
fin=Sets.end();

int cptr = 0;
for(;it!=fin;++it, ++cptr){
liste = (*it)->attribute()->thePts;
//lglLOG("liste.size() : "<<liste.size()<<std::endl);
if (liste.size())
{
//RGBpoint p = pal((*liste.begin())(0));
RGBpoint p = pal(cptr);
std::vector <lgl::Point4D <float> >::const_iterator it2=liste.begin(), fin2=liste.end();
for(;it2!=fin2;++it2){
m_points->FastAddPoint((*it2)(0),(*it2)(1),(*it2)(2),((float) p.r)/255,((float) p.g)/255,((float) p.b)/255,1.0f);
}
}
}

m_points->Pack();
m_points->SetPointSize(1);

m_scene->AddGeometrySet(m_points);

m_3D_Viewer->m_canvas->SetScene(m_scene);

ImageFrame->updateView();
}*/




BEGIN_EVENT_TABLE(PointSizeDialog, wxDialog)
EVT_CLOSE(PointSizeDialog::OnExit)
END_EVENT_TABLE()

PointSizeDialog::PointSizeDialog(InterfaceMedSeg *parent) : wxDialog(parent, -1, wxT("Point Size"), wxDefaultPosition, wxSize(320, 140), wxDEFAULT_DIALOG_STYLE)
{

	/*wxBoxSizer *TopSizer = new wxBoxSizer( wxVERTICAL );
	wxBoxSizer *Sizer = new wxBoxSizer( wxHORIZONTAL );
	Sizer->Add(new wxStaticText(this, -1, "Point Size: "), 0, wxALIGN_LEFT);
	m_spinctrl=new wxSpinCtrl(this, -1, wxString::Format("%d",(int)floor(parent->m_points->GetPointSize())),wxDefaultPosition, wxDefaultSize,wxSP_ARROW_KEYS, 1, 20, (int)floor(parent->m_points->GetPointSize()),wxString("wxSpinCtrl"));
	Sizer->Add(m_spinctrl,0,wxALIGN_RIGHT);
	TopSizer->Add(Sizer,0,wxALIGN_CENTER);

	wxButton *OkButton = new wxButton(this, wxID_OK, "Ok", wxDefaultPosition);
	TopSizer->Add(OkButton,0,wxALIGN_CENTER);

	OkButton->SetFocus();
	SetSizerAndFit(TopSizer);
	SetAutoLayout(TRUE);
	Layout();*/
}

void PointSizeDialog::OnExit(wxCloseEvent & event){
	if(event.CanVeto()){
		event.Veto();	
	}
}






BEGIN_EVENT_TABLE( ScrollWinMedSeg, wxScrolledWindow ) 
EVT_PAINT( ScrollWinMedSeg::OnPaint ) 
EVT_MOUSE_EVENTS( ScrollWinMedSeg::OnMouse ) 
EVT_KEY_DOWN ( ScrollWinMedSeg::OnKeyPress )
  //EVT_SCROLLWIN_BOTTOM( ScrollWinMedSeg::OnScroll )
EVT_SCROLLWIN (ScrollWinMedSeg::OnScroll)
EVT_MOUSEWHEEL( ScrollWinMedSeg::OnWheelRotation )
EVT_SCROLL ( ScrollWinMedSeg::OnWheelRotation2 )
EVT_MOTION (ScrollWinMedSeg::OnMouseMove)
END_EVENT_TABLE()

void ScrollWinMedSeg::OnWheelRotation2(wxScrollEvent& ev)
{
  wxLogMessage(wxT("testttttttttttttttttttttttttttttttttttttttu"));
}

void ScrollWinMedSeg::OnWheelRotation(wxMouseEvent& ev)
{
  wxLogMessage(wxT("testtttttttttttttttttttttttttttttttttttttt"));
	/*if (ev.GetWheelRotation()>0)
	{
		m_owner->OptionFrame->m_scale_slider->SetValue(m_owner->OptionFrame->m_scale_slider->GetValue()+10);
	}
	else
	{
		m_owner->OptionFrame->m_scale_slider->SetValue(m_owner->OptionFrame->m_scale_slider->GetValue()-10);
	}*/
}

void ScrollWinMedSeg::OnScroll(wxScrollWinEvent & event)
{
  wxScrolledWindow::OnScroll(event);
  if(m_owner->OptionFrame->m_navigationWindow!=NULL)
    m_owner->OptionFrame->m_navigationWindow->RePaint(event);
}

/*void ScrollWinMedSeg::OnScroll(wxScrollWinEvent & event){
if(m_owner->p_is_3D_Window_Created){
if(m_owner->p_scoll_slaved){
FollowScroll();
}
}
event.Skip();
}

void ScrollWinMedSeg::FollowScroll(){
lgl::ImageSite pos(0,0,0,0); 

int w,h,x,y;
GetClientSize(&w,&h);
CalcUnscrolledPosition( w/2, h/2, &x, &y);

pos(1) = x/m_zoom;
pos(2) = y/m_zoom;
pos(3) = 0;

int lab =  (int)m_xslice_base->get( pos );
ScaleSets::Set* s = (*m_sxs)[lab]; 
unsigned int t = s->attribute()->thePts.size();
float x_pos,y_pos,z_pos;
x_pos=y_pos=z_pos=0;
for(unsigned int j=0;j<t;++j){
lgl::Point4D <float> pt(s->attribute()->thePts[j]);
x_pos+=pt(0);
y_pos+=pt(1);
z_pos+=pt(2);
}
x_pos/=(float)t;y_pos/=(float)t;z_pos/=(float)t;
//	m_owner->m_3D_Viewer->m_canvas->SetSceneAt(x_pos,y_pos,z_pos);

}*/

 void ScrollWinMedSeg::ShowPolygon(ImageSite &pos){
   vector<MPolygon*> *pols = m_owner->GetPolygons();
   vector<MPolygon*>::iterator itPols;
   mRegion *points;
   mPoint *p = new mPoint(pos(1),pos(2));
   int polId;
   for(itPols=pols->begin();itPols!=pols->end();++itPols){
     points=(*itPols)->GetPixels();
     if(points->count(p)>0){
       polId=(*itPols)->GetId();
       break;
     }
   }
   m_owner->ShowTableWindow(polId);
   delete p;
 }

//=======================================================================================
void ScrollWinMedSeg::OnMouse(wxMouseEvent& ev)
{

  int m_nlevels = m_owner->m_nlevels;
  // for checking wheter ctrl is pressed
  //  wxKeyboardState kbdState();

	// Permet de modifier le niveau de segmentation en scrollant sur l'image.
	//this->SetFocus();
	if (ev.GetWheelRotation()!=0 && m_owner->OptionFrame->m_scale_slider->IsEnabled()) 
	{
		if (ev.GetWheelRotation()>0)
		{
			m_owner->OptionFrame->m_scale_slider->SetValue(m_owner->OptionFrame->m_scale_slider->GetValue()+(m_owner->OptionFrame->m_scale_slider->GetMax()*1./m_nlevels));
		}
		else
		{
			m_owner->OptionFrame->m_scale_slider->SetValue(m_owner->OptionFrame->m_scale_slider->GetValue()-(m_owner->OptionFrame->m_scale_slider->GetMax()*1./m_nlevels));
		}
		m_scale_changed = true;
		updateView();
	}
	if(m_owner->m_scale_set_loaded){
		lgl::ImageSite pos(0,0,0); 
		int x,y,w,h;
		CalcUnscrolledPosition( ev.m_x, ev.m_y, &x, &y);

		GetClientSize(&w,&h);
		pos(1) = x/m_zoom;
		pos(2) = y/m_zoom;
		pos(3) = 0;

		int x_ini,y_ini,rate_x,rate_y;
		GetViewStart(&x_ini, &y_ini);
		GetScrollPixelsPerUnit(&rate_x,&rate_y);
		x_ini*=rate_x;
		y_ini*=rate_y;


		if(ev.m_x<w && ev.m_y<h && 
			x<m_image_frame_size.x && y<m_image_frame_size.y
			&& x>x_ini && y>y_ini && ev.m_x>0 && ev.m_y>0){
				wxString stat;
				stat.Printf(wxT("%d,%d"),pos(1),pos(2));
				m_owner->SetStatusText(stat,0);
				// image value
				/*
				int v = (int)(m_image->get(pos));
				stat.Printf("%d",v);
				m_owner->SetStatusText(stat,1);
				*/
				wxClientDC dc( this );
				wxPen pen(wxColor(m_cur_set_color.r,m_cur_set_color.g,m_cur_set_color.b),2,wxSOLID);
				//				m_controlKey = kbdState.ControlDown();

				if (ev.RightDown()) {
					// memo position
					m_last_sel_x = ev.m_x;
					m_last_sel_y = ev.m_y;

					int lab =  (int)m_xslice_base->get( pos );
					pSet s = (*m_sxs)[lab]; 
					m_cur_sets.clear();
					m_cur_sets.insert(s);

					m_mouse_test=true;
				}

				if (ev.RightIsDown()&& m_mouse_test){
					// set selection 
					dc.SetPen(pen);
					int lab =  (int)m_xslice_base->get( pos );
					pSet s = (*m_sxs)[lab]; 
					m_cur_sets.insert(s);
					// Draws the selection 
					dc.DrawLine(m_last_sel_x,m_last_sel_y,ev.m_x,ev.m_y);
					m_last_sel_x = ev.m_x;
					m_last_sel_y = ev.m_y;

				}
				if (ev.RightUp() && m_mouse_test) {
					// set selection 
					dc.DrawLine(m_last_sel_x,m_last_sel_y,ev.m_x,ev.m_y);

					m_last_sel_x = ev.m_x;
					m_last_sel_y = ev.m_y;

					std::set <int> slab;

					int lab =  (int)m_xslice_base->get( pos );
					pSet s = (*m_sxs)[lab]; 
					m_cur_sets.insert(s);





					std::map<ScaleSets::Set*,ScaleSets::Set*> out;
					getMaximalAncestors(m_cur_sets,m_scale,out);
					m_cur_sets.clear();

					ScaleSets::Set* set;

					std::map<ScaleSets::Set*,ScaleSets::Set*>::iterator it = out.begin();
					for(;it!=out.end();++it){
						set = (*it).first;
						m_cur_sets.insert(set);
						slab.insert(set->index());

						std::vector <ScaleSets::Set*> fils;
						getBase (set,  fils);
						std::vector<ScaleSets::Set*>::iterator it2 = fils.begin();

						for(;it2!=fils.end();++it2){
							m_cur_sets.insert(*it2);
							slab.insert((*it2)->index());
						}
					}

					// S�lection d'une r�gion avec un click.
					UnLayersOnSelectedRegions( *m_xslice_img, *m_xslice_lab, slab, m_xslice_mask, m_xslice_layer, m_map_image);

					m_set_changed = true;
					m_mouse_test=false;
					m_owner->UpdateClassGrid(m_owner->OptionFrame->classGrid);

					updateView();
				}

				if (ev.LeftDown()&&!m_controlKey&&!m_scaling) {
					// memo position
					m_last_sel_x = ev.m_x;
					m_last_sel_y = ev.m_y;

					//pos.dim = 3;
					int lab =  (int)m_xslice_base->get( pos );
					pSet s = (*m_sxs)[lab]; 
					m_cur_sets.clear();
					m_cur_sets.insert(s);

					m_mouse_test=true;
				}

				if (ev.LeftIsDown()&& m_mouse_test&&!m_controlKey&&!m_scaling){
					// set selection 
					dc.SetPen(pen);
					int lab =  (int)m_xslice_base->get( pos );
					pSet s = (*m_sxs)[lab]; 
					m_cur_sets.insert(s);
					// Draws the selection 
					dc.DrawLine(m_last_sel_x,m_last_sel_y,ev.m_x,ev.m_y);
					m_last_sel_x = ev.m_x;
					m_last_sel_y = ev.m_y;

				}
				if (ev.LeftUp() && m_mouse_test) {
					// set selection 
					dc.DrawLine(m_last_sel_x,m_last_sel_y,ev.m_x,ev.m_y);

					m_last_sel_x = ev.m_x;
					m_last_sel_y = ev.m_y;

					std::set <int> slab;

					int lab =  (int)m_xslice_base->get( pos );
					pSet s = (*m_sxs)[lab]; 
					m_cur_sets.insert(s);



					std::map<ScaleSets::Set*,ScaleSets::Set*> out;
					getMaximalAncestors(m_cur_sets,m_scale,out);
					m_cur_sets.clear();

					ScaleSets::Set* set;

					std::map<ScaleSets::Set*,ScaleSets::Set*>::iterator it = out.begin();
					for(;it!=out.end();++it){
						set = (*it).first;
						m_cur_sets.insert(set);
						slab.insert(set->index());

						std::vector <ScaleSets::Set*> fils;
						getBase (set,  fils);
						std::vector<ScaleSets::Set*>::iterator it2 = fils.begin();

						for(;it2!=fils.end();++it2){
							m_cur_sets.insert(*it2);
							slab.insert((*it2)->index());
						}
					}

					// S�lection d'une r�gion avec un click.
					LayersOnSelectedRegions( *m_xslice_img, *m_xslice_lab, slab, m_xslice_mask, m_xslice_layer, m_map_image);

					m_set_changed = true;
					m_mouse_test=false;
					m_owner->UpdateClassGrid(m_owner->OptionFrame->classGrid);

					updateView();
				}	
				if(ev.LeftUp()&&m_controlKey&&!m_scaling){
				  ShowPolygon(pos);
				  m_controlKey = false;
				}
				if(ev.LeftUp()&&m_scaling){
				  if(m_scaling_x0==-1){
				    m_scaling_x0 = pos(1);
				    m_scaling_y0 = pos(2);
				    m_scaling_changed=true;
				    updateView();
					Refresh();
				  }
				  else{
				    m_scaling_x1 = pos(1);
				    m_scaling_y1 = pos(2);
				    m_scaling_changed=true;
				    updateView();
				    GatherSegmentScale();
				    updateView();
					Refresh();
				  }
				}
			}

	}
	// Set change

	/*if (ev.GetWheelRotation()!=0) {    
		std::set<ImageScaleSets::Set*> old;
		std::set<ImageScaleSets::Set*>::iterator i;
		pSet m_cur;
		if (ev.GetWheelRotation()>0) {    
			wxLogMessage("UP");
			old = m_cur_sets;
			m_cur_sets.clear();
			for (i=old.begin();i!=old.end();++i) {
				m_cur_sets.insert((*i)->father());
			}
		}else{
			wxLogMessage("HOME");
			old = m_cur_sets;
			m_cur_sets.clear();
			std::set<ImageScaleSets::Set*>::iterator i;
			for (i=old.begin();i!=old.end();++i) {
				m_cur = (*i)->father();
				while ((!m_cur->isRoot())&&(m_cur->persistence()<m_set_persistence)) {
					m_cur = m_cur->father();
				}
				m_cur_sets.insert(m_cur);
			}
		}
		m_set_changed = true;
		updateView();
	}*/
}

vector<MPolygon*>* InterfaceMedSeg::GetPolygons(){
  return _polygons;
}

void InterfaceMedSeg::UpdateClassGrid(wxGrid *classGrid){
  /*
	// reset grid
	for(int i=0;i<classGrid->GetNumberRows();++i){
		for(int j=0;j<classGrid->GetNumberCols();++j){
		  classGrid->SetCellValue(i,j,wxT(""));
		}
	}
	// refill grid
	classPixels::iterator it;
	int i=0;
	double surface=0, ximage, yimage, xcm, ycm;
	// get image width and height input on text ctrl
	if(!OptionFrame->m_image_width->GetValue().ToDouble(&xcm)) xcm = 10;
	if(!OptionFrame->m_image_height->GetValue().ToDouble(&ycm)) ycm = 10;
	yimage = ImageFrame->m_isize(2);
	ximage = ImageFrame->m_isize(1);
	/*
	Specie *_specie;
	for(it=_pixelsSpecies.begin();it!=_pixelsSpecies.end();++it){
		_specie = _locationDealer->getSpecieById((*it).first);
		if(_specie!=NULL)
		  classGrid->SetCellValue(i,0,wxString(_specie->name.c_str(),wxConvUTF8));
		else
		  classGrid->SetCellValue(i,0,wxT("ERROR"));
		surface = (double)(*it).second*(xcm/ximage)*(ycm/yimage);
		wxString foo;
		foo<<surface;
		classGrid->SetCellValue(i,1,foo);
		++i;
	}*
	*/
  /*
	vector<MPolygon *>::iterator itPols;
	MPolygon *pol;
	for(itPols=_polygons->begin();itPols!=_polygons->end();++itPols){
	  pol = (*itPols);
	  pol->SetPixelSideLength(xcm);
	  classGrid->SetCellValue(i,0,wxString::Format(wxT("%i"),pol->GetClassId()));
	  classGrid->SetCellValue(i,1,wxString::Format(wxT("%f"),pol->GetPerimeter()));
	  ++i;
	}
*/
}


//=======================================================================================
void ScrollWinMedSeg::OnKeyPress(wxKeyEvent& ev)
{
	std::set<ImageScaleSets::Set*> old;
	std::set<ImageScaleSets::Set*>::iterator i;

	pSet m_cur;
	switch (ev.GetKeyCode()) {
case WXK_SPACE :
	{
	  wxLogMessage(wxT("Saving region"));
		std::ofstream f;
		lgl::Filename filename = "region.txt";
		const char* filenameb = "region.txt";
		f.open(filenameb/*.GetFullPath()*/,PARAM_OFSTREAM_BINARY);

		//				for (i=m_cur_sets.begin();i!=m_cur_sets.end();++i) {
		//					ImageScaleSets::Set* s = (*i);

		//					unsigned int t = s->attribute()->thePts.size();
		//					f.write((char*)&t,sizeof(t));
		//					for(unsigned int j=0;j<t;++j)
		//					{
		//						lgl::Point4D <float> pt(s->attribute()->thePts[j]);
		//						f.write((char*)&pt(0),sizeof(pt(0)));
		//						f.write((char*)&pt(1),sizeof(pt(1)));
		//						f.write((char*)&pt(2),sizeof(pt(2)));
		//					}
		//				}
		wxLogMessage(wxT("Region saved"));
		break;
	}
case WXK_UP :
  wxLogMessage(wxT("UP"));
	old = m_cur_sets;
	m_cur_sets.clear();
	for (i=old.begin();i!=old.end();++i) {
		m_cur_sets.insert((*i)->father());
	}
	m_set_changed = true;
	updateView();
	break;
case WXK_HOME :
  wxLogMessage(wxT("HOME"));
	old = m_cur_sets;
	m_cur_sets.clear();
	for (i=old.begin();i!=old.end();++i) {
		m_cur = (*i)->father();
		while ((!m_cur->isRoot())&&(m_cur->persistence()<m_set_persistence)) {
			m_cur = m_cur->father();
		}
		m_cur_sets.insert(m_cur);
	}
	m_set_changed = true;
	updateView();
	break;
case WXK_F11 :
	if(m_owner->OptionFrame->m_zoom_combo->GetSelection()+1<m_owner->OptionFrame->m_zoom_combo->GetCount()){
		m_owner->OptionFrame->m_zoom_combo->SetSelection(m_owner->OptionFrame->m_zoom_combo->GetSelection()+1);
		m_zoom=m_owner->OptionFrame->m_zoom_combo->GetSelection()+1;
		m_owner->OptionFrame->Fit();
		m_zoom_changed=true;
		updateView();
	}

	break;
	case WXK_F12 :
	if(m_owner->OptionFrame->m_zoom_combo->GetSelection()-1 > -1){
		m_zoom=m_owner->OptionFrame->m_zoom_combo->GetSelection();	
		m_owner->OptionFrame->m_zoom_combo->SetSelection(m_owner->OptionFrame->m_zoom_combo->GetSelection()-1);
		m_owner->OptionFrame->Fit();
		m_zoom_changed=true;
		updateView();
	}

	break;				   
	case WXK_CONTROL:
	  if(ev.ControlDown())
	    m_controlKey = true;
	  else
	    m_controlKey = false;
	  break;

	}
}

void ScrollWinMedSeg::GatherSegmentScale(){
  languageDealer *_languageDealer = m_owner->_languageDealer;
  String currentLanguage = m_owner->currentLanguage;
  SegmentSizingWindow *_segSizWin = new SegmentSizingWindow(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00101").c_str(),wxConvUTF8));
  _segSizWin->Show(true);
  m_scaling_changed=false;
}

//======================================================================================= 	
void ScrollWinMedSeg::updateView() 
{
	int w,h;
	int client_w,client_h;
	int x_ini,y_ini;
	int rate_x,rate_y;
	char *_cursorPath = new char[256];
	_cursorPath[0]='\0';

#ifndef __WINDOWS__  
	strcat(_cursorPath,getenv("HOME"));
	_cursorPath = strcat((char*)_cursorPath,"/.seascape/cursor.gif");
#else		   
	_cursorPath = "data/cursor.gif";
#endif
	// Taille de la fenetre visible
	GetClientSize(&w,&h);
	// Taille du DC
	GetVirtualSize(&client_w,&client_h);
	// Position de la scrollbar en scrollunit
	GetViewStart(&x_ini, &y_ini);
	// Vitesse de Scroll
	GetScrollPixelsPerUnit(&rate_x,&rate_y);

	// D�finition de la couleur des contours qui vont �tre trac�s.
	if (m_bActive && m_owner && m_owner->OptionFrame && m_owner->OptionFrame->m_bm_contour_col)
	{
		if (!(m_contours_color.r == m_owner->OptionFrame->m_bm_contour_col->getColor()->Red()
			&& m_contours_color.g == m_owner->OptionFrame->m_bm_contour_col->getColor()->Green()
			&& m_contours_color.b == m_owner->OptionFrame->m_bm_contour_col->getColor()->Blue()))
		{
			m_contours_color.r = m_owner->OptionFrame->m_bm_contour_col->getColor()->Red();
			m_contours_color.g = m_owner->OptionFrame->m_bm_contour_col->getColor()->Green();
			m_contours_color.b = m_owner->OptionFrame->m_bm_contour_col->getColor()->Blue();
			m_palette_changed = true;
		}
	}

	//wxLogMessage("=========================================");
	// Pour garder le mask � la bonne position en cas de scrolling de la fenetre contenant l'image
	if (m_bIsScroll || (!m_xslice_changed  && !m_scale_changed && !m_set_changed && !m_zoom_changed && !m_palette_changed)||m_scaling_changed)
	{
		/*m_vsize(1) = m_isize(1)*m_zoom;
		m_vsize(2) = m_isize(2)*m_zoom;
		m_vsize(3) = m_isize(3)*m_zoom;			
		m_image_frame_size.x = m_vsize(1);
		m_image_frame_size.y = m_vsize(2);
		m_has_to_fit=true;*/
		//dcClear();
		//*************************************************************************
		/*if (m_shouldScroll)
		{
			wxClientDC dc3 ( this );
			m_super_mask = new wxMask(m_xslice_bmpmask,wxColour(0,0,0));
			m_xslice_bmp.SetMask(m_super_mask);
			wxMemoryDC temp_dc3;
			temp_dc3.SelectObject(m_xslice_bmp);
			wxBufferedDC super_dc3(&dc3, m_xslice_bmplayer);
			super_dc3.Blit(0, 0, client_w, client_h, &temp_dc3, 0, 0, wxCOPY, true);
			m_shouldScroll = false;
		}*/
		//*************************************************************************
		m_set_changed = true;
		m_mouse_test = false;
		m_bIsScroll = false;
		Scroll(x_ini,y_ini);
		//wxClientDC dc2 ( this );
		wxPaintDC dc2( this );
		PrepareDC( dc2 );
		dc2.Clear();
		// Creation du memory DC
		wxMemoryDC temp_dc2;
		// Cr�ation d'un mask � partir d'une bitmap noire et blanche d�finie dans le LayerOnSelection().
		//m_super_mask = new wxMask(m_xslice_bmpmask,wxColour(0,0,0));
		// Association du mask avec l'image (initiale) sur laquelle porte le travail.
		//m_xslice_bmp.SetMask(m_super_mask);
		// Association de l'image (initiale) sur laquelle porte le travail au memory DC.
		temp_dc2.SelectObject(m_xslice_bmp);
		if (&m_xslice_bmplayer && m_xslice_bmplayer.Ok() && !m_owner->OptionFrame->m_affich_classif_cb->GetValue())
		{
			wxBufferedDC super_dc2(&dc2, m_xslice_bmp);
			super_dc2.Blit(0, 0,client_w, client_h,&super_dc2, 0, 0);
		}
		else if (&m_xslice_bmplayer && m_xslice_bmplayer.Ok())
		{
			//wxBufferedDC super_dc2(&dc2, m_xslice_bmplayer);
			//super_dc2.Blit(0, 0, client_w, client_h, &super_dc2, 0, 0);
			wxBufferedDC super_dc2(&dc2, m_xslice_bmplayer);
			//wxBufferedPaintDC super_dc2(this, m_xslice_bmplayer);
			//super_dc2.Blit(-rate_x*x_ini, -rate_y*y_ini, client_w, client_h, &temp_dc2, -rate_x*x_ini, -rate_y*y_ini, wxCOPY, true);		
			//wxBufferedDC super_dc3(&dc2, m_xslice_bmplayer);
			super_dc2.Blit(0, 0, client_w, client_h, &temp_dc2, 0, 0, wxCOPY, true);
			//super_dc2.Blit(0 ,0 ,w, h,&super_dc2,rate_x*x_ini, rate_y*y_ini);
			//super_dc3.Blit(0, 0, client_w, client_h, &super_dc2, 0, 0, wxCOPY, true);
		}

		if(m_scaling_x1!=-1&&m_scaling){
		  int _x0 = m_scaling_x0*m_zoom;
		  int _y0 = m_scaling_y0*m_zoom;
		  int _x1 = m_scaling_x1*m_zoom;
		  int _y1 = m_scaling_y1*m_zoom;
		  wxIcon cursorIcon;
		  wxPen _pen(wxColour(255,100,100),2);
 		  dc2.SetPen(_pen);
 		  dc2.DrawLine(_x0,_y0,_x1,_y1);
		  if(cursorIcon.LoadFile(wxString((char*)_cursorPath, wxConvUTF8),wxBITMAP_TYPE_GIF)){
		    dc2.DrawIcon(cursorIcon,_x0-(cursorIcon.GetWidth()/2),_y0-(cursorIcon.GetHeight()/2));
		    dc2.DrawIcon(cursorIcon,_x1-(cursorIcon.GetWidth()/2),_y1-(cursorIcon.GetHeight()/2));
		  }
		}
		else if(m_scaling_x0!=-1&&m_scaling){
		  wxIcon cursorIcon;
		  int _x0 = m_scaling_x0*m_zoom;
		  int _y0 = m_scaling_y0*m_zoom;
		  if(cursorIcon.LoadFile(wxString((char*)_cursorPath,wxConvUTF8),wxBITMAP_TYPE_GIF)){
		    dc2.DrawIcon(cursorIcon,_x0-(cursorIcon.GetWidth()/2),_y0-(cursorIcon.GetHeight()/2));
		  }

		}

		//dc2.DrawBitmap(m_xslice_bmplayer, (int)((float)rate_x*(float)x_ini), (int)((float)rate_y*(float)y_ini) );
	}
	else
	{
		resolveChanges();

		wxStopWatch sw;

		getSliceImage();

		wxLogMessage(wxT("Image slicing : %ldms "),sw.Time());
		//wxMessageBox("OK");

		sw.Start();
		getSliceBase();
		wxLogMessage(wxT("Base slicing : %ldms "),sw.Time());
		//	wxMessageBox("OK");

		sw.Start();
		//if (m_owner->OptionFrame->m_scale_slider->GetValue()*1./m_owner->OptionFrame->m_scale_slider->GetMax() == 0.4)
		//if ( m_owner->OptionFrame->m_scale_slider->GetValue() == 4 )
		//{
		ImageRam* sliceSec = getGoodSliceSection();
		if (sliceSec)
		{
			ImageRamT<int>* iml = dynamic_cast<ImageRamT<int>*>(m_xslice_lab);
			ImageRamT<int>* imt = dynamic_cast<ImageRamT<int>*>(sliceSec);
			ImageRamT<int>::iterator itlab=iml->begin();
			ImageRamT<int>::iterator ittest=imt->begin();
			m_xslice_lab->resize(sliceSec->size());
			for (;ittest!=imt->end();++ittest)
			{
				*itlab = *ittest;
				++itlab;
			}
		}
		else
		{
			getSliceSection();
		}
		wxLogMessage(wxT("Section : %ldms "),sw.Time());
		//	wxMessageBox("OK");

		sw.Start();
		getCurrentSetsBaseMapping();
		getSliceSets();
		wxLogMessage(wxT("Sets : %ldms "),sw.Time());
		//	wxMessageBox("OK");*/


		sw.Start();
		getSliceBitmap();
		wxLogMessage(wxT("Bitmaps : %ldms "),sw.Time());

		/*	getSlicePoints();
		p_mem_dc.DrawBitmap( m_xslice_bmp, 0, 0 );
		p_mem_dc.SelectObject(m_xslice_bmp);
		Refresh(false);
		wxLogMessage("REFRESH : %ldms ",sw.Time());*/
		//	}
		//sw.Start();
		//if (m_bShouldDraw)
		//{
		wxClientDC dc ( this );
		//wxPaintDC dc( this );
		//PrepareDC( dc );
		//dc.Clear();


		wxLogMessage(wxT("w:%d , h:%d , client_w:%d , client_h:%d , x_ini:%d , y_ini:%d, rate_x:%d , rate_y:%d"),w,h,client_w,client_h,x_ini,y_ini,rate_x,rate_y);

		// Cr�ation d'un mask � partir d'une bitmap noire et blanche d�finie dans le LayerOnSelection().
		m_super_mask = new wxMask(m_xslice_bmpmask,wxColour(0,0,0));
		// Association du mask avec l'image (initiale) sur laquelle porte le travail.
		m_xslice_bmp.SetMask(m_super_mask);

		// Creation du memory DC
		wxMemoryDC temp_dc;
		// Association de l'image (initiale) sur laquelle porte le travail au memory DC.
		temp_dc.SelectObject(m_xslice_bmp);
		//wxBufferedDC super_dc(&dc, *testbmp);

		// Blit qui permet d'obtenir un effet de calque avec les images contenue dans le MemoryDC et 
		// le BufferedDC.
		//if (m_xslice_bmp.GetWidth() < 100 && m_xslice_bmp.GetHeight() < 100)
		//	super_dc.Blit(0, 0, m_xslice_bmp.GetWidth(), m_xslice_bmp.GetHeight(), &temp_dc, rate_x*x_ini, rate_y*y_ini, wxCOPY, true);
		if (m_owner->OptionFrame->m_affich_classif_cb->GetValue() && m_bActive) 
		{
			// Cr�ation d'un BufferedDC pour �viter les effets de clignotement de l'image, les op�rations 
			// se font en m�moire et l'affichage ne se fait qu'� la destruction du bufferedDC.
			wxBufferedDC super_dc(&dc, m_xslice_bmplayer);
			//wxBufferedPaintDC super_dc(this, m_xslice_bmplayer);
			if (client_w>w || client_h>h)
			{
				//SetScrollbars(1, 1, 50, 50, 0 , 0, false);
				//Scroll(x_ini,y_ini);
				//Scroll(rate_x*x_ini,rate_y*y_ini);
				super_dc.Blit(rate_x*x_ini, rate_y*y_ini, w, h, &temp_dc, rate_x*x_ini, rate_y*y_ini, wxCOPY, true);
				// A UTILISER SI ON VEUT METTRE UNE SCROLLBAR DE VITESSE DE SCROLL SUP A 1 PPU
				/*if (client_w < rate_x*x_ini + w)
				{
					//wxLogMessage("...................................................dsfds");
					//int mod = client_w % vitesse de scroll;
					//super_dc.Blit(0 ,0 ,w-mod, h,&super_dc,rate_x*x_ini, rate_y*y_ini);
					super_dc.Blit(0 ,0 ,w, h,&super_dc,rate_x*x_ini, rate_y*y_ini);
				}
				else
				{*/
				super_dc.Blit(0 ,0 ,w, h,&super_dc,rate_x*x_ini, rate_y*y_ini);
				//}
				m_bShoulScrollEx = true;
				m_shouldScroll = true;
			}
			else
			{
				super_dc.Blit(0, 0, client_w, client_h, &temp_dc, 0, 0, wxCOPY, true);
				/*if (m_bShoulScrollEx)
				{
					wxLogMessage("3333");
					dcClear();
					Refresh();
					m_bShoulScrollEx = false;
				}*/
				m_bShoulScrollEx = false;
			}
			if(m_scaling_x1!=-1&&m_scaling){
			  int _x0 = m_scaling_x0*m_zoom;
			  int _y0 = m_scaling_y0*m_zoom;
			  int _x1 = m_scaling_x1*m_zoom;
			  int _y1 = m_scaling_y1*m_zoom;

			  super_dc.DrawLine(_x0,_y0,_x1,_y1);

			}
			else if(m_scaling_x0!=-1&&m_scaling){
			  int _x0 = m_scaling_x0*m_zoom;
			  int _y0 = m_scaling_y0*m_zoom;
			}

		}
		else if (m_bActive)
		{
			// Cr�ation d'un BufferedDC pour �viter les effets de clignotement de l'image, les op�rations 
			// se font en m�moire et l'affichage ne se fait qu'� la destruction du bufferedDC.
			wxBufferedDC super_dc(&dc, m_xslice_bmp);
			//wxBufferedPaintDC super_dc(this, m_xslice_bmplayer);
			super_dc.Blit(0, 0,w, h,&super_dc,rate_x*x_ini, rate_y*y_ini);
			if(m_scaling_x1!=-1&&m_scaling){
			  int _x0 = m_scaling_x0*m_zoom;
			  int _y0 = m_scaling_y0*m_zoom;
			  int _x1 = m_scaling_x1*m_zoom;
			  int _y1 = m_scaling_y1*m_zoom;

			  super_dc.DrawLine(_x0,_y0,_x1,_y1);
			  
			}
			else if(m_scaling_x0!=-1&&m_scaling){
			  int _x0 = m_scaling_x0*m_zoom;
			  int _y0 = m_scaling_y0*m_zoom;
			}


		}
		//ScrollDC();
		//super_dc.Blit(0, 0, m_xslice_bmp.GetWidth(), m_xslice_bmp.GetHeight(), &temp_dc, 0, 0, wxCOPY, true);
		//else
		//	super_dc.Blit(0, 0, 100, 100, &temp_dc, 0, 0, wxCOPY, true);

		//wxLogMessage(wxString("Drawing : %ldms "),sw.Time());

		//wxClientDC dc2(m_option->m_color_sel);
		//dc2.DrawBitmap(m_xslice_bmplayer,20,20);

	}
	m_xslice_changed  = m_scale_changed = m_set_changed = false;
	m_zoom_changed = m_palette_changed = false;

	if (!m_bShoulScrollEx && (client_w > w || client_h > h))
	{
		m_vsize(1) = m_isize(1)*m_zoom;
		m_vsize(2) = m_isize(2)*m_zoom;
		m_vsize(3) = m_isize(3)*m_zoom;			
		m_image_frame_size.x = m_vsize(1);
		m_image_frame_size.y = m_vsize(2);
		m_has_to_fit=true;
		dcClear();
		SetScrollbars(1, 1, 20, 20, 0 , 0, false);
		Refresh();
		m_bShoulScrollEx = true;
	}
	m_owner->UpdateStatusBarScaling();
}

ImageRam* ScrollWinMedSeg::getGoodSliceSection() 
{
  int m_nlevels = m_owner->m_nlevels;
	// Utiliser des double sinon imprecision trop importante au del� de NB_ENCHANT_SEG > 10.
	double f = m_owner->OptionFrame->m_scale_slider->GetValue()*1./m_owner->OptionFrame->m_scale_slider->GetMax();
	int i_int;
	double i=0.;
	for (;i<=1.;i=i+(1./m_nlevels))
	{
		//if (f >= i-0.05 && f <= i+0.05)
		if (f >= i-(1./(2.*m_nlevels)) && f <= i+(1./(2.*m_nlevels)))
		{
			i_int = i*m_nlevels;
			if (m_vSliceSection)
			{
				if (m_vSliceSection->size() >= i_int && m_vSliceSection->size() != 0)
				{
					/*if (i_int == m_iLastSliceSection)
					{
						m_bShouldDraw = false;
					}
					else
					{*/
					//	m_bShouldDraw = true;
						m_iLastSliceSection = i_int;
					//}
					m_scale = invlogscale(i);
					//m_owner->OptionFrame->OnScaleChange();
					return m_vSliceSection->at(i_int);
				}
				else
				{
					return NULL;
				}
			}
		}
	}
	return NULL;
}


void ScrollWinMedSeg::OnMouseMove(wxMouseEvent &event)
{
	int x_ini,y_ini;
	int rate_x,rate_y;
	// Position de la scrollbar en scrollunit
	GetViewStart(&x_ini, &y_ini);
	// Vitesse de Scroll
	GetScrollPixelsPerUnit(&rate_x,&rate_y);
	Scroll(rate_x*x_ini,rate_y*y_ini);
	wxLogMessage(wxT("CAPASSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS ?"));
}


void ScrollWinMedSeg::getSlicePoints(bool forced) {
	/*if(m_owner->p_is_3D_Window_Created){
	if (m_cur_sets.size()){

	std::set<ImageScaleSets::Set*>::const_iterator it3=m_cur_sets.begin(),fin3=m_cur_sets.end();
	std::vector <lgl::Point4D<float> > Pts;

	for(;it3!=fin3;++it3){
	std::vector<lgl::Point4D<float> > cur_pts = (*it3)->attribute()->thePts;
	for(unsigned int i = 0;i<cur_pts.size();++i){
	Pts.push_back(cur_pts[i]);
	}
	}
	//Display 3D BOUNDING BOX

	float xmin, xmax, ymin, ymax, zmin, zmax;
	xmin = xmax = (Pts[0])(0);
	ymin = ymax = (Pts[0])(1);
	zmin = zmax = (Pts[0])(2);

	for(unsigned int ak = 1;ak<Pts.size();++ak){
	float X = (Pts[ak])(0);
	float Y = (Pts[ak])(1);
	float Z = (Pts[ak])(2);

	if(xmin>X) xmin = X;
	else if (xmax<X) xmax = X;

	if(ymin>Y) ymin = Y;
	else if (ymax<Y) ymax = Y;

	if(zmin>Z) zmin = Z;
	else if (zmax<Z) zmax = Z;

	}

	m_owner->m_3D_Viewer->m_canvas->Display_bbox(xmin, xmax, ymin, ymax, zmin, zmax);

	}
	}
	*/


	// if (!m_scale_changed && !forced) return;

	// IMPORTANT : probleme encore inconnu
	/*std::set<ScaleSets::Set*> Sets;
	m_sxs->getSection(m_scale, Sets);



	if(m_owner->p_is_3D_Window_Created){

	PaletteLUT pal = getPalette();
	std::vector < lgl::Point4D <float> > liste;

	//Nettoyage de la palette et stop de l'affichage 3D
	//		m_owner->m_3D_Viewer->m_canvas->p_info.is_scene_set=false;
	//		m_owner->m_points->Clear();


	//Recherche de la palette optimal
	std::set <ScaleSets::Set*>::const_iterator it=Sets.begin(),fin=Sets.end();

	//		liste = (*it)->attribute()->thePts;

	// NE PAS DECOMMENTER !!!
	/*
	float min, max;

	if (liste.size())
	{
	min = max = liste[0](0);
	}
	else{ min = 1e30; max = -1e30; }

	for(;it!=fin;++it){
	liste = (*it)->attribute()->thePts;
	if (liste.size())
	{
	if(min>(*liste.begin())(0) ){
	min=(*liste.begin())(0);
	}
	if(max<(*liste.begin())(0) ){
	max=(*liste.begin())(0);
	}
	}
	}*/
	//lglLOG("min max : "<< min <<" "<<max<<std::endl);
	//pal.setRandomPalette(min,max);
	/*pal.setRandomPalette(0, liste.size());

	it=Sets.begin();
	fin=Sets.end();

	int cptr=0;
	for(;it!=fin;++it,++cptr){
	//			liste = (*it)->attribute()->thePts;
	if (liste.size())
	{
	//RGBpoint p = pal((*liste.begin())(0));
	RGBpoint p = pal(cptr);
	std::vector <lgl::Point4D <float> >::const_iterator it2=liste.begin(), fin2=liste.end();
	for(;it2!=fin2;++it2){
	//					m_owner->m_points->FastAddPoint((*it2)(0),(*it2)(1),(*it2)(2),((float) p.r)/255,((float) p.g)/255,((float) p.b)/255,1.0f);
	}
	}
	}

	//		m_owner->m_points->Pack();
	//		m_owner->m_3D_Viewer->m_canvas->p_info.is_scene_set=true;
	//		m_owner->m_3D_Viewer->m_canvas->Refresh(false);
	}*/
}







lgl::F32 ScrollWinMedSeg::getCurrentScale()
{
	return m_scale;
}
//=======================================================================================

//======================================================================================
// VIEW UPDATES
//=======================================================================================

//=======================================================================================
void ScrollWinMedSeg::InitLayer()
{
	InitLayersOnSelectedRegions( *m_xslice_img, *m_xslice_lab, m_xslice_mask, m_xslice_layer, m_map_image);
}


void ScrollWinMedSeg::getSliceImage() 
{
	if (!m_comp_ximg) return;
	//if (m_owner->OptionFrame->m_image_style_radio->GetSelection()==0) {
	// zero image
	//m_xslice_img->resize( m_image->size() );
	//m_xslice_img->mset(0);
	//}else {
	if (m_xslice_img) delete m_xslice_img;
	//m_xslice_img = m_image->clone(); //probl�me avec clone, seule la moiti� de l'image est copi�e, pbm li� au type ?
	m_xslice_img = m_image;

	/*ImageRGB tmp;
	lgl::PaletteId palZ;
	ImageToImageRGB ( *m_image, tmp, palZ, m_zoom );
	tmp.SaveFile("theImg.tif");
	lglLOG("theImg.tif sauvegardee"<<std::endl);*/
	//}

}
//=======================================================================================


//=======================================================================================
void ScrollWinMedSeg::getSliceBase() {
	if (!m_comp_xbase) return;

	m_xslice_base = &m_sxs->getBaseImage();

}
//=======================================================================================

//=======================================================================================
void ScrollWinMedSeg::getSliceSection() {
	if (!m_comp_xsec) return;
	m_sxs->getSection( *m_xslice_base , getCurrentScale(), m_xslice_lab);

}
//=======================================================================================


//=======================================================================================
void ScrollWinMedSeg::getCurrentSetsBaseMapping() {
	if (!m_set_changed) return;

	ScaleSets::Set* set = (*m_cur_sets.begin());
	wxLogMessage(wxT("... base mapping %d "),set->index());

	// get base 
	std::vector<ScaleSets::Set*> sbase;
	getBase(set,sbase);
	// mapping 
	m_cur_sets_base_map.clear();
	//std::vector<ScaleSets::Set*>::iterator i;
	//for (i=sbase.begin();i!=sbase.end();++i) {
	std::set<ScaleSets::Set*>::iterator i;
	for (i=m_cur_sets.begin();i!=m_cur_sets.end();++i) {

		m_cur_sets_base_map.insert(std::pair<int,int>((*i)->index(),1));
		//		wxLogMessage("base mapping : %d -> 1",(*i)->index());
	}
	wxLogMessage(wxT("base mapping : card = %d"),m_cur_sets_base_map.size());

	/**/
	wxString mess;
	mess.Printf(wxT("%d"),set->index());
	m_owner->SetStatusText(mess,2);
	mess.Printf(wxT("%d"),m_cur_sets_base_map.size());
	m_owner->SetStatusText(mess,3);
	mess.Printf(wxT("%f"),set->scaleOfAppearance());
	m_owner->SetStatusText(mess,4);
	mess.Printf(wxT("%f"),set->scaleOfDisappearance());
	m_owner->SetStatusText(mess,5);
	if (set->scaleOfAppearance()!=0) {
	  mess.Printf(wxT("%f"),set->persistence());
		m_owner->SetStatusText(mess,6);
	}
	else {
	  mess.Printf(wxT("+oo"));
		m_owner->SetStatusText(mess,6);
	}
	/**/
}
//=======================================================================================


//=======================================================================================
void ScrollWinMedSeg::getSliceSets() {
	if (!m_comp_xset) return;

	lgl::map ( *m_xslice_base, m_cur_sets_base_map, m_xslice_set, 0 );

}
//=======================================================================================

//=======================================================================================
void ScrollWinMedSeg::getSliceBitmap() {
	//  lglLOG("m_xslice_img.size() "<<m_xslice_img->size()<<std::endl);
	if (!m_comp_xbmp) return;
	// Mean values image requested
	//if (m_owner->OptionFrame->m_image_style_radio->GetSelection()==2) {
	//	averageOnRegions(*m_xslice_img,*m_xslice_lab,m_xslice_img);
	//}

	//wxStopWatch sw;
	//sw.Start();


	// contours
	ImageRGB tmp;
	lgl::PaletteId pal;
	if (m_xslice_img->channels()==1) {
		if (m_owner->OptionFrame->m_section_style_radio->GetSelection()==0) {
			MonoChannelImageToImageRGB ( *m_xslice_img, tmp, *m_color_map, m_zoom ) ;
		}
		else if (m_owner->OptionFrame->m_section_style_radio->GetSelection()==1) {
			MonoChannelImageAndLabelsToImageRGB ( *m_xslice_img, *m_xslice_lab, tmp, *m_color_map, m_contours_color , m_zoom );
		}
		else if (m_owner->OptionFrame->m_section_style_radio->GetSelection()==2) {
			MonoChannelImageToImageRGB ( *m_xslice_lab, tmp, m_palette_regions, m_zoom ) ;
		}
	}
	else {
		//WE ASSUME WE USE HERE MULTICHANNEL IMAGES
		if (m_owner->OptionFrame->m_section_style_radio->GetSelection()==0) {
			ImageToImageRGBmed ( *m_xslice_img, tmp, pal, m_zoom ) ;
		}
		else if (m_owner->OptionFrame->m_section_style_radio->GetSelection()==1) {
			ImageAndLabelsToImageRGBmed ( *m_xslice_img, *m_xslice_lab, tmp, pal, m_contours_color , m_zoom );
			//ImageAndLabelsToImageRGBmed ( *m_xslice_img, *m_xslice_img, tmp, pal, m_contours_color , m_zoom );
		}
		else if (m_owner->OptionFrame->m_section_style_radio->GetSelection()==2) {
			ImageToImageRGBmed ( *m_xslice_lab, tmp, m_palette_regions, m_zoom ) ;
		}

	}
	// set
	//if (m_owner->OptionFrame->m_set_draw_check->IsChecked()) {
	//	drawContours( *m_xslice_set, m_cur_set_color, m_zoom, tmp );
	//}


	//wxLogMessage("Bitmaps1 : %ldms ",sw.Time());

	//sw.Start();


	// bmp
	//wxBitmap bmp(400, 400);
	wxMemoryDC dc;
	dc.SelectObject(m_xslice_bmp);
	dc.SetBackground(*wxBLACK_BRUSH);
	dc.Clear();

	//wxLogMessage("Bitmaps2 : %ldms ",sw.Time());
	//sw.Start();

	//dc.SetClippingRegion(*this);
	/*
	dc.SetBackground(*wxWHITE_BRUSH);
	dc.Clear();
	dc.SelectObject(wxNullBitmap);
	*/
	//m_xslice_bmp = ((wxRegion)tmp).ConvertToBitmap();//.ConvertToBitmap();

	// Transformation d'une imageRam en imageRGB (d�rivant de wxImage) afin de permettre de transformer
	// en wxBitmap pour l'affichage.
	ImageRGB tmp_mask;
	ImageRGB tmp_layer;
	//ImageToImageRGB ( *m_xslice_mask, tmp_mask, pal, m_zoom ) ;
	//ImageToImageRGB ( *m_xslice_layer, tmp_layer, pal, m_zoom ) ;
	ImageToImageRGBmed ( *m_xslice_mask, tmp_mask, pal, m_zoom ) ;
	ImageToImageRGBmed ( *m_xslice_layer, tmp_layer, pal, m_zoom ) ;
	//Sauvegarde de l'image.
	if (m_bShould_save)
	{
		//tmp_layer.SaveFile(m_sExFilename,wxBITMAP_TYPE_BMP);
		//tmp_layer.SaveFile("theImg.jpg",wxBITMAP_TYPE_JPEG);
		//tmp_layer.SaveFile("theImg.png",wxBITMAP_TYPE_PNG);
		//tmp_layer.SaveFile("theImg.pcx",wxBITMAP_TYPE_PCX);
		//tmp_layer.SaveFile("theImg.pnm",wxBITMAP_TYPE_PNM);
		tmp_layer.SaveFile(m_sExFilename,wxBITMAP_TYPE_TIF);
		//tmp_layer.SaveFile("theImg.xpm",wxBITMAP_TYPE_XPM);
		m_bShould_save = false;
	}

	//wxLogMessage("Bitmaps3 : %ldms ",sw.Time());
	//sw.Start();

	m_xslice_bmp = wxBitmap(tmp);
	m_xslice_bmpmask = wxBitmap(tmp_mask);
	m_xslice_bmplayer = wxBitmap(tmp_layer);

	//wxLogMessage("Bitmaps4 : %ldms ",sw.Time());
	//sw.Start();
	/*
	Filename filename("m_xslice_img");
	FILE* f_src = fopen(filename.c_str(),"wb");
	m_xslice_img->rawWrite(f_src);
	*/
	lglLOG("m_xslice_bmp.size() "<<tmp.size()<<std::endl);
	//wxLogMessage("Bitmaps5 : %ldms ",sw.Time());

}
//=======================================================================================


//=======================================================================================
void ScrollWinMedSeg::OnPaint(wxPaintEvent& ev ) 
{
	//m_owner->OptionFrame->m_scale_slider->SetFocus();
	wxPaintDC dc( this );
	PrepareDC(dc);
	dc.Clear();
	//wxMemoryDC p_mem_dc;

	int w,h;
	int client_w,client_h;

	int x_ini,y_ini;
	int rate_x,rate_y;

	lglLOG("onPaint"<<std::endl);
	if(m_owner->m_scale_set_loaded){
	  if(!m_bActive) return;

		if(m_has_to_fit){
			//lglLOG("in m_has_to_fit"<<std::endl);
			GetClientSize(&w,&h);
			if(m_image_frame_size.x>w|| m_image_frame_size.y>h){
				SetVirtualSizeHints(m_image_frame_size.x,m_image_frame_size.y);
				SetVirtualSize(m_image_frame_size.x,m_image_frame_size.y);
			}else{
				SetVirtualSizeHints(w,h);
				SetVirtualSize(w,h);
			}
			m_has_to_fit=false;
		}

		GetVirtualSize(&client_w,&client_h);

		GetViewStart(&x_ini, &y_ini);
		GetScrollPixelsPerUnit(&rate_x,&rate_y);
		lglLOG("BLIT"<<std::endl);
		if(client_w>w || client_h>h)
		{
			//dc.Blit(rate_x*x_ini,rate_y*y_ini,client_w,client_h,&p_mem_dc,rate_x*x_ini,rate_y*y_ini);
			dc.Blit(0,0,client_w,client_h,&p_mem_dc,rate_x*x_ini,rate_y*y_ini);
			//dc.Blit(0,0,client_w,client_h,&p_mem_dc,0,0);
		}else{
			//dc.Blit(rate_x*x_ini,rate_y*y_ini,w,h,&p_mem_dc,rate_x*x_ini,rate_y*y_ini);
			dc.Blit(0,0,w,h,&p_mem_dc,rate_x*x_ini,rate_y*y_ini);
			//dc.Blit(0,0,w,h,&p_mem_dc,0,0);
		}
		updateView();
	}else{
	  wxBitmap appLogo;
	  dc.SetBrush(wxColour(*m_owner->background_color));
	  dc.SetPen(wxColour(*m_owner->background_color));
	  dc.DrawRectangle(0,0,3000,3000);
	  char *_pathLogo = new char[256];_pathLogo[0]='\0';
#ifndef __WINDOWS__
	  strcat((char*)_pathLogo,getenv("HOME"));
	  _pathLogo = strcat((char*)_pathLogo,"/.seascape/logo_seascape.png");
	  cout<<"logo path "<<_pathLogo<<endl;
#else
	  _pathLogo = "data/logo_seascape.png";
	  cout<<"logo path "<<_pathLogo<<endl;
#endif
	  appLogo.LoadFile(wxString((char*)_pathLogo,wxConvUTF8), wxBITMAP_TYPE_PNG);
	  dc.DrawBitmap(appLogo,150,150);
	}
}
//=======================================================================================


//======================================================================================= 	
void ScrollWinMedSeg::resolveChanges() 
{
	m_comp_ximg = m_xslice_changed; //&& (m_owner->OptionFrame->m_image_style_radio->GetSelection()>0);
	m_comp_xsec = ( m_xslice_changed || m_scale_changed ) && ( (m_owner->OptionFrame->m_section_style_radio->GetSelection()>0) ); //|| (m_owner->OptionFrame->m_image_style_radio->GetSelection()==2) );
	m_comp_xset = /*(m_owner->OptionFrame->m_set_draw_check->IsChecked()) &&*/ ( m_xslice_changed || m_set_changed );
	m_comp_xbase = m_comp_xsec || m_comp_xset ;
	m_comp_xbmp = ( m_xslice_changed || m_scale_changed || m_set_changed || m_zoom_changed || m_palette_changed );

}	






BEGIN_EVENT_TABLE( LeftWindow, wxWindow )  
EVT_COMBOBOX ( idZoomCombo, LeftWindow::OnZoomChange ) 
EVT_COMMAND_SCROLL_CHANGED ( idScaleSlider, LeftWindow::OnScaleSlider ) 
EVT_TEXT_ENTER ( idScaleCtrl, LeftWindow::OnScaleCtrl ) 
EVT_RADIOBOX ( idImageStyleRadio, LeftWindow::OnImageStyleChange ) 
EVT_RADIOBOX ( idSectionStyleRadio, LeftWindow::OnSectionStyleChange ) 
EVT_COMBOBOX ( idSectionColorCombo, LeftWindow::OnSectionColorChange ) 
EVT_CHECKBOX ( idSetDrawCheck, LeftWindow::OnSetDrawCheck )
EVT_CHECKBOX ( idAffichClassifCb, LeftWindow::OnAffichClassifCheck ) 
EVT_COMBOBOX ( idSetColorCombo, LeftWindow::OnSetColorChange ) 
EVT_TEXT_ENTER ( idSetPersistenceCtrl, LeftWindow::OnSetPersistenceCtrl )
//EVT_BUTTON ( idButtonAddClass, LeftWindow::OnClickButtons )
EVT_COMBOBOX ( idComboSel, LeftWindow::OnChangeSiteOrGroup )
EVT_TEXT ( idImageSizes1, LeftWindow::OnChangeImageSizes)
EVT_TEXT( idImageSizes2, LeftWindow::OnChangeImageSizes)
EVT_TEXT( idImagePixelsCm, LeftWindow::OnChangeImagePixelsCm )
EVT_BUTTON( idButtonShowTable, LeftWindow::OnShowTable)
EVT_BUTTON( idButtonShowNavigationWindow, LeftWindow::OnShowNavigationWindow)
EVT_BUTTON( idButtonSegmentSize, LeftWindow::OnButtonSegmentSize )
END_EVENT_TABLE() 

LeftWindow::LeftWindow(wxWindow *parent, ScrollWinMedSeg *draw_canvas,InterfaceMedSeg *owner):wxWindow(parent, -1, wxDefaultPosition, wxSize(240, 600), 0 , wxT("Left Windows"))
{
	ImageFrame=draw_canvas;
	m_owner=owner;
	m_image_size_propagate=true;
	m_image_ready = false;
	m_navigationWindow=NULL;
	InitInterface();

	//On d�sactive tout
	//m_image_style_radio->Enable(false);
	m_section_style_radio->Enable(false);
	m_image_width->Enable(false);
	m_image_height->Enable(false);
	//m_set_draw_check->Enable(false);
	m_zoom_combo->Enable(false);
//	m_section_color_combo->Enable(false);
	m_scale_slider->Enable(false);
	m_scale_ctrl->Enable(false);
	m_image_pixels_cm->Enable(false);
	//m_
	//m_set_color_combo->Enable(false);
	//m_color_sel->Enable(false);
	//m_set_persistence_ctrl->Enable(false);
	m_class_sel->Enable(false);
	// m_site_sel->Enable(false);// PREVIOUS MULTILOCATION VERSION
	m_group_sel->Enable(false);
	m_bm_contour_col->Enable(false);
//	m_but_new_class->Enable(false);
	m_affich_classif_cb->Enable(false);
	//		m_object_list->Enable(true);

	buttonShowTable->Enable(false);
	buttonShowNavigationWindow->Enable(false);
	buttonSegmentSize->Enable(false);
}

LeftWindow::~LeftWindow(){
	/*
	if (ImageFrame->m_delete_sxs) delete ImageFrame->m_sxs;
	if (ImageFrame->m_color_map) delete ImageFrame->m_color_map;
	if (ImageFrame->m_xslice_lab) delete ImageFrame->m_xslice_lab;
	if (ImageFrame->m_xslice_set) delete ImageFrame->m_xslice_set;
	*/
}


void LeftWindow::OnShowTable(wxCommandEvent &ev){
  m_owner->ShowTableWindow();
}

void LeftWindow::OnShowNavigationWindow(wxCommandEvent &ev){
  languageDealer *_languageDealer = m_owner->_languageDealer;
  String currentLanguage = m_owner->currentLanguage;

  if(m_navigationWindow==NULL){
    m_navigationWindow = new NavigationWindow(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00090").c_str(),wxConvUTF8));
  }
  m_navigationWindow->Show(!m_navigationWindow->IsShown());
}

// AAE: while change image sizes on text ctrls
void LeftWindow::OnChangeImageSizes( wxCommandEvent &event )
{
  double val, m_npix;
  // semaphor to avoid event recursive propagation
  if(m_image_size_propagate&&m_image_ready){
    if(!m_image_width->GetValue().ToDouble(&val)) val=1;
    m_npix = m_owner->ImageFrame->m_isize(1);
    m_owner->m_pixel_side_length = val/m_npix;
    m_owner->UpdateStatusBarScaling();
    wxString a; a<<(m_npix/val);
    m_image_size_propagate=false;
    m_image_pixels_cm->SetValue(a);
  }
  else
    m_image_size_propagate=true;
}

void LeftWindow::OnChangeImagePixelsCm(wxCommandEvent &ev){
  double val,x,y;
  // semaphor to avoid event recursive propagation
  if(m_image_size_propagate&&m_image_ready){
    if(!m_image_pixels_cm->GetValue().ToDouble(&val)) val = 1;
    x = m_owner->ImageFrame->m_isize(1);
    y = m_owner->ImageFrame->m_isize(2);
    m_owner->m_pixel_side_length = 1/val;
    m_owner->UpdateStatusBarScaling();
    wxString a; a<<(x/val);
    wxString b; b<<(y/val);
    m_image_width->SetValue(a);
    m_image_size_propagate=false;
    m_image_height->SetValue(b);}
  else
    m_image_size_propagate=true;
}

void LeftWindow::UpdateImageSizeCtrls(){
  m_image_size_propagate=false;
  double val = m_owner->m_pixel_side_length,x,y;
  x = m_owner->ImageFrame->m_isize(1);
  y = m_owner->ImageFrame->m_isize(2);
  wxString a; a<<(x*val);
  wxString b; b<<(y*val);
  m_image_size_propagate=false;
  m_image_width->SetValue(a);
  m_image_size_propagate=false;
  m_image_height->SetValue(b);
  m_image_size_propagate=false;
  wxString c; c<<(1/val);
  m_image_pixels_cm->SetValue(c);
  m_image_size_propagate=true;
}

void LeftWindow::OnChangeSiteOrGroup( wxCommandEvent &event )
{
	ImageMapping *mapping_im = ImageFrame->getImageMapping();
	m_class_sel->ReloadComboBox();
	wxLogMessage(wxT("ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd"));
}

void LeftWindow::OnClickButtons( wxCommandEvent &event )
{
	switch (event.GetId())
	{
	case idButtonAddClass:
		{
			m_owner->OnMenuAdd(event);
			break;
		}
	}
}


//======================================================================================= 	
lgl::BOOL LeftWindow::InitBrowser() 
{
  wxLogMessage(wxT("InitBrowser"));

	//On r�active tout
	//m_image_style_radio->Enable(true);
	m_section_style_radio->Enable(true);
	m_image_width->Enable(true);
	m_image_height->Enable(true);
	m_image_ready = true;
	//m_set_draw_check->Enable(true);
	m_zoom_combo->Enable(true);
//	m_section_color_combo->Enable(true);
	m_scale_slider->Enable(true);
	m_scale_ctrl->Enable(true);
	//m_set_color_combo->Enable(true);
	//m_color_sel->Enable(true);
	//m_set_persistence_ctrl->Enable(true);
	m_class_sel->Enable(true);
//	m_site_sel->Enable(true);// PREVIOUS MULTILOCATION VERSION
	m_group_sel->Enable(true);
	m_bm_contour_col->Enable(true);
	//m_but_new_class->Enable(true);
	m_affich_classif_cb->Enable(true);
	//	m_object_list->Enable(true);

	m_image_pixels_cm->Enable(true);
	buttonShowNavigationWindow->Enable(true);
	buttonShowTable->Enable(true);
	buttonSegmentSize->Enable(true);

	// maximum scale
	ImageFrame->m_scale_max = ImageFrame->m_sxs->getRoot()->scaleOfAppearance()*1.00001;
	// minimum scale (for log scale) 
	ImageFrame->m_scale_min = ImageFrame->m_scale_max / pow(10.0,6.0); //MD pow(10,10)
	/// The maximum value of logscale
	ImageFrame->m_logscale_max = 1; 
	ImageFrame->m_logscale_max = ImageFrame->logscale(ImageFrame->m_scale_max);
	// initial scale 
	ImageFrame->m_scale = ImageFrame->m_scale_min ;

	wxLogMessage(wxT("scale min     = %f"),ImageFrame->m_scale_min); 
	wxLogMessage(wxT("scale max     = %f"),ImageFrame->m_scale_max); 
	wxLogMessage(wxT("log scale max = %f"),ImageFrame->m_logscale_max); 
	wxLogMessage(wxT("scale         = %f"),ImageFrame->m_scale); 

	// palette for regions drawing with random colors
	ImageFrame->m_palette_regions.setRandomPalette(0,ImageFrame->m_sxs->getRoot()->index());

	// image
	ImageFrame->m_image = &(ImageFrame->m_sxs->getInputImage());
	// allocation of label images by procreation
	ImageFrame->m_xslice_lab = ImageFrame->m_sxs->getBaseImage().procreate();
	ImageFrame->m_xslice_base = ImageFrame->m_sxs->getBaseImage().procreate();
	ImageFrame->m_xslice_set = ImageFrame->m_sxs->getBaseImage().procreate();

	// allocation of slices by procreation of m_image
	ImageFrame->m_xslice_img = ImageFrame->m_image->procreate();
	ImageFrame->m_xslice_img->channels();
	ImageFrame->m_xslice_mask = ImageFrame->m_image->procreate();
	ImageFrame->m_xslice_layer = ImageFrame->m_image->procreate();

	ImageRGB tmp;
	lgl::PaletteId palZ;
	ImageToImageRGB ( *(ImageFrame->m_xslice_img), tmp, palZ, ImageFrame->m_zoom );
	//tmp.SaveFile("theImg0.tif");
	//lglLOG("theImg0.tif sauvegardee"<<std::endl)

	ImageFrame->m_isize = ImageFrame->m_image->size();
	ImageFrame->m_zoom = 2;

	ImageFrame->m_slice_position = lgl::ImageSite(0, ImageFrame->m_isize(1)/2, ImageFrame->m_isize(2)/2, ImageFrame->m_isize(3)/2);

	wxLogMessage(wxT("m_isize(1) : %f"),ImageFrame->m_isize(1));
	wxLogMessage(wxT("m_isize(2) : %f"),ImageFrame->m_isize(2));

	ImageFrame->m_mouse_in_image = false;

	// initial set = Root
	//	m_cur_sets.insert(m_sxs->getRoot());
	ImageFrame->m_cur_sets.insert(ImageFrame->m_sxs->Sets().front());

	ImageFrame->m_color_maps[0] = wxT("gray");
	ImageFrame->m_color_maps[1] = wxT("red-blue");
	ImageFrame->m_color_maps[2] = wxT("black-red-white");
	ImageFrame->m_color_maps[3] = wxT("black-blue-white");
	ImageFrame->m_color_maps[4] = wxT("black-yellow-white");
	ImageFrame->m_color_maps[5] = wxT("blue-white");
	ImageFrame->m_color_maps[6] = wxT("black body");
	ImageFrame->m_color_maps[7] = wxT("spectrum");

	ImageFrame->m_color_maps_id[0] = PaletteMultilinear::gray;
	ImageFrame->m_color_maps_id[1] = PaletteMultilinear::red_blue;
	ImageFrame->m_color_maps_id[2] = PaletteMultilinear::black_red_white;
	ImageFrame->m_color_maps_id[3] = PaletteMultilinear::black_blue_white;
	ImageFrame->m_color_maps_id[4] = PaletteMultilinear::black_yellow_white;
	ImageFrame->m_color_maps_id[5] = PaletteMultilinear::blue_white;
	ImageFrame->m_color_maps_id[6] = PaletteMultilinear::black_body;
	ImageFrame->m_color_maps_id[7] = PaletteMultilinear::spectrum;

	ImageFrame->m_set_color[0] = RGBpoint::WHITE();
	ImageFrame->m_set_color[1] = RGBpoint::BLACK();
	ImageFrame->m_set_color[2] = RGBpoint::RED();
	ImageFrame->m_set_color[3] = RGBpoint::GREEN();
	ImageFrame->m_set_color[4] = RGBpoint::BLUE();
	ImageFrame->m_set_color[5] = RGBpoint::YELLOW();

	ImageFrame->m_contours_color = ImageFrame->m_set_color[2];
	ImageFrame->m_cur_set_color = ImageFrame->m_set_color[3];

	ImageFrame->m_set_persistence = (F32) 1.2;
	wxString persini;
	persini.Printf(wxT("%f"),ImageFrame->m_set_persistence);
	//m_set_persistence_ctrl->SetValue(persini);

	//=====================================================================================================
	// Color map
	ImageFrame->m_color_map = new PaletteMultilinear(PaletteMultilinear::gray,false,ImageFrame->m_value_min,ImageFrame->m_value_max);
	//=====================================================================================================

	m_zoom_combo->SetSelection(ImageFrame->m_zoom-1);
	Fit();


	ImageFrame->m_mouse_in_image = false;

	ImageFrame->m_xslice_changed  = ImageFrame->m_scale_changed = 
		ImageFrame->m_set_changed = ImageFrame->m_zoom_changed = 
		ImageFrame->m_palette_changed = true;

	wxLogMessage(wxT("Browser init ok"));
	if(ImageFrame->m_image->channels()==1){
	  cout<<"hi"<<endl;
	};

	return true;
}

//=======================================================================================
void LeftWindow::InitInterface(){
  // background color

  wxColour *backGrd = m_owner->background_color;
  this->SetBackgroundColour(*backGrd);
	
	// language dealer instantiation
	languageDealer *m_languageDealer;
	m_languageDealer = m_owner->_languageDealer;
	String currentLanguage = m_owner->currentLanguage;
	// its topsizer
	wxBoxSizer* ctrlsizer = new wxBoxSizer( wxVERTICAL ); 

	//=====================================================================================================
	// Image box 
	wxBoxSizer* test = new wxBoxSizer( wxVERTICAL ); 
	

	viewsizer = new wxStaticBoxSizer( new wxStaticBox(this,-1,wxString(m_languageDealer->getStringById(currentLanguage,"00049").c_str(),wxConvUTF8)), wxVERTICAL );
	// Display style
	/*wxString istyle[2];
	istyle[0] = "invisible";
	istyle[1] = "image";

	m_image_style_radio = new wxRadioBox ( this, idImageStyleRadio, "Display style",wxDefaultPosition,wxDefaultSize,
	2,istyle,1,wxRA_SPECIFY_ROWS);
	m_image_style_radio->SetSelection(1);
	viewsizer->Add(m_image_style_radio,0,wxALIGN_CENTRE|wxBOTTOM|wxGROW,10) ;*/

	// Zoom
	wxBoxSizer* zoomsizer = new wxBoxSizer( wxHORIZONTAL ); 
	wxString zchoice[6];
	zchoice[0] = wxT("x1");
	zchoice[1] = wxT("x2");
	zchoice[2] = wxT("x3");
	zchoice[3] = wxT("x4");
	zchoice[4] = wxT("x5");
	zchoice[5] = wxT("x6");
	//m_zoom_combo = new wxComboBox( this, idZoomCombo, "", wxDefaultPosition, 
	//	wxDefaultSize, 6, zchoice );
	m_zoom_combo = new wxComboBox( this, idZoomCombo, wxT(""), wxDefaultPosition, 
		wxSize(45,18), 6, zchoice );
	m_zoom_combo->SetSelection(1);
	zoomsizer->Add( new wxStaticText(this,-1,wxT("")),0,wxALIGN_CENTER|wxLEFT,10);
	zoomsizer->Add( m_zoom_combo,0,wxALIGN_CENTRE|wxLEFT,5);
	viewsizer->Add( zoomsizer,0,wxALIGN_CENTRE|wxBOTTOM,10) ;

	test->Add( viewsizer,0,wxALL|wxGROW,10);
	ctrlsizer->Add( test,0,wxGROW); 
	//ctrlsizer->Add( viewsizer,0,wxGROW); 
	//=====================================================================================================

	//=====================================================================================================
	// Section box
	test = new wxBoxSizer( wxVERTICAL ); 
	sectionsizer = new wxStaticBoxSizer( new wxStaticBox(this,-1,wxString(m_languageDealer->getStringById(currentLanguage,"00050").c_str(),wxConvUTF8)), wxVERTICAL );
	// Display style
	wxString sstyle[3];
	sstyle[0] = wxString(m_languageDealer->getStringById(currentLanguage,"00054").c_str(),wxConvUTF8);
	sstyle[1] = wxString(m_languageDealer->getStringById(currentLanguage,"00055").c_str(),wxConvUTF8);

	m_section_style_radio = new wxRadioBox ( this, idSectionStyleRadio, wxT(""),wxDefaultPosition,wxDefaultSize,
		2,sstyle,1,wxRA_SPECIFY_ROWS);
	m_section_style_radio->SetSelection(1);

	sectionsizer->Add(m_section_style_radio,0,wxALIGN_CENTRE|wxBOTTOM,10) ;
	/// Section color 
	wxString scol[6];
	scol[0] = wxT("blanc");
	scol[1] = wxT("noir");
	scol[2] = wxT("rouge");
	scol[3] = wxT("vert");
	scol[4] = wxT("bleu");
	scol[5] = wxT("jaune");

	//m_cd_contour_col = new wxColourDialog(this);
	//sectionsizer->Add(m_cd_contour_col,0,wxALIGN_CENTRE|wxBOTTOM,10);

	wxImage testbitmap(20,20);
	for (int a=0;a<testbitmap.GetWidth();a++)
	{
		for (int b=0;b<testbitmap.GetHeight();b++)
		{
			testbitmap.SetRGB(a,b,255,0,0);
		}
	}
	wxBitmap testbitmap_bmp(testbitmap);

	m_bm_contour_col = new BitmapButtonMedSeg(this,idColorContour,testbitmap_bmp);
	m_bm_contour_col->setImageFrame(ImageFrame);
	sectionsizer->Add(m_bm_contour_col,0,wxALIGN_CENTRE|wxBOTTOM,10);

	//m_section_color_combo = new wxComboBox( this, idSectionColorCombo, "", wxDefaultPosition, 
	//	wxDefaultSize, 6, scol );
	//m_section_color_combo->SetSelection(2);
	//sectionsizer->Add(m_section_color_combo,0,wxALIGN_CENTRE|wxBOTTOM,10) ;
	test->Add( sectionsizer,0,wxALL|wxGROW,10); 

	ctrlsizer->Add( test,0,wxGROW); 
	////=====================================================================================================


	//=====================================================================================================
	// Section box
/*	test = new wxBoxSizer( wxVERTICAL ); 
	wxStaticBoxSizer* classifsizer = new wxStaticBoxSizer( new wxStaticBox(this,-1,"Classification"), wxVERTICAL );

	// Display style
	m_affich_classif_cb = new wxCheckBox ( this, idAffichClassifCb, "afficher",wxDefaultPosition,wxDefaultSize);
	m_affich_classif_cb->SetValue(true);
	classifsizer->Add(m_affich_classif_cb,0,wxALIGN_CENTRE|wxBOTTOM|wxGROW,10) ;
	test->Add( classifsizer,0,wxALIGN_CENTRE|wxALL|wxGROW,10); 
	ctrlsizer->Add( test,0,wxGROW); */
	////=====================================================================================================



	////=====================================================================================================
	//// Scale box
	test = new wxBoxSizer( wxVERTICAL ); 
	scalesizer = new wxStaticBoxSizer( new wxStaticBox(this,-1,wxString(m_languageDealer->getStringById(currentLanguage,"00051").c_str(),wxConvUTF8)), wxVERTICAL );
	m_scale_slider = new wxSlider( this, idScaleSlider,5000,0,10000, wxDefaultPosition, wxSize(200,30),wxSL_SELRANGE/*, wxSL_LABELS  */);
	scalesizer->Add( m_scale_slider,0,wxALIGN_CENTRE|wxGROW|wxBOTTOM,5) ;

	m_scale_ctrl = new wxTextCtrl(this,idScaleCtrl,wxT(""),wxDefaultPosition,wxSize(0,0),wxTE_PROCESS_ENTER|wxTE_CENTRE);
	//scalesizer->Add( m_scale_ctrl,0,wxALIGN_CENTRE) ;
	//test->Add( scalesizer,0,wxGROW); 	
	//ctrlsizer->Add( test,0,wxGROW);
	m_scale_ctrl->Show(false);

	//wxStaticBoxSizer *gauge_sizer = new wxStaticBoxSizer( wxHORIZONTAL, panel, wxString("&wxGauge and wxSlider") );
	//main_sizer->Add( gauge_sizer, 0, wxALL, 5 );
	//wxBoxSizer *sz = new wxBoxSizer( wxVERTICAL );
	//gauge_sizer->Add( sz );
	//m_gauge = new wxGauge( this, wxID_ANY, 100, wxDefaultPosition, wxSize(155, 30), wxGA_HORIZONTAL|wxNO_BORDER );
	//m_gauge->SetBackgroundColour(*wxGREEN);
	//m_gauge->SetForegroundColour(*wxRED);
	//scalesizer->Add( m_gauge,0,wxALIGN_CENTRE|wxGROW|wxBOTTOM,10) ;
	//test->Add( m_gauge, 0, wxALL, 10 );
	//ctrlsizer->Add( test,0,wxGROW);
	/*wxSlider *m_slider = new wxSlider( this, idScaleSlider2, 0, 0, 200,
		wxDefaultPosition, wxSize(155,wxDefaultCoord),
		wxSL_AUTOTICKS | wxSL_LABELS);
	m_slider->SetTickFreq(40, 0);
	scalesizer->Add( m_slider,0,wxALIGN_CENTRE|wxGROW) ;*/

	//m_gaugeVert = new wxGauge( panel, wxID_ANY, 100,
	//	wxDefaultPosition, wxSize(wxDefaultCoord, 90),
	//	wxGA_VERTICAL | wxGA_SMOOTH | wxNO_BORDER );
	//gauge_sizer->Add( m_gaugeVert, 0, wxALL, 10 );
	//scalesizer->Add( m_scale_slider,0,wxALIGN_CENTRE|wxGROW) ;
	test->Add( scalesizer, 0, wxALL|wxGROW, 10 );
	ctrlsizer->Add( test,0,wxGROW);
	////=====================================================================================================

	////=====================================================================================================
	// Set box
	//test = new wxBoxSizer( wxVERTICAL ); 
	//wxStaticBoxSizer* setsizer = new wxStaticBoxSizer( new wxStaticBox(this,-1,"Classes"), wxVERTICAL );
	/*wxBoxSizer* setshowsizer = new wxBoxSizer(wxHORIZONTAL);
	// Draw check
	m_set_draw_check = new wxCheckBox( this, idSetDrawCheck, "Draw set");	
	m_set_draw_check->SetValue(true);
	setshowsizer->Add( m_set_draw_check,0,wxALIGN_CENTER|wxGROW) ;
	// Color
	m_set_color_combo = new wxComboBox( this, idSetColorCombo, "", wxDefaultPosition, wxDefaultSize, 6, scol );
	m_set_color_combo->SetSelection(3);
	setshowsizer->Add(m_set_color_combo,0,wxALIGN_CENTER|wxGROW); //|wxBOTTOM,10) ;

	m_color_sel = new wxComboBox( this, idSetColorCombo, "", wxDefaultPosition, wxDefaultSize, 7, scol, wxCB_READONLY|wxCB_SORT );
	m_color_sel->SetSelection(3);
	setshowsizer->Add(m_color_sel,0,wxALIGN_CENTER|wxGROW); //|wxBOTTOM,10) ;
	wxClientDC dc2(m_color_sel);
	wxBitmap tt(50,50, true);
	dc2.DrawBitmap(tt,0,0);


	setsizer->Add(setshowsizer,0,wxGROW);

	m_set_persistence_ctrl = new wxTextCtrl(this,idSetPersistenceCtrl,"",wxDefaultPosition,wxSize(50,20),wxTE_PROCESS_ENTER|wxTE_CENTRE);
	setsizer->Add(m_set_persistence_ctrl,0,wxCENTER);
	test->Add( setsizer,0,wxGROW);
	ctrlsizer->Add( test,0,wxGROW); */

	//Gestionnaire de chantier

	test = new wxBoxSizer( wxVERTICAL ); 
	setsizer = new wxStaticBoxSizer( new wxStaticBox(this,-1,wxString(m_languageDealer->getStringById(currentLanguage,"00052").c_str(),wxConvUTF8)), wxVERTICAL );

	// Display style
	m_affich_classif_cb = new wxCheckBox ( this, idAffichClassifCb, wxString(m_languageDealer->getStringById(currentLanguage,"00053").c_str(),wxConvUTF8),wxDefaultPosition,wxDefaultSize);
	m_affich_classif_cb->SetValue(true);

	setsizer->Add(m_affich_classif_cb,0,wxALIGN_CENTRE|wxTOP,5) ;

	wxArrayString teststable(1);
	wxArrayString teststable2(1);
	wxArrayString teststable3(1);
	m_group_sel = new wxComboBox(this,idComboSel,wxT(""),wxDefaultPosition,wxSize(120,21), teststable3 , wxCB_READONLY|wxCB_SORT);
	m_class_sel = new ComboClassMedSeg(this,-1,wxT(""),wxDefaultPosition,wxSize(120,21), teststable , wxCB_READONLY|wxODCB_PAINTING_SELECTED|wxODCB_PAINTING_CONTROL, m_site_sel, m_group_sel);
	m_class_sel->SetColorMaps(m_owner->m_colormap_red, m_owner->m_colormap_green, m_owner->m_colormap_blue);
	setsizer->Add(m_group_sel,0,wxALIGN_CENTRE|wxGROW|wxBOTTOM|wxTOP,12);
	setsizer->Add(m_class_sel,0,wxALIGN_CENTRE|wxGROW|wxBOTTOM,12);
	//m_but_new_class = new wxButton(this,idButtonAddClass,"Ajouter une classe");
	//setsizer->Add(m_but_new_class,0,wxALIGN_CENTRE|wxBOTTOM,10);
	test->Add(setsizer,0,wxALL|wxGROW,10);
	ctrlsizer->Add(test,0,wxGROW);

	//	m_object_list = new wxListCtrl(this, idObjectList,
	//                                  wxDefaultPosition, wxSize(200,100),
	//                                wxLC_REPORT| wxSUNKEN_BORDER | wxLC_EDIT_LABELS |wxLC_SINGLE_SEL);
	//    m_object_list->InsertColumn(0, wxString("Name"));
	//    m_object_list->InsertColumn(1, wxString("Nb Points"));
	//    m_object_list->SetColumnWidth(0, 50);
	//    m_object_list->SetColumnWidth(1, 50);

	//	setsizer->Add(m_object_list,0,wxCENTER);

	//test->Add( setsizer,0,wxGROW);
	//ctrlsizer->Add( test,0,wxGROW); 

	//=====================================================================================================

	// AAE: image size fields
	
	wxBoxSizer *sizerAreaSize = new wxBoxSizer(wxVERTICAL);
	sizerStaticAreaSize = new wxStaticBoxSizer(new wxStaticBox(this,-1,wxString(m_languageDealer->getStringById(currentLanguage,"00061").c_str(),wxConvUTF8)), wxVERTICAL );
	wxBoxSizer *sizerControlsSize = new wxBoxSizer(wxHORIZONTAL);

	m_image_width_label = new wxStaticText(this,wxID_ANY,wxString(m_languageDealer->getStringById(currentLanguage,"00023").c_str(),wxConvUTF8), wxPoint( 20, 350 ), wxSize(50,20));
	m_image_width = new wxTextCtrl(this, idImageSizes1, wxT("1"), wxPoint(50,350), wxSize(50,20));	
	m_image_height_label = new wxStaticText(this,wxID_ANY,wxString(m_languageDealer->getStringById(currentLanguage,"00024").c_str(),wxConvUTF8), wxPoint( 110, 350 ), wxSize(50,20));
	m_image_height = new wxTextCtrl(this, idImageSizes2, wxT("1"), wxPoint(140,350), wxSize(50,20));	

	sizerControlsSize->Add(m_image_width_label,0,wxGROW|wxALIGN_CENTRE);
	sizerControlsSize->Add(m_image_width,0,wxGROW|wxALIGN_CENTRE,10);
	sizerControlsSize->Add(m_image_height_label,0,wxGROW|wxALIGN_CENTRE);
	sizerControlsSize->Add(m_image_height,0,wxGROW|wxALIGN_CENTRE,10);

	wxBoxSizer *sizerButtonSize = new wxBoxSizer(wxHORIZONTAL);
	buttonSegmentSize = new wxButton(this,idButtonSegmentSize,wxString(m_languageDealer->getStringById(currentLanguage,"00100").c_str(),wxConvUTF8));
	sizerButtonSize->Add(buttonSegmentSize, wxALIGN_CENTRE);
	sizerStaticAreaSize->Add(sizerControlsSize, 0,wxALIGN_CENTRE);
	sizerStaticAreaSize->AddSpacer(5);
	sizerStaticAreaSize->Add(sizerButtonSize,0,wxALL|wxALIGN_CENTRE|wxGROW,10);

	wxBoxSizer *sizerFieldPixelsCm = new wxBoxSizer(wxHORIZONTAL);
	m_image_pixels_cm_label = new wxStaticText(this,wxID_ANY,wxString(m_languageDealer->getStringById(currentLanguage,"00131").c_str(),wxConvUTF8),wxDefaultPosition,wxSize(80,20));
	m_image_pixels_cm = new wxTextCtrl(this,idImagePixelsCm,wxString(m_languageDealer->getStringById(currentLanguage,"00132").c_str(),wxConvUTF8),wxDefaultPosition,wxSize(50,20));
	sizerFieldPixelsCm->Add(m_image_pixels_cm_label);
	sizerFieldPixelsCm->AddSpacer(20);
	sizerFieldPixelsCm->Add(m_image_pixels_cm);
	sizerStaticAreaSize->AddSpacer(5);
	sizerStaticAreaSize->Add(sizerFieldPixelsCm);

	sizerAreaSize->Add(sizerStaticAreaSize,0,wxALL|wxALIGN_CENTRE|wxGROW,10);
	ctrlsizer->Add( sizerAreaSize,0,wxGROW);
	// AAE: classes grid
	/*
	test = new wxBoxSizer( wxVERTICAL ); 
 	classGrid = new wxGrid(this,-1,wxPoint( 20, 380 ),wxSize( 200, 300 ) );
	classGrid->CreateGrid(100, 2 );
	classGrid->SetRowLabelSize(0);
	classGrid->SetColLabelSize(20);
	classGrid->SetColSize(0,130);
	classGrid->SetColSize(1,70);
	classGrid->SetColLabelValue(0,wxString(m_languageDealer->getStringById(currentLanguage,"00021").c_str(),wxConvUTF8));
	classGrid->SetColLabelValue(1,wxString(m_languageDealer->getStringById(currentLanguage,"00022").c_str(),wxConvUTF8));
	test->Add( classGrid,0,wxALL|wxGROW,10);

	scalesizer->Add(test);
	*/
	//	test2->Add(scalesizer);

	// BUTTON FOR TABLE SHOWING
	wxBoxSizer *sizerButtonShowTable = new wxBoxSizer(wxVERTICAL);
	sizerStaticAreaTable = new wxStaticBoxSizer( new wxStaticBox(this,-1,wxString(m_languageDealer->getStringById(currentLanguage,"00087").c_str(),wxConvUTF8)), wxVERTICAL );
	wxBoxSizer *sizerButtonHor = new wxBoxSizer(wxHORIZONTAL);
	buttonShowTable = new wxButton(this,idButtonShowTable,wxString(m_languageDealer->getStringById(currentLanguage,"00086").c_str(),wxConvUTF8));
	sizerButtonHor->Add(buttonShowTable, wxALIGN_CENTRE|wxGROW);
	sizerStaticAreaTable->Add(sizerButtonHor, wxALIGN_CENTRE|wxGROW);
	sizerButtonShowTable->Add(sizerStaticAreaTable, 0,wxALL|wxGROW|wxALIGN_CENTRE,10);
	ctrlsizer->Add(sizerButtonShowTable,0,wxGROW);

	// BUTTON FOR NAVIGATION WINDOW
	wxBoxSizer *sizerAreaButtonNavigation = new wxBoxSizer(wxVERTICAL);
	sizerStaticAreaButtonNavigation = new wxStaticBoxSizer(new wxStaticBox (this,-1,wxString(m_languageDealer->getStringById(currentLanguage,"00088").c_str(),wxConvUTF8)), wxVERTICAL );
	buttonShowNavigationWindow = new wxButton(this,idButtonShowNavigationWindow,wxString(m_languageDealer->getStringById(currentLanguage,"00088").c_str(),wxConvUTF8));
	sizerStaticAreaButtonNavigation->Add(buttonShowNavigationWindow,wxALIGN_CENTRE|wxGROW);
	sizerAreaButtonNavigation->Add(sizerStaticAreaButtonNavigation,0,wxALL|wxGROW,10);
	ctrlsizer->Add(sizerAreaButtonNavigation,0,wxGROW);

	//=====================================================================================================
	// EO control panel creation 
	//=====================================================================================================
	//=====================================================================================================
	SetSizer( ctrlsizer );
	// sets minimum frame size
	///ctrlsizer->SetSizeHints( this );
	//SetAutoLayout( true );
	Layout();
	ctrlsizer->Fit( this );
	//m_gauge->SetValue(0);
	//=====================================================================================================

wxLogMessage(wxT("Interface init ok"));
}

void LeftWindow::OnButtonSegmentSize(wxCommandEvent &ev){
  if(ImageFrame->m_scaling)
    ImageFrame->m_scaling=false;
  else
    ImageFrame->m_scaling=true;
  m_owner->ImageFrame->updateView();
}

//=======================================================================================

void LeftWindow::OnZoomChange(wxCommandEvent& event){
	ImageFrame->m_zoom=m_zoom_combo->GetSelection()+1;	
	Fit();
	ImageFrame->m_zoom_changed=true;
	ImageFrame->dcClear();
	ImageFrame->updateView();
	ImageFrame->SetScrollbars(1, 1, 50, 50, 0 , 0, false);
	ImageFrame->Refresh();
}

//=======================================================================================
void LeftWindow::Fit() 
{
	ImageFrame->m_vsize(1) = ImageFrame->m_isize(1)*ImageFrame->m_zoom;
	ImageFrame->m_vsize(2) = ImageFrame->m_isize(2)*ImageFrame->m_zoom;
	ImageFrame->m_vsize(3) = ImageFrame->m_isize(3)*ImageFrame->m_zoom;			

	ImageFrame->m_image_frame_size.x = ImageFrame->m_vsize(1);
	ImageFrame->m_image_frame_size.y = ImageFrame->m_vsize(2);

	ImageFrame->m_has_to_fit=true;

	wxLogMessage(wxT("Fit ok"));
}

//=======================================================================================
// EVENTS
//=======================================================================================


//=======================================================================================	
void LeftWindow::OnScaleSlider (wxScrollEvent& envent)
{
	lgl::F64 ls = m_scale_slider->GetValue()*1./m_scale_slider->GetMax();
	ImageFrame->m_scale = ImageFrame->invlogscale(ls);
	//ImageFrame->getGoodSliceSection();

	wxLogMessage(wxT("logs  = %f"),ls);
	wxLogMessage(wxT("ilogs = %f"),ImageFrame->m_scale);

	OnScaleChange();
}
//=======================================================================================

//=======================================================================================
void LeftWindow::OnScaleCtrl (wxCommandEvent& event) 
{
	/*ImageFrame->m_scale = atof ( m_scale_ctrl->GetValue() );
	if (ImageFrame->m_scale<0) ImageFrame->m_scale = 0;
	if (ImageFrame->m_scale>ImageFrame->m_scale_max) ImageFrame->m_scale = ImageFrame->m_scale_max;
	OnScaleChange();*/
}
//=======================================================================================

//=======================================================================================
void LeftWindow::OnSetDrawCheck ( wxCommandEvent& event) 
{
	ImageFrame->m_set_changed = true;
	ImageFrame->updateView();

}
//=======================================================================================

//=======================================================================================
void LeftWindow::OnAffichClassifCheck ( wxCommandEvent& event)
{
	ImageFrame->m_set_changed = true;
	ImageFrame->updateView();
}
//=======================================================================================

//=======================================================================================
void LeftWindow::OnSetColorChange(wxCommandEvent& event)
{
	ImageFrame->m_cur_set_color = lgl::RGBpoint(100,100,100);//ImageFrame->m_set_color[m_set_color_combo->GetSelection()];
	ImageFrame->m_palette_changed = true;
	ImageFrame->updateView();
	/*if (m_color_sel->GetSelection() == 0)
	{
	LayerOnSelectionDesc::setLayerOnSelection(255,255,255,1);
	wxLogMessage("<<WHITE ACTIVATED>>");
	}
	else if (m_color_sel->GetSelection() == 1)
	{
	LayerOnSelectionDesc::setLayerOnSelection(0,0,0,1);
	wxLogMessage("<<BLACK ACTIVATED>>");
	}
	else if (m_color_sel->GetSelection() == 2)
	{
	LayerOnSelectionDesc::setLayerOnSelection(255,0,0,1);
	wxLogMessage("<<RED ACTIVATED>>");
	}
	else if (m_color_sel->GetSelection() == 3)
	{
	LayerOnSelectionDesc::setLayerOnSelection(0,255,0,1);
	wxLogMessage("<<GREEN ACTIVATED>>");
	}
	else if (m_color_sel->GetSelection() == 4)
	{
	LayerOnSelectionDesc::setLayerOnSelection(0,0,255,1);
	wxLogMessage("<<BLUE ACTIVATED>>");
	}
	else
	{
	LayerOnSelectionDesc::setLayerOnSelection(255,255,0,1);
	wxLogMessage("<<YELLOW COLOR ACTIVATED>>");
	}*/
}
//=======================================================================================


//=======================================================================================
void LeftWindow::OnSetPersistenceCtrl(wxCommandEvent& event)
{
	//ImageFrame->m_set_persistence = atof ( m_set_persistence_ctrl->GetValue() );
	//wxLogMessage("per=%f",ImageFrame->m_set_persistence);
}
//=======================================================================================


//=======================================================================================
void LeftWindow::OnScaleChange () 
{
	//m_gauge->SetValue(m_gauge->GetRange()*(m_scale_slider->GetValue()*1./m_scale_slider->GetMax()));
	// updates slider and ctrl
	wxString v;
	v.Printf(wxT("%lf"),ImageFrame->m_scale);
	m_scale_ctrl->SetValue(v);
	lgl::F64 ls = ImageFrame->logscale(ImageFrame->m_scale);

	wxLogMessage(wxT("logs = %lf"),ls);

	m_scale_slider->SetValue((int)(ls*m_scale_slider->GetMax()));
	ImageFrame->m_scale_changed = true;
	//if (m_image_style_radio->GetSelection()==2) {
	//ImageFrame->m_xslice_changed  = true;
	//}
	ImageFrame->updateView();


}
//=======================================================================================


//=======================================================================================

void LeftWindow::OnImageStyleChange(wxCommandEvent& event)
{
	//	GetSelection();
	ImageFrame->m_xslice_changed  = true;
	ImageFrame->updateView();
}
//=======================================================================================

//=======================================================================================
void LeftWindow::OnSectionStyleChange(wxCommandEvent& event)
{
	ImageFrame->m_scale_changed = true;
	ImageFrame->updateView();
}
//=======================================================================================


//=======================================================================================
void LeftWindow::OnSectionColorChange(wxCommandEvent& event)
{
//	ImageFrame->m_contours_color = 	ImageFrame->m_set_color[m_section_color_combo->GetSelection()];
	ImageFrame->m_palette_changed = true;
	ImageFrame->updateView();
}


/*--------------------------------------------------------------------
*--
*-- ComboBox - Drawing
*--
--------------------------------------------------------------------*/


BEGIN_EVENT_TABLE( ComboClassMedSeg, wxOwnerDrawnComboBox )  
//EVT_COMBOBOX(id, func) Evenement quand la s�lection change dans la combobox
END_EVENT_TABLE()


ComboClassMedSeg::ComboClassMedSeg(wxWindow *parent, int id, wxString value, wxPoint pos, wxSize size, wxArrayString choices, long style, wxComboBox* site_sel, wxComboBox* group_sel/*, wxValidator validator, wxString name*/)
: wxOwnerDrawnComboBox(parent, id, value, pos, size, choices, style)//, validator, name)
{
	m_group_sel = group_sel;
	m_site_sel = site_sel;
}

ComboClassMedSeg::~ComboClassMedSeg()
{
}

void ComboClassMedSeg::SetColorMaps(int *red, int *green, int *blue){
m_colormap_red = red;
m_colormap_green = green;
m_colormap_blue = blue;
}

void ComboClassMedSeg::OnDrawItem(wxDC& dc, const wxRect& rect, int item, int flags) const
{
	if ( item == wxNOT_FOUND )
		return;

	wxColour txtCol;
	txtCol = *wxBLACK;
	// int mod = item % 4; modulo = % interessant..

	if (flags == 1)
	{
		wxColour CadreCol(0,0,0);
		wxRect CadreRect(rect.x+5 , rect.y+5, 23, 10);
		dc.SetBrush(wxBrush(CadreCol));
		dc.SetPen(wxPen(CadreCol));
		dc.DrawRectangle(CadreRect);
	}
	else
	{
		wxColour CadreCol(0,0,0);
		wxRect CadreRect(rect.x+5 , rect.y+3, 23, 10);
		dc.SetBrush(wxBrush(CadreCol));
		dc.SetPen(wxPen(CadreCol));
		dc.DrawRectangle(CadreRect);
	}

	if (m_mapping_im)
	{
	  string fooString = std::string(GetString(item).mb_str());
		Specie* fooSpecie = _locationDealer->getSpecieByName(fooString);
		/*
		std::vector<MapClass*> *map_class_vector = m_mapping_im->getMapClassVector();
		std::vector<MapClass*>::iterator itmc = map_class_vector->begin();
		MapClass *map_class;
		for (; itmc!=map_class_vector->end(); ++itmc) 
		{
			map_class = *itmc;
			char citem_id[256];
			sprintf(citem_id,"%d",(int)map_class->getId());
			wxLogMessage("--------------------------- %s ,%s",citem_id,GetString(item));
			if (!GetString(item).compare(wxString(citem_id)))
			{*/
				int col = fooSpecie->color;
				wxColour RectCol(m_colormap_red[col],m_colormap_green[col],m_colormap_blue[col]);
				if (flags == 1)
				{
					wxRect RectRect(rect.x+6 , rect.y+6, 21, 8);
					dc.SetBrush(wxBrush(RectCol));
					dc.SetPen(wxPen(RectCol));
					dc.DrawRectangle(RectRect);
				}
				else
				{
					wxRect RectRect(rect.x+6 , rect.y+4, 21, 8);
					dc.SetBrush(wxBrush(RectCol));
					dc.SetPen(wxPen(RectCol));
					dc.DrawRectangle(RectRect);
				}
				dc.DrawText(wxString(fooSpecie->name.c_str(),wxConvUTF8), rect.x + 35, rect.y + ((rect.height - dc.GetCharHeight())/2));
				if (flags == 1)
				{
					LayerOnSelectionDesc::setLayerOnSelection(col,0,0,1);
				}/*			
			}
		}^*/
	}
}

void ComboClassMedSeg::OnDrawBackground(wxDC& dc, const wxRect& rect, int item, int flags ) const
{
	wxColour txtCol;
	txtCol = *wxWHITE;
	// If item is selected or even, or we are painting the
	// combo control itself, use the default rendering.
	/**/
	if ( (flags & (wxODCB_PAINTING_CONTROL|wxODCB_PAINTING_SELECTED)) )
	{
		wxOwnerDrawnComboBox::OnDrawBackground(dc,rect,item,flags);
		return;
	}
	/**/
	if (this->GetSelection() == item)
	{
		wxColour bgCol(250,242,165);
		dc.SetBrush(wxBrush(bgCol));
		dc.SetPen(wxPen(bgCol));
		dc.DrawRectangle(rect);
	}
	// Otherwise, draw every other background with different colour.
	else
	{
		wxColour bgCol = *wxWHITE;
		dc.SetBrush(wxBrush(bgCol));
		dc.SetPen(wxPen(bgCol));
		dc.DrawRectangle(rect);
	}
}

wxCoord ComboClassMedSeg::OnMeasureItem(size_t WXUNUSED(item)) const
{
	return 16;
	//return -1;
}

wxCoord ComboClassMedSeg::OnMeasureItemWidth(size_t WXUNUSED(item)) const
{ 
	//return -1; // default = automatiquement r�gl� selon la longueur du texte.
	return 80;
}

static bool isThereComboBox = false;

void ComboClassMedSeg::LoadComboBox(ImageMapping *mapping_im, wxString filename)
{
	if (!isThereComboBox)
	{
		this->Clear();
		//mapping_im->parseClassification(filename);
		m_mapping_im = mapping_im;
		//InitSite();// PREVIOUS MULTILOCATION VERSION
		InitGroup();
		/*
		std::vector<MapClass*> *map_class_vector = mapping_im->getMapClassVector();
		std::vector<MapClass*>::iterator itmc = map_class_vector->begin();
		MapClass *map_class;
		m_site_sel->SetStringSelection(wxString("- Tous -"));
		m_group_sel->SetStringSelection(wxString("- Tous -"));
		for (; itmc!=map_class_vector->end(); ++itmc) 
		{
			map_class = *itmc;
			char citem_id[256];
			sprintf(citem_id,"%d",(int)map_class->getId());
			this->AppendString(wxString(citem_id));
		}
		*/
		isThereComboBox = true;
	}
}

void ComboClassMedSeg::ReloadComboBox()
{
	this->Clear();
	/*
	InitSite();
	InitGroup();
	std::vector<MapClass*> *map_class_vector = m_mapping_im->getMapClassVector();
	std::vector<MapClass*>::iterator itmc = map_class_vector->begin();
	MapClass *map_class;
	for (; itmc!=map_class_vector->end(); ++itmc) 
	{
		map_class = *itmc;
		char citem_id[256];
		sprintf(citem_id,"%d",(int)map_class->getId());
		if ((m_group_sel->GetStringSelection() == wxString(map_class->getGroup()) || m_group_sel->GetStringSelection() == "- Tous -")
			&& (wxString(map_class->getSite()) == m_site_sel->GetStringSelection() || m_site_sel->GetStringSelection() == "- Tous -"))
		{
			this->AppendString(wxString(citem_id));
		}
	}
	*/
	vector<Specie* >* fooSpecies;
	vector<Specie* >::iterator itS;
	// string currentLocation = (m_site_sel->GetStringSelection()).mb_str(); // PREVIOUS MULTILOCATION VERSION
	string currentLocation("default");
	string currentGroups = std::string((m_group_sel->GetStringSelection()).mb_str());
	if(currentLocation!="- Tous -"&& currentLocation!=""&&currentGroups!="- Tous -"&&currentGroups!=""){
		fooSpecies = _locationDealer->getSpecies(currentLocation, currentGroups);
		for(itS = fooSpecies->begin(); itS!=fooSpecies->end();++itS){
		  this->AppendString(wxString((*itS)->name.c_str(),wxConvUTF8));
		}
	}
}

void ComboClassMedSeg::InitGroup()
{
	//	m_group_sel->AddChild..
	bool already_is;
	std::vector<string >* fooLocations;
	std::vector<string >* fooGroups;
	std::vector<string >::iterator itG;
	std::vector<string >::iterator itL;
	fooLocations = _locationDealer->getLocations();
	int _size = fooLocations->size();
	// string fooString = (m_site_sel->GetStringSelection()).mb_str(); // PREVIOUS MULTILOCATION VERSION
	string fooString("default");
	if(fooString!="- Tous -"&&fooString!=""){
		fooGroups = _locationDealer->getGroups(fooString);	
		_size = fooGroups->size();
		for(itG= fooGroups->begin(); itG!=fooGroups->end();++itG){
			already_is=false;
			for (unsigned int i=0; i<m_group_sel->GetCount(); i++)
			{
			  if (m_group_sel->GetString(i) == wxString((*itG).c_str(),wxConvUTF8))
			{
				already_is = true;
			}
			}
			if (!already_is)
			{
			  m_group_sel->AppendString(wxString((*itG).c_str(),wxConvUTF8));		
			}
		}
	}
	else{
		for(itL = fooLocations->begin(); itL!=fooLocations->end();++itL){
			fooGroups = _locationDealer->getGroups((*itL));	
			_size = fooGroups->size();
			for(itG= fooGroups->begin(); itG!=fooGroups->end();++itG){
				already_is=false;
				for (unsigned int i=0; i<m_group_sel->GetCount(); i++)
				{
				  if (m_group_sel->GetString(i) == wxString((*itG).c_str(),wxConvUTF8))
				{
					already_is = true;
				}
				}
				if (!already_is)
				{
				  m_group_sel->AppendString(wxString((*itG).c_str(),wxConvUTF8));		
				}
			}
		}
		if (m_group_sel->FindString(wxT("- Tous -")) ==	wxNOT_FOUND)
		{
		  m_group_sel->AppendString(wxT("- Tous -"));
		}
	}
}

void ComboClassMedSeg::InitSite()
{
	//	m_group_sel->AddChild..
	bool already_is;
	std::vector<string >* fooLocations;
	std::vector<string >::iterator itL;
	
	fooLocations = _locationDealer->getLocations();
	for(itL=fooLocations->begin();itL!=fooLocations->end();++itL){
	  m_site_sel->AppendString(wxString((*itL).c_str(),wxConvUTF8));	
	}

	/*
	std::vector<MapClass*> *map_class_vector = m_mapping_im->getMapClassVector();
	std::vector<MapClass*>::iterator itmc = map_class_vector->begin();
	MapClass *map_class;
	for (; itmc!=map_class_vector->end(); ++itmc) 
	{
		map_class = *itmc;
		//char citem_id[256];
		//sprintf(citem_group,"%s", map_class->getGroup());
		already_is = false;
		for (unsigned int i=0; i<m_site_sel->GetCount(); i++)
		{
			if (m_site_sel->GetString(i) == wxString(map_class->getSite()))
			{
				already_is = true;
			}
		}
		if (!already_is)
		{
			m_site_sel->AppendString(wxString(map_class->getSite()));	
		}
	}
	*/
	if (m_site_sel->FindString(wxT("- Tous -")) ==	wxNOT_FOUND)
	{
	  m_site_sel->AppendString(wxT("- Tous -"));
	}

}
/*--------------------------------------------------------------------
*--
*-- BitmapButton
*--
--------------------------------------------------------------------*/

BEGIN_EVENT_TABLE(BitmapButtonMedSeg, wxBitmapButton)
EVT_BUTTON(idClickEvent, BitmapButtonMedSeg::OnClick)
EVT_MOUSE_EVENTS( BitmapButtonMedSeg::OnMouse )
END_EVENT_TABLE()

BitmapButtonMedSeg::BitmapButtonMedSeg(wxWindow* parent, wxWindowID id, const wxBitmap& bitmap) : wxBitmapButton(parent, id, bitmap)
{
	m_cd_contour_col = new wxColourDialog(parent);
	m_color = new wxColour();
	m_color->Set(255,0,0);
	ImageFrame = NULL;
}

BitmapButtonMedSeg::~BitmapButtonMedSeg()
{
}

void BitmapButtonMedSeg::OnClick(wxCommandEvent& event)
{
	int verif = m_cd_contour_col->ShowModal();
	if (verif == wxID_OK)
	{
	  wxLogMessage(wxT("Ca marche tranquilou"));
	}
}

void BitmapButtonMedSeg::OnMouse(wxMouseEvent& event)
{
	if (event.LeftUp())
	{
		int verif = m_cd_contour_col->ShowModal();
		if (verif == wxID_OK)
		{
			*m_color = m_cd_contour_col->GetColourData().GetColour();
			WriteBitmap(*m_color);
			if (ImageFrame)
			{
				ImageFrame->updateView();
			}
		}
	}
}

void BitmapButtonMedSeg::WriteBitmap(wxColour color)
{
	wxImage image(20,20);
	for (int a=0;a<image.GetWidth();a++)
	{
		for (int b=0;b<image.GetHeight();b++)
		{
			image.SetRGB(a,b,color.Red(),color.Green(),color.Blue());
		}
	}
	wxBitmap bitmap(image);
	SetBitmapLabel(bitmap);
}

wxColour* BitmapButtonMedSeg::getColor()
{
	return m_color;
}

void BitmapButtonMedSeg::setColor(wxColour color)
{
	*m_color = color;
	WriteBitmap(*m_color);
}

void BitmapButtonMedSeg::setImageFrame(ScrollWinMedSeg *win)
{
	ImageFrame = win;
}

/*--------------------------------------------------------------------
*--
*-- Fenetre d'ajout d'une classe
*--
--------------------------------------------------------------------*/
BEGIN_EVENT_TABLE(AddClassMedSeg, wxFrame)
EVT_CLOSE(AddClassMedSeg::OnClose) 
END_EVENT_TABLE()

AddClassMedSeg::AddClassMedSeg(wxWindow* parent, wxWindowID id, const wxString& title) : wxFrame (parent, id, title, wxPoint(parent->GetSize().x/2-100+parent->GetPosition().x,parent->GetSize().y/2-100+parent->GetPosition().y), wxSize(0,0))
{
	m_parent = (wxFrame*)parent;
}

AddClassMedSeg::~AddClassMedSeg()
{
}

void AddClassMedSeg::OnClose(wxCloseEvent& event)
{
	m_parent->Enable(true);
	Destroy();
}

/* window for segmentation parameters
 */
BEGIN_EVENT_TABLE(SegmentationParametersWindow, wxFrame)
EVT_COMMAND_SCROLL( idSlideBase, SegmentationParametersWindow::OnChangeSlider)
EVT_COMMAND_SCROLL( idSlideEnergy, SegmentationParametersWindow::OnChangeSlider)
EVT_COMMAND_SCROLL( idSlideLevels, SegmentationParametersWindow::OnChangeSlider)
EVT_KEY_DOWN(SegmentationParametersWindow::OnKey)
EVT_CLOSE(SegmentationParametersWindow::OnClose)
EVT_BUTTON(idButtonClose, SegmentationParametersWindow::OnButtonClose)
END_EVENT_TABLE()

void SegmentationParametersWindow::OnButtonClose(wxCommandEvent &_ev){
  wxCloseEvent ev;
  SegmentationParametersWindow::OnClose(ev);
}

void SegmentationParametersWindow::OnKey(wxKeyEvent &_ev){
  wxCloseEvent ev;
  if(_ev.GetKeyCode()==27)
    SegmentationParametersWindow::OnClose(ev);
}

void SegmentationParametersWindow::OnChangeSlider(wxScrollEvent &event){
  int cur_base, cur_energy, cur_levels;

  cur_base = m_slider_baseSeg_thrd->GetValue();
  cur_energy = m_slider_energyComplexity->GetValue();
  cur_levels = m_slider_levels->GetValue();
  m_text_baseSeg_thrd->SetLabel(wxString::Format(wxT("%i"),cur_base));
  m_text_energyComplexity->SetLabel(wxString::Format(wxT("%i"),cur_energy));
  m_text_levels->SetLabel(wxString::Format(wxT("%i"),cur_levels));
}

SegmentationParametersWindow::SegmentationParametersWindow(wxWindow *parent, wxWindowID id, const wxString &title):wxFrame(parent,id,title,wxPoint(50,50),wxSize(300,200)){
  int init_base, min_base, max_base, init_energy, min_energy, max_energy,init_levels,min_levels,max_levels;
  this->m_parent = (wxFrame*)parent;
  languageDealer *m_languageDealer;
  m_languageDealer = ((InterfaceMedSeg*)parent)->_languageDealer;
  string currentLanguage = ((InterfaceMedSeg*)parent)->currentLanguage;
  this->SetBackgroundColour(*(((InterfaceMedSeg*)parent)->background_color));	

  init_base = 15;
  min_base = 1;
  max_base=50;
  init_energy=1;
  min_energy=1;
  max_energy=32;
  init_levels=10;
  min_levels=1;
  max_levels=20;

  // MAIN BOX
  wxBoxSizer *_boxS = new wxBoxSizer(wxHORIZONTAL);
  //  wxStaticText *_text = new wxStaticText(this,-1,wxString(m_languageDealer->getStringById(currentLanguage,"00082").c_str(),wxConvUTF8));;
  // BOX FOR TITLE
  wxStaticBoxSizer *_boxTitle = new wxStaticBoxSizer(new wxStaticBox(this,-1,wxString(m_languageDealer->getStringById(currentLanguage,"00082").c_str(),wxConvUTF8),wxDefaultPosition,wxSize(300,200)),wxVERTICAL);
  // BOX FOR BASE SEG THRESHOLD TITLE
  wxStaticBoxSizer *_boxTitleBase = new wxStaticBoxSizer(new wxStaticBox(this,-1,wxString(m_languageDealer->getStringById(currentLanguage,"00083").c_str(),wxConvUTF8),wxDefaultPosition,wxSize(200,30)),wxHORIZONTAL);
  // BOX FOR GROUPING BASE CONTROLS 
  wxBoxSizer *_boxBase = new wxBoxSizer(wxHORIZONTAL);
  m_slider_baseSeg_thrd = new wxSlider(this,idSlideBase,init_base,min_base, max_base,wxDefaultPosition,wxSize(150,30));
  m_text_baseSeg_thrd = new wxStaticText(this,-1,wxString::Format(wxT("%i"),init_base));
  _boxBase->Add(m_slider_baseSeg_thrd,0,wxGROW);
  _boxBase->Add(m_text_baseSeg_thrd,wxGROW);
  // BOX FOR DESCRIPTION TEXT
  wxBoxSizer *_boxThrdDesc = new wxBoxSizer(wxHORIZONTAL);
  m_text_baseSeg_thrd_desc = new wxStaticText(this,-1,wxString(m_languageDealer->getStringById(currentLanguage,"00092").c_str(),wxConvUTF8),wxDefaultPosition,wxSize(250,80));
  _boxThrdDesc->Add(m_text_baseSeg_thrd_desc);
  _boxTitleBase->Add(_boxBase,wxGROW);
  _boxTitleBase->AddSpacer(10);
  _boxTitleBase->Add(_boxThrdDesc,wxGROW);
  _boxTitle->Add(_boxTitleBase,wxGROW);

  // BOX FOR ENERGY COMPLEXITY TITLE
  wxStaticBoxSizer *_boxTitleEnergy = new wxStaticBoxSizer(new wxStaticBox(this,-1,wxString(m_languageDealer->getStringById(currentLanguage,"00084").c_str(),wxConvUTF8),wxDefaultPosition,wxSize(200,30)),wxHORIZONTAL);
  // BOX FOR GROUPING ENERGY CONTROLS 
  wxBoxSizer *_boxEnergy = new wxBoxSizer(wxHORIZONTAL);
  m_slider_energyComplexity = new wxSlider(this,idSlideEnergy,init_energy,min_energy,max_energy,wxDefaultPosition,wxSize(150,30));
  m_text_energyComplexity = new wxStaticText(this,-1,wxString::Format(wxT("%i"),init_energy));
  _boxEnergy->Add(m_slider_energyComplexity,0,wxGROW);
  _boxEnergy->Add(m_text_energyComplexity,wxGROW);
  // BOX FOR DESCRIPTION TEXT
  wxBoxSizer *_boxEnergyDesc = new wxBoxSizer(wxHORIZONTAL);
  m_text_energyComplexity_desc = new wxStaticText(this,-1,wxString(m_languageDealer->getStringById(currentLanguage,"00093").c_str(),wxConvUTF8),wxDefaultPosition,wxSize(250,80));
  _boxEnergyDesc->Add(m_text_energyComplexity_desc);
  _boxTitleEnergy->Add(_boxEnergy,wxGROW);
  _boxTitleEnergy->AddSpacer(10);
  _boxTitleEnergy->Add(_boxEnergyDesc,wxGROW);
  _boxTitle->Add(_boxTitleEnergy,wxGROW);

  // BOX FOR LEVELS OF SCALE
  wxStaticBoxSizer *_boxTitleLevels = new wxStaticBoxSizer(new wxStaticBox(this,-1,wxString(m_languageDealer->getStringById(currentLanguage,"00133").c_str(),wxConvUTF8),wxDefaultPosition,wxSize(200,30)),wxHORIZONTAL);
  // BOX FOR GROUPING LEVEL CONTROLS
  wxBoxSizer *_boxLevels = new wxBoxSizer(wxHORIZONTAL);
  m_slider_levels = new wxSlider(this,idSlideLevels,init_levels,min_levels,max_levels,wxDefaultPosition,wxSize(150,30));
  m_text_levels = new wxStaticText(this,-1,wxString::Format(wxT("%i"),init_levels));
  _boxLevels->Add(m_slider_levels,0,wxGROW);
  _boxLevels->Add(m_text_levels,wxGROW);
  // BOX FOR DESCRIPTION TEXT
  wxBoxSizer *_boxLevelsDesc = new wxBoxSizer(wxHORIZONTAL);
  m_text_levels_desc = new wxStaticText(this,-1,wxString(m_languageDealer->getStringById(currentLanguage,"00134").c_str(),wxConvUTF8),wxDefaultPosition,wxSize(250,80));
  _boxLevelsDesc->Add(m_text_levels_desc);
  _boxTitleLevels->Add(_boxLevels,wxGROW);
  _boxTitleLevels->AddSpacer(10);
  _boxTitleLevels->Add(_boxLevelsDesc,wxGROW);
  _boxTitle->Add(_boxTitleLevels,wxGROW);

  // BUTTON FOR CLOSING
  wxButton *_buttonClose = new wxButton(this, idButtonClose,wxString(m_languageDealer->getStringById(currentLanguage,"00091").c_str(),wxConvUTF8));
  _boxTitle->AddSpacer(10);
  _boxTitle->Add(_buttonClose);

  // END OF LAYOUT
  _boxS->Add(_boxTitle, wxGROW);
  //  _boxS->Fit(this);
  this->SetSizer( _boxS );
  _boxS->Fit(this);
  this->Layout();
}

SegmentationParametersWindow::~SegmentationParametersWindow(){
}

void SegmentationParametersWindow::OnClose(wxCloseEvent& event){
  int cur_base, cur_energy, cur_levels;

  cur_base = m_slider_baseSeg_thrd->GetValue();
  cur_energy = m_slider_energyComplexity->GetValue();
  cur_levels = m_slider_levels->GetValue();

  ImageScaleClimbingParameters* param = new ImageScaleClimbingParameters;  
  param->setBaseSegmentationThreshold(cur_base);
  param->setEnergyNPieces(cur_energy);
  ((InterfaceMedSeg*)m_parent)->m_nlevels = cur_levels;
  m_parent->Enable(true);
  Destroy();
  ((InterfaceMedSeg*)m_parent)->DoSegmentation(param);
}

/* window for managing colormaps
*/
BEGIN_EVENT_TABLE(ManageColorMaps, wxFrame)
EVT_CLOSE(ManageColorMaps::OnClose) 
EVT_BUTTON ( idButtonUndo , ManageColorMaps::Undo )
EVT_BUTTON ( idButtonSave, ManageColorMaps::Save )
EVT_BUTTON ( idButtonLoad , ManageColorMaps::Load )
EVT_GRID_CELL_CHANGE (ManageColorMaps::UpdateGrid )
EVT_GRID_CELL_LEFT_CLICK (ManageColorMaps::SelectColor )
END_EVENT_TABLE()

ManageColorMaps::ManageColorMaps(wxWindow *parent, wxWindowID id, const wxString &title):wxFrame(parent,id,title,wxPoint(50,50),wxSize(700,500)){
	this->parent = parent;
	languageDealer *m_languageDealer;
	m_languageDealer = ((InterfaceMedSeg*)parent)->_languageDealer;
	string currentLanguage = ((InterfaceMedSeg*)parent)->currentLanguage;
	m_red = ((InterfaceMedSeg*)parent)->m_colormap_red;
	m_green = ((InterfaceMedSeg*)parent)->m_colormap_green;
	m_blue = ((InterfaceMedSeg*)parent)->m_colormap_blue;
	this->SetBackgroundColour(*(((InterfaceMedSeg*)parent)->background_color));

	// MAIN BOX
	wxBoxSizer *_boxS= new wxBoxSizer(wxHORIZONTAL);
	wxBoxSizer *_boxS1 = new wxBoxSizer(wxVERTICAL);
	wxStaticText *_warning = new wxStaticText(this,-1,wxString(m_languageDealer->getStringById(currentLanguage,"00080").c_str(),wxConvUTF8));
	_boxS1->Add(_warning);
	// GRID
	wxStaticBoxSizer* classSizer = new wxStaticBoxSizer( new wxStaticBox(this,-1,wxString(m_languageDealer->getStringById(currentLanguage,"00070").c_str(),wxConvUTF8)), wxVERTICAL );
	
	m_grid_colorMaps = new wxGrid(this,idGrid,wxPoint(20,20),wxSize(540,550));
	m_grid_colorMaps->CreateGrid(256, 6 );
	m_grid_colorMaps->SetRowLabelSize(0);
	m_grid_colorMaps->SetColLabelValue(0,wxString(m_languageDealer->getStringById(currentLanguage,"00065").c_str(),wxConvUTF8));
	m_grid_colorMaps->SetColLabelValue(1,wxString(m_languageDealer->getStringById(currentLanguage,"00066").c_str(),wxConvUTF8));
	m_grid_colorMaps->SetColLabelValue(2,wxString(m_languageDealer->getStringById(currentLanguage,"00067").c_str(),wxConvUTF8));
	m_grid_colorMaps->SetColLabelValue(3,wxString(m_languageDealer->getStringById(currentLanguage,"00068").c_str(),wxConvUTF8));
	m_grid_colorMaps->SetColLabelValue(4,wxString(m_languageDealer->getStringById(currentLanguage,"00069").c_str(),wxConvUTF8));
	m_grid_colorMaps->SetColLabelValue(5,wxString(m_languageDealer->getStringById(currentLanguage,"00095").c_str(),wxConvUTF8));
	SetGrid();
	// ADD GRID INTO SIZER
	classSizer->Add(m_grid_colorMaps);
	// CONTROLS SIZER
	wxStaticBoxSizer* actionsSizer = new wxStaticBoxSizer( new wxStaticBox(this,-1,wxString(m_languageDealer->getStringById(currentLanguage,"00071").c_str(),wxConvUTF8)), wxVERTICAL );
	wxBoxSizer *_boxV = new wxBoxSizer(wxVERTICAL);
	wxBoxSizer *_boxH = new wxBoxSizer(wxHORIZONTAL);
	wxButton *buttonUndo = new wxButton(this,idButtonUndo,wxString(m_languageDealer->getStringById(currentLanguage,"00072").c_str(),wxConvUTF8));
	_boxH->Add(buttonUndo);
	_boxV->Add(_boxH);
	_boxH = new wxBoxSizer(wxHORIZONTAL);
	wxButton *buttonSave = new wxButton(this,idButtonSave,wxString(m_languageDealer->getStringById(currentLanguage,"00073").c_str(),wxConvUTF8));
	wxButton *buttonLoad = new wxButton(this,idButtonLoad,wxString(m_languageDealer->getStringById(currentLanguage,"00074").c_str(),wxConvUTF8));
	_boxH->Add(buttonSave);
	_boxH->Add(buttonLoad);
	_boxV->Add(_boxH);
	actionsSizer->Add(_boxV,wxBOTTOM);
	_boxS1->Add(classSizer);
	_boxS->Add(_boxS1);
	_boxS->Add(actionsSizer,wxGROW);
	this->SetSizer( _boxS );
	this->Layout();
	_boxS->Fit( this);
}

ManageColorMaps::~ManageColorMaps(){

}

void ManageColorMaps::SetGrid(){
	LocationLoader *_locationDealer = ((InterfaceMedSeg*)m_parent)->_locationDealer;
	
	wxGrid *gr = m_grid_colorMaps;
	for(int i=0;i<256;++i){
		wxString foo, foo2,foo3,foo4;
		foo<<i;foo2<<m_red[i];foo3<<m_green[i];foo4<<m_blue[i];
		gr->SetCellValue(i,0,foo);
		gr->SetCellValue(i,1,foo2);	
		gr->SetCellValue(i,2,foo3);
		gr->SetCellValue(i,3,foo4);
		wxColor fooColor(m_red[i],m_green[i],m_blue[i]);
		gr->SetCellBackgroundColour(i,4,fooColor);
		Specie* fooSpecie = _locationDealer->getSpecieById(i);
		if(fooSpecie!=NULL)
			gr->SetCellValue(i,5,toWxString(fooSpecie->name));
	}
}

void ManageColorMaps::SelectColor(wxGridEvent &event){
	int row, col;
	col = event.GetCol();
	// user clicked on the column for the colors
	if(col==4){
		wxColourDialog *_selColor = new wxColourDialog(this);
		if(_selColor->ShowModal()==wxID_CANCEL) return;
		wxColour _color=_selColor->GetColourData().GetColour();
		m_grid_colorMaps->SetCellBackgroundColour(event.GetRow(),col,_color);
		wxString foo;
		foo<<_color.Red();
		m_grid_colorMaps->SetCellValue(event.GetRow(),1,foo);
		foo.clear();
		foo<<_color.Green();
		m_grid_colorMaps->SetCellValue(event.GetRow(),2,foo);
		foo.clear();
		foo<<_color.Blue();
		m_grid_colorMaps->SetCellValue(event.GetRow(),3,foo);
	}
}

void ManageColorMaps::UpdateGrid(wxGridEvent &event){
	int red, green, blue;
	double foo;
	for(int i=0;i<256;++i){
		m_grid_colorMaps->GetCellValue(i,1).ToDouble(&foo);
		red=(int)foo;
		m_grid_colorMaps->GetCellValue(i,2).ToDouble(&foo);
		green=(int)foo;
		m_grid_colorMaps->GetCellValue(i,3).ToDouble(&foo);
		blue=(int)foo;
		wxColor fooColor(red,green,blue);
		m_grid_colorMaps->SetCellBackgroundColour(i,4,fooColor);
	}
}

void ManageColorMaps::OnClose(wxCloseEvent& event){
	UseColorMap();
	m_parent->Enable(true);
	Destroy();
}

void ManageColorMaps::Undo(wxCommandEvent& event){
	SetGrid();
}

void ManageColorMaps::Save(wxCommandEvent& event){
	languageDealer *_languageDealer;
	_languageDealer = ((InterfaceMedSeg*)parent)->_languageDealer;
	string currentLanguage = ((InterfaceMedSeg*)parent)->currentLanguage;
	wxString mystring(_languageDealer->getStringById(currentLanguage,"00075").c_str(), wxConvUTF8);
	wxString filename = wxFileSelector(mystring,wxT(""),wxT(""),wxT(""),wxString(_languageDealer->getStringById(currentLanguage,"00076").c_str(),wxConvUTF8),wxOVERWRITE_PROMPT|wxSAVE);
	ofstream outf(filename.mb_str());
	for(int i=0;i<256;++i){
		outf<<m_grid_colorMaps->GetCellValue(i,1)<<endl<<m_grid_colorMaps->GetCellValue(i,2)<<endl<<m_grid_colorMaps->GetCellValue(i,3)<<endl;
	}
	outf.close();
	this->SetFocus();
}

void ManageColorMaps::UseColorMap(){
	int red, green, blue;
	double foo;

	for(int i=0;i<256;++i){
		m_grid_colorMaps->GetCellValue(i,1).ToDouble(&foo);
		red=(int)foo;
		m_grid_colorMaps->GetCellValue(i,2).ToDouble(&foo);
		green=(int)foo;
		m_grid_colorMaps->GetCellValue(i,3).ToDouble(&foo);
		blue=(int)foo;
		m_red[i]=red; m_green[i]=green; m_blue[i]=blue;
	}
}

void ManageColorMaps::Load(wxCommandEvent& event){
	languageDealer *_languageDealer;
	_languageDealer = ((InterfaceMedSeg*)parent)->_languageDealer;
	string currentLanguage = ((InterfaceMedSeg*)parent)->currentLanguage;
	wxString mystring(wxString(_languageDealer->getStringById(currentLanguage,"00078").c_str(), wxConvUTF8));
	wxString fooF = wxFileSelector(mystring,wxT(""),wxT(""),wxT(""),wxString(_languageDealer->getStringById(currentLanguage,"00079").c_str(),wxConvUTF8),wxFD_OPEN);
	ifstream inf(fooF.mb_str());
	int val;
	wxString foo;
	int i=0;
	while(!inf.eof()&&i<m_grid_colorMaps->GetNumberRows()){
		foo.clear();
		foo<<i;
		m_grid_colorMaps->SetCellValue(i,0,foo);
		inf>>val;
		foo.clear();
		foo<<val;
		m_grid_colorMaps->SetCellValue(i,1,foo);
		inf>>val;
		foo.clear();
		foo<<val;
		m_grid_colorMaps->SetCellValue(i,2,foo);
		inf>>val;
		foo.clear();
		foo<<val;
		m_grid_colorMaps->SetCellValue(i,3,foo);
		++i;
	}
	wxGridEvent dumm;
	UpdateGrid(dumm);
	this->SetFocus();
}

/* Window for managing classes
*/
BEGIN_EVENT_TABLE(ManageClassMedSeg, wxFrame)
EVT_CLOSE(ManageClassMedSeg::OnClose) 
EVT_BUTTON ( idButtonDo , ManageClassMedSeg::DoSelection )
EVT_BUTTON ( idButtonAll, ManageClassMedSeg::DoAll )
EVT_BUTTON ( idButtonSelAll , ManageClassMedSeg::SelectAll )
EVT_BUTTON ( idButtonSelNone , ManageClassMedSeg::SelectNone )
END_EVENT_TABLE()

ManageClassMedSeg::ManageClassMedSeg(wxWindow *parent, wxWindowID id, const wxString &title):wxFrame(parent,id,title,wxPoint(50,50),wxSize(700,500)){
	bool class_finished=false;
	m_parent = (wxFrame*)parent;
	this->SetBackgroundColour(*(((InterfaceMedSeg*)m_parent)->background_color));
	languageDealer *_languageDealer = ((InterfaceMedSeg*)m_parent)->_languageDealer;
	LocationLoader *_locationDealer = ((InterfaceMedSeg*)m_parent)->_locationDealer;
	string currentLanguage = ((InterfaceMedSeg*)m_parent)->currentLanguage;

	wxString mystring(_languageDealer->getStringById(currentLanguage,"00032").c_str(), wxConvUTF8);
	bool done = false;
	wxString filename;
	char foo[1024];
	do{
	  wxString fooF = wxFileSelector(mystring,wxT(""),wxT(""),wxT(""),wxString(_languageDealer->getStringById(currentLanguage,"00033").c_str(),wxConvUTF8),wxFD_OPEN);
	  if(fooF){ done = true;
	    strcpy(foo, (const char*)fooF.mb_str(wxConvUTF8));
	    filename = fooF;
	  }
	}while(!filename.size()>0);
	_locationDealer->loadLocations(foo);
	speciesArray = new wxArrayString ;
	vector<string >* _groups = _locationDealer->getGroups(string("default"));
	vector<string >::iterator itG;
	vector<Specie* >::iterator itS;
	for(itG=_groups->begin();itG!=_groups->end(); ++itG){
		vector<Specie* >* _species = _locationDealer->getSpecies(string("default"),(*itG));
		for(itS=_species->begin();itS!=_species->end();++itS){
			wxString foo((*itG).c_str(), wxConvUTF8);
			foo += wxT(" : ");
			wxString foo2(((*itS)->name).c_str(), wxConvUTF8);
			foo +=foo2;
			speciesArray->Add(foo);
		}
	}
	wxBoxSizer *_boxS= new wxBoxSizer(wxHORIZONTAL);
	// check list
	wxStaticBoxSizer* classSizer = new wxStaticBoxSizer( new wxStaticBox(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00034").c_str(),wxConvUTF8)), wxVERTICAL );
	m_checkList = new wxCheckListBox(this,wxID_ANY,wxPoint(10,10),wxSize(200,400),*speciesArray,wxLB_NEEDED_SB );
	classSizer->Add(m_checkList);
	// selection all/none
	wxStaticBoxSizer* selSizer = new wxStaticBoxSizer( new wxStaticBox(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00038").c_str(),wxConvUTF8)), wxVERTICAL );
	wxBoxSizer *boxSel = new wxBoxSizer(wxHORIZONTAL);
	wxButton *selAll = new wxButton(this,idButtonSelAll,wxString(_languageDealer->getStringById(currentLanguage,"00039").c_str(),wxConvUTF8));
	wxButton *selNone = new wxButton(this,idButtonSelNone,wxString(_languageDealer->getStringById(currentLanguage,"00040").c_str(),wxConvUTF8));
	boxSel->Add(selAll);
	boxSel->Add(selNone);
	selSizer->Add(boxSel);
	// selection controls
	wxStaticBoxSizer* controlSizer = new wxStaticBoxSizer( new wxStaticBox(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00035").c_str(),wxConvUTF8)), wxVERTICAL );
	wxBoxSizer *boxControls = new wxBoxSizer(wxHORIZONTAL);
	wxButton *doSelect = new wxButton(this,idButtonDo,wxString(_languageDealer->getStringById(currentLanguage,"00036").c_str(),wxConvUTF8));
	wxButton *useAll = new wxButton(this,idButtonAll,wxString(_languageDealer->getStringById(currentLanguage,"00037").c_str(),wxConvUTF8));
	boxControls->Add(doSelect);
	boxControls->Add(useAll);
	controlSizer->Add(boxControls);
	wxBoxSizer *rightS = new wxBoxSizer(wxVERTICAL);
	rightS->Add(selSizer);
	rightS->Add(controlSizer);
	_boxS->Add(classSizer);
	_boxS->Add(rightS);
	this->SetSizer( _boxS );
	this->Layout();
	_boxS->Fit( this);
}

ManageClassMedSeg::~ManageClassMedSeg(){
}

void ManageClassMedSeg::OnClose(wxCloseEvent& event){
	m_parent->Enable(true);
	Destroy();
}

void ManageClassMedSeg::DoSelection(wxCommandEvent &event){
	vector<string> *_species = new vector<string>;
	LocationLoader *_locationDealer = ((InterfaceMedSeg*)m_parent)->_locationDealer;

	for(int i=0;i<speciesArray->GetCount();++i){
		if(!m_checkList->IsChecked(i)){
			wxString fooS = (*speciesArray)[i];
			_species->push_back(string(fooS.mb_str()));
		}
	}
	_locationDealer->deleteSpecies(_species);
	m_parent->Enable(true);
	Destroy();
}

void ManageClassMedSeg::DoAll(wxCommandEvent &event){
m_parent->Enable(true);
Destroy();
}

void ManageClassMedSeg::SelectAll(wxCommandEvent &event){
	for(int i=0;i<speciesArray->GetCount();++i){
		m_checkList->Check(i,TRUE);
	}
}

void ManageClassMedSeg::SelectNone(wxCommandEvent &event){
	for(int i=0;i<speciesArray->GetCount();++i){
		m_checkList->Check(i,FALSE);
	}
}

BEGIN_EVENT_TABLE(TableWindow, wxFrame)
EVT_CLOSE(TableWindow::OnClose)
EVT_BUTTON(idButtonClose, TableWindow::ButtonClose)
END_EVENT_TABLE()

void TableWindow::OnClose(wxCloseEvent &ev){
  m_parent->Enable(true);
  Destroy();
}

TableWindow::TableWindow(wxWindow *parent, wxWindowID id, const wxString &title, int highlight):wxFrame(parent,id,title,wxPoint(50,50),wxSize(800,620)){
  m_parent = (wxFrame*)parent;
  this->SetBackgroundColour(*(((InterfaceMedSeg*)m_parent)->background_color));
  double _pixel_side_length;

  wxBoxSizer *_boxS = new wxBoxSizer(wxVERTICAL);
  tableGrid = new wxGrid(this,-1,wxPoint(5,5),wxSize(650,600));
  tableGrid->CreateGrid(250,5);
  tableGrid->SetRowLabelSize(0);
  tableGrid->EnableGridLines(false);
  tableGrid->EnableEditing(false);
  tableGrid->SetDefaultCellBackgroundColour(*(((InterfaceMedSeg*)m_parent)->background_color));
  vector<MPolygon*> *_pols = ((InterfaceMedSeg*)m_parent)->GetPolygons();
  languageDealer *_languageDealer = ((InterfaceMedSeg*)m_parent)->_languageDealer;
  LocationLoader *_locationDealer = ((InterfaceMedSeg*)m_parent)->_locationDealer;
  _pixel_side_length = ((InterfaceMedSeg*)m_parent)->m_pixel_side_length;
  string currentLanguage = ((InterfaceMedSeg*)m_parent)->currentLanguage;
  vector<MPolygon*>::iterator itPols;
  int *red = ((InterfaceMedSeg*)m_parent)->m_colormap_red;
  int *green = ((InterfaceMedSeg*)m_parent)->m_colormap_green;
  int *blue = ((InterfaceMedSeg*)m_parent)->m_colormap_blue;
    
  int indRow=0;
  int polId;
  int indHigh=0;
  for(itPols=_pols->begin();itPols!=_pols->end();++itPols){
    polId = (*itPols)->GetId();
    Specie* fooSpecie = _locationDealer->getSpecieById((*itPols)->GetClassId());
    int col = fooSpecie->color;
    wxColour specieColor(red[col],green[col],blue[col]);
    (*itPols)->SetPixelSideLength(_pixel_side_length);
    tableGrid->SetCellValue(indRow,0,wxString::Format(wxT("%i"),(*itPols)->GetId()));
    tableGrid->SetCellValue(indRow,1,wxString((fooSpecie->name).c_str(),wxConvUTF8));
    tableGrid->SetCellValue(indRow,2,wxString::Format(wxT("%f"),(*itPols)->GetArea()));
    tableGrid->SetCellValue(indRow,3,wxString::Format(wxT("%f"),(*itPols)->GetPerimeter()));
    tableGrid->SetCellValue(indRow,4,wxT(""));
    tableGrid->SetCellBackgroundColour(indRow,4,specieColor);
    if(polId == highlight){
      wxColor col(255,100,100);
      tableGrid->SetCellBackgroundColour(indRow,0,col);
      tableGrid->SetCellBackgroundColour(indRow,1,col);
      tableGrid->SetCellBackgroundColour(indRow,2,col);
      tableGrid->SetCellBackgroundColour(indRow,3,col);
      indHigh = indRow;
    }
    else{
      wxColor col(*(((InterfaceMedSeg*)m_parent)->background_color));
      tableGrid->SetCellBackgroundColour(indRow,0,col);
      tableGrid->SetCellBackgroundColour(indRow,1,col);
      tableGrid->SetCellBackgroundColour(indRow,2,col);
      tableGrid->SetCellBackgroundColour(indRow,3,col);
    }
    ++indRow;
  }
  tableGrid->MakeCellVisible(indRow,0);

  //wxString(_languageDealer->getStringById(currentLanguage,"00038").c_str(),wxConvUTF8))
  tableGrid->SetColLabelValue(0,wxString(_languageDealer->getStringById(currentLanguage,"00094").c_str(),wxConvUTF8));
  tableGrid->SetColLabelValue(1,wxString(_languageDealer->getStringById(currentLanguage,"00095").c_str(),wxConvUTF8));
  tableGrid->SetColLabelValue(2,wxString(_languageDealer->getStringById(currentLanguage,"00096").c_str(),wxConvUTF8));
  tableGrid->SetColLabelValue(3,wxString(_languageDealer->getStringById(currentLanguage,"00097").c_str(),wxConvUTF8));
  tableGrid->SetColLabelValue(4,wxString(_languageDealer->getStringById(currentLanguage,"00098").c_str(),wxConvUTF8));
  tableGrid->SetLabelBackgroundColour(*(((InterfaceMedSeg*)m_parent)->background_color));
  /*  tableGrid->SetColSize(0,80);
  tableGrid->SetColSize(1,120);
  tableGrid->SetColSize(2,80);
  tableGrid->SetColSize(3,80);
  tableGrid->SetColSize(4,80);
  */
  tableGrid->AutoSize();

  _boxS->Add(tableGrid);
  _boxS->Fit(this);
}

void TableWindow::ButtonClose(wxCommandEvent &_ev){
  wxCloseEvent ev;
  TableWindow::OnClose(ev);
}

TableWindow::~TableWindow(){
  delete tableGrid;
}

BEGIN_EVENT_TABLE(NavigationWindow, wxFrame)
EVT_CLOSE(NavigationWindow::OnClose)
EVT_PAINT(NavigationWindow::OnPaint)
END_EVENT_TABLE()

NavigationWindow::NavigationWindow(wxWindow *parent, wxWindowID id, const wxString &title):wxFrame(parent,id,title,wxPoint(50,50),wxSize(320,200),wxFRAME_FLOAT_ON_PARENT|wxMINIMIZE_BOX|wxCLOSE_BOX|wxSYSTEM_MENU| wxCAPTION){
  m_parent = (wxFrame*)parent;
  String foo=(((LeftWindow*)m_parent)->m_owner->m_image_path);
  _imagePath = new wxString(wxString(foo.c_str(),wxConvUTF8));
  m_flagRefresh = false;
}

void NavigationWindow::OnClose(wxCloseEvent &ev){
  this->Show(false);
}

void NavigationWindow::OnPaint(wxPaintEvent &ev){
  wxPaintDC dc( this );
  PrepareDC(dc);
  dc.Clear();
  int x0,y0,x1,y1,rectX, rectY, rectX2, rectY2,offX, offY, m_zoom, imW, imH;
  double scaleX, scaleY;
  //  string _imagePath = (((LeftWindow*)m_parent)->m_owner->m_image_path);
  wxImage scaled(*_imagePath);
  imW = scaled.GetWidth();
  imH = scaled.GetHeight();
  scaleX = 320.0/((double)scaled.GetWidth());
  scaleY=200.0/((double)scaled.GetHeight());
  scaled.Rescale(320,200);
  wxBitmap imageScaled(scaled);
  dc.DrawBitmap(imageScaled,0,0);
  ScrollWinMedSeg *imageFrame  =(((LeftWindow*)m_parent)->m_owner->ImageFrame);
  imageFrame->GetViewStart(&x0,&y0);
  imageFrame->GetClientSize(&x1,&y1);
  imageFrame->GetScrollPixelsPerUnit(&offX, &offY);
  m_zoom = imageFrame->m_zoom;
  rectX = offX*x0;
  rectY = offY*y0;
  rectX/=m_zoom;
  rectY/=m_zoom;
  rectX2 = x1/m_zoom;
  rectY2 = y1/m_zoom;
  wxBrush mBrush(wxColour(0,0,0),wxTRANSPARENT);
  wxPen mPen(wxColour(255,150,150),2);
  dc.SetBrush(mBrush);
  dc.SetPen(mPen);
  dc.DrawRectangle(rectX*scaleX, rectY*scaleY, rectX2*scaleX, rectY2*scaleY);
  dc.SetBrush(wxNullBrush);
  dc.SetPen(wxNullPen);
//  this->Update();
}

void NavigationWindow::RePaint(wxScrollWinEvent &_ev){
  wxPaintEvent ev;
  //  OnPaint(ev);
  this->Refresh();
}

/* WINDOW FOR EXPORTING CLASSIFICATION DATA INTO A XLS FORMAT FILE
   IT OPTIONALLY ALSO EXPORTS THE DATA INTO A CSV ASCII FILE
 */
BEGIN_EVENT_TABLE(ExportXLSWindow, wxFrame)
EVT_CLOSE(ExportXLSWindow::OnClose)
EVT_BUTTON(idButtonSave,ExportXLSWindow::OnSave)
END_EVENT_TABLE()

ExportXLSWindow::ExportXLSWindow(wxWindow *parent, wxWindowID id, const wxString &title):wxFrame(parent,id,title,wxPoint(50,50),wxSize(320,200)){
  m_parent = (wxFrame*)parent;
  m_root = (InterfaceMedSeg*)m_parent;
  this->SetBackgroundColour(*(m_root->background_color));
  languageDealer *_languageDealer = m_root->_languageDealer;
  String currentLanguage = m_root->currentLanguage;

  // MAIN BOX
  wxBoxSizer *_boxMain = new wxBoxSizer(wxVERTICAL);
  // MAIN BOX WITH TEXT
  wxStaticBoxSizer *_staticBoxMain = new wxStaticBoxSizer(new wxStaticBox(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00106").c_str(),wxConvUTF8)),wxVERTICAL);
  // OPTIONAL FIELDS BOX WITH TEXT
  wxStaticBoxSizer *_staticBoxOptionals = new wxStaticBoxSizer(new wxStaticBox(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00107").c_str(),wxConvUTF8)),wxVERTICAL);
  // OPTIONAL FIELD: REGION
  wxBoxSizer *_boxRegion = new wxBoxSizer(wxHORIZONTAL);
  _checkRegion = new wxCheckBox(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00108").c_str(),wxConvUTF8),wxDefaultPosition,wxSize(180,30));
  _textRegion = new wxTextCtrl(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00109").c_str(),wxConvUTF8),wxDefaultPosition,wxSize(180,30));
  _boxRegion->Add(_checkRegion);
  _boxRegion->AddSpacer(20);
  _boxRegion->Add(_textRegion);

  // OPTIONAL FIELD: LOCALITY
  wxBoxSizer *_boxLocality = new wxBoxSizer(wxHORIZONTAL);
  _checkLocality = new wxCheckBox(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00110").c_str(),wxConvUTF8),wxDefaultPosition,wxSize(180,30));
  _textLocality = new wxTextCtrl(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00111").c_str(),wxConvUTF8),wxDefaultPosition,wxSize(180,30));
  _boxLocality->Add(_checkLocality);
  _boxLocality->AddSpacer(20);
  _boxLocality->Add(_textLocality);

  // OPTIONAL FIELD: DATE
  wxBoxSizer *_boxDate = new wxBoxSizer(wxHORIZONTAL);
  _checkDate = new wxCheckBox(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00112").c_str(),wxConvUTF8),wxDefaultPosition,wxSize(180,30));
  _textDate = new wxTextCtrl(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00113").c_str(),wxConvUTF8),wxDefaultPosition,wxSize(180,30));
  _boxDate->Add(_checkDate);
  _boxDate->AddSpacer(20);
  _boxDate->Add(_textDate);

  // OPTIONAL FIELD: Depth
  wxBoxSizer *_boxDepth = new wxBoxSizer(wxHORIZONTAL);
  _checkDepth = new wxCheckBox(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00114").c_str(),wxConvUTF8),wxDefaultPosition,wxSize(180,30));
  _textDepth = new wxTextCtrl(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00115").c_str(),wxConvUTF8),wxDefaultPosition,wxSize(180,30));
  _boxDepth->Add(_checkDepth);
  _boxDepth->AddSpacer(20);
  _boxDepth->Add(_textDepth);

  _staticBoxOptionals->Add(_boxRegion);
  _staticBoxOptionals->Add(_boxLocality);
  _staticBoxOptionals->Add(_boxDate);
  _staticBoxOptionals->Add(_boxDepth);
  _staticBoxMain->Add(_staticBoxOptionals);

  // EXPORT OPTIONS BOX
  wxStaticBoxSizer *_staticBoxExportOptions = new wxStaticBoxSizer(new wxStaticBox(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00116").c_str(),wxConvUTF8)),wxVERTICAL);
  // EXPORT OPTION: export plain ascii file
  wxBoxSizer *_boxAscii = new wxBoxSizer(wxHORIZONTAL);
  _checkAscii = new wxCheckBox(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00117").c_str(),wxConvUTF8),wxDefaultPosition,wxSize(220,30));
  _boxAscii->Add(_checkAscii, wxGROW);
  // EXPORT OPTION: sum content to existing file
  wxBoxSizer *_boxSum = new wxBoxSizer(wxHORIZONTAL);
  _checkSum = new wxCheckBox(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00119").c_str(),wxConvUTF8),wxDefaultPosition,wxSize(220,30));
  _boxSum->Add(_checkSum, wxGROW);

  _staticBoxExportOptions->Add(_boxAscii, wxGROW);
  _staticBoxExportOptions->Add(_checkSum,wxGROW);
  _staticBoxMain->Add(_staticBoxExportOptions);

  // BUTTON FOR SAVING
  wxBoxSizer *_boxButton = new wxBoxSizer(wxHORIZONTAL);
  wxButton *_buttonSave = new wxButton(this,idButtonSave,wxString(_languageDealer->getStringById(currentLanguage,"00118").c_str(),wxConvUTF8));
  _boxButton->Add(_buttonSave,wxALIGN_RIGHT);
  _staticBoxMain->Add(_boxButton,wxALIGN_RIGHT);

  _boxMain->Add(_staticBoxMain,wxALL,10);
  this->SetSizer( _boxMain );
  this->Layout();
  _boxMain->Fit(this);
}

void ExportXLSWindow::OnSave(wxCommandEvent &ev){
  bool newFile;
  int beginRow;

  languageDealer *_languageDealer = m_root->_languageDealer;
  string currentLanguage = m_root->currentLanguage;
  LocationLoader *_locationDealer = ((InterfaceMedSeg*)m_parent)->_locationDealer;
  vector<MPolygon *> *_pols = m_root->GetPolygons();

  // strings for column headers
  wxString head1 = wxString(_languageDealer->getStringById(currentLanguage,"00120").c_str(),wxConvUTF8);
  wxString head2 = wxString(_languageDealer->getStringById(currentLanguage,"00121").c_str(),wxConvUTF8);
  wxString head3 = wxString(_languageDealer->getStringById(currentLanguage,"00122").c_str(),wxConvUTF8);
  wxString head4 = wxString(_languageDealer->getStringById(currentLanguage,"00123").c_str(),wxConvUTF8);
  wxString head5 = wxString(_languageDealer->getStringById(currentLanguage,"00124").c_str(),wxConvUTF8);
  wxString head6 = wxString(_languageDealer->getStringById(currentLanguage,"00125").c_str(),wxConvUTF8);
  wxString head7 = wxString(_languageDealer->getStringById(currentLanguage,"00126").c_str(),wxConvUTF8);
  wxString head8 = wxString(_languageDealer->getStringById(currentLanguage,"00127").c_str(),wxConvUTF8);
  wxString head9 = wxString(_languageDealer->getStringById(currentLanguage,"00128").c_str(),wxConvUTF8);
  wxString head10 = wxString(_languageDealer->getStringById(currentLanguage,"00129").c_str(),wxConvUTF8);

  // flag signaling whether we're exporting to a new file
  // or adding content to a previously existing one
  newFile = !_checkSum->IsChecked();

  // get working image filename, strip out image format extension
  // and prepare names for XLS and ASCII output filenames
  wxString filenameImg(m_root->m_image_filename.c_str(),wxConvUTF8);
  if(filenameImg.Find('.') != -1 ){
    filenameImg = filenameImg.BeforeLast('.');
  } 
  wxString filenameXLS = filenameImg +=wxT(".xls");
  wxString filenameAscii = filenameImg +=wxT(".txt");

  // if new file, new file dialogue mode.
  // loading file dialogue mode elseway
  wxString filename;
  wxFileDialog *openFile;
  if(newFile)
    filename = wxFileSelector(wxString(_languageDealer->getStringById(currentLanguage,"00025").c_str(),wxConvUTF8),wxT(""),filenameXLS,wxT(""),wxString(_languageDealer->getStringById(currentLanguage,"00026").c_str(),wxConvUTF8),wxOVERWRITE_PROMPT|wxSAVE);
  else{
    // filename = wxFileSelector(wxString(_languageDealer->getStringById(currentLanguage,"00025").c_str(),wxConvUTF8),wxT(""),wxT(""),wxT("xls"),wxString(_languageDealer->getStringById(currentLanguage,"00026").c_str(),wxConvUTF8),wxFD_OPEN);
    openFile = new wxFileDialog(this,wxString(_languageDealer->getStringById(currentLanguage,"00025").c_str(),wxConvUTF8),wxT(""),filenameXLS,wxT("*.xls"),wxFD_OPEN);
    if(!(openFile->ShowModal()==wxID_OK)) return;
    filename = openFile->GetPath();
  }

  // export content to xls structure
  BasicExcel e;
  BasicExcelWorksheet *sheet;
  beginRow=1;
  // if new file, set column headers
  if(newFile){
    e.New(1);
    sheet = e.GetWorksheet("Sheet1");
    sheet->Cell(0,0)->SetString(head1.mb_str());
    sheet->Cell(0,1)->SetString(head2.mb_str());
    sheet->Cell(0,2)->SetString(head3.mb_str());
    sheet->Cell(0,3)->SetString(head4.mb_str());
    sheet->Cell(0,4)->SetString(head5.mb_str());
    sheet->Cell(0,5)->SetString(head6.mb_str());
    sheet->Cell(0,6)->SetString(head7.mb_str());
    sheet->Cell(0,7)->SetString(head8.mb_str());
    sheet->Cell(0,8)->SetString(head9.mb_str());
    sheet->Cell(0,9)->SetString(head10.mb_str());
  }
  // if not new, move cursor to first empty row
  else{
    char foo[1024];
    strcpy(foo, (const char*)filename.mb_str(wxConvUTF8));
    e.Load(foo);
    sheet = e.GetWorksheet("Sheet1");
    beginRow = sheet->GetTotalRows();
  }
  // now set the new content rows, iterating through polygons vector
  vector<MPolygon*>::iterator itPols;
  for(itPols=_pols->begin();itPols!=_pols->end();++itPols){
    if((*itPols)->GetArea()==0) continue;
    Specie* fooSpecie = _locationDealer->getSpecieById((*itPols)->GetClassId());
    if(_checkRegion->IsChecked()) sheet->Cell(beginRow,0)->SetString((_textRegion->GetValue()).mb_str());
    else sheet->Cell(beginRow,0)->SetString("");
    if(_checkLocality->IsChecked()) sheet->Cell(beginRow,1)->SetString((_textLocality->GetValue()).mb_str());
    else sheet->Cell(beginRow,1)->SetString("");
    if(_checkDate->IsChecked()) sheet->Cell(beginRow,2)->SetString((_textDate->GetValue()).mb_str());
    else sheet->Cell(beginRow,2)->SetString("");
    if(_checkDepth->IsChecked()) sheet->Cell(beginRow,3)->SetString((_textDepth->GetValue()).mb_str());
    else sheet->Cell(beginRow,3)->SetString("");
    sheet->Cell(beginRow,4)->SetString(m_root->m_image_filename.c_str());
    sheet->Cell(beginRow,5)->SetString(fooSpecie->name.c_str());
    sheet->Cell(beginRow,6)->SetInteger((*itPols)->GetId());
    sheet->Cell(beginRow,7)->SetInteger((*itPols)->GetClassId());
    sheet->Cell(beginRow,8)->SetDouble((*itPols)->GetArea());
    sheet->Cell(beginRow,9)->SetDouble((*itPols)->GetPerimeter());
    beginRow++;
  }
  //  if(newFile)
  e.SaveAs(filename.mb_str());
  //  else
  //    e.SaveAs("dos.xls");
  
  // EXPORT TO AN ASCII FILE, 
  // IF CHECKBOX IS CHECKED
  if(_checkAscii->IsChecked()){
    ofstream asciiFile;
    asciiFile.open(filenameAscii.mb_str());
    wxString sep = wxT("\t");
    asciiFile<<head1.mb_str()<<sep.mb_str()<<head2.mb_str()<<sep.mb_str()<<head3.mb_str()<<sep.mb_str()<<head4.mb_str()<<sep.mb_str()<<head5.mb_str()<<sep.mb_str()<<head6.mb_str()<<sep.mb_str()<<head7.mb_str()<<sep.mb_str()<<head8.mb_str()<<sep.mb_str()<<head9.mb_str()<<sep.mb_str()<<head10.mb_str()<<endl;
    for(itPols=_pols->begin();itPols!=_pols->end();++itPols){
      if((*itPols)->GetArea()==0) continue;
      Specie* fooSpecie = _locationDealer->getSpecieById((*itPols)->GetClassId());
      if(_checkRegion->IsChecked()) asciiFile<<(_textRegion->GetValue()).mb_str();
      asciiFile<<sep.mb_str();
      if(_checkLocality->IsChecked()) asciiFile<<(_textLocality->GetValue()).mb_str();
      asciiFile<<sep.mb_str();
      if(_checkDate->IsChecked()) asciiFile<<(_textDate->GetValue()).mb_str();
      asciiFile<<sep.mb_str();
      if(_checkDepth->IsChecked()) asciiFile<<(_textDepth->GetValue()).mb_str();
      asciiFile<<sep.mb_str();
      asciiFile<<m_root->m_image_filename<<sep.mb_str();
      asciiFile<<fooSpecie->name<<sep.mb_str();
      asciiFile<<((*itPols)->GetId())<<sep.mb_str();
      asciiFile<<((*itPols)->GetClassId())<<sep.mb_str();
      asciiFile<<((*itPols)->GetArea())<<sep.mb_str();
      asciiFile<<((*itPols)->GetPerimeter())<<sep.mb_str();
      asciiFile<<endl;
    }
    asciiFile.close();
  }
  wxCloseEvent event;
  this->OnClose(event);
}

void ExportXLSWindow::OnClose(wxCloseEvent &ev){
  m_parent->Enable(true);
  Destroy();
}

BEGIN_EVENT_TABLE(SegmentSizingWindow, wxFrame)
EVT_CLOSE(SegmentSizingWindow::OnClose)
EVT_BUTTON( idButtonClose,SegmentSizingWindow::OnButtonClose)
END_EVENT_TABLE()

SegmentSizingWindow::SegmentSizingWindow(wxWindow *parent, wxWindowID id, const wxString &title):wxFrame(parent,id,title,wxPoint(50,50),wxSize(320,200)){
  m_parent = (wxFrame*)parent;
  m_root = ((ScrollWinMedSeg*)m_parent)->m_owner;
  m_left = m_root->OptionFrame;
  this->SetBackgroundColour(*(m_root->background_color));
  languageDealer *_languageDealer = m_root->_languageDealer;
  String currentLanguage = m_root->currentLanguage;
  // MAIN BOX
  wxBoxSizer *_boxMain = new wxBoxSizer(wxVERTICAL);
  wxStaticBoxSizer *_staticBoxMain = new wxStaticBoxSizer(new wxStaticBox(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00101").c_str(),wxConvUTF8)),wxVERTICAL);

  // BOX FOR SIZE CONTROLS
  wxBoxSizer *_boxControls = new wxBoxSizer(wxHORIZONTAL);
  wxStaticText *m_label_text = new wxStaticText(this,-1,wxString(_languageDealer->getStringById(currentLanguage,"00102").c_str(),wxConvUTF8));
  m_size_text = new wxTextCtrl(this,-1,wxT("1"));
  _boxControls->Add(m_label_text);
  _boxControls->AddSpacer(10);
  _boxControls->Add(m_size_text);

  // BOX FOR CLOSE BUTTON

  wxBoxSizer *_boxButtonClose = new wxBoxSizer(wxHORIZONTAL);
  wxButton *_buttonClose = new wxButton(this,idButtonClose,wxString(_languageDealer->getStringById(currentLanguage,"00103").c_str(),wxConvUTF8));
  _boxButtonClose->Add(_buttonClose);


  _staticBoxMain->Add(_boxControls,wxALL|wxGROW);
  _staticBoxMain->AddSpacer(20);
  _staticBoxMain->Add(_boxButtonClose);
  _boxMain->Add(_staticBoxMain,wxGROW,10);
  this->SetSizer( _boxMain );
  this->Layout();
  _boxMain->Fit(this);
}

void SegmentSizingWindow::OnClose(wxCloseEvent &ev){
  double segmentPix, segmentCm, x0,x1,y0,y1;
  x0 = ((ScrollWinMedSeg*)m_parent)->m_scaling_x0;
  x1 = ((ScrollWinMedSeg*)m_parent)->m_scaling_x1;
  y0 = ((ScrollWinMedSeg*)m_parent)->m_scaling_y0;
  y1 = ((ScrollWinMedSeg*)m_parent)->m_scaling_y1;
  segmentPix = sqrt(pow(x1-x0,2)+pow(y0-y1,2));
  if(segmentPix==0) segmentPix=1;
  if(!m_size_text->GetValue().ToDouble(&segmentCm)) segmentCm = 1;
  m_root->m_pixel_side_length = segmentCm/segmentPix;
  m_root->Enable(true);
  ((ScrollWinMedSeg*)m_parent)->m_scaling_x0 =  ((ScrollWinMedSeg*)m_parent)->m_scaling_x1 =  ((ScrollWinMedSeg*)m_parent)->m_scaling_y1 =  ((ScrollWinMedSeg*)m_parent)->m_scaling_y1 = -1;
  ((ScrollWinMedSeg*)m_parent)->m_scaling = false;
  m_root->UpdateStatusBarScaling();
  m_root->OptionFrame->UpdateImageSizeCtrls();
  Destroy();
}

void SegmentSizingWindow::OnButtonClose(wxCommandEvent &_ev){
  wxCloseEvent ev;
  SegmentSizingWindow::OnClose(ev);
}

BEGIN_EVENT_TABLE(MyAboutWindow, wxFrame)
EVT_CLOSE(MyAboutWindow::OnClose)
END_EVENT_TABLE()

MyAboutWindow::MyAboutWindow(wxWindow *parent, wxWindowID id, const wxString &title):wxFrame(parent,id,title,wxPoint(50,50),wxSize(640,480)){
  m_parent = (wxFrame*)parent;
  m_root = (InterfaceMedSeg*)m_parent;
  languageDealer *_languageDealer = m_root->_languageDealer;
  String currentLanguage = m_root->currentLanguage;
  
  this->SetBackgroundColour(*(m_root->background_color));
  wxBoxSizer *_boxMain = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *_boxMain2 = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer *_boxMain3 = new wxBoxSizer(wxVERTICAL);
//  wxImagePanel *paneStarlab = new wxImagePanel( this, wxT("data/logo_starlab.png"), wxBITMAP_TYPE_PNG);
//  wxImagePanel *paneUpf = new wxImagePanel( this, wxT("data/logo_ceab.png"), wxBITMAP_TYPE_PNG);
  char *_pathLogo = new char[256];_pathLogo[0]='\0';
#ifndef __WINDOWS__
  strcat(_pathLogo,getenv("HOME"));
  _pathLogo = strcat((char*)_pathLogo,"/.seascape/logo_seascape_small.png");
#else
  _pathLogo = "data/logo_seascape_small.png";
#endif
  wxImagePanel *paneSeascape = new wxImagePanel( this, wxString((char*)_pathLogo,wxConvUTF8), wxBITMAP_TYPE_PNG);
  _boxMain2->AddSpacer(20);
  _boxMain2->Add(paneSeascape);
  _boxMain2->AddSpacer(20);
//  _boxMain2->Add(paneStarlab);
//  _boxMain2->AddSpacer(20);
//  _boxMain2->Add(paneUpf);
//  _boxMain2->AddSpacer(20);

  wxString foo;
  foo = wxString(_languageDealer->getStringById(currentLanguage,"00135").c_str(),wxConvUTF8);
  foo.Replace(wxT("\\n"),wxT("\n"),true);
  wxString msg;
  msg.Printf( foo);
  wxStaticText *textAbout = new wxStaticText(this,-1,msg);
  wxFont fooFont = textAbout->GetFont();
  fooFont.SetPointSize(18);
  textAbout->SetFont(fooFont);
  _boxMain3->AddSpacer(40);
  _boxMain3->Add(textAbout);
  _boxMain3->AddSpacer(40);

  _boxMain->AddSpacer(20);
  _boxMain->Add(_boxMain2);
  _boxMain->AddSpacer(20);
  _boxMain->Add(_boxMain3);
  _boxMain->AddSpacer(20);
  this->SetSizer( _boxMain );
  this->Layout();
  _boxMain->Fit(this);
}

void MyAboutWindow::OnClose(wxCloseEvent &ev){
  m_parent->Enable(true);
  Destroy();
}

wxString toWxString(string _val){
	return wxString(_val.c_str(), wxConvUTF8);
}

 
BEGIN_EVENT_TABLE(wxImagePanel, wxPanel)
// some useful events
/*
 EVT_MOTION(wxImagePanel::mouseMoved)
 EVT_LEFT_DOWN(wxImagePanel::mouseDown)
 EVT_LEFT_UP(wxImagePanel::mouseReleased)
 EVT_RIGHT_DOWN(wxImagePanel::rightClick)
 EVT_LEAVE_WINDOW(wxImagePanel::mouseLeftWindow)
 EVT_KEY_DOWN(wxImagePanel::keyPressed)
 EVT_KEY_UP(wxImagePanel::keyReleased)
 EVT_MOUSEWHEEL(wxImagePanel::mouseWheelMoved)
 */
 
// catch paint events
EVT_PAINT(wxImagePanel::paintEvent)
//Size event
EVT_SIZE(wxImagePanel::OnSize)
END_EVENT_TABLE()
 
 
// some useful events
/*
 void wxImagePanel::mouseMoved(wxMouseEvent& event) {}
 void wxImagePanel::mouseDown(wxMouseEvent& event) {}
 void wxImagePanel::mouseWheelMoved(wxMouseEvent& event) {}
 void wxImagePanel::mouseReleased(wxMouseEvent& event) {}
 void wxImagePanel::rightClick(wxMouseEvent& event) {}
 void wxImagePanel::mouseLeftWindow(wxMouseEvent& event) {}
 void wxImagePanel::keyPressed(wxKeyEvent& event) {}
 void wxImagePanel::keyReleased(wxKeyEvent& event) {}
 */
 
wxImagePanel::wxImagePanel(wxFrame* parent, wxString file, wxBitmapType format) :
wxPanel(parent)
{
    // load the file... ideally add a check to see if loading was successful
    image.LoadFile(file, format);
	this->SetSize(image.GetWidth(),image.GetHeight());
}
 
/*
 * Called by the system of by wxWidgets when the panel needs
 * to be redrawn. You can also trigger this call by
 * calling Refresh()/Update().
 */
 
void wxImagePanel::paintEvent(wxPaintEvent & evt)
{
    // depending on your system you may need to look at double-buffered dcs
    wxPaintDC dc(this);
    render(dc);
}
 
/*
 * Alternatively, you can use a clientDC to paint on the panel
 * at any time. Using this generally does not free you from
 * catching paint events, since it is possible that e.g. the window
 * manager throws away your drawing when the window comes to the
 * background, and expects you will redraw it when the window comes
 * back (by sending a paint event).
 */
void wxImagePanel::paintNow()
{
    // depending on your system you may need to look at double-buffered dcs
    wxClientDC dc(this);
    render(dc);
}
 
/*
 * Here we do the actual rendering. I put it in a separate
 * method so that it can work no matter what type of DC
 * (e.g. wxPaintDC or wxClientDC) is used.
 */
void wxImagePanel::render(wxDC&  dc)
{
   
    dc.DrawBitmap( image, 0, 0, false );
}
 
/*
 * Here we call refresh to tell the panel to draw itself again.
 * So when the user resizes the image panel the image should be resized too.
 */
void wxImagePanel::OnSize(wxSizeEvent& event){
    Refresh();
    //skip the event.
    event.Skip();
}
 
