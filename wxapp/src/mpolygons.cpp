#include "mpolygons.h"
#include <cstddef>

double pattern_values[256] = {4,4,3,1.414,4,4,1.414,2.828,	       \
			      3,1.414,2,2,3,1.414,1.414,1.414, \
			      3,3,2,1.414,1.414,1.414,2,1.414, \
			      2,2,1,1,2,2,1,1, \
			      4,4,3,1.414,4,4,3,3, \
			      1.414,2.828,1.414,1.414,3,3,1.414,1.414,	\
			      3,3,2,2,3,3,2,2, \
			      2,2,1,1.414,2,2,1,1,	\
			      3,3,2,2,3,3,2,2, \
			      2,1.414,1,1,2,2,1,1.414, \
			      2,2,1,1,1.414,2,1,1.414, \
			      1,1,0,0,1,2,0,0, \
			      1.414,1.414,2,2,3,3,2,2, \
			      2,1.414,1,1,2,2,1,1, \
			      1.414,2,1,2,1.414,1.414,1,1, \
			      1,1.414,0,0,1,1,0,0, \
			      4,4,3,3,4,4,1.414,3, \
			      3,3,2,2,3,3,2,2, \
			      1.414,3,1.414,1.414,2.828,3,1.414,1.414, \
			      2,2,1,1,2,2,1.414,1, \
			      4,4,3,3,4,4,3,3, \
			      1.414,3,2,2,3,3,1.414,1.414, \
			      1.414,3,2,1.414,3,3,2,1.414, \
			      2,2,2,1,2,1.414,1,1, \
			      1.414,3,2,2,1.414,3,2,2, \
			      1.414,1.414,1,1,2,1.414,2,1, \
			      2,2,1,1,1.414,2,1,1, \
			      1,1,0,0,1.414,1,0,0, \
			      2.828,3,2,2,3,3,2,1.414, \
			      1.414,1.414,1.414,1,2,1.414,1,1, \
			      1.414,2,1.414,1,1.414,1.414,1,1, \
			      1,1,0,0,1,1,0,0};


MPolygon::MPolygon(){
  m_area = 0;
  m_perimeter = 0;
  m_side_pixel_length = 0;
  int classId = -1;
  m_pixels = new mRegion;
  m_Id = MPolygon::nextId();
}

MPolygon::~MPolygon(){
  mRegion::iterator it;

  for(it=m_pixels->begin(); it!=m_pixels->end();++it)
    delete (*it);
  delete m_pixels;
}

/* dtor. Deallocates all the points 
 */ 
void MPolygon::SetPixels(mRegion *_pixels){
  mRegion::iterator it;
  if(m_pixels!=NULL){
    for(it=m_pixels->begin(); it!=m_pixels->end();++it)
      delete (*it);
    delete m_pixels;
  }
  this->m_pixels = _pixels;
  this->ComputeArea();
  this->ComputePerimeter();
}

/* compute the area of the polygon.
   multiply n_pixels * side_pixel^2
 */
double MPolygon::ComputeArea(){
  if(m_side_pixel_length>0&&m_pixels->size()>0)
    m_area = (m_side_pixel_length*m_side_pixel_length)*m_pixels->size();
  else
    m_area = 0;
  return m_area;
}

/* compute the perimeter of the polygon
 */ 
double MPolygon::ComputePerimeter(){
  mRegion::iterator it;
  mPoint p1,p2,p3,p4,p5,p6,p7,p8,*p;
  int pattern_index;
  m_perimeter = 0;
  for(it=m_pixels->begin();it!=m_pixels->end();++it){
    p=(*it);
    MoveNorthWest(p,&p1);MoveNorth(p,&p2);MoveNorthEast(p,&p3);MoveWest(p,&p4);MoveEast(p,&p5);MoveSouthWest(p,&p6);MoveSouth(p,&p7);MoveSouthEast(p,&p8);
    pattern_index=0;
    pattern_index+=m_pixels->count(&p1);
    pattern_index+=m_pixels->count(&p2)*2;
    pattern_index+=m_pixels->count(&p3)*4;
    pattern_index+=m_pixels->count(&p4)*8;
    pattern_index+=m_pixels->count(&p5)*16;
    pattern_index+=m_pixels->count(&p6)*32;
    pattern_index+=m_pixels->count(&p7)*64;
    pattern_index+=m_pixels->count(&p8)*128;
    m_perimeter += pattern_values[pattern_index];
  }

  return m_perimeter;
}

/* Splits the first connected area found in _region and create
   a polygon with the pixels/points. CAUTION: the pixels/points
   droped at the resulting polygon will be deleted from _region
 */
MPolygon* SplitPolygons(mRegion *_region){
  // if there is not points to attach to a polygon
  // we return NULL
  if(_region->size()==0) return NULL;
  // resulting output polygon
  MPolygon* polygon = new MPolygon();
  // resulting splitted region
  mRegion* s1 = new mRegion;
  // stack of points on the current region to explore
  std::vector<mPoint*> v1 ;
  // current exploring point 
  mPoint *p1;
  p1 = (*_region->begin());
  // we add the first point present on the region
  v1.push_back(p1);
  // points north, south, east and west of current point
  mPoint pNorth, pSouth, pEast, pWest, *pNew;
  // while there are points on the region to explore
  while(!v1.empty()){
    p1 = v1.back(); v1.pop_back();
    _region->erase(p1);
    s1->insert(p1);
    MoveNorth(p1,&pNorth);
    MoveEast(p1,&pEast);
    MoveSouth(p1,&pSouth);
    MoveWest(p1,&pWest);
    if(_region->count(&pNorth)>0){
      pNew = new mPoint(pNorth);
      v1.push_back(pNew);
    }
    if(_region->count(&pEast)>0){
      pNew = new mPoint(pEast);
      v1.push_back(pNew);
    }
    if(_region->count(&pSouth)>0) {
      pNew = new mPoint(pSouth);
      v1.push_back(pNew);
    }
    if(_region->count(&pWest)>0){
      pNew = new mPoint(pWest);
      v1.push_back(pNew);
    }
  }
  polygon->SetPixelSideLength(1);
  polygon->SetPixels(s1);
  return polygon;
}

/* moves _north point north of _from
   returns the pointer to _north
   WARNING: DOES'NT ALLOC _north
 */
mPoint* MoveNorth(mPoint *_from, mPoint *_north){
  _north->second = _from->second-1;
  _north->first = _from->first;
  return _north;
}

/* moves _northEast point north east  of _from
   returns the pointer to _northEast
   WARNING: DOES'NT ALLOC _northEast
 */
mPoint* MoveNorthEast(mPoint *_from, mPoint *_northEast){
  _northEast->second = _from->second-1;
  _northEast->first = _from->first+1;
  return _northEast;
}

/* moves _east point east of _from
   returns the pointer to _east
   WARNING: DOES'NT ALLOC _east
 */
mPoint* MoveEast(mPoint *_from, mPoint *_east){
  _east->first = _from->first+1;
  _east->second = _from->second;
  return _east;
}

mPoint* MoveSouthEast(mPoint *_from, mPoint *_southEast){
  _southEast->first = _from->first+1;
  _southEast->second = _from->second+1;
  return _southEast;
}

/* moves _south point south of _from
   returns the pointer to _south
   WARNING: DOES'NT ALLOC _south
 */
mPoint* MoveSouth(mPoint *_from, mPoint *_south){
  _south->second = _from->second+1;
  _south->first = _from->first;
  return _south;
}

mPoint* MoveSouthWest(mPoint *_from, mPoint *_southWest){
  _southWest->second = _from->second+1;
  _southWest->first = _from->first-1;
  return _southWest;
}

/* moves _west point west of _from
   returns the pointer to _west
   WARNING: DOES'NT ALLOC _west
 */
mPoint* MoveWest(mPoint *_from, mPoint *_west){
  _west->first = _from->first-1;
  _west->second = _from->second;
  return _west;
}

mPoint* MoveNorthWest(mPoint *_from, mPoint *_northWest){
  _northWest->second = _from->second-1;
  _northWest->first = _from->first-1;
  return _northWest;
}

int MPolygon::m_lastId = 0;
