#include "languageLoader.h"

int languageDealer::loadLanguages(const char *_path){
	int ret;
	string _type;
	TiXmlDocument _doc(_path);
	
	if(!_doc.LoadFile()) return false;
  	TiXmlHandle _hdoc(&_doc);
	TiXmlElement *_elem;
	TiXmlHandle _hroot(0);
	// module elements
	_elem=_hdoc.FirstChildElement().Element();
	if(!_elem) return false;
	_type = _elem->Attribute("name");
	if(_type!=string("sxsGuiLanguages")){
		return false;
	}
	ret = loadLanguages(_elem);	
	return ret;
}


int languageDealer::loadLanguages(TiXmlElement *_element){
	string _nameLanguage;
	int cont=0;
	dictionary *language;

	TiXmlHandle _hroot(_element);
	TiXmlElement *_elem;
	_elem=_hroot.FirstChildElement().Element();
	for(_elem;_elem;_elem=_elem->NextSiblingElement()){
		_nameLanguage = _elem->Attribute("name");
		language = loadXMLLanguage(_elem);
		if(language==NULL) continue;
		++cont;
		languages[_nameLanguage]=language;
	}
	return cont;
}

dictionary* languageDealer::loadXMLLanguage(TiXmlElement *_element){
	string _id;
	string _string;
	TiXmlHandle _hlanguage(_element);
	TiXmlElement *_elem;
	_elem = _hlanguage.FirstChild().Element();
	dictionary *language = new dictionary;
	for(_elem;_elem;_elem=_elem->NextSiblingElement()){
		_id = _elem->Attribute("id");
		_string = _elem->Attribute("value");
		(*language)[_id]=_string;
	}
	return language;
}

string languageDealer::getStringById(string _languageName, string _stringId){
	std::map<string,dictionary*>::iterator it = languages.find(_languageName);
	if (it == languages.end())
		return "error";
	dictionary::iterator it2 = it->second->find(_stringId);
	if (it2 == (it->second)->end())
		return "error";
	std::string result = it2->second;
	return result;
}

vector< string >* languageDealer::getLanguagesVector(){
	vector< string > *res = new vector< string >;
	string foo;

	std::map<string,dictionary*>::iterator it = languages.begin();
	for(;it!=languages.end();++it){
		foo=(*it).first;
		res->push_back(foo);
	}
	return res;
}
