//=============================================================================
/// \file
/// \brief Main GUI Application for Scale-Sets Image Analysis
//=============================================================================
/// SxS lib uses wxWindows 
#ifndef __SXSWX__
#define __SXSWX__ 1
#endif
// SxS includes
#include "sxs.h"

#include "InterfaceMedSeg.h"
#include "lglwxLogWindow.h"
//=============================================================================




//=============================================================================
/// The main app
class Application : public wxApp
{
public:
  /// Main
  virtual bool OnInit();
};
//=============================================================================


//=============================================================================
class ScaleSetsImageAnalysis : public lgl::FrameWithLog
{
  
public:
  ScaleSetsImageAnalysis(wxFrame *parent, const wxString& title=_T("Scale-Sets Image Analysis"));
  ~ScaleSetsImageAnalysis() {}
  
  void InitInterface();
  
  void OnMenuOpen(wxCommandEvent& event);
  void OnMenuQuit(wxCommandEvent& event);
  void OnMenuSegment(wxCommandEvent& event);
  void OnMenuAbout(wxCommandEvent& event);
  void OnLogOpenClose(wxCommandEvent& event);
	
  //=========================================================
  /// Menu
  wxMenuBar *menuBar;
  wxMenu *menuFile;
  wxMenu *menuHelp;
	
  //=========================================================
  // IDs for the events
  enum
    {
      idMenuOpen,
      idMenuQuit,
      idMenuSegment,
      idMenuLog,
      // it is important for the id corresponding to the "About" command to have
      // this standard value as otherwise it won't be handled properly under Mac
      // (where it is special and put into the "Apple" menu)
      idMenuAbout = wxID_ABOUT,
    };
	
  DECLARE_EVENT_TABLE()
    };
//=============================================================================

//=======================================================================================
/// EVENT_TABLE 
BEGIN_EVENT_TABLE( ScaleSetsImageAnalysis, wxFrame ) 
  EVT_MENU ( idMenuOpen, ScaleSetsImageAnalysis::OnMenuOpen ) 
  EVT_MENU ( idMenuQuit, ScaleSetsImageAnalysis::OnMenuQuit ) 
  EVT_MENU ( idMenuSegment, ScaleSetsImageAnalysis::OnMenuSegment ) 
  EVT_MENU ( idMenuAbout, ScaleSetsImageAnalysis::OnMenuAbout ) 
  EVT_MENU( idMenuLog,  ScaleSetsImageAnalysis::OnLogOpenClose) 
  END_EVENT_TABLE() 
  //=======================================================================================

  //=======================================================================================
  // Definition of the various algorithms used
  // All ScaleClimbingUsers (SCU) inherit from N_wxProgressDialog_User in order to handle the wxProgressDialog while running
  //=======================================================================================

  //=======================================================================================
  // Mumford-Shah piece-wise constant model

  // ScaleClimbingUser
  class MS_ScaleClimbing_User : 
  public virtual sxs::N_wxProgressDialog_User,
  public virtual sxs::E_MumfordShah_User
  {};


//=======================================================================================


//=======================================================================================
// EO Definition of the various algorithms used
//=======================================================================================


//=============================================================================
bool Application::OnInit()
{
  // loads all available Image Handlers
#if wxUSE_LIBPNG
  wxImage::AddHandler( new wxPNGHandler );
#endif

#if wxUSE_LIBJPEG
  wxImage::AddHandler( new wxJPEGHandler );
#endif

#if wxUSE_LIBTIFF
  wxImage::AddHandler( new wxTIFFHandler );
#endif

#if wxUSE_GIF
  wxImage::AddHandler( new wxGIFHandler );
#endif

#if wxUSE_PCX
  wxImage::AddHandler( new wxPCXHandler );
#endif

#if wxUSE_PNM
  wxImage::AddHandler( new wxPNMHandler );
#endif

#if wxUSE_XPM
  wxImage::AddHandler( new wxXPMHandler );
#endif

#if wxUSE_ICO_CUR
  wxImage::AddHandler( new wxICOHandler );
  wxImage::AddHandler( new wxCURHandler );
  wxImage::AddHandler( new wxANIHandler );
#endif



  //  ScaleSetsImageAnalysis* m_sxsia = new ScaleSetsImageAnalysis(NULL);	
  InterfaceMedSeg* m_sxsia = new InterfaceMedSeg(NULL,true,wxT("MAIN WIN"));
  m_sxsia->Show(TRUE);
  return true;
}
//=============================================================================



//=======================================================================================
ScaleSetsImageAnalysis::ScaleSetsImageAnalysis(wxFrame *parent, const wxString& title)
  : FrameWithLog(parent,-1, title, wxPoint(0,0), wxSize(400,60), wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxSYSTEM_MENU | wxCAPTION  )
{
  InitInterface();
}
//=======================================================================================
void ScaleSetsImageAnalysis::InitInterface()
{
  //=========================================
  // Menu
#if wxUSE_MENUS
  // File menu 
  menuFile = new wxMenu;
  menuFile->Append(idMenuOpen, _T("&Open scale-sets\tAlt-O"), _T("Opens a scale-sets"));
  menuFile->AppendCheckItem(idMenuLog, _T("Show log\tAlt-L"), _T("Shows the log window"));
  menuFile->Append(idMenuQuit, _T("E&xit\tAlt-X"), _T("Quit this program"));
	
  // Algorithms menu
  wxMenu* menuAlgo = new wxMenu;
  menuAlgo->Append(idMenuSegment,_T("&Analyze image \tAlt-R"), _T("Computes a Scale-Sets analysis on an image"));
	
  // Help menu
  menuHelp = new wxMenu;
  menuHelp->Append(idMenuAbout, _T("&About...\tF1"), _T("Show about dialog"));
	
  // Menu bar
  menuBar = new wxMenuBar();
  menuBar->Append(menuFile, _T("&File"));
  menuBar->Append(menuAlgo, _T("&Algorithms"));
  menuBar->Append(menuHelp, _T("&Help"));
  SetMenuBar(menuBar);
#endif // wxUSE_MENUS
	
  lgl::BOOL init_show_log = TRUE;
  menuFile->Check(idMenuLog,init_show_log);
  getLogWindow()->Show(init_show_log);
  /*	
    #if wxUSE_STATUSBAR
    CreateStatusBar(4);
    SetStatusText(_T("Welcome"));
    #endif // wxUSE_STATUSBAR
  */
  /*
  // Sizer
  // top level sizer
  wxBoxSizer* topsizer = new wxBoxSizer( wxHORIZONTAL ); 
  SetSizer( topsizer );
  // sets minimum frame size
  topsizer->SetSizeHints( this );
  SetAutoLayout( TRUE );
  topsizer->Fit( this );
  */
  //=====================================================================================================	
}
//=======================================================================================
// MENU
//=======================================================================================


//=======================================================================================
void ScaleSetsImageAnalysis::OnMenuOpen (wxCommandEvent& event) 
{
  wxString filename = wxFileSelector(_T("Select image scale-sets file"),
				     _T(""),_T(""),_T("iss"),
				     _T("Image scale-sets files (*.iss)|*.iss"));
  if ( !filename ) return;
	
  sxs::ImageScaleSets* m = new sxs::ImageScaleSets;
	
  if ( !m->load(lgl::wx2std(filename)) ) {
    wxLogError(_T("Error loading '%s'."), filename.c_str());
    delete m;
    return;	
  }
	
  sxs::wxScaleSetsBrowser* b = new sxs::wxScaleSetsBrowser(this,*m,true,filename);
  b->Show(true);
}
//=======================================================================================



//===========================================================

void ScaleSetsImageAnalysis::OnLogOpenClose(wxCommandEvent& WXUNUSED(event))
{
  static lgl::BOOL check = FALSE;
  check = !check;
  getLogWindow()->Show(check);
  menuFile->Check(idMenuLog,check);
}
//===========================================================

//=======================================================================================

void ScaleSetsImageAnalysis::OnMenuAbout (wxCommandEvent& event) 
{
  wxString msg;
  msg.Printf( _T("Scale-Sets Image Analysis\n") 
	      _T("      (c) Laurent Guigues 2004 (laurent.guigues@ign.fr)\n")
	      );
  wxMessageBox(msg, _T("About"), wxOK | wxICON_INFORMATION, this);
}
//=======================================================================================

//=======================================================================================
void ScaleSetsImageAnalysis::OnMenuQuit (wxCommandEvent& event) 
{
  Close();
}
//=======================================================================================

//=======================================================================================

void ScaleSetsImageAnalysis::OnMenuSegment (wxCommandEvent& event) 
{
  lgl::ImageRam* tmp = 0;
  if (! lgl::wxOpenImage(tmp))  return;
  //	lglLOG("Image : "<<tmp->size());
  // Image crop
  //	ImageRam* image= tmp->procreate();
  //	image->crop(*tmp,ImageBloc(lgl::ImageSite(0,32,32,32),lgl::ImageSite(1,64,64,64)));
  //	image->crop(*tmp,ImageBloc(lgl::ImageSite(0,32,32,32),lgl::ImageSite(1,96,96,96)));
  //	image->crop(*tmp,ImageBloc(lgl::ImageSite(0,32,32,32),lgl::ImageSite(1,96,96,150)));
  //	image->crop(*tmp,ImageBloc(lgl::ImageSite(0,0,32,0),lgl::ImageSite(1,128,110,214)));
  //	image->crop(*tmp,ImageBloc(lgl::ImageSite(0,0,63,32),lgl::ImageSite(1,181,217,181)));
	
  lgl::ImageRam* image=tmp;
  //	subsample(*tmp,4,image);
	
  //	wxSaveImageAnalyze(*image);
  //	return;
	
  //	delete tmp;
	
	
  //	lglLOG("Image cropped : "<<image->size()<<ENDL);
	
  /// Allocation of the ImageScaleSets
  sxs::ImageScaleSets* sxs = new sxs::ImageScaleSets;
  /// The scale-climbing user
  MS_ScaleClimbing_User* user = new MS_ScaleClimbing_User;
  user->setImage(*image);
  // The algorithm
  sxs::ImageScaleClimbing  algo;
  algo.setUser(user);
  algo.setScaleSets(*sxs);
  algo.setImage(image);
  // Its parameters
  sxs::ImageScaleClimbingParameters* param = new sxs::ImageScaleClimbingParameters;
  lgl::Neighborhood N;
  if (image->dimension()==2) N.setType(lgl::Neighborhood::d2_4n);
  else N.setType(lgl::Neighborhood::d3_6n);
  param->setNeighborhood(N);
  algo.setParameters(param);
	
	
  // Progress dialog
  wxProgressDialog* pd = new wxProgressDialog(_T("Scale Climbing"), 
					      _T(""),  1000, this,  
					      wxPD_AUTO_HIDE | wxPD_CAN_ABORT | wxPD_ELAPSED_TIME | wxPD_ESTIMATED_TIME | wxPD_REMAINING_TIME);
  user->setProgressDialog(pd,1000);
	
  // If algo run ok : open sxs in a browser 
  if (algo.run()) {
    delete pd; 
    sxs::wxScaleSetsBrowser* b = new sxs::wxScaleSetsBrowser(this,*sxs,true,_T("Scale-Sets Browser"));
    b->Show(true);
  }
  // Else : delete sxs
  else {
    delete pd; 
    delete sxs; 
  }	
}
//=======================================================================================



//=============================================================================

IMPLEMENT_APP(Application)
  //=============================================================================
  
  //=============================================================================
  // EOF
  //=============================================================================
