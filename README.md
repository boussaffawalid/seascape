# SeaScape
Segmentation and Cover Classification Analyses of Seabed Images

Seascape is a free software program to get semi-automatically segmented images (homogenous regions)
from underwater photographs of benthic communities, where each individual patch (species/categories)
is routinely associated to its area cover and perimeter. The process starts with a hierarchical segmentation,
using a colour criteria adapted to the problem of segmenting complex benthic images.
As an end product, the software generates a set of images segmented into classified homogenous regions
at different resolution levels (hierarchical segmentation). 
Cover and perimeter values for each patch are calculated and the results exported to Excel spreadsheets.
